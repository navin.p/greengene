//
//  CommonCollectionCell.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/27/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class CommonCollectionCell: UICollectionViewCell {
    
    //Welcome
    
    @IBOutlet weak var welcome_lblTitle: UILabel!
    
    @IBOutlet weak var welcome_lblDetail: UILabel!
    @IBOutlet weak var welcome_image: UIImageView!

    //About Tree
    
    @IBOutlet weak var aboutTree_Image: UIImageView!
    
    @IBOutlet weak var aboutTree_lblTitle: UILabel!
    //QRCode Tree
    
    @IBOutlet weak var QR_Image: UIImageView!
    
    @IBOutlet weak var QR_lblTitle: UILabel!
}
