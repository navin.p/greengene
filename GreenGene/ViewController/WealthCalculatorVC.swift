//
//  WealthCalculatorVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/13/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import SafariServices

class WealthCalculatorVC: UIViewController {
    
    
    
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viewheader: CardView!
    @IBOutlet weak var btnCount: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewCertificate: UIView!

    @IBOutlet weak var tvList: UITableView!

    @IBOutlet weak var tvViewContain: UIView!

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgQRcode: UIImageView!

    var strcome = String()
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        loginDict = getLoginData()
        tvList.tableFooterView = UIView()
        viewheader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnShare.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
     
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            self.lblName.text = "\(dict.value(forKey: "Name")!)"
            self.lblSubTitle.text = "your\nGreen Wealth is"
            //self.lblCount.text = "\(plantCount)"
        }
        btnCount.setTitleColor(hexStringToUIColor(hex: colorDarkPrimary), for: .normal)
        
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let strProfileUrl = "\(dict.value(forKey: "Profile_Image")!)"
            if(strProfileUrl.count != 0){
                imgProfile.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "profile_img_1"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
            let strQrCodeUrl = "\(dict.value(forKey: "QRCode")!).png"
            if(strQrCodeUrl.count != 0){
                self.imgQRcode.setImageWith(URL(string: "\(BaseURLQrcodeImageDownload)\(strQrCodeUrl)"), placeholderImage: UIImage(named: "qr_code_scan_to_know_benifits"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.call_GetDonatedTreeCount()

        }
    }
    
    override func viewDidLayoutSubviews() {
        self.tvList.frame = CGRect(x: self.tvList.frame.origin.x , y: self.tvList.frame.origin.y, width: 310, height: 570)
         self.tvList.center = tvViewContain.center
        imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
        imgProfile.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        imgProfile.layer.borderWidth  = 2.0
        imgProfile.layer.masksToBounds = false
        imgProfile.clipsToBounds = true
        imgProfile.contentMode = .scaleAspectFill
        
        imgQRcode.layer.borderColor = hexStringToUIColor(hex: colorGrayPrimary).cgColor
        imgQRcode.layer.borderWidth  = 1.0
        imgQRcode.layer.masksToBounds = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnWebLink(_ sender: Any) {
        let pth = "http://greengene.citizencop.org"
        let url = NSURL(string: pth)
        let svc = SFSafariViewController(url: url! as URL)
        present(svc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func actionOnShare(_ sender: Any) {
        let IMG = captureScreen()
        let imageShare = [ IMG! ]
        let activityViewController = UIActivityViewController(activityItems: imageShare , applicationActivities: nil)
 activityViewController.popoverPresentationController?.sourceView = self.view
    self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func tapFunction(sender:UITapGestureRecognizer) {

        let pth = "http://greengene.citizencop.org"
        let url = NSURL(string: pth)
        let svc = SFSafariViewController(url: url! as URL)
        present(svc, animated: true, completion: nil)
    }
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    func captureScreen() -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: viewCertificate.frame.size.width, height: viewCertificate.frame.size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context1 = UIGraphicsGetCurrentContext()
        if let aContext1 = context1 {
            viewCertificate.layer.render(in: aContext1)
        }
        let screengrab: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        return screengrab
    }
    func call_GetDonatedTreeCount()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)

        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let GetMyTreeDetail = "\(URL_GetDonatedTreeCount)\(dict.value(forKey: "User_Id")!)"
                print(GetMyTreeDetail)
                self.btnCount.tag = 99
                customDotLoaderShowOnButton(btn: self.btnCount, view: self.viewCertificate, controller: self)

                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                  customeDotLoaderRemove()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            self.btnCount.setTitle("\(dict.value(forKey: "Success")!)", for: .normal)
                        }else{

                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(alertSomeError)", viewcontrol: self)
                    }
                })
            }
        }
    }
}
