//
//  GenerateQRCodeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/6/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class AllocateQRCodeVC: UIViewController {
  
    
    // MARK: - ----------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewheader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtNoOfQRCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOrganization: SkyFloatingLabelTextField!


    // MARK: - --------------Variable
    // MARK: -
    var aryForListData = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewheader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 4.0
      

        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
           GetOrganizationDropdownAPI()
        }
        
        loginDict = getLoginData()
        if (loginDict.count != 0){
            let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            txtEmailAddress.text = "\(dict.value(forKey: "Email_Id")!)"
        }
    }
    
  
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnDropDown(_ sender: UIButton) {

        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        vc.strTitle = "Select Organization Name"
        vc.strTag = 13
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        vc.aryTBL =  self.aryForListData
        self.present(vc, animated: false, completion: {})
    }
    
    @IBAction func actiononGenerate(_ sender: UIButton) {
       if(txtNoOfQRCode.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_NumberOfQRcode, viewcontrol: self)

        }else if ((txtEmailAddress.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPEmail, viewcontrol: self)
        }
        else if !(validateEmail(email: txtEmailAddress.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPValidEmail, viewcontrol: self)
        }
       else if ((txtOrganization.text?.count)! == 0){
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OrganizationName, viewcontrol: self)
       }
        else{
        self.view.endEditing(true)
            CallGenerateQRCodeAPI()
        }
    }
    
    // MARK: - --------------API Call
    // MARK: -
    func CallGenerateQRCodeAPI() {
        let dictSendData = NSMutableDictionary()
        var json1 = Data()
        var jsonAdd = NSString()
      
        dictSendData.setValue(Int(txtNoOfQRCode.text!), forKey: "No_Of_QRCode")
        dictSendData.setValue(txtOrganization.tag, forKey: "Organization_Coupen_Id")
        dictSendData.setValue(txtEmailAddress.text!, forKey: "Email_Id")

        if JSONSerialization.isValidJSONObject(dictSendData) {
            // Serialize the dictionary
            json1 = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
            jsonAdd = String(data: json1, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonAdd)")
        }
        if(dictSendData.count == 0){
            jsonAdd = ""
        }
        customDotLoaderShowOnButton(btn: btnSubmit, view: self.view, controller: self)
        
        WebService.callAPIBYPOSTWithRaw(json: jsonAdd as String, url: URL_QRCodePdfonEmail) { (responce, status) in
            print(responce)
            customeDotLoaderRemove()
            if (status == "success"){
                let dict  = (responce.value(forKey: "data")as! NSDictionary)
                if (dict.value(forKey: "Result")as! String == "True"){
                    let alert = UIAlertController(title: alertInfo, message: dict.value(forKey: "Success")as? String, preferredStyle: .alert)
                  
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                       self.navigationController?.popViewController(animated: true)
                    }))
                    
                    self.present(alert, animated: true)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: dict.value(forKey: "Success")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
       
        
        
    }
    func GetOrganizationDropdownAPI() {
          customDotLoaderShowOnFull(message: "", controller: self)
        WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetOrganizationDropdown, OnResultBlock: { (responce, status) in
            print(responce)
            customeDotLoaderRemove()
            if (status == "success"){
                let dict  = (responce.value(forKey: "data")as! NSDictionary)
                if (dict.value(forKey: "Result")as! String == "True"){
                    self.aryForListData = NSMutableArray()
                    self.aryForListData = (dict.value(forKey: "OrganizationDropdownDc")as! NSArray).mutableCopy()as! NSMutableArray
                  
                }else{
                 
                }
            }else{
            
            }
        })
        
        
    }
}

// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension AllocateQRCodeVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtNoOfQRCode {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 2)
        }
        else if textField == txtEmailAddress  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 85)
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
// MARK: - ----------------Selection Delegate
// MARK: -
extension AllocateQRCodeVC: CommonTableDelegate {
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        
    }
    
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
        txtOrganization.text = dictData.value(forKey: "OrganizationName")as? String
        txtOrganization.tag = Int ("\(dictData.value(forKey: "Organization_Coupen_Id")!)")!
        
    }
}
