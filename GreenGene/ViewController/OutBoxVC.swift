//
//  OutBoxVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 9/3/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class OutBoxVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!

    var aryMyOutBoxList = NSMutableArray()
    let refreshControl = UIRefreshControl()
    var object = NSManagedObject()

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRefresh.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
     getOutBoxDataList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: UIButton) {
         if(sender.tag == 1){
            getOutBoxDataList()
            getDataFromLocalForSync()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - ----------------CoreData
    // MARK:
    func getDataFromLocalForSync() {
        let aryData = getDataFromLocalUsingAntity(entityName: "Outbox")
        if aryData.count > 0 {
            object = aryData[0]as! NSManagedObject
            let User_Id = "0"
            let Role_Id = "\(String(describing: object.value(forKey: "role")!))"
            let Plant_Id = "\(String(describing: object.value(forKey: "treeId")!))"
            let QRCode = "\(String(describing: object.value(forKey: "qrcode")!))"
            let Plantation_Date = "\(String(describing: object.value(forKey: "date")!))"
            let PlantedLocation = "\(String(describing: object.value(forKey: "location")!))"
            let Lat = "\(String(describing: object.value(forKey: "lat")!))"
            let Long = "\(String(describing: object.value(forKey: "long")!))"
            let Plant_Status_Description = "\(String(describing: object.value(forKey: "plantdescription")!))"
            let Plant_Status_Image = "\(String(describing: object.value(forKey: "plantImagename")!))"
            let IsFertilizer = "\(String(describing: object.value(forKey: "fertilizer")!))"
            let IsTrim = "\(String(describing: object.value(forKey: "istrim")!))"
            let IsPest = "\(String(describing: object.value(forKey: "pestcontrol")!))"
            let StemSize = "\(String(describing: object.value(forKey: "stemsize")!))"
            let Plant_Condition = "\(String(describing: object.value(forKey: "plantcondition")!))"
            let Care_Taker_Id = "\(String(describing: object.value(forKey: "userid")!))"
            let Plant_Status = "Accept"
            let Plant_Suggession = ""
            // let imagePlant = getImagefromDirectory(strname: plantImagename)
            if !(isInternetAvailable()){
                FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
            }else{
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                let dictOutBoxData = NSMutableDictionary()
                dictOutBoxData.setValue(User_Id, forKey: "User_Id")
                dictOutBoxData.setValue(Role_Id, forKey: "Role_Id")
                dictOutBoxData.setValue(Plant_Id, forKey: "Plant_Id")
                dictOutBoxData.setValue(QRCode, forKey: "QRCode")
                dictOutBoxData.setValue(Plantation_Date, forKey: "Plantation_Date")
                dictOutBoxData.setValue(PlantedLocation, forKey: "PlantedLocation")
                dictOutBoxData.setValue(Lat, forKey: "Lat")
                dictOutBoxData.setValue(Long, forKey: "Long")
                dictOutBoxData.setValue(Plant_Status_Description, forKey: "Plant_Status_Description")
                dictOutBoxData.setValue(Plant_Status_Image, forKey: "Plant_Status_Image")
                dictOutBoxData.setValue(IsFertilizer, forKey: "IsFertilizer")
                dictOutBoxData.setValue(IsTrim, forKey: "IsTrim")
                dictOutBoxData.setValue(IsPest, forKey: "IsPest")
                dictOutBoxData.setValue(StemSize, forKey: "StemSize")
                dictOutBoxData.setValue(Plant_Condition, forKey: "Plant_Condition")
                dictOutBoxData.setValue(Care_Taker_Id, forKey: "Care_Taker_Id")
                dictOutBoxData.setValue(Plant_Status, forKey: "Plant_Status")
                dictOutBoxData.setValue(Plant_Suggession, forKey: "Plant_Suggession")
                dictOutBoxData.setValue("", forKey: "Status_Image_Byte_Array")
                let aryOutboxSync = NSMutableArray()
                aryOutboxSync.add(dictOutBoxData)
                let strJson = jsontoString(fromobject: aryOutboxSync)
                let urlOutbox = URL_OfflineDataSave + "\(strJson!)"
                print(urlOutbox)
                WebService.callAPIBYGET(parameter: NSDictionary(), url: urlOutbox, OnResultBlock:{ (responce, status) in
                    FTIndicator.dismissProgress()
                    print(responce)
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            self.call_UPLOADIMGE_API(imagename: Plant_Status_Image)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                        }
                    }else{
                         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
    
    func call_UPLOADIMGE_API(imagename : String)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
           FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            var plantImage = UIImage()
            plantImage = getImagefromDirectory(strname: imagename)
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURLImageUPLOAD, image: plantImage, imageName: imagename, OnResultBlock: { (responce, status) in
                      print(responce)
             FTIndicator.dismissProgress()
                if (status == "success"){
                    if (responce.value(forKey: "message")as! String == "SUCCESS"){
                        removeImageFromDirectory(itemName: imagename)
                        self.deletdataFromlocal()
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    
    func deletdataFromlocal() {
        let aryTemp = getDataFromLocalUsingAntity(entityName: "Outbox")
        if aryTemp.count > 0 {
            let obj = aryTemp.object(at: 0) as! NSManagedObject
            let context = AppDelegate.getContext()

            do {
                context.delete(obj)
                try context.save()
                self.getOutBoxDataList()
                self.getDataFromLocalForSync()
            } catch _ as NSError  {
            } catch {
            }
        }
    }
    
    func getOutBoxDataList()  {
        self.aryMyOutBoxList = NSMutableArray()
        self.aryMyOutBoxList = getDataFromLocalUsingAntity(entityName: "Outbox")
        refreshControl.endRefreshing()
        if  self.aryMyOutBoxList.count == 0 {
            self.tvlist.isHidden = true
            self.lblError.text = alertDataNotFound
            self.imgError.image = #imageLiteral(resourceName: "notfound")
        }else{
            tvlist.isHidden = false
            tvlist.reloadData()
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension OutBoxVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return 10
        return aryMyOutBoxList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "OutBox", for: indexPath as IndexPath) as! CommonTableCell
        
        cell.outBox_lblQRCode.textColor = hexStringToUIColor(hex: colorGreenPrimary)
        cell.outBox_lblQRCode.text = (aryMyOutBoxList.object(at: indexPath.row)as AnyObject).value(forKey: "qrcode") as? String
        cell.outBox_lblTitle.text = (aryMyOutBoxList.object(at: indexPath.row)as AnyObject).value(forKey: "treename") as? String
        let strDate = dateTimeConvertor(str: (aryMyOutBoxList.object(at: indexPath.row)as AnyObject).value(forKey: "date") as! String, formet: "Sort")
        cell.outBox_lblDate.text = strDate
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}
