//
//  MyTreeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/30/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class MyTreeVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewSearch: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!

    @IBOutlet weak var lblTreeCount: UILabel!
    
    var aryMyTreeList = NSMutableArray()
    let refreshControl = UIRefreshControl()
    let imgView = UIImageView()
    var arytvlist = NSMutableArray()
    
    var ErrorMsg = ""

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
        lblTitle.text = "My Trees"
       
        if (loginDict.count != 0){
            self.call_MyTreeList_API(txtafterUpdate: "no")
            DispatchQueue.main.async {
                self.call_MyTreeCountt_API()
            }
        }
        if #available(iOS 10.0, *) {
            tvlist.refreshControl = refreshControl
        } else {
            //  tvlist.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIView.animate(withDuration: 0.6, animations: { [weak self] in
//            let frame = self?.view.frame
//            let yComponent = self?.partialView
//            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
//        })
    }
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {

       self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc private func refreshData(_ sender: Any) {
        txtSearch.text = ""
        
        tvlist.tag = 1
        self.call_MyTreeList_API(txtafterUpdate: "no")
    }

    // MARK: - ---------------API's Calling
    // MARK: -
    func call_MyTreeList_API(txtafterUpdate : String)  {
          ErrorMsg = ""
            
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            
             ErrorMsg = alertInternet
             FTIndicator.showNotification(withTitle: "", message: alertInternet)
            
            refreshControl.endRefreshing()
            getTreeDataFromLocal()
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let dictData = NSMutableDictionary()
                 dictData.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "User_Id")
                dictData.setValue("0", forKey: "CareTakerId")
                if(self.tvlist.tag == 1){
                    dictData.setValue("0", forKey: "Skip")
                }else{
                    dictData.setValue("\(self.arytvlist.count)", forKey: "Skip")
                }
                dictData.setValue("20", forKey: "Take")
                if(txtafterUpdate == ""){
                    dictData.setValue(txtafterUpdate, forKey: "CommonSearch")
                }else{
                    dictData.setValue(txtSearch.text!, forKey: "CommonSearch")
                }
                dictData.setValue("1", forKey: "Plant_Id")
                
                let aryOutboxSync = NSMutableArray()
                aryOutboxSync.add(dictData)
                let strJson = jsontoString(fromobject: aryOutboxSync)
                
                let GetMyTreeDetail = URL_GetMyTreeByUserIdLezyLoading + "\(strJson!)"
                print(GetMyTreeDetail)
                let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
                if aryMyTreeList.count == 0{
                    if(tvlist.tag != 1){
                        loading.startLoading(text: "Loading...")
                    }
                    
                }
                
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    loading.endLoading()
                    self.tvlist.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            print(self.arytvlist.count)
                            if(self.tvlist.tag == 1){
                                self.arytvlist = NSMutableArray()
                                self.aryMyTreeList = NSMutableArray()
                                deleteAllRecords(strEntity:"MYtree")
                                saveDataInLocalArray(strEntity: "MYtree", strKey: "tree", data: (dict.value(forKey: "MyTreeList")as! NSArray).mutableCopy() as! NSMutableArray)
                            }else{
                                for item in  (dict.value(forKey: "MyTreeList")as! NSArray){
                                    self.arytvlist.add(item)
                                }
                                deleteAllRecords(strEntity:"MYtree")
                                saveDataInLocalArray(strEntity: "MYtree", strKey: "tree", data: self.arytvlist)
                            }
                            self.tvlist.tag = 0
                            self.getTreeDataFromLocal()
                        }else{
                            if (self.arytvlist.count == 0){
                                self.ErrorMsg = "\(dict.value(forKey: "Success")as! String)"
                                
                                self.tvlist.reloadData()
                            }
                        }
                    }else{
                        self.ErrorMsg = alertSomeError
                        self.tvlist.reloadData()
                    }
                })
            }
        }
    }
    
    //MARK: Count Tree & Qr Code
    func call_MyTreeCountt_API()  {
          ErrorMsg = ""
            
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            ErrorMsg = alertInternet
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
            refreshControl.endRefreshing()
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let dictData = NSMutableDictionary()
                
                dictData.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "User_Id")
                dictData.setValue("0", forKey: "Plant_Id")
                dictData.setValue("0", forKey: "CareTakerId")
                dictData.setValue("0", forKey: "Skip")
                dictData.setValue("5", forKey: "Take")
                dictData.setValue("", forKey: "CommonSearch")
                dictData.setValue("false", forKey: "IsTreeTagged")
                dictData.setValue("0", forKey: "Organization_Coupen_Id")
                dictData.setValue("0", forKey: "State_Id")
                dictData.setValue("0", forKey: "City_Id")
                
                
            
                let aryOutboxSync = NSMutableArray()
                aryOutboxSync.add(dictData)
                
                let strJson = jsontoString(fromobject: dictData)
                
                let GetMyTreeDetail = URL_GetMyTreeByCount + "\(strJson!)"
                print(GetMyTreeDetail)
                
                let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
                loading.startLoading(text: "Loading...")
                
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    loading.endLoading()
                    self.tvlist.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            let aryList = dict.value(forKey: "MyTreeCount") as? NSDictionary ?? [:]
                            self.lblTreeCount.text = "Total My Trees : \(aryList.value(forKey: "TotalTaggedTree") ?? "") / \(aryList.value(forKey: "TotalTree") ?? "")"
                        }else{
                            
                        }
                    }else{
                        self.ErrorMsg = alertSomeError
                        self.tvlist.reloadData()
                    }
                })
            }
        }
    }
    
    
    // MARK: - ---------------GetDataFromLocal
    // MARK: -
    func getTreeDataFromLocal()   {
        
        self.aryMyTreeList = NSMutableArray()
        self.arytvlist = NSMutableArray()
        
        let aryTemp = getDataFromLocal(strEntity: "MYtree", strkey: "tree")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "tree") ?? 0)
            }
        }
        print(aryList)
        let aryTempFinal = NSMutableArray()
        if aryList.count != 0 {
            for item in aryList.object(at: 0)as! NSArray{
                let dict  = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let donated_Plant_Id = "\(dict.value(forKey: "Donated_Plant_Id")!)"
                dict.setValue(donated_Plant_Id, forKey: "Donated_Plant_Id")
                aryTempFinal.add(dict)
                
            }
        }
        
        if aryTempFinal.count != 0 {
            ErrorMsg = ""
            self.aryMyTreeList = NSMutableArray()
            self.aryMyTreeList = aryTempFinal
            self.arytvlist = NSMutableArray()
            self.arytvlist = aryTempFinal

            self.tvlist.reloadData()
        }else{
            if !(isInternetAvailable()){

                self.ErrorMsg = alertInternet

                self.tvlist.reloadData()
            }else{
                self.ErrorMsg = alertDataNotFound

                self.tvlist.reloadData()
              
            }
        }
    }
    
    // MARK: - --------------Extra Function / Action on Table
    // MARK: -
    
    @objc func gotoReTag(_ sender: UIButton) {

        let dict = removeNullFromDict(dict: (self.arytvlist.object(at: sender.tag)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
     var strLat = ""
        if (dict.value(forKey: "Lat") is String){
           strLat = "\(dict.value(forKey: "Lat")!)"
        }
        
        if(strLat != ""){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReTagVC")as! ReTagVC
            testController.dictTreeData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_TreeLocation, viewcontrol: self)
        }
       
    }
    
    @objc func gotoMapDirection(_ sender: UIButton) {
        var strlet = ""
        var strLong = ""
        var strTitle = ""
        
        let location = arytvlist.object(at: sender.tag)
        if ((location as AnyObject).value(forKey: "Lat") is String){
            strlet = ((location as AnyObject).value(forKey: "Lat")as! String)
        }
        if ((location as AnyObject).value(forKey: "Long") is String){
            strLong = ((location as AnyObject).value(forKey: "Long")as! String)
        }
        
        if ((location as AnyObject).value(forKey: "Plant_Name") is String){
            strTitle =  ((location as AnyObject).value(forKey: "Plant_Name")as! String)
        }
        
        if ((location as AnyObject).value(forKey: "PlantedLocation") is String){
            strTitle = strTitle + "\n" + ((location as AnyObject).value(forKey: "PlantedLocation")as! String)
        }
        if ((location as AnyObject).value(forKey: "OwnerName") is String){
            strTitle =  strTitle + "\n" + ((location as AnyObject).value(forKey: "OwnerName")as! String)
        }
        if (strlet != "") && (strLong != "") {
            if let UrlNavigation = URL.init(string: "comgooglemaps://") {
                if UIApplication.shared.canOpenURL(UrlNavigation){
                    
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(strlet),\(strLong)&directionsmode=driving") {
                        UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                    }
                }
                else {
                    NSLog("Can't use comgooglemaps://");
                    self.openTrackerInBrowser(strlat: strlet, strlong: strLong, strTitle: strTitle)
                    
                }
            }
            else
            {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(strlat: strlet, strlong: strLong, strTitle: strTitle)
            }
            
            
            
            //openMap(strTitle: (sender.titleLabel?.text!)!, strlat: strlet, strLong: strLong)
            
        }
    }
    func openTrackerInBrowser(strlat : String , strlong : String , strTitle : String){
        
        openMap(strTitle: strTitle, strlat: strlat, strLong: strlong)
        
        
    }
    @objc func dismissFullscreenImage(_ sender: UIButton) {
        print(sender.tag)
        let dict = removeNullFromDict(dict: (arytvlist.object(at: (sender.tag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        if dict.value(forKey: "DonatedPlantStatusDcs") is String {
            FTIndicator.showToastMessage("Plant Status Not Available!")
        }else{
            let ary =  (dict.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
            if ary.count != 0{
                let dict = removeNullFromDict(dict: (ary.object(at: (0))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
                if(strProfileUrl.count != 0){
                    imgView.image = #imageLiteral(resourceName: "default_img")
                    if(strProfileUrl.count != 0){
                        imgView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                        }, usingActivityIndicatorStyle: .gray)
                    }
                    let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    vc.imageSHow = self.imgView.image!
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    // MARK: - --------------Search By name
    // MARK: -
    func searchAutocomplete_Name(Searching: NSString) -> Void {
        let resultPredicate = NSPredicate(format: "PlantedLocation contains[c]%@ OR Donated_Plant_Id contains[c]%@ OR Plant_Name contains[c]%@" ,Searching , Searching ,Searching)
        if !(Searching.length == 0) {
            let arrayfilter = (aryMyTreeList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arytvlist = NSMutableArray()
            self.arytvlist = nsMutableArray.mutableCopy() as! NSMutableArray
            if arytvlist.count != 0 {
                 self.ErrorMsg =  ""
                self.tvlist.reloadData()
               
            }else{
                if !(isInternetAvailable()){
                   
                    self.ErrorMsg =  alertInternet

                    self.tvlist.reloadData()
                }else{
                    self.ErrorMsg =  alertDataNotFound

                    self.tvlist.reloadData()
                   
                   
                }
            }
        }
        else{
            self.arytvlist = NSMutableArray()
            self.arytvlist = aryMyTreeList.mutableCopy() as! NSMutableArray

            self.tvlist.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
            if arytvlist.count != 0 {
                self.ErrorMsg =  ""

                self.tvlist.reloadData()
            }else{
                if !(isInternetAvailable()){
                  self.ErrorMsg =  alertInternet

                 self.tvlist.reloadData()
                }else{
                    self.ErrorMsg =  alertDataNotFound

                    self.tvlist.reloadData()
                  
                }
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyTreeVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arytvlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "TreeList", for: indexPath as IndexPath) as! CommonTableCell
        let dict = removeNullFromDict(dict: (arytvlist.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        print(dict)
        var strPlantName = "\(dict.value(forKey: "Plant_Name")!)"
        if strPlantName == "" {
            strPlantName = "Not Available"
        }
        cell.tree_lblPlantName.text = strPlantName
        
        var strCaretakerName = "\(dict.value(forKey: "OwnerName")!)"
        var strMobileNumber = "\(dict.value(forKey: "OwnerMobile_Number")!)"
        if strCaretakerName == "" {
            strCaretakerName = "Not Available"
        }
        if strMobileNumber == "" {
            strMobileNumber = "Not Available"
        }
        
        var strAddress = "\(dict.value(forKey: "PlantedLocation")!)"
        if strAddress == "" {
            strAddress = "Not Available"
        }
        var strDonated_Plant_Id = "\(dict.value(forKey: "Donated_Plant_Id")!)"
        if strDonated_Plant_Id == "" {
            strDonated_Plant_Id = "Not Available"
        }
        
        let strOwnerAlias = "\(dict.value(forKey: "OwnerAlias")!)"
        let strIsShowQRCodeNo = "\(dict.value(forKey: "IsShowQRCodeNo")!)"
        var strIsQRCodeNo = "\(dict.value(forKey: "QRCodeNo")!)"
        if(strIsQRCodeNo == "" ||  strIsQRCodeNo == "<null>"){
            strIsQRCodeNo = strDonated_Plant_Id
        }
        if strOwnerAlias == "" {
            if (strIsShowQRCodeNo == "0"||strIsShowQRCodeNo == "<null>") {
                cell.tree_lblQr.text = "  QR-\(strDonated_Plant_Id)"
            }else{
                cell.tree_lblQr.text = "  QR-\(strIsQRCodeNo)"
            }
        }else{
            if (strIsShowQRCodeNo == "0"||strIsShowQRCodeNo == "<null>") {
                cell.tree_lblQr.text = "  QR-\(strDonated_Plant_Id) \(strOwnerAlias)"
            }else{
                cell.tree_lblQr.text = "  QR-\(strIsQRCodeNo) \(strOwnerAlias)"
            }
        }
        cell.bgVIew.layer.borderColor = UIColor.darkGray.cgColor
        cell.bgVIew.layer.borderWidth = 0.5
        cell.tree_lblAddress.text = strAddress
        cell.tree_BtnQRcode.tag = indexPath.row
        cell.tree_BtnQRcode.addTarget(self, action: #selector(self.tapFunction(sender:)), for: .touchUpInside)
        cell.tree_img.image = #imageLiteral(resourceName: "default_img")
        if dict.value(forKey: "DonatedPlantStatusDcs") is String {
            cell.tree_img.image = #imageLiteral(resourceName: "default_img")
           // cell.tree_lblDate.text = ""
        }else{
            let ary =  (dict.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
            if ary.count != 0{
                let dict = removeNullFromDict(dict: (ary.object(at: (0))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
           //     cell.tree_lblDate.text = "Plantation Date - " + strDate
                let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
                if(strProfileUrl.count != 0){
                    cell.tree_img.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    }, usingActivityIndicatorStyle: .gray)
                }else{
                    cell.tree_img.image = #imageLiteral(resourceName: "default_img")
                }
            }else{
              //  cell.tree_lblDate.text = ""
                cell.tree_img.image = #imageLiteral(resourceName: "default_img")
            }
            cell.tree_img.layer.cornerRadius =  10
            cell.tree_img.layer.borderWidth  = 1.0
            cell.tree_img.layer.masksToBounds = false
            cell.tree_img.clipsToBounds = true
            cell.tree_img.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            
            cell.tree_btnImg.tag = indexPath.row
            cell.tree_btnImg.addTarget(self, action: #selector(dismissFullscreenImage(_:)), for: .touchUpInside)
        }
        cell.tree_Btnmap.tag = indexPath.row
        cell.tree_Btnmap.addTarget(self, action: #selector(gotoMapDirection(_:)), for: .touchUpInside)
        cell.tree_Btnmap.layer.cornerRadius = 15
        
        
        if("\(dict.value(forKey: "IsReTag")!)" == "0"){
            cell.tree_BtnRetag.isHidden = true
        }else{
            cell.tree_BtnRetag.isHidden = false
        }
        cell.tree_BtnRetag.tag = indexPath.row
        cell.tree_BtnRetag.addTarget(self, action: #selector(gotoReTag(_:)), for: .touchUpInside)
        cell.tree_BtnRetag.layer.cornerRadius = 15
        cell.tree_BtnRetag.setTitle("Re-Tag", for: .normal)
      
        cell.tree_BtnStatus.layer.cornerRadius = 15
        cell.tree_BtnStatus.isUserInteractionEnabled = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if txtSearch.text == ""{
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if(arytvlist.count > 10 ){
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                    // print("this is the last cell")
                    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    spinner.startAnimating()
                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                    self.tvlist.tableFooterView = spinner
                    self.tvlist.tableFooterView?.isHidden = false
                }
            }
        }else{
            self.tvlist.tableFooterView?.isHidden = true
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeDetailVC")as! MyTreeDetailVC
            let dict = removeNullFromDict(dict: (self.arytvlist.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            print(dict)
            if dict.value(forKey: "DonatedPlantStatusDcs") is String {
                FTIndicator.showToastMessage("Plant Status Not Available!")
            }else{
                testController.strPlantName = "\(dict.value(forKey: "Plant_Name")!)"
                testController.aryMyTreeListDetail = (dict.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if(arytvlist.count != 0){
            ErrorMsg = ""
        }else{
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        }
        button.setTitle(ErrorMsg, for: .normal)
        customView.addSubview(button)
        customView.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return customView
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
//        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
//        callNewsAPI(strCityID: strCityId, strLoaderTag: 1)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height + 44
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom  < height {
            if txtSearch.text == ""{
                self.call_MyTreeList_API(txtafterUpdate: "no")
                print(" you reached end of the table")
            }
        }
    }
    
    @objc
    func tapFunction(sender:UIButton) {
        print("tap working \(sender.tag)")
        let dict = removeNullFromDict(dict: (arytvlist.object(at: (sender.tag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let strProfileUrl = "\(dict.value(forKey: "QRCode")!)"
        let imgViewQR = UIImageView()
        if(strProfileUrl.count != 0){
            imgViewQR.image = #imageLiteral(resourceName: "default_img")
            if(strProfileUrl.count != 0){
                imgViewQR.setImageWith(URL(string: "\(BaseURLWeb)Qrcode/\(strProfileUrl).png"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    vc.imageSHow = imgViewQR.image!
                    self.navigationController?.pushViewController(vc, animated: true)
                }, usingActivityIndicatorStyle: .gray)
            }
        }
    }
}

//MARK: - UITextFieldDelegate
//MARK: -

extension MyTreeVC: UITextFieldDelegate  {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
      //  self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)

    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        if txtSearch == textField {
            if (txtAfterUpdate == "") {
                self.arytvlist = NSMutableArray()
                self.aryMyTreeList = NSMutableArray()
                txtSearch.text = ""
                self.call_MyTreeList_API(txtafterUpdate: "")
            }else{
                self.searchAutocomplete_Name(Searching: txtAfterUpdate)
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        if !(isInternetAvailable()){
            self.tvlist.isHidden = true
           // self.lblError.text = alertInternet
           
        }else{
            self.arytvlist = NSMutableArray()
            self.aryMyTreeList = NSMutableArray()
            self.call_MyTreeList_API(txtafterUpdate: "no")
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.arytvlist = NSMutableArray()
        self.aryMyTreeList = NSMutableArray()
        self.call_MyTreeList_API(txtafterUpdate: "")
        
        return true
    }
}
//MARK: -------Protocol

protocol treelistDelegate : class{
    func getDataFromTreelistDelegate(arylist : NSMutableArray )
}
