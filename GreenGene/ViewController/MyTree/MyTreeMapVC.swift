//
//  MyTreeMapVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 10/4/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit

class MyTreeMapVC: UIViewController, MKMapViewDelegate {
    // MARK: - ----------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var mapView: MKMapView!
     var arytreeList = NSMutableArray()
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        mapView.delegate = self
     
          addTreelistView()
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
       
    }
    // MARK: - --------------Extra function
    // MARK: -
    func addTreelistView() {
        let bottomSheetVC = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeVC")as! MyTreeVC
        self.addChildViewController(bottomSheetVC)
        self.view.addSubview(bottomSheetVC.view)
        bottomSheetVC.didMove(toParentViewController: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
   
    func loadDataOnMap(arytree : NSMutableArray)  {
        print(arytreeList)
        arytreeList = NSMutableArray()
        arytreeList = arytree
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        for (index, location) in arytreeList.enumerated() {
            print("Item \(index): \(location)")
            print(location)
            var strlet = ""
            var strLong = ""
            if ((location as AnyObject).value(forKey: "Lat") is String){
                strlet = ((location as AnyObject).value(forKey: "Lat")as! String)
            }
            if ((location as AnyObject).value(forKey: "Long") is String){
                strLong = ((location as AnyObject).value(forKey: "Long")as! String)
            }
            if (strlet != "") && (strLong != "") {
                let latitude: CLLocationDegrees = Double (strlet)!
                let longitude: CLLocationDegrees = Double (strLong)!
                let location1: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location1
                var Plant_Name = "\((location as AnyObject).value(forKey: "Plant_Name")!)"
                
                if(Plant_Name == "" || Plant_Name == "<null>"){
                    Plant_Name = "Not Available"
                }
                
                var OwnerName = "\((location as AnyObject).value(forKey: "OwnerName")!)"
                if(OwnerName == "" || OwnerName == "<null>"){
                    OwnerName = "Not Available"
                }
                annotation.title =  ""
                annotation.subtitle = "\(index)"
                mapView.addAnnotation(annotation)
                mapView.showAnnotations(mapView.annotations, animated: true)
                mapView.showsUserLocation = true
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        print("\(String(describing: annotation.subtitle!))")
        annotationView!.image = UIImage(named: "about_tree")
        configureDetailView(annotationView: annotationView!, tag:(annotation.subtitle as? NSString)!)
        return annotationView
        
        
    }
    
    func configureDetailView(annotationView: MKAnnotationView , tag : NSString) {
        
        let index = Int("\(tag)")!
        
        let dictData = arytreeList.object(at: index) as! NSDictionary
        
        var OwnerName = "\(dictData.value(forKey: "OwnerName")!)"
        if OwnerName == "" ||  OwnerName == "<null>" {
            OwnerName = "Not Available"
        }
        var Plant_Name = "\(dictData.value(forKey: "Plant_Name")!)"
        if Plant_Name == "" ||  Plant_Name == "<null>" {
            Plant_Name = "Not Available"
        }
        var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
        if PlantedLocation == "" ||  PlantedLocation == "<null>" {
            PlantedLocation = "Not Available"
        }
        var strImageUrl = ""
        if dictData.value(forKey: "DonatedPlantStatusDcs") is String {
         strImageUrl = ""
        }else{
            let ary =  (dictData.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
            if ary.count != 0{
                let dict = removeNullFromDict(dict: (ary.object(at: (0))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                 strImageUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
                
            }
            
        }
        let width = 85
        let height = 85
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(85)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotOptions()
        options.size = CGSize(width: width, height: height)
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if snapshot != nil {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                let lbltitle = UILabel(frame: CGRect(x: 90, y: 5, width: 215, height: 20))
                let lblSubtitle = UILabel(frame: CGRect(x: 90, y: lbltitle.frame.maxY, width: 215, height: 60))
             
                    imageView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strImageUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    }, usingActivityIndicatorStyle: .gray)
                lbltitle.numberOfLines = 1
                lblSubtitle.numberOfLines = 3
                lbltitle.font = UIFont.systemFont(ofSize: 18)
                lblSubtitle.font = UIFont.systemFont(ofSize: 14)
                lbltitle.text = "Not available"
                lblSubtitle.text = "Not available"
                lblSubtitle.textColor = UIColor.darkGray
                lbltitle.text = "\(Plant_Name)"
                lblSubtitle.text = "\(PlantedLocation)"
                
                snapshotView.addSubview(imageView)
                snapshotView.addSubview(lbltitle)
                snapshotView.addSubview(lblSubtitle)
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.AnotationhandleTap(_:)))
            snapshotView.addGestureRecognizer(tap)
            snapshotView.isUserInteractionEnabled = true
            snapshotView.tag = index
            annotationView.detailCalloutAccessoryView = snapshotView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
    
    
    @objc func AnotationhandleTap(_ sender: UITapGestureRecognizer) {
        let tag = sender.view?.tag
        DispatchQueue.main.async {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeDetailVC")as! MyTreeDetailVC
            let dict = removeNullFromDict(dict: (self.arytreeList.object(at: tag!)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            print(dict)
            if dict.value(forKey: "DonatedPlantStatusDcs") is String {
                FTIndicator.showToastMessage("Plant Status Not Available!")
            }else{
                testController.strPlantName = "\(dict.value(forKey: "Plant_Name")!)"
                testController.aryMyTreeListDetail = (dict.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
   
    
    
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension MyTreeMapVC : treelistDelegate
{
    func getDataFromTreelistDelegate(arylist: NSMutableArray) {
        loadDataOnMap(arytree: arylist)
    }
    
}
