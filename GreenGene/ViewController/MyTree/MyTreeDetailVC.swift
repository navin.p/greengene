//
//  MyDetailDetailVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/31/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class MyTreeDetailVC: UIViewController {
   
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblPlantName: UILabel!

    var aryMyTreeListDetail = NSMutableArray()
    var strPlantName = String()
    let imgView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        lblPlantName.textColor = hexStringToUIColor(hex: colorGreenPrimary)

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
        self.lblPlantName.text = "Plant Name : \(strPlantName)"
        if strPlantName == "" {
            self.lblPlantName.text = ""
        }
        if (aryMyTreeListDetail.count == 0){
            self.tvlist.isHidden = true
            self.lblError.text = alertDataNotFound
            self.imgError.image = #imageLiteral(resourceName: "notfound")
        }else{
            self.tvlist.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @objc func dismissFullscreenImage(_ sender: UIButton) {
        print(sender.tag)
        
        let dict = removeNullFromDict(dict: (aryMyTreeListDetail.object(at: (sender.tag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
        imgView.image = #imageLiteral(resourceName: "default_img")
        if(strProfileUrl.count != 0){
           imgView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
        }
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.imgView.image!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension MyTreeDetailVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return 10
        return aryMyTreeListDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "TreeListDetail", for: indexPath as IndexPath) as! CommonTableCell

        let dict = removeNullFromDict(dict: (aryMyTreeListDetail.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        print(dict)
        var strCare_Taker = "\(dict.value(forKey: "Care_Taker")!)"
        if strCare_Taker == "" {
            strCare_Taker = "Not Available"
        }
        var strCare_Taker_MobileNo = "\(dict.value(forKey: "Care_Taker_MobileNo")!)"
        if strCare_Taker_MobileNo == "" {
            strCare_Taker_MobileNo = "Not Available"
        }
        
        var strPlant_Status_Description = "\(dict.value(forKey: "Plant_Status_Description")!)"
        if strPlant_Status_Description == "" {
            strPlant_Status_Description = "Not Available"
        }
        var strIsTrim = "\(dict.value(forKey: "IsTrim")!)"
        if strIsTrim == "" {
            strIsTrim = "Not Available"
        }else{
           
            if strIsTrim == "0" {
                strIsTrim = "NO"
            }else{
                 strIsTrim = "YES"
            }
            
        }
        var strIsFertilizer = "\(dict.value(forKey: "IsFertilizer")!)"
        if strIsFertilizer == "" {
            strIsFertilizer = "Not Available"
        }else{
            if strIsFertilizer == "0" {
                strIsFertilizer = "NO"
            }else{
                strIsFertilizer = "YES"
            }
        }
        
        var strIsPestControl = "\(dict.value(forKey: "IsPestControl")!)"
        if strIsPestControl == "" {
            strIsPestControl = "Not Available"
        }else{
            if strIsPestControl == "0" {
                strIsPestControl = "NO"
            }else{
                strIsPestControl = "YES"
            }
        }
        let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
        if(strProfileUrl.count != 0){
            cell.treeDetail_Imge.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
        }else{
            cell.treeDetail_Imge.image = #imageLiteral(resourceName: "default_img")
        }
     
        cell.treeDetail_btnImg.tag = indexPath.row
        cell.treeDetail_btnImg.addTarget(self, action: #selector(dismissFullscreenImage(_:)), for: .touchUpInside)
        if(strCare_Taker == "" || strCare_Taker == "Not Available") && (strCare_Taker_MobileNo == "" || strCare_Taker_MobileNo == "Not Available"){
              cell.treeDetail_lblCaretaker.text = "Not Available"
        }else{
            if(strCare_Taker_MobileNo != ""){
                cell.treeDetail_lblCaretaker.text = strCare_Taker + " (\(strCare_Taker_MobileNo))"
            }else{
                cell.treeDetail_lblCaretaker.text = strCare_Taker
            }
        }
      
        cell.treeDetail_lblDescription.text = "\(strPlant_Status_Description)"
        cell.treeDetail_lblIsPest.text = "\(strIsPestControl)"
        cell.treeDetail_lblIsTirm.text = "\(strIsTrim)"
        cell.treeDetail_lblIsFertilizer.text = "\(strIsFertilizer)"
        let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
        cell.treeDetail_lblDate.text = strDate
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
