//
//  ReTagVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/9/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class ReTagVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnUnderMaintanance: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var holdView: UIView!

    @IBOutlet weak var mapView: MKMapView!
    var strCurrentLatitude = "0"
    var strCurrentLongitude = "0"
    var strletDrop = "0"
    var strLongDrop = "0"
    var dictTreeData = NSMutableDictionary()
    var locationManager = CLLocationManager()

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        btnUnderMaintanance.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        var strAddress = "\(dictTreeData.value(forKey: "PlantedLocation")!)"
        if strAddress == "" {
            strAddress = "Not Available"
        }
        lblAddress.text = strAddress
        if (dictTreeData.value(forKey: "Lat") is String){
            strletDrop = (dictTreeData.value(forKey: "Lat")as! String)
        }
        if (dictTreeData.value(forKey: "Long") is String){
            strLongDrop = (dictTreeData.value(forKey: "Long")as! String)
        }
       
       
        let MomentaryLatitudeDesti = Double(self.strletDrop)
        let MomentaryLongitudeDesti = Double(self.strLongDrop)
        let destinationLocation = CLLocationCoordinate2D(latitude: (MomentaryLatitudeDesti)!, longitude: (MomentaryLongitudeDesti)!)
        self.addRadiusCircle(location: CLLocation(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude))
        self.mapView.showsUserLocation = true
        
        let tree = MKPointAnnotation()
        tree.title = ""
        tree.subtitle = (dictTreeData.value(forKey: "PlantedLocation")as! String)
        tree.coordinate = CLLocationCoordinate2D(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude)
        self.mapView.addAnnotation(tree)
        let strQRUrl = "\(dictTreeData.value(forKey: "QRCode")!)"
        if(strQRUrl.count != 0){
                imgQR.setImageWith(URL(string: "\(BaseURLWeb)Qrcode/\(strQRUrl).png"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                   
                }, usingActivityIndicatorStyle: .gray)
        }
        let noLocation = tree.coordinate
        let viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 100, 100)
        self.mapView.setRegion(viewRegion, animated: false)
        self.lblMessage.blink()
        btnUnderMaintanance.layer.cornerRadius = 06

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isAuthorizedtoGetUserLocation()
        getCurrentLocation()
    
      
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        locationManager.stopUpdatingLocation()
        print("Location Stop")
    }
   
    // MARK: - ---------------EXTRA
    // MARK: -
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }


    func addRadiusCircle(location: CLLocation){
       
        self.mapView.delegate = self
        let circle = MKCircle(center: location.coordinate, radius: CLLocationDistance(reduisMeter))
        self.mapView.add(circle)

    }
  
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }


    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        locationManager.startUpdatingLocation()

    }
    @IBAction func actionONUnderMaintanance(_ sender: Any) {
        locationManager.startUpdatingLocation()
        let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
        vc.strPlantID =  "\(dictTreeData.value(forKey: "Plant_Id")!)"
        vc.strDonated_Plant_Id =  "\(dictTreeData.value(forKey: "Donated_Plant_Id")!)"
        vc.strPlant_Name = "\(dictTreeData.value(forKey: "Plant_Name")!)"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    
}
// MARK: - ---------------MKMapViewDelegate
// MARK: -

extension ReTagVC: MKMapViewDelegate ,CLLocationManagerDelegate  {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        print("\(String(describing: annotation.subtitle!))")
        annotationView!.image = UIImage(named: "about_tree")
        configureDetailView(annotationView: annotationView!, tag:(annotation.subtitle as? NSString)!)
        return annotationView
        
    }
 
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = hexStringToUIColor(hex: colorGreenPrimary)
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKPolylineRenderer()
        }
    }
  
    func configureDetailView(annotationView: MKAnnotationView , tag : NSString) {
        var Plant_Name = (dictTreeData.value(forKey: "Plant_Name")as! String)
        var PlantedLocation = (dictTreeData.value(forKey: "PlantedLocation")as! String)
        var PlantedImage = ""
            if(Plant_Name == "" || Plant_Name == "<null>"){
                Plant_Name = "Not Available"
            }
        if PlantedLocation == "" ||  PlantedLocation == "<null>" {
            PlantedLocation = "Not Available"
        }
        let ary =  (dictTreeData.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
        if ary.count != 0{
            let dict = removeNullFromDict(dict: (ary.object(at: (0))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
           PlantedImage = "\(dict.value(forKey: "Plant_Status_Image")!)"
        }
        
        let width = 85
        let height = 85
        
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(85)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotOptions()
        options.size = CGSize(width: width, height: height)
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if snapshot != nil {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                let lbltitle = UILabel(frame: CGRect(x: 90, y: 5, width: 215, height: 20))
                let lblSubtitle = UILabel(frame: CGRect(x: 90, y: lbltitle.frame.maxY, width: 215, height: 60))
                    imageView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(PlantedImage)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    }, usingActivityIndicatorStyle: .gray)
                lbltitle.numberOfLines = 1
                lblSubtitle.numberOfLines = 3
                lbltitle.font = UIFont.systemFont(ofSize: 18)
                lblSubtitle.font = UIFont.systemFont(ofSize: 14)
                lblSubtitle.textColor = UIColor.darkGray
                lbltitle.text = "\(Plant_Name)"
                lblSubtitle.text = "\(PlantedLocation)"
                
                snapshotView.addSubview(imageView)
                snapshotView.addSubview(lbltitle)
                snapshotView.addSubview(lblSubtitle)
            }
            annotationView.detailCalloutAccessoryView = snapshotView
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("Did location updates is called", manager.location!.coordinate)
        let coordinate₀ = locations.last
        let MomentaryLatitudeDesti = Double(self.strletDrop)
        let MomentaryLongitudeDesti = Double(self.strLongDrop)
        let coordinate₁ = CLLocation(latitude: MomentaryLatitudeDesti!, longitude: MomentaryLongitudeDesti!)
        let distanceInMeters = coordinate₀!.distance(from: coordinate₁)
        if(Int(distanceInMeters) > reduisMeter){
            DispatchQueue.main.async {
                self.lblMessage.text = "\(Alert_TreeLocationclouser)"
                self.btnUnderMaintanance.isUserInteractionEnabled = false
                self.btnUnderMaintanance.alpha = 0.5
            }
           
        }else{
             DispatchQueue.main.async {
            self.btnUnderMaintanance.isUserInteractionEnabled = true
            self.btnUnderMaintanance.alpha = 1.0
                self.lblMessage.text = "\(Alert_TreeLocationclouserYes) \(self.btnUnderMaintanance.titleLabel?.text! ?? "")"
            }
        }
        self.mapView.showAnnotations(mapView.annotations, animated: true)

    }
    
}

extension UILabel {
    func blink() {
        self.alpha = 0.0;
        UIView.animate(withDuration: 0.8, //Time duration you want,
            delay: 0.0,
            options: [.curveEaseInOut, .autoreverse, .repeat],
            animations: { [weak self] in self?.alpha = 1.0 },
            completion: { [weak self] _ in self?.alpha = 0.0 })
    }
}

