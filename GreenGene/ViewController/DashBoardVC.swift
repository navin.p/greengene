//
//  DashBoardVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/25/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import NotificationCenter
class DashBoardVC: UIViewController {
    
    // MARK: - -------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewPager: CPImageSlider!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewheader: CardView!
    @IBOutlet weak var heightPager: NSLayoutConstraint!
    @IBOutlet weak var collection_DashBoard: UICollectionView!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    
    // MARK: - -------------variable
    // MARK: -
    var locationManager = CLLocationManager()
    var ary_of_Collection = NSMutableArray()
    var dictNotificationData = NSMutableDictionary()
    var activeStatus = Bool()
    var aryForListData = NSMutableArray()
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        } else {
            let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        }
        
        getCurrentLocation()
        
        viewPager.images =  ["image" , "image"]
        viewPager.delegate = self
        viewPager.autoSrcollEnabled = false
        viewPager.enableArrowIndicator = false
        viewPager.enablePageIndicator = true
        viewPager.enableSwipe = true
        lblTitle.text = appName
        viewheader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        if DeviceType.IS_IPHONE_5{
            heightPager.constant = 160
        }
        if DeviceType.IS_IPHONE_6 {
            heightPager.constant = 225.0
        }else if DeviceType.IS_IPHONE_6P ||  DeviceType.IS_IPHONE_X ||  DeviceType.IS_IPHONE_XR_XS_MAX{
            heightPager.constant = 260.0
        }
        
        loginDict = getLoginData()
        print(loginDict)
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            self.call_LOGIN_API(userid: "\(dict.value(forKey: "User_Id")!)")
            self.call_AddDeviceRegistration_API()
        }
        call_AboutTreeList_API()
        
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        
        if((nsud.value(forKey: "greenGENEVersionUpdateDate")) != nil){
            if(Int(day) != Int((nsud.value(forKey: "greenGENEVersionUpdateDate")!)as! String)){
                checkUpdate()
            }
        }else{
            checkUpdate()
        }
        
//        let timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { _ in
//            let imageName = LeafView.leafImages.randomElement() ?? "leaf1" // Randomly select leaf image name
//            let leafView = LeafView(frame: .zero, imageName: imageName)
//            self.view.addSubview(leafView)
//        }
//        timer.fire() // Fire immediately
        self.createRain()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRain() {
        let emitterLayer = CAEmitterLayer()
        emitterLayer.emitterPosition = CGPoint(x: view.bounds.width / 2, y: -50)
        emitterLayer.emitterSize = CGSize(width: view.bounds.width, height: 1)
        emitterLayer.emitterShape = kCAEmitterLayerLine
        let rainParticle = makeRainParticle()
        emitterLayer.emitterCells = [rainParticle]
        view.layer.addSublayer(emitterLayer)
    }
    
    func makeRainParticle() -> CAEmitterCell {
        let rainDrop = CAEmitterCell()
        rainDrop.birthRate = 150
        rainDrop.lifetime = 3
        rainDrop.velocity = 100
        rainDrop.velocityRange = 50
        rainDrop.emissionLongitude = CGFloat(Double.pi)
        rainDrop.emissionRange = CGFloat(Double.pi / 4)
        rainDrop.spin = 0.5
        rainDrop.spinRange = 1.0
        rainDrop.scale = 0.5
        rainDrop.scaleRange = 0.5
        rainDrop.contents = UIImage(named: "drawing")?.cgImage // Make sure to add an image named "raindrop" to your assets
        return rainDrop
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // viewPager.startAutoPlay()
//        self.ary_of_Collection = [["title":"Generate\nQR Code","image":"generate_qr_code","subtitle":"\(Alert_Content_GenerateQRCode)"],["title":"Scan Tree\nQR Code","image":"scan_qr_code","subtitle":"\(Alert_Content_ScanQRCode)"],["title":"Green Wealth\nCard","image":"green_wealth_card","subtitle":"\(Alert_Content_GreenWealthCard)"],["title":"My Tree","image":"my_tree-1","subtitle":"\(Alert_Content_Mytrees)"],["title":"Redeem\nBenefits","image":"discount","subtitle":"\(Alert_Content_RedeemBenefits)"],["title":"Donate","image":"donate","subtitle":"\(Alert_Content_Donate)"]]
        
        
        self.ary_of_Collection = [["title":"Generate\nQR Code","image":"generate_qr_code","subtitle":"\(Alert_Content_GenerateQRCode)"],["title":"My QR Code","image":"gift_qr_new","subtitle":"\(Alert_Content_ScanQRCode)"],["title":"Scan Tree QR Code","image":"scan_qr_code","subtitle":"\(Alert_Content_GreenWealthCard)"],["title":"My Tree","image":"my_tree-1","subtitle":"\(Alert_Content_Mytrees)"],["title":"Green Wealth Card","image":"green_wealth_card","subtitle":"\(Alert_Content_RedeemBenefits)"],["title":"Donate","image":"donate","subtitle":"\(Alert_Content_Donate)"]]
        
        
        let collectionViewLayout = (self.collection_DashBoard.collectionViewLayout as! UICollectionViewFlowLayout)
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0,left: 0, bottom: 0, right: 0)
        collectionViewLayout.scrollDirection = .vertical
        self.collection_DashBoard.collectionViewLayout = collectionViewLayout
        self.collection_DashBoard.dataSource = self
        self.collection_DashBoard.delegate = self
        self.collection_DashBoard.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            if(nsud.value(forKey: "greenGENE_Guide") != nil){
                if !(nsud.value(forKey: "greenGENE_Guide")as! Bool){
                    self.showGuides()
                }
            }else{
                //  self.showGuides()
            }
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.PushNotificationCheck()
        }
        
    }
    
    func PushNotificationCheck() {
        
        if (loginDict.count != 0){
            if(dictNotificationData.count != 0){
                let strPushType = "\(dictNotificationData.value(forKey: "PushType")!)"
                let aps = dictNotificationData.value(forKey: "aps")as! NSDictionary
                let alert = aps.value(forKey: "alert")as! NSDictionary
                let strbody = "\(alert.value(forKey: "body")!)"
                let strtitle = "\(alert.value(forKey: "title")!)"
                print(dictNotificationData)
                if(activeStatus){
                    showAlertWithoutAnyAction(strtitle: strtitle, strMessage: strbody, viewcontrol: self)
                }else{
                    if(strPushType == "ForUpdateTreeStatus"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeVC")as! MyTreeVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                    else if(strPushType == "ForNews"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC")as! NotificationVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                    else if(strPushType == "ForFollowUp"){
                        showAlertWithoutAnyAction(strtitle: strtitle, strMessage: strbody, viewcontrol: self)

                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: strtitle, strMessage: strbody, viewcontrol: self)
                    }
                    
                }
                
                
                dictNotificationData = NSMutableDictionary()
                activeStatus = Bool()
            }else{
                
                
            }
        }else{
            //   FTIndicator.showNotification(withTitle: alertMessage, message: "Session has expired and must log in again." )
        }
    }
    
    
    
    //Mark: - --------GUID
    func showGuides() {
        // Reset to show everytime.
        KSGuideDataManager.reset(for: "MainGuide")
        var items = [KSGuideItem]()
        let item = KSGuideItem(sourceView: btnHome, arrowImage: #imageLiteral(resourceName: "one-finger-click"), text: String("MENU-\n\n\(Alert_Content_Menu)"))
        let item1 = KSGuideItem(sourceView: btnMore, arrowImage: #imageLiteral(resourceName: "one-finger-click"), text: String("OTHER-\n\n\(Alert_Content_Other)"))
        let item2 = KSGuideItem(sourceView: viewPager, arrowImage: #imageLiteral(resourceName: "one-finger-click"), text: String("BANNER-\n\n\(Alert_Content_Banner)"))
        items.append(item)
        items.append(item1)
        items.append(item2)
        var top = Int(collection_DashBoard.frame.origin.y)
        let indexPath = NSIndexPath(row: 0, section: 0)
        let cell = collection_DashBoard!.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath as IndexPath ) as! DashboardCell
        
        for (index , item) in ary_of_Collection.enumerated() {
            
            if(index % 3 == 0 && index == 3){
                top = top + Int(cell.frame.height)
            }
            let indexPath = NSIndexPath(row: index, section: 0)
            let cell = collection_DashBoard!.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath as IndexPath ) as! DashboardCell
            let vieTemp = UIView.init(frame: CGRect(x: cell.frame.origin.x, y: CGFloat(top), width: cell.frame.width, height: cell.frame.height))
            let item3 = KSGuideItem(sourceView: vieTemp, arrowImage: #imageLiteral(resourceName: "one-finger-click"), text: "\((((item as AnyObject).value(forKey: "title")as! String).replacingOccurrences(of: "\n", with: " ")).uppercased())-\n\n\((item as AnyObject).value(forKey: "subtitle")!)")
            items.append(item3)
        }
        
        
        let vc = KSGuideController(items: items, key: "MainGuide")
        vc.setIndexWillChangeBlock { (index, item) in
            print("Index will change to \(index)")
        }
        vc.setIndexDidChangeBlock { (index, item) in
            print("Index did change to \(index)")
        }
        vc.show(from: self) {
            print("Guide controller has been dismissed")
            nsud.set(true, forKey: "greenGENE_Guide")
            
        }
    }
    
    //MARK:
    //MARK: checkUpdate
    func checkUpdate() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        nsud.set(day, forKey: "greenGENEVersionUpdateDate")
        
        VersionCheck.shared.checkAppStore() { isNew, version , message in
            if(isNew!){
                let alert = UIAlertController(title: "New Version Available", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { action in
                    guard let url = URL(string: appID) else { return }
                    UIApplication.shared.open(url)
                    
                }))
                alert.addAction(UIAlertAction(title: "Skip", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - --------------API Calling
    // MARK: -
    func call_AboutTreeList_API()  {
        if !(isInternetAvailable()){
        }else{
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetPlantList, OnResultBlock: { (responce, status) in
                // print(responce)
                
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"AboutTree")
                        saveDataInLocalArray(strEntity: "AboutTree", strKey: "abouttree", data: (dict.value(forKey: "PlantList")as! NSArray).mutableCopy() as! NSMutableArray)
                    }else{
                        
                    }
                }else{
                    
                }
            })
        }
    }
    
    func call_AddDeviceRegistration_API()  {
        if !(isInternetAvailable()){
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let User_Id = "\(dict.value(forKey: "User_Id")!)"
            let City_Id = "\(dict.value(forKey: "City_Id")!)"
            let State_Id = "\(dict.value(forKey: "State_Id")!)"
            var Token = ""
            if(nsud.value(forKey: "greenGENE_FCM_Token") != nil){
                Token = "\(nsud.value(forKey: "greenGENE_FCM_Token")!)"
            }
            let dictSend = NSMutableDictionary()
            dictSend.setValue(deviceID, forKey: "ImeiNo")
            dictSend.setValue(Token, forKey: "DeviceId")
            dictSend.setValue(State_Id, forKey: "State_Id")
            dictSend.setValue(City_Id, forKey: "City_Id")
            dictSend.setValue(User_Id, forKey: "User_Id")
            print(dictSend)
            WebService.callAPIBYGET(parameter: dictSend, url: URL_AddDeviceRegistration, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    
                }else{
                    
                }
            })
        }
    }
    
    func call_LOGIN_API(userid: String)  {
        if !(isInternetAvailable()){
            //  FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let loginURL = "\(URL_GetUserLoginByUserId)\(userid)"
            //  FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: loginURL, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        nsud.synchronize()
                        deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalDictionary(strEntity: "LoginData", strKey: "login", data: (dict).mutableCopy() as! NSMutableDictionary)
                        loginDict = getLoginData()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    
    
    
    // MARK: - ----------------ALL IBAction's
    // MARK: -
    
    @IBAction func actionOnMenu(_ sender: UIButton) {
        //    showGuides()
        
        let nextVc: DrawerVC = mainStoryboard.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        nextVc.handleDrawerView = self
        nextVc.modalPresentationStyle = .overCurrentContext
        nextVc.modalTransitionStyle = .crossDissolve
        self.present(nextVc, animated: false, completion: nil)
        
    }
    
    @IBAction func actionOnMore(_ sender: Any) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "OtherVC")as! OtherVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    // MARK: - ----------------Extra Function
    // MARK: -
    func checkUserLoginOrNot() -> Bool {
        if (loginDict.count == 0){
            let alert = UIAlertController(title: alertMessage, message: alertLogin, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction (title: "Cancel", style: .default, handler: { (nil) in
            }))
            alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                let home = storyBoard.instantiateViewController(withIdentifier: "LOGINVC") as! LOGINVC
                self.navigationController?.pushViewController(home, animated: true);
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied: break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    // MARK: - ----------------API's Calling
    // MARK: -
    
    func call_TreeDetail_API(plantID : String)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
            
        }else{
            let GetMyTreeDetail = URL_GetPlantDetail + "\(plantID)"
            print(GetMyTreeDetail)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        print(dict)
                        let plantData = dict.value(forKey: "PlantList")as! NSDictionary
                        let treename = "\(plantData.value(forKey: "Plant_Name")as! String)"
                        let Description = "\(plantData.value(forKey: "Description")as! String)"
                        
                        showAlertWithoutAnyAction(strtitle: treename, strMessage: Description, viewcontrol: self)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            
        }
    }
    
    func call_GetOragazation_API(){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetOrganizationDropdown, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForListData = NSMutableArray()
                        self.aryForListData = (dict.value(forKey: "OrganizationDropdownDc")as! NSArray).mutableCopy()as! NSMutableArray
                        DispatchQueue.main.async {
                            self.gotoPopView(sender: UIButton(), aryData: self.aryForListData)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CommonTableVC = storyBoard.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            vc.aryTBL = aryData
            vc.strTag = 13
            self.present(vc, animated: false, completion: {})
        }
    }
    
}

// MARK: - ----------------CPSliderDelegate
// MARK: -
extension  DashBoardVC  : CPSliderDelegate , CommonTableDelegate{
    
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
//        let testController = mainStoryboard.instantiateViewController(withIdentifier: "TreeByLocationVC")as! TreeByLocationVC
//        testController.dictData = dictData
//        testController.isSlideMenu = true
//        self.navigationController?.pushViewController(testController, animated: true)
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "FindTreeVC")as! FindTreeVC
        testController.dictData = dictData
        testController.isSlideMenu = true
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        
    }
    
    func sliderImageindex(slider: CPImageSlider, index: Int) {
        
    }
    
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
        //        let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
        //        testController.strViewComeFrom = "How and Why?"
        //        testController.strURL = URL_greengene_help_doc
        //        self.navigationController?.pushViewController(testController, animated: true)
    }
}

// MARK: - --------String
// MARK: -


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


// MARK: - ----------------DrawerScreenDelegate
// MARK: -
extension DashBoardVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        
        if(strType == "LOGIN"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LogInRegisterVC")as! LogInRegisterVC
            testController.strViewComeFrome = "LOGIN"
            self.navigationController?.pushViewController(testController, animated: true)
            return
        }
        if(loginDict.count == 0){
            if checkUserLoginOrNot(){}
        }else{
            if(strType == "PROFILE"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC")as! ProfileVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Logout"){
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: alertMessage, message: alertLogOut, preferredStyle: UIAlertControllerStyle.alert)
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction (title: "Cancel", style: .default, handler: { (nil) in
                    }))
                    alert.addAction(UIAlertAction (title: "Yes", style: .destructive, handler: { (nil) in
                        deleteAllRecords(strEntity:"LoginData")
                        deleteAllRecords(strEntity:"AboutTree")
                        deleteAllRecords(strEntity:"CaretakerList")
                        deleteAllRecords(strEntity:"MYtree")
                        deleteAllRecords(strEntity:"Notification")
                        deleteAllRecords(strEntity:"OrganizationTree")
                        
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "WelComeVC")as! WelComeVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
            else if(strType == "As a Caretaker"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ASaCaretakerVC")as! ASaCaretakerVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "My Trees"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeVC")as! MyTreeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Did You Know?"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "DidYouKnowVC")as! DidYouKnowVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "News & Updates"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC")as! NotificationVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Green Wealth Calculator"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "WealthCalculatorVC")as! WealthCalculatorVC
                self.navigationController?.pushViewController(testController, animated: true)
            } else if(strType == "Change Password"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")as! ChangePasswordVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Tree Tagging"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "QRScannerController")as! QRScannerController
                testController.viewComeFrom = "TreeTagging"
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "OutBox"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "OutBoxVC")as! OutBoxVC
                self.navigationController?.pushViewController(testController, animated: true)
            }else if(strType == "Associated Partners"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AssociatedPartnerVC")as! AssociatedPartnerVC
                self.navigationController?.pushViewController(testController, animated: true)
            }else if(strType == "Fetch QR Code"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "FatchQRCodeVC")as! FatchQRCodeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Report Tree Cutting"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "TreeCuttingReportVC")as! TreeCuttingReportVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Nearby Trees"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "TreeByLocationVC")as! TreeByLocationVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            
            else if(strType == "Find Tree"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "FindTreeVC")as! FindTreeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "About Trees"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AboutTreeVC")as! AboutTreeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "My QR Code"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "FreshQRCodeVC")as! FreshQRCodeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            else if(strType == "Allocate QR Code"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AllocateQRCodeVC")as! AllocateQRCodeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
            
            else if(strType == "Organization Trees"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "OrganizationTreeVC")as! OrganizationTreeVC
                self.navigationController?.pushViewController(testController, animated: true)
            }else if (strType == "How App Works?"){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
                testController.strViewComeFrom = "How and Why?"
                testController.strURL = URL_greengene_help_doc
                self.navigationController?.pushViewController(testController, animated: true)
            }else if (strType == "QR Tag Parks"){
                self.call_GetOragazation_API()
            }else if strType == "QR Code Order History"{
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                let QRCodeHistoryOrderVC = storyBoard.instantiateViewController(withIdentifier: "QRCodeHistoryOrderVC") as! QRCodeHistoryOrderVC
                self.navigationController?.pushViewController(QRCodeHistoryOrderVC, animated: true);
            }else if strType == "Task Management"{
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                let QRCodeHistoryOrderVC = storyBoard.instantiateViewController(withIdentifier: "TaskManagementVC") as! TaskManagementVC
                self.navigationController?.pushViewController(QRCodeHistoryOrderVC, animated: true);
            }else if strType == "Tree Tagging QR Code"{
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TreeTaggingQRCodeVC") as! TreeTaggingQRCodeVC
                self.navigationController?.pushViewController(vc, animated: true);
            }
        }
    }
}
// MARK: - ----------------UICollectionViewDelegate
//CALayer -

extension DashBoardVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_of_Collection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(indexPath.section)
        print(indexPath.row)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath as IndexPath) as! DashboardCell
        
        let dict =  ary_of_Collection[indexPath.row]as! NSDictionary
        
        cell.dashBoard_lbl_Title.text = "\(dict.value(forKey: "title")!)"
        cell.dashBoard_Image.image = UIImage(named: "\(dict.value(forKey: "image")!)")
        
        if(DeviceType.IS_IPHONE_5){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 23, y: cell.frame.height/2 - 40, width: 46, height: 46)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 08, width: cell.frame.width - 10, height: 32)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 14.0)
            
        }else if (DeviceType.IS_IPHONE_6){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 30, y: cell.frame.height/2 - 45, width: 60, height: 60)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 08, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 15.0)
        }
        else if (DeviceType.IS_IPHONE_6P){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 32, y: cell.frame.height/2 - 47, width: 64, height: 64)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 10, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 16.0)
        }
        else if (DeviceType.IS_IPHONE_X ){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 40, y: cell.frame.height/2 - 60, width: 80, height: 80)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 10, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 17.0)
        }else if (DeviceType.IS_IPAD ){
            cell.dashBoard_Image.frame = CGRect(x: cell.frame.width/2 - 45, y: cell.frame.height/2 - 60, width: 90, height: 90)
            cell.dashBoard_lbl_Title.frame = CGRect(x: 5, y: cell.dashBoard_Image.frame.maxY + 10, width: cell.frame.width - 10, height: 35)
            cell.dashBoard_lbl_Title.font = UIFont(name: cell.dashBoard_lbl_Title.font.fontName, size: 20.0)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection_DashBoard.frame.size.width)  / 3, height:(self.collection_DashBoard.frame.size.height)  / 2 )
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        if(indexPath.row == 2){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "QRScannerController")as! QRScannerController
            testController.viewComeFrom = "GETDiscount"
            self.navigationController?.pushViewController(testController, animated: true)
        }else{
            if(self.checkUserLoginOrNot()){
                if(indexPath.row == 0){
                    self.viewPager.stopAutoPlay()
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "BuyQRcodeVC")as! BuyQRcodeVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }else if (indexPath.row == 1) {
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "FreshQRCodeVC")as! FreshQRCodeVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }
                else if(indexPath.row == 4){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "GreenWealtCardVC")as! GreenWealtCardVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }
                else if(indexPath.row == 3){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeVC")as! MyTreeVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }
                else if(indexPath.row == 2){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "QRScannerController")as! QRScannerController
                    testController.viewComeFrom = "GETDiscount"
                    self.navigationController?.pushViewController(testController, animated: true)
                }
                else if(indexPath.row == 5){
                    let dictLogin = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                    if("\(dictLogin.value(forKey: "User_Id")!)" == "93"){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertnComingSoon, viewcontrol: self)
                    }else{
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "DonateVC")as! DonateVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                }
            }
            
        }
        
        
        
    }
}

class DashboardCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var dashBoard_Image: UIImageView!
    @IBOutlet weak var dashBoard_lbl_Title: UILabel!
}
