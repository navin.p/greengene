//
//  TreeByLocationVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 9/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class TreeByLocationVC: UIViewController, MKMapViewDelegate ,CLLocationManagerDelegate{
    
    
    @IBOutlet var viewTreeDetail: UIView!
    @IBOutlet weak var lblownerName: UILabel!
    @IBOutlet weak var img_Plant: UIImageView!
    @IBOutlet weak var lblPlantLocation: UILabel!
    @IBOutlet weak var lblQRCode: UILabel!
    @IBOutlet weak var btnMap: UIButton!

    @IBOutlet weak var lblPlantName: UILabel!
    @IBOutlet weak var viewHeader: CardView!

    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var mapView: MKMapView!

    var arytreeList = NSMutableArray()
     var strRadius = "0"
    var strlat = "0"
    var strlong = "0"
    var locationManager = CLLocationManager()
    // MARK: - ----------------LifeCycle
    // MARK: -
  
    override func viewDidLoad() {
        super.viewDidLoad()
  
       // segment.tintColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        strRadius = "100"
        strlat = "\(mapView.userLocation.coordinate.latitude)"
        strlong = "\(mapView.userLocation.coordinate.longitude)"
        getCurrentLocation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - ----------------getCurrentLocation
    // MARK: -
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        self.navigationController?.popViewController(animated: true)
       
    }
   
    @IBAction func actionOnClose(_ sender: UIButton) {
        if(sender.tag == 0){
            viewTreeDetail.removeFromSuperview()

        }else{
         
            

            let dictData = arytreeList.object(at: sender.tag) as! NSDictionary
            var Plant_Name = "\(dictData.value(forKey: "Plant_Name")!)"
            if Plant_Name == "" ||  Plant_Name == "<null>" {
                Plant_Name = "Not Available"
            }
            var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
            if PlantedLocation == "" ||  PlantedLocation == "<null>" {
                PlantedLocation = "Not Available"
            }
            
            var Lat = "\(dictData.value(forKey: "Lat")!)"
            if Lat == "" ||  Lat == "<null>" {
                Lat = "0.0"
            }
            var Long = "\(dictData.value(forKey: "Long")!)"
            if Long == "" ||  Long == "<null>" {
                Long = "0.0"
            }
            
            
            
            if let UrlNavigation = URL.init(string: "comgooglemaps://") {
                if UIApplication.shared.canOpenURL(UrlNavigation){
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(Lat),\(Long)&directionsmode=driving") {
                        UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                    }
                }
                else {
                    NSLog("Can't use comgooglemaps://");
                    self.openTrackerInBrowser(strlat: Lat, strlong: Long, strTitle: Plant_Name + "\n" + PlantedLocation)
                }
            }
            else
            {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(strlat: Lat, strlong: Long, strTitle: Plant_Name + "\n" + PlantedLocation)
            }
            
        }

    }
    func openTrackerInBrowser(strlat : String , strlong : String , strTitle : String){
        openMap(strTitle: strTitle, strlat: strlat, strLong: strlong)

    }
  
    // MARK: - -------------CLLocationManager
    // MARK: -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.strlat = "\(locValue.latitude)"
        self.strlong = "\(locValue.longitude)"

        if  mapView.tag == 0{
            mapView.showAnnotations(mapView.annotations, animated: true)
            call_TreeList_LocationBaseAPI()
        }

    }
    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_TreeList_LocationBaseAPI()  {
        self.view.endEditing(true)
        mapView.tag = 1
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
                //let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let GetMyTreeDetail = URL_GetPlantedTreeByLocation + "0&Lat=\(self.strlat)&Long=\(self.strlong)&Radius=\(strRadius)"
                print(GetMyTreeDetail)
//                if arytreeList.count == 0{
                    FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            //    }
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            print(self.arytreeList.count)
                            self.mapView.isHidden = false
                            self.arytreeList = NSMutableArray()
                            self.arytreeList = (dict.value(forKey: "PlantedTreeList")as! NSArray).mutableCopy()as! NSMutableArray
                            self.loadDataOnMap()
                        }else{
                            if (self.arytreeList.count == 0){
                                self.mapView.isHidden = true
                                self.lblError.text = "\(dict.value(forKey: "Success")as! String)"
                                self.imgError.image = #imageLiteral(resourceName: "notfound")
                            }
                        }
                    }else{
                        self.mapView.isHidden = true
                        self.lblError.text = alertSomeError
                        self.imgError.image = #imageLiteral(resourceName: "notfound")
                    }
                })
        }
    }
    // MARK: - --------------Extra function
    // MARK: -
    func loadDataOnMap()  {
        print(arytreeList)
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
      
        for (index, location) in arytreeList.enumerated() {
            print("Item \(index): \(location)")
            print(location)
            var strlet = ""
            var strLong = ""
            if ((location as AnyObject).value(forKey: "Lat") is String){
                strlet = ((location as AnyObject).value(forKey: "Lat")as! String)
            }
            if ((location as AnyObject).value(forKey: "Long") is String){
                strLong = ((location as AnyObject).value(forKey: "Long")as! String)
            }
            if (strlet != "") && (strLong != "") {
                let latitude: CLLocationDegrees = Double (strlet)!
                let longitude: CLLocationDegrees = Double (strLong)!
                let location1: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location1
                var Plant_Name = "\((location as AnyObject).value(forKey: "Plant_Name")!)"
                
                if(Plant_Name == "" || Plant_Name == "<null>"){
                    Plant_Name = "Not Available"
                }
               
                var OwnerName = "\((location as AnyObject).value(forKey: "OwnerName")!)"
                if(OwnerName == "" || OwnerName == "<null>"){
                    OwnerName = "Not Available"
                }
                annotation.title =  ""
                annotation.subtitle = "\(index)"
                mapView.addAnnotation(annotation)
                mapView.showAnnotations(mapView.annotations, animated: true)
                mapView.showsUserLocation = true
            }
        }
    }
  
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        print("\(String(describing: annotation.subtitle!))")
        annotationView!.image = UIImage(named: "about_tree")
        configureDetailView(annotationView: annotationView!, tag:(annotation.subtitle as? NSString)!)
        return annotationView
        
      
    }
    
    func configureDetailView(annotationView: MKAnnotationView , tag : NSString) {
      
        let index = Int("\(tag)")!
        
        let dictData = arytreeList.object(at: index) as! NSDictionary
        
        var OwnerName = "\(dictData.value(forKey: "OwnerName")!)"
        if OwnerName == "" ||  OwnerName == "<null>" {
            OwnerName = "Not Available"
        }
        var Plant_Name = "\(dictData.value(forKey: "Plant_Name")!)"
        if Plant_Name == "" ||  Plant_Name == "<null>" {
            Plant_Name = "Not Available"
        }
        var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
        if PlantedLocation == "" ||  PlantedLocation == "<null>" {
            PlantedLocation = "Not Available"
        }
        let strImageUrl = "\(dictData.value(forKey: "Plant_Status_Image")!)"

        let width = 85
        let height = 85
        
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(85)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotOptions()
        options.size = CGSize(width: width, height: height)
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if snapshot != nil {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                let lbltitle = UILabel(frame: CGRect(x: 90, y: 5, width: 215, height: 20))
                let lblSubtitle = UILabel(frame: CGRect(x: 90, y: lbltitle.frame.maxY, width: 215, height: 60))
                    if(strImageUrl.count != 0){
                        imageView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strImageUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                        }, usingActivityIndicatorStyle: .gray)
                    }
                    lbltitle.numberOfLines = 1
                    lblSubtitle.numberOfLines = 3
                    lbltitle.font = UIFont.systemFont(ofSize: 18)
                    lblSubtitle.font = UIFont.systemFont(ofSize: 14)
                    lbltitle.text = "Not available"
                    lblSubtitle.text = "Not available"
                    lblSubtitle.textColor = UIColor.darkGray
                    lbltitle.text = "\(Plant_Name)"
                    lblSubtitle.text = "\(PlantedLocation)"
                
                    snapshotView.addSubview(imageView)
                    snapshotView.addSubview(lbltitle)
                    snapshotView.addSubview(lblSubtitle)
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.AnotationhandleTap(_:)))
            snapshotView.addGestureRecognizer(tap)
            snapshotView.isUserInteractionEnabled = true
snapshotView.tag = index
            annotationView.detailCalloutAccessoryView = snapshotView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
      
    }
    

    @objc func AnotationhandleTap(_ sender: UITapGestureRecognizer) {
        let tag = sender.view?.tag
        viewTreeDetail.frame = self.view.frame
        viewTreeDetail.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            self.viewTreeDetail.transform = CGAffineTransform.identity
            self.view.addSubview(self.viewTreeDetail)

        }
        let dictData = arytreeList.object(at: tag!) as! NSDictionary
        
        var OwnerName = "\(dictData.value(forKey: "OwnerName")!)"
        if OwnerName == "" ||  OwnerName == "<null>" {
            OwnerName = "Not Available"
        }
        var Plant_Name = "\(dictData.value(forKey: "Plant_Name")!)"
        if Plant_Name == "" ||  Plant_Name == "<null>" {
            Plant_Name = "Not Available"
        }
        var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
        if PlantedLocation == "" ||  PlantedLocation == "<null>" {
            PlantedLocation = "Not Available"
        }
        var Donated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id")!)"
        if Donated_Plant_Id == "" ||  Donated_Plant_Id == "<null>" {
            Donated_Plant_Id = "Not Available"
        }
        
    
         btnMap.tag = tag!
        lblownerName.text = OwnerName
        lblPlantName.text = Plant_Name
        lblPlantLocation.text = PlantedLocation
        lblQRCode.text = Donated_Plant_Id

     //   img_Plant.layer.cornerRadius =  img_Plant.frame.width / 2
        img_Plant.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        img_Plant.layer.borderWidth  = 1.0
        img_Plant.layer.masksToBounds = false
        img_Plant.clipsToBounds = true
        img_Plant.contentMode = .scaleAspectFill
        let strImageUrl = "\(dictData.value(forKey: "Plant_Status_Image")!)"
        if(strImageUrl.count != 0){
            img_Plant.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strImageUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
                self.img_Plant.isUserInteractionEnabled = true
                self.img_Plant.addGestureRecognizer(tapGestureRecognizer)
            }, usingActivityIndicatorStyle: .gray)
        }else{
            img_Plant.image = #imageLiteral(resourceName: "default_img")
        }
        if #available(iOS 11.0, *) {
            viewTreeDetail.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
   @objc func connected(_ sender:AnyObject){
        print("yo tap image number : \(sender.view.tag)")
    let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    vc.modalTransitionStyle = .crossDissolve
    vc.imageSHow = self.img_Plant.image!
    self.navigationController?.pushViewController(vc, animated: true)
    }
}

