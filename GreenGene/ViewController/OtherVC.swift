//
//  OtherVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/24/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class OtherVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvList: UITableView!

    var aryProfile = NSMutableArray()
    var aryPolicy = NSMutableArray()
    var aryOther = NSMutableArray()
    var aryPayment = NSMutableArray()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        if(loginDict.count != 0){
            aryProfile = [["title":"Edit Profile"],["title":"Change Password"]]
        }else{
           aryProfile = NSMutableArray()
        }
        aryOther = [["title":"How and Why?"],["title":"About App"],["title":"Contact Us"],["title":"Disclaimer"],["title":"Share App"],["title":"Version"]]
        aryPolicy = [["title":"Privacy Policy"],["title":"Terms & Conditions"]]
       
       aryPayment = [["title":"Return, Refund, & Cancellation policy"],["title":"Know More"]]



        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated:true)
                break
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension OtherVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0){
            return aryProfile.count
        }else if(section == 1){
            return aryPolicy.count
        }else if(section == 2){
            return aryPayment.count
        }
        else if(section == 3){
            return aryOther.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath as IndexPath) as! OtherCell
        if(indexPath.section == 0){
              let strTitle = (aryProfile[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        }else if(indexPath.section == 1){
            let strTitle = (aryPolicy[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        }else if(indexPath.section == 2){
            let strTitle = (aryPayment[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        }else if(indexPath.section == 3){
            let strTitle = (aryOther[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tvList.cellForRow(at: indexPath)as! OtherCell
        let title = "\(cell.lblTitle.text!)"
  

   if(title == "Edit Profile"){
     let testController = mainStoryboard.instantiateViewController(withIdentifier: "UpdateProfile")as! UpdateProfile
     self.navigationController?.pushViewController(testController, animated: true)
    }
    
   else if(title == "Change Password"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")as! ChangePasswordVC
    self.navigationController?.pushViewController(testController, animated: true)
    }
    
   else if(title == "About App"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_About
    self.navigationController?.pushViewController(testController, animated: true)
     }
    
   else if(title == "Contact Us"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_ContactUS
    self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "Disclaimer"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_Disclaimer
    self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "Share App"){
        let text = appName
        let myWebsite = NSURL(string:appID)
        let shareAll = [text , myWebsite ?? 0] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
   }
   
   else if(title == "Privacy Policy"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_privacypolicy
    self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "Return, Refund, & Cancellation policy"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_returnpolicy
    self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "Terms & Conditions"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_termconditions
    self.navigationController?.pushViewController(testController, animated: true)
   }
  
   
   else if(title == "Know More"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_KnowMore
    self.navigationController?.pushViewController(testController, animated: true)
    
        }
   else if(title == "Version"){
       showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:"App Version : \(app_Version)\nDate : \(app_VersionDate)\n\(app_VersionSupport)" , viewcontrol: self)

        }
   else if(title == "How and Why?"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.strURL = URL_greengene_help_doc
    self.navigationController?.pushViewController(testController, animated: true)
    
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
         return aryProfile.count == 0 ? 0 : 50
        }
        return 50
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "Profile"
        }else if(section == 1){
            return "Policy"
        }else if(section == 2){
            return "Payment"
        }else if(section == 3){
            return "Other"
        }
        return ""
    }
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class OtherCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
