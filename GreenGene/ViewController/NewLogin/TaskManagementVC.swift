//
//  TaskManagementVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/10/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class TaskManagementVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    
    var arrayList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getSearchTaskManagementList()
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func clickOnTask(sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let dict = arrayList[indexPath!.row] as? NSDictionary
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let UpdateTaskVC = storyBoard.instantiateViewController(withIdentifier: "UpdateTaskVC") as! UpdateTaskVC
        UpdateTaskVC.dict = dict ?? NSDictionary()
        self.navigationController?.pushViewController(UpdateTaskVC, animated: true);
    }

}

extension TaskManagementVC {

    func getSearchTaskManagementList() {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
               
            let dict = NSMutableDictionary()
            var strUserID = ""
            if (loginDict.count != 0){
                let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                strUserID = "\(dict.value(forKey: "User_Id")!)"
            }
            
            dict.setValue("0", forKey: "TaskId")
            dict.setValue("0", forKey: "AssignByUser_Id")
            dict.setValue(strUserID, forKey: "AssignToUser_Id") //
            dict.setValue("1", forKey: "CategoryId")
            dict.setValue("", forKey: "TaskTitle")
            dict.setValue("", forKey: "Priority")
            dict.setValue("", forKey: "Status")
            dict.setValue("True", forKey: "IsActive")
           
            let url = "http://ggapi.citizencop.org/api/giftcard/SearchTask?"
            WebService.callAPIBYPOST(parameter: dict, url: url, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    self.arrayList = (dict.value(forKey: "TaskList")as? NSArray)?.mutableCopy()as? NSMutableArray ?? NSMutableArray()
                    self.tblView.reloadData()
                }else{
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "\(dict.value(forKey: "Success") ?? "")", viewcontrol: self)
                }
            })
        }
    }
    
}

extension TaskManagementVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskManagementCell", for: indexPath) as? taskManagementCell
        let dict = removeNullFromDict(dict: (arrayList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        cell?.lblTaskTitle.text = "\(dict.value(forKey: "TaskTitle") ?? "")"
        cell?.lblTaskDescription.text = "\(dict.value(forKey: "TaskDescription") ?? "")"
        let startDate = dateTimeConvertor(str: "\(dict.value(forKey: "StartDate")!)", formet: "")
        let endDate = dateTimeConvertor(str: "\(dict.value(forKey: "DueDate")!)", formet: "")
        cell?.lblDate.text = startDate + " To " + endDate
        cell?.lblCategory.text = "\(dict.value(forKey: "CategoryName") ?? "")"
        cell?.lblPriority.text = "\(dict.value(forKey: "Priority") ?? "")"
        cell?.lblAssignTo.text = "\(dict.value(forKey: "AssignToName") ?? "")"
        cell?.lblAssignBy.text = "\(dict.value(forKey: "AssignByName") ?? "")"
        cell?.lblStatus.text = "\(dict.value(forKey: "Status") ?? "")"
        cell?.lblRemark.text = "\(dict.value(forKey: "Remarks") ?? "")"
        cell?.btnUpdateTask.addTarget(self, action: #selector(clickOnTask), for: .touchUpInside)
        
        let TaskImages = "\(dict.value(forKey: "TaskImages") ?? "")"
        var arr = TaskImages.components(separatedBy: ",")
        
        cell?.list = arr
        cell?.collectionView.reloadData()
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

class taskManagementCell : UITableViewCell ,UICollectionViewDelegate ,UICollectionViewDataSource{
   
    
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet weak var lblTaskDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var lblAssignBy: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnUpdateTask: UIButton!
    @IBOutlet weak var lblRemark: UILabel!
    
    var list = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.layer.borderWidth = 0.9
        self.collectionView.layer.borderColor = UIColor.placeholderText.cgColor
        self.collectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "taskCollectionCell", for: indexPath) as? taskCollectionCell
        let dict = list[indexPath.row]
        let showImage = BaseURLTaskImages + dict
        cell?.imgVIew.layer.cornerRadius = 10
        cell?.imgVIew.setImageWith(URL(string: showImage), usingActivityIndicatorStyle: .medium)
        
        return cell ?? UICollectionViewCell()
    }
    
}

class taskCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var imgVIew: UIImageView!
    
}
