//
//  CountTreeVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/7/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class CountTreeVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tblView: UITableView!
    
    var dictData = NSDictionary()
    var arytreeList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.GetUpvanTreeCountSummaryBySearchFilter()
    }
    
    //MARK: Summary Count
    func GetUpvanTreeCountSummaryBySearchFilter() {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
               
            let dict = NSMutableDictionary()
            
            dict.setValue("", forKey: "Lat")
            dict.setValue("", forKey: "Long")
            dict.setValue("0", forKey: "Radius")
            dict.setValue("", forKey: "Plant_Type")
            dict.setValue("0", forKey: "UserId")
            dict.setValue("0", forKey: "Plant_Id")
            dict.setValue("0", forKey: "Donated_Plant_Id")
            dict.setValue(dictData.value(forKey: "Organization_Coupen_Id"), forKey: "Organization_Coupen_Id")
            dict.setValue("0", forKey: "Take")
            dict.setValue("0", forKey: "Skip")
            dict.setValue("", forKey: "CommonSearch")
            
            WebService.callAPIBYPOST(parameter: dict, url: URL_GetUpvanTreeCountSummaryBySearchFilter, OnResultBlock: { (responce, status) in
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.arytreeList = NSMutableArray()
                        self.arytreeList = (dict.value(forKey: "dt")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tblView.reloadData()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "\(dict.value(forKey: "Success") ?? "")", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

class countTreeCell : UITableViewCell {
    
    @IBOutlet weak var backGourdView: UIView!
    @IBOutlet weak var lblPlantName: UILabel!
    @IBOutlet weak var lblPlantCount: UILabel!
    
}

extension CountTreeVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arytreeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countTreeCell", for: indexPath) as? countTreeCell
        cell?.backGourdView.layer.cornerRadius = 10
        cell?.backGourdView.layer.borderWidth = 0.5
        let dict = removeNullFromDict(dict: (arytreeList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        cell?.lblPlantName.text = "\(dict.value(forKey: "Plant_Name") ?? "")"
        cell?.lblPlantCount.text = "\(dict.value(forKey: "NoOfTree") ?? "")"
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
