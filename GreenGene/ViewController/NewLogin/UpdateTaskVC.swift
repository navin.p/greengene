//
//  UpdateTaskVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/10/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit
import Alamofire

class UpdateTaskVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var collectionVIew: UICollectionView!
    @IBOutlet weak var txtRemark: SkyFloatingLabelTextField!
    @IBOutlet weak var bgVIew: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    var imagePicker = UIImagePickerController()
    var arrOfImage = [UIImage]()
    var arrOfGlobalData  = [Data]()
    var arrOfGlobalNameOfImages = NSMutableArray()
    var arrOfGlobalDocumentType = [String]()
    var dict = NSDictionary()
    var arrImageName = [String]()
    var aryTempImages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.bgVIew.layer.cornerRadius = 10
        self.bgVIew.layer.borderWidth = 0.5
        self.bgVIew.layer.borderColor = UIColor.lightGray.cgColor
        self.collectionVIew.delegate = self
        self.collectionVIew.dataSource = self
        
    }
    

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func actionOnStatus(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        let Pending = (UIAlertAction(title: "Pending", style: .default , handler:{ (UIAlertAction)in
            self.lblStatus.text = "Pending"
        }))
        alert.addAction(Pending)
        
        let Progress = (UIAlertAction(title: "In Progress", style: .default , handler:{ (UIAlertAction)in
            self.lblStatus.text = "In Progress"
        }))
        alert.addAction(Progress)
        
        let OnHold = (UIAlertAction(title: "On Hold", style: .default , handler:{ (UIAlertAction)in
            self.lblStatus.text = "On Hold"
        }))
        alert.addAction(OnHold)
        
        let Complete = (UIAlertAction(title: "Completed", style: .default , handler:{ (UIAlertAction)in
            self.lblStatus.text = "Completed"
        }))
        alert.addAction(Complete)
        
        let notDone = (UIAlertAction(title: "Not Done", style: .default , handler:{ (UIAlertAction)in
            self.lblStatus.text = "Not Done"
        }))
        alert.addAction(notDone)
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    @IBAction func actionOnnSlectImage(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        let camera = (UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertCalling, viewcontrol: self)
            }
        }))
        alert.addAction(camera)
        let Gallery = (UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertCalling, viewcontrol: self)
            }
        }))
        
        alert.addAction(Gallery)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        self.apiCallig()
    }
    
    
    @objc func deleteImage(sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.collectionVIew)
        let indexPath = self.collectionVIew.indexPathForItem(at: buttonPosition)
        self.arrOfGlobalData.remove(at: indexPath!.row)
        collectionVIew.reloadData()
    }
    
    
    func apiCallig(){
        
      //  let strAttachements = arrOfGlobalNameOfImages.joined(separator: ",")
        let strAttachements = arrOfGlobalNameOfImages.componentsJoined(by: ",")
        
        let dictSendData = NSMutableDictionary()
        
        dictSendData.setValue("\(self.dict.value(forKey: "TaskId") ?? "")", forKey: "TaskId")
        dictSendData.setValue(lblStatus.text ?? "", forKey: "Status")
        dictSendData.setValue(txtRemark.text ?? "", forKey: "Remarks")
        dictSendData.setValue(strAttachements, forKey: "TaskImages")
        
        var json1 = Data()
        var jsonAdd = NSString()
        
        if JSONSerialization.isValidJSONObject(dictSendData) {
            // Serialize the dictionary
            json1 = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
            jsonAdd = String(data: json1, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonAdd)")
        }
        if(dictSendData.count == 0){
            jsonAdd = ""
        }
        
        let disctNs = NSMutableDictionary()
        disctNs.setValue(jsonAdd, forKey: "TaskUpdateDc")
        
        self.call_UPLOADIMGE_API(parameter: disctNs)

    }
    
    func call_UPLOADIMGE_API(parameter : NSDictionary)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIWithImageMultiple(parameter: parameter, url: URL_UpdateTask, image: arrOfImage, imageName: arrImageName, arrayList: aryTempImages , OnResultBlock: { (responce, status) in
                FTIndicator.dismissProgress()
                if (status == "True"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                   
                    let alert = UIAlertController(title: alertInfo, message: "\(dict.value(forKey: "Success") ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.navigationController?.popViewController(animated: false)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            
        }
    }
    
}

// MARK: ---------UIImagePickerControllerDelegate------
extension UpdateTaskVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageData = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            let Data = UIImageJPEGRepresentation(imageData, 1.0)
            self.arrOfGlobalData.append(Data!)
            self.arrOfGlobalDocumentType.append("image")
            self.arrOfGlobalNameOfImages.add("\(getUniqueValueForId())" + ".jpg")
            self.arrImageName.append("\(getUniqueValueForId())" + ".jpg")
            self.arrOfImage.append(info[UIImagePickerControllerOriginalImage] as? UIImage ?? UIImage())
            
            
            let dict = NSMutableDictionary()
            dict.setValue(info[UIImagePickerControllerOriginalImage] as? UIImage ?? UIImage(), forKey: "image")
            dict.setValue("\(getUniqueValueForId())" + ".jpg", forKey: "imageName")
            aryTempImages.add(dict)
            
            
            self.collectionVIew.reloadData()
            dismiss(animated: true, completion: nil)
        }
    }
    
    func getUniqueValueForId() -> String{
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        let today_string = String(year!) + String(month!)  + String(day!) + String(hour!)  + String(minute!) +  String(second!)
        
        let dateFMT = DateFormatter()
        dateFMT.locale = Locale(identifier: "en_US_POSIX")
        dateFMT.dateFormat = "yyyyMMddHHmmssSSSS"
        let now = Date()
        return today_string
        
    }
}

extension UpdateTaskVC : UICollectionViewDataSource , UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfGlobalData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "updateTaskCollectionCell", for: indexPath) as? updateTaskCollectionCell
        if arrOfGlobalDocumentType[indexPath.row] == "image"{
            if UIImage(data: arrOfGlobalData[indexPath.row]) != nil{
                let imageTemp : UIImage = UIImage(data: arrOfGlobalData[indexPath.row])!
                cell?.imgView.image = imageTemp
                cell?.btnClose.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
            }
        }
        return cell!
    }
}

class updateTaskCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    
}
