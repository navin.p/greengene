//
//  QRTagParksSerachVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/13/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit

class QRTagParksSerachVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var mapVIew: MKMapView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtRedius: SkyFloatingLabelTextField!
    
    var dict = NSDictionary()
    var aryList = NSMutableArray()
    var arytreeList = NSMutableArray()
    
    var lat = ""
    var long = ""
    
    var aryRedius : NSMutableArray = ["10 Meter" , "20 Meter" , "30 Meter" , "40 Meter" , "50 Meter" , "100 Meter" , "200 Meter" , "300 Meter" ,"400 Meter" , "500 Meter" , "600 Meter" , "700 Meter" , "800 Meter" , "900 Meter" , "1 Kilometer" , "2 Kilometer" ,"3 Kilometer" , "4 Kilometer" , "5 Kilometer" , "6 Kilometer" , "7 Kilometer" , "8 Kilometer" , "9 Kilometer" , "10 Kilometer" ,"11 Kilometer" , "12 Kilometer" ,"13 Kilometer" , "14 Kilometer" , "15 Kilometer"]
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.lblTitle.text = "\(dict.value(forKey: "OrganizationName") ?? "")"
        self.searchView.layer.cornerRadius = 10
        self.searchView.layer.borderWidth = 0.7
        self.searchView.layer.borderColor = UIColor.lightGray.cgColor
        mapVIew.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CommonTableVC = storyBoard.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            vc.aryTBL = aryData
            vc.strTag = 1000
            self.present(vc, animated: false, completion: {})
        }
    }
    
    func GetUpvanTreeBySearchFilter() {
        
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
               
            let dict = NSMutableDictionary()
            
            dict.setValue(lat, forKey: "Lat")
            dict.setValue(long, forKey: "Long")
            dict.setValue(txtRedius.text ?? "", forKey: "Radius")
            dict.setValue("", forKey: "Plant_Type")
            dict.setValue("0", forKey: "UserId")
            dict.setValue("0", forKey: "Plant_Id")
            dict.setValue("0", forKey: "Donated_Plant_Id")
            dict.setValue(self.dict.value(forKey: "Organization_Coupen_Id"), forKey: "Organization_Coupen_Id")
            dict.setValue("0", forKey: "Take")
            dict.setValue("0", forKey: "Skip")
            dict.setValue("", forKey: "CommonSearch")
            
            WebService.callAPIBYPOST(parameter: dict, url: URL_GetUpvanTreeBySearchFilter, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        var arrytreeList = NSMutableArray()
                        arrytreeList = (dict.value(forKey: "dt")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        if arrytreeList.count > 0 {
                            
                            for i in arrytreeList {
                                let dict = i as? NSDictionary
                                if "\(dict?.value(forKey: "PlantedLocation") ?? "")".lowercased().contains(self.txtAddress.text?.lowercased() ?? ""){
                                    self.arytreeList.add(dict!)
                                }
                            }
                            
                            
                            if self.arytreeList.count > 0{
                                let testController = mainStoryboard.instantiateViewController(withIdentifier: "FindTreeVC")as! FindTreeVC
                                testController.arytreeList = self.arytreeList
                                testController.isSearchFilter = true
                                self.navigationController?.pushViewController(testController, animated: true)
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Data Not Found", viewcontrol: self)
                            }
        
                        }
                        
                    }else{
                        
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnRedius(_ sender: UIButton) {
        self.gotoPopView(sender: sender, aryData: aryRedius)
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        if txtAddress.text == "" {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Please Fill The Address..", viewcontrol: self)
        } else if txtRedius.text == ""{
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Please Select Redius..", viewcontrol: self)
        } else{
            self.GetUpvanTreeBySearchFilter()
        }
    }
    
    @IBAction func actionOnClose(_ sender: UIButton) {
        self.txtAddress.text =  ""
    }
    
    @IBAction func actionOnSearchIcon(_ sender: UIButton) {
        if txtAddress.text == "" {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Please Fill The Address..", viewcontrol: self)
        }else{
            searchForLocation(query: txtAddress.text!)
        }
    }
    
}

extension QRTagParksSerachVC : CommonTableDelegate {
    
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
        print(dictData)
    }
    
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        self.txtRedius.text = strTitle
    }
    
    
}


extension QRTagParksSerachVC :  MKMapViewDelegate , CLLocationManagerDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        print("\(String(describing: annotation.subtitle!))")
        annotationView!.image = UIImage(named: "Target")
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapVIew.setRegion(region, animated: true)
        }
    }

    func searchForLocation(query: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = query
        
        let search = MKLocalSearch(request: request)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            
            for item in response.mapItems {
                let annotation = MKPointAnnotation()
                annotation.title = item.name
                annotation.coordinate = item.placemark.coordinate
                self.mapVIew.addAnnotation(annotation)
                
                // Print latitude and longitude
                let latitude = item.placemark.coordinate.latitude
                let longitude = item.placemark.coordinate.longitude
                print("Name: \(item.name ?? "Unknown"), Latitude: \(latitude), Longitude: \(longitude)")
                
                self.lat = "\(latitude)"
                self.long = "\(longitude)"
                
            }
            
            if let firstItem = response.mapItems.first {
                let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
                let region = MKCoordinateRegion(center: firstItem.placemark.coordinate, span: span)
                self.mapVIew.setRegion(region, animated: true)
            }
        }
    }
}
