//
//  SIGNUPVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 5/31/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class SIGNUPVC: UIViewController {
    
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var btnIAgree: UIButton!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    
    var aryForStateList = NSMutableArray()
    var aryForCityList = NSMutableArray()
    var isTermsCondition = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetup()
    }
    
    func intialSetup(){
        self.backGroundView.layer.cornerRadius = 25
        self.backGroundView.layer.borderWidth = 4
        self.backGroundView.layer.borderColor = UIColor.white.cgColor
        self.btnSignup.layer.cornerRadius = 25
        self.txtEmail.delegate = self
        self.txtUserName.delegate = self
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        self.txtMobileNumber.delegate = self
    }
    
    // MARK: - --------------ValidationSignUP
    // MARK: -
    
    func validationSignUp() -> Bool {
        if txtUserName.text == "" {
            showToast(message: alertSignUPFullName)
            return false
        }else if ((txtEmail.text?.count)! == 0){
            showToast(message: alertSignUPEmail)
            return false
        }
        else if !(validateEmail(email: txtEmail.text!)){
            showToast(message: alertSignUPValidEmail)
            return false
        }
        else if ((txtMobileNumber.text?.count)! == 0){
            showToast(message: alertLoginMobile)
            return false
        }
        else if ((txtMobileNumber.text?.count)! < 10){
            showToast(message: alertLoginValidMobile)
            return false
        }else if ((txtPassword.text?.count)! == 0){
            showToast(message: alertLoginPassword)
            return false
        }else if ((txtPassword.text?.count)! < 6){
            showToast(message: alertSignUPValidPassword)
            return false
        }else if ((txtConfirmPassword.text?.count)! == 0){
            showToast(message: alertSignUPCPassword)
            return false
        }else if ((txtConfirmPassword.text!) != (txtPassword.text!) ){
            showToast(message: alertSignUPValidConfirmPass)
            return false
        }else if ((txtState.text?.count)! == 0){
            showToast(message: alertSignUPState)
            return false
        }else if ((txtCity.text?.count)! == 0){
            showToast(message: alertSignUPCity)
            return false
        }else if isTermsCondition == false{
            showToast(message: alertSignTerms)
            return false
        }else{
            return true
        }
    }

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func actionOnSignUp(_ sender: UIButton) {
        if (validationSignUp()){
            call_SIGNUP_API(sender: sender)
        }
    }
    
    
    @IBAction func actionOnAgreeTerms(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if isTermsCondition == false {
            isTermsCondition = true
        }else{
            isTermsCondition = false
        }
    }
    
    @IBAction func actionOnCity(_ sender: UIButton) {
        if txtState.text?.count != 0 {
            self.call_CityList_API(sender: sender)
        }else{
            showToast(message: alertSignUPStateFirst)
        }
    }
    
    @IBAction func actionOnState(_ sender: UIButton) {
        if aryForStateList.count != 0 {
            self.gotoPopView(sender: sender, aryData: aryForStateList)
        }else{
            self.call_StateList_API(sender: sender)
        }
    }
    
    
}

extension SIGNUPVC : refreshSignUpView{
    
    
    func refreshview(dictData: NSDictionary, tag: Int) {
        if tag == 1 { // State
            txtState.text = dictData.value(forKey: "State_Name")as? String
            txtState.tag = dictData.value(forKey: "State_Id")as! Int
        }else if tag == 2 { // City
            txtCity.text = dictData.value(forKey: "City_Name")as? String
            txtCity.tag = dictData.value(forKey: "City_Id")as! Int
        }
    }
    
    
    func call_StateList_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetStateListByCountryId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForStateList = NSMutableArray()
                        self.aryForStateList = (dict.value(forKey: "StateList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForStateList)
                    }else{
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                    }
                }else{
                    self.showToast(message: alertSomeError)
                }
            })
        }
    }
    
    func call_CityList_API(sender : UIButton) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetCityListByStateId = "\(URL_GetCityListByStateId)\(String(describing: txtState.tag))"
            print(GetCityListByStateId)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetCityListByStateId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForCityList = NSMutableArray()
                        self.aryForCityList = (dict.value(forKey: "CityList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForCityList)
                    }else{
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                    }
                }else{
                    self.showToast(message: alertSomeError)
                }
            })
        }
    }
    
    func call_SIGNUP_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            let urlSignUp = "\(URL_UserSignUp)Name=\(txtUserName.text!)&Email_Id=\(txtEmail.text!)&Mobile_Number=\(txtMobileNumber.text!)&Password=\(txtPassword.text!)&State_Id=\(txtState.tag)&City_Id=\(txtCity.tag)&Address=\(txtAddress.text ?? "")"
            print(urlSignUp)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: urlSignUp, OnResultBlock: { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.showToast(message: alertSignUP)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                            let home = storyBoard.instantiateViewController(withIdentifier: "LOGINVC") as! LOGINVC
                            self.navigationController?.pushViewController(home, animated: true);
                        }
                    }else{
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                    }
                }else{
                    self.showToast(message: alertSomeError)
                }
            })
        }
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CommonTableVC = storyBoard.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        if sender.tag == 0 { //State
            vc.strTitle = "Select State"
            vc.strTag = 1
        }else if sender.tag == 1 { // City
            vc.strTitle = "Select City"
            vc.strTag = 2
        }
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.handleSignUPView = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
    }
    
}

extension SIGNUPVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtUserName  {
            return  txtFiledValidation(textField: txtUserName, string: string, returnOnly: "ALL", limitValue: 30)
        }
        if textField == txtMobileNumber  {
            return  txtFiledValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField ==  txtPassword  {
            return  txtFiledValidation(textField: txtPassword, string: string, returnOnly: "ALL", limitValue: 11)
        }
        if  textField ==  txtConfirmPassword {
            return  txtFiledValidation(textField: txtConfirmPassword, string: string, returnOnly: "ALL", limitValue: 11)
        }else{
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 60)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
