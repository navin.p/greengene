//
//  ForgotVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 5/31/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class ForgotVC: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var backGroundView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetup()
    }
    
    func intialSetup(){
        self.backGroundView.layer.cornerRadius = 25
        self.backGroundView.layer.borderWidth = 4
        self.backGroundView.layer.borderColor = UIColor.white.cgColor
        self.btnForgotPassword.layer.cornerRadius = 25
        self.txtEmail.delegate = self
    }
    
    func validationLogin() -> Bool {
        if txtEmail.text?.count == 0 {
            showToast(message: alertSignUPEmail)
            return false
        } else if !(validateEmail(email: txtEmail.text!)){
            showToast(message: alertSignUPValidEmail)
            return false
        }else{
            return true
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if (validationLogin()){
            self.call_FORGOT_API(sender: sender)
        }
    }
    
}

// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension ForgotVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEmail {
            return  txtFiledValidation(textField: txtEmail, string: string, returnOnly: "All", limitValue: 55)
            
        }else{
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

// MARK: - ---------------Api Calling
// MARK: -
extension ForgotVC {
    func call_FORGOT_API(sender: UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let forgotURL = "\(URL_ForgetPassword)Email_Id=\(String(describing: txtEmail.text!))"
            
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: forgotURL, OnResultBlock: { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                    }
                }else{
                    self.showToast(message: alertSomeError)
                }
            })
        }
    }
}
