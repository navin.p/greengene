//
//  UpvanViewController.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/5/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class UpvanViewController: UIViewController {
    

    @IBOutlet weak var seaarchBarText: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var upvanTblView: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!
    
    var arrOfSearchList = NSMutableArray()
    var aryList = NSMutableArray()
    var dict = NSDictionary()
    var isSeaching = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.upvanTblView.delegate = self
        self.upvanTblView.dataSource = self
        self.lblTitle.text = "\(dict.value(forKey: "OrganizationName") ?? "")"
        self.seaarchBarText.delegate = self
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func clickOnStatus(sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.upvanTblView)
        let indexPath = self.upvanTblView.indexPathForRow(at: buttonPosition)
        let dict = aryList[indexPath!.row] as? NSDictionary
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let PlantTreeVC = storyBoard.instantiateViewController(withIdentifier: "PlantTreeVC") as! PlantTreeVC
        PlantTreeVC.dict = dict ?? NSDictionary()
        navigationController?.pushViewController(PlantTreeVC, animated: false);
    }
    
    @objc func clickOnLocation(sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.upvanTblView)
        let indexPath = self.upvanTblView.indexPathForRow(at: buttonPosition)
        let dict = aryList[indexPath!.row] as? NSDictionary
        let lat = "\(dict?.value(forKey: "Lat") ?? "")"
        let long = "\(dict?.value(forKey: "Long") ?? "")"
        let Plant_Name = "\(dict?.value(forKey: "Plant_Name") ?? "")"
        let PlantedLocation = "\(dict?.value(forKey: "PlantedLocation") ?? "")"
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=driving") {
                    UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                }
            }else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(strlat: lat, strlong: long, strTitle: Plant_Name + "\n" + PlantedLocation)
            }
        }else{
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser(strlat: lat, strlong: long, strTitle: Plant_Name + "\n" + PlantedLocation)
        }
    }
    
    func openTrackerInBrowser(strlat : String , strlong : String , strTitle : String){
        openMap(strTitle: strTitle, strlat: strlat, strLong: strlong)
    }
    
}

extension UpvanViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
            return self.aryList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "upvanCell", for: indexPath) as? upvanCell
        var dict = NSDictionary()
        
        if self.isSeaching == true {
             dict = removeNullFromDict(dict: (arrOfSearchList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        }else{
             dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        }
        
        
        cell?.lblQRNumber.text = "QR-\(dict.value(forKey: "Donated_Plant_Id") ?? "")"
        cell?.lblTreeName.text = "\(dict.value(forKey: "Plant_Name") ?? "")"
        cell?.lblAddress.text = "\(dict.value(forKey: "PlantedLocation") ?? "")"
        
        let showImage = BaseURLImageDownLoad + "\(dict.value(forKey: "Plant_Status_Image") ?? "")"
        
        cell?.imgView.setImageWith(URL(string: showImage), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
        }, usingActivityIndicatorStyle: .gray)
        
        cell?.imgView.layer.cornerRadius = 10
        cell?.btnStatus.addTarget(self, action: #selector(clickOnStatus), for: .touchUpInside)
        cell?.btnLocation.addTarget(self, action: #selector(clickOnLocation), for: .touchUpInside)
        
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

class upvanCell : UITableViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewHeader2: UIView!
    @IBOutlet weak var lblQRNumber: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTreeName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        self.btnStatus.layer.cornerRadius = 15
        self.btnLocation.layer.cornerRadius = 15
        self.btnStatus.clipsToBounds = true
        self.btnLocation.clipsToBounds = true
    }
    
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
    
    func roundBottomCorners(radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: [.bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
}



extension UpvanViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            // btnCancle.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                self.upvanTblView.reloadData()
                //  btnCancle.isHidden = true
                
            }
            if searchedText.count > 0 {
                self.arrOfSearchList = NSMutableArray()
                //  btnCancle.isHidden = false
                let resultPredicate = NSPredicate(format: "Plant_Name contains[c]%@" ,searchedText)
                let filtered = aryList.filtered(using: resultPredicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                upvanTblView.reloadData()
                
            }else{
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                // btnCancle.isHidden = true
                upvanTblView.reloadData()
            }
            return true
        }
    }
}
