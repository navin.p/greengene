//
//  TreeTaggingQRCodeVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/12/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class TreeTaggingQRCodeVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var arrayList = NSMutableArray()
    var isDashboard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        let layout = UICollectionViewFlowLayout()
        let spacing: CGFloat = 10
        let numberOfItemsPerRow: CGFloat = 2
        let totalSpacing = (numberOfItemsPerRow - 1) * spacing
        
        let itemWidth = (view.bounds.width - totalSpacing) / numberOfItemsPerRow
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        layout.minimumInteritemSpacing = spacing
        layout.minimumLineSpacing = spacing
        
        collectionView.collectionViewLayout = layout
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        if isDashboard == true{
            self.lblTitle.text = "My QR Code"
        }else{
            self.call_GetQRCodeOrder_API()
        }
        
    }
    

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension TreeTaggingQRCodeVC {
    func call_GetQRCodeOrder_API(){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            var strUserId = ""
            if (loginDict.count != 0){
                let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                strUserId = "\(dict.value(forKey: "User_Id")!)"
            }
            let url = URL_GetUntaggedQRCodeByRange + strUserId
            WebService.callAPIBYGET(parameter: NSDictionary(), url: url, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.arrayList = (dict.value(forKey: "dt")as? NSArray)?.mutableCopy()as? NSMutableArray ?? NSMutableArray()
                        self.collectionView.reloadData()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
}

extension TreeTaggingQRCodeVC : UICollectionViewDelegate , UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "treeTaggingQrCodeCollection", for: indexPath) as! treeTaggingQrCodeCollection
        let dict = removeNullFromDict(dict: (arrayList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        cell.imgQR.contentMode = .scaleAspectFit
        cell.lblQRName.text = "\(dict.value(forKey: "QRCode") ?? "")"
        cell.lblQRNumber.text = "\(dict.value(forKey: "Donated_Plant_Id") ?? "")"
        cell.backgroundColor = .white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc: TreeTaggingVC = mainStoryboard.instantiateViewController(withIdentifier: "TreeTaggingVC") as! TreeTaggingVC
        let dict = removeNullFromDict(dict: (arrayList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        vc.dictData =  dict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

class treeTaggingQrCodeCollection : UICollectionViewCell {
    
    @IBOutlet weak var lblQRNumber: UILabel!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblQRName: UILabel!
    
    
}
