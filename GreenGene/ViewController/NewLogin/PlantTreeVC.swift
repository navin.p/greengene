//
//  PlantTreeVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/7/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class PlantTreeVC: UIViewController {
    
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var plantTreeTableView: UITableView!
    
    var aryList = NSMutableArray()
    var dict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.plantTreeTableView.delegate = self
        self.plantTreeTableView.dataSource = self
        self.call_GetPlantMonthlyStatusByDonatedPlantId_API()
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

class plantsTreeCell : UITableViewCell {
    
    @IBOutlet weak var lblPest: UILabel!
    @IBOutlet weak var lblTrip: UILabel!
    @IBOutlet weak var lblFertilizers: UILabel!
    @IBOutlet weak var lblCareTakar: UILabel!
    @IBOutlet weak var lblDonatedPlant: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
}


extension PlantTreeVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "plantsTreeCell", for: indexPath) as? plantsTreeCell
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        
        if "\(dict.value(forKey: "IsTrim") ?? "")" == "False" || "\(dict.value(forKey: "IsTrim") ?? "")" == "false" || "\(dict.value(forKey: "IsTrim") ?? "")" == "0" {
            cell?.lblTrip.text = "No"
        }else{
            cell?.lblTrip.text = "Yes"
        }
        
        if "\(dict.value(forKey: "IsPestControl") ?? "")" == "False" || "\(dict.value(forKey: "IsPestControl") ?? "")" == "false" || "\(dict.value(forKey: "IsPestControl") ?? "")" == "0" {
            cell?.lblPest.text = "No"
        }else{
            cell?.lblPest.text = "Yes"
        }
        
        if "\(dict.value(forKey: "IsFertilizer") ?? "")" == "False" || "\(dict.value(forKey: "IsFertilizer") ?? "")" == "false" || "\(dict.value(forKey: "IsFertilizer") ?? "")" == "0" {
            cell?.lblFertilizers.text = "No"
        }else{
            cell?.lblFertilizers.text = "Yes"
        }
        
        cell?.lblCareTakar.text = "\(dict.value(forKey: "Care_Taker") ?? "")" +  "(\(dict.value(forKey: "Care_Taker_MobileNo") ?? ""))"
        
        let showImage = BaseURLImageDownLoad + "\(dict.value(forKey: "Plant_Status_Image") ?? "")"
        
        cell?.imgView.setImageWith(URL(string: showImage), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
        }, usingActivityIndicatorStyle: .gray)
        
        let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
        cell?.lblDate.text =  strDate
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}


extension PlantTreeVC {
    func call_GetPlantMonthlyStatusByDonatedPlantId_API(){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let url = URL_GetPlantMonthlyStatusByDonatedPlantId + "\(dict.value(forKey: "Donated_Plant_Id") ?? "")"
            WebService.callAPIBYGET(parameter: NSDictionary(), url: url, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryList = NSMutableArray()
                        self.aryList = (dict.value(forKey: "DonatedPlantStatusList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.plantTreeTableView.reloadData()
                    
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
}
