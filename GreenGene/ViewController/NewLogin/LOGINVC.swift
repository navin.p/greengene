//
//  LOGINVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 5/30/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreTelephony

class LOGINVC: UIViewController {

    @IBOutlet weak var backGourdView: UIView!
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnRemember: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtCountryCode: SkyFloatingLabelTextField!
    
    var isRemember = false
    var aryCountryList = NSMutableArray()
    var StrCountry_Name = ""
    var countryName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetup()
    }
    
    func intialSetup(){
        self.backGourdView.layer.cornerRadius = 25
        self.backGourdView.layer.borderWidth = 4
        self.backGourdView.layer.borderColor = UIColor.white.cgColor
        self.btnSignIn.layer.cornerRadius = 25
        self.txtMobile.delegate = self
        
        
        let regionCode = Locale.current.regionCode ?? ""
        countryInformation(countryCode: regionCode)
        
    }
    
    func countryInformation(countryCode:String){
        
        var flag: String? = ""
        let flagBaseCode = UnicodeScalar("🇦").value - UnicodeScalar("A").value
        countryCode.uppercased().unicodeScalars.forEach {
            if let scaler = UnicodeScalar(flagBaseCode + $0.value) {
                flag?.append(String(describing: scaler))
            }
        }
        if flag?.count != 1 {
            flag = nil
        }
        
        self.countryName = Locale.current.localizedString(forRegionCode: countryCode)!
        print(countryName ?? "No name")
        print(flag ?? "No flag")
        print(countryCode)
        
    }
    
    
    func validationLogin() -> Bool {
        
        if txtCountryCode.text?.count == 0 {
            showToast(message: "Please Select Country Code")
            return false
        }else if txtMobile.text?.count == 0 {
          //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginMobile, viewcontrol: self)
            showToast(message: alertLoginMobile)
            return false
        }else if ((txtMobile.text?.count)! < 10){
           // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginValidMobile, viewcontrol: self)
            showToast(message: alertLoginValidMobile)
            return false
        }else if ((txtPassword.text?.count)! == 0){
          //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginPassword, viewcontrol: self)
            showToast(message: alertLoginPassword)
            return false
        }else if countryName.lowercased() != StrCountry_Name.lowercased() {
            showToast(message: "Invalid Country Code")
            return false
        }else{
            return true
        }
    }
    
    //MARK: Action Button
    
    @IBAction func actionOnRemember(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if isRemember == false {
            isRemember = true
        }else{
            isRemember = false
        }
    }
    
    @IBAction func actionOnForgotPassword(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let forgotPassword = storyBoard.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
        navigationController?.pushViewController(forgotPassword, animated: false);
    }
    
    @IBAction func actionOnSignUp(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let forgotPassword = storyBoard.instantiateViewController(withIdentifier: "SIGNUPVC") as! SIGNUPVC
        navigationController?.pushViewController(forgotPassword, animated: false);
    }
    
    @IBAction func actionLogin(_ sender: UIButton) {
        if (validationLogin()){
            call_LOGIN_API(sender: sender)
        }
    }
    
    
    @IBAction func actionOnCountryCode(_ sender: UIButton) {
        self.call_CountryCode_API(sender: sender)
    }
    
}

extension LOGINVC : CommonTableDelegate{
    
    
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
        txtCountryCode.text = "\(dictData.value(forKey: "Country_Code") ?? "")"
        self.StrCountry_Name = "\(dictData.value(forKey: "Country_Name") ?? "")"
    }
    
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        print("sssds1100")
    }
    
    func call_LOGIN_API(sender: UIButton)  {
        if !(isInternetAvailable()){
           FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let loginURL = "\(URL_Login)UserName=\(String(describing: txtMobile.text!))&Password=\(String(describing: txtPassword.text!))"
            print(loginURL)
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)

            WebService.callAPIBYGET(parameter: NSDictionary(), url: loginURL, OnResultBlock: { (responce, status) in
                //print(responce)
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        if self.btnRemember.currentImage == #imageLiteral(resourceName: "uncheck"){
                            nsud.set("False", forKey: "loginRemberstatus")
                        }else{
                            nsud.set("True", forKey: "loginRemberstatus")
                        }
                        nsud.synchronize()
                        deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalDictionary(strEntity: "LoginData", strKey: "login", data: (dict).mutableCopy() as! NSMutableDictionary)
                        
                        FTIndicator.showToastMessage(AlertLogin)
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                    }
                }else{
                    self.showToast(message: alertSomeError)
                }
            })
        }
    }
    
    
    func call_CountryCode_API(sender: UIButton)  {
        if !(isInternetAvailable()){
           FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
           
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetCountryList, OnResultBlock: { (responce, status) in
                //print(responce)
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryCountryList = NSMutableArray()
                        self.aryCountryList = (dict.value(forKey: "CountryList")as! NSArray).mutableCopy()as! NSMutableArray
                        DispatchQueue.main.async {
                            self.gotoPopView(sender: UIButton(), aryData: self.aryCountryList)
                        }
                        
                    }else{
                        self.showToast(message: "\(dict.value(forKey: "Success")as! String)")
                    }
                }else{
                    self.showToast(message: alertSomeError)
                }
            })
        }
    }
    
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CommonTableVC = storyBoard.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            vc.aryTBL = aryData
            vc.strTag = 555
            self.present(vc, animated: false, completion: {})
        }
    }
}


extension LOGINVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMobile  {
            return  txtFiledValidation(textField: txtMobile, string: string, returnOnly: "NUMBER", limitValue: 9)
        }else{
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 60)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
