//
//  QRCodeHistoryOrderVC.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 6/7/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import UIKit

class QRCodeHistoryOrderVC: UIViewController {
    
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    
    var aryList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.call_GetQRCodeOrder_API()
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension QRCodeHistoryOrderVC {
    func call_GetQRCodeOrder_API(){
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            var strUserId = ""
            if (loginDict.count != 0){
                let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                strUserId = "\(dict.value(forKey: "User_Id")!)"
            }
            let url = URL_GetQRCodeOrderByUserId + strUserId
            WebService.callAPIBYGET(parameter: NSDictionary(), url: url, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryList = NSMutableArray()
                        self.aryList = (dict.value(forKey: "QRCodeOrderTableDc")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tblView.reloadData()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
}

extension QRCodeHistoryOrderVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderHistoryQrCodeCell", for: indexPath) as? orderHistoryQrCodeCell
        let dict = removeNullFromDict(dict: (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        cell?.backgroundVIew.layer.cornerRadius = 10
        cell?.backgroundVIew.layer.borderWidth = 0.5
        cell?.lblGiftedBy.text = "\(dict.value(forKey: "GiftedBy") ?? "")"
        cell?.lblTag.text = "\(dict.value(forKey: "Tag") ?? "")"
        cell?.lblTransactionId.text = "\(dict.value(forKey: "PayTM_TranjectionId") ?? "")"
        cell?.lblPaidVia.text = "\(dict.value(forKey: "GatewayName") ?? "")"
        cell?.lblAmount.text = "₹ \(dict.value(forKey: "Total_Amount") ?? "")"
        cell?.lblDate.text = "\(dict.value(forKey: "TranjectionDate") ?? "")"
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 198
    }
    
}

class orderHistoryQrCodeCell : UITableViewCell {
    @IBOutlet weak var lblGiftedBy: UILabel!
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var lblTransactionId: UILabel!
    @IBOutlet weak var lblPaidVia: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var backgroundVIew: UIView!
}
