//
//  CreatFeedBackVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class CreatFeedBackVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var txtFeedBack: KMPlaceholderTextView!
    @IBOutlet weak var txtTreeName: SkyFloatingLabelTextField!
     var ary_of_TreeName = NSMutableArray()
    var dictQRdetail = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        txtFeedBack.layer.borderColor = UIColor.darkGray.cgColor
        txtFeedBack.delegate = self
        lblCount.text =  "0/250"
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 8.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(txtTreeName.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Name, viewcontrol: self)
        } else if(txtFeedBack.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Feedback, viewcontrol: self)
        }else{
            callAddFeedbackSuggesionAPI(sender: sender)
        }
    }
   
    @IBAction func actionOnselectTree(_ sender: UIButton) {
        call_AboutTreeList_API()
    }
    //MARK:- --------------getAbouttreeDataFromLocal
    //MARK:
   
    
    func getAbouttreeDataFromLocal()   {
        let aryTemp = getDataFromLocal(strEntity: "AboutTree", strkey: "abouttree")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "abouttree") ?? 0)
            }
        }
        print(aryList)
        if aryList.count != 0 {
            self.ary_of_TreeName = NSMutableArray()
            self.ary_of_TreeName = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
            let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
            vc.strTitle = "Select  Tree Name"
            vc.strTag = 11
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handleTreeQRDetailView = self
            vc.aryTBL =  self.ary_of_TreeName
            self.present(vc, animated: false, completion: {})
        }
        
    }
    //MARK:- --------------- API CAlling
    //MARK:
    func call_AboutTreeList_API()  {
        if !(isInternetAvailable()){
            getAbouttreeDataFromLocal()
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetPlantList, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"AboutTree")
                        saveDataInLocalArray(strEntity: "AboutTree", strKey: "abouttree", data: (dict.value(forKey: "PlantList")as! NSArray).mutableCopy() as! NSMutableArray)
                        self.getAbouttreeDataFromLocal()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            //     }
        }
    }
    func callAddFeedbackSuggesionAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let dictData = NSMutableDictionary()
             dictData.setValue("\(dictQRdetail.value(forKey: "Donated_Plant_Id")!)", forKey: "Donated_Plant_Id")
            dictData.setValue("\(txtTreeName.tag)", forKey: "Plant_Id")
            dictData.setValue("\(txtFeedBack.text!)", forKey: "Suggestion")

            WebService.callAPIBYGET(parameter: dictData, url: URL_AddSuggestion, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title:alertInfo, message: "\(dict.value(forKey: "Success")!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            //     }
        }
    }
}
extension CreatFeedBackVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        lblCount.text =  "\(newText.count)/250"
        return newText.count < 250
    }

}
//MARK: ---------------Protocol
// MARK: -
protocol refreshQRDetailScreen : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}
extension CreatFeedBackVC: refreshQRDetailScreen {
    func refreshview(dictData: NSDictionary, tag: Int) {
        txtTreeName.text = dictData.value(forKey: "Plant_Name")as? String
        txtTreeName.tag = Int ("\(dictData.value(forKey: "Plant_Id")!)")!
        
    }
}
