//
//  AssociatedPartnerVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/17/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class AssociatedPartnerVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblError: UILabel!

    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var refresher = UIRefreshControl()
    
    
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        call_AssociatedPatnerList_API(strLoaderTag: 1)

    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
       call_AssociatedPatnerList_API(strLoaderTag: 0)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }

    
    
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_AssociatedPatnerList_API(strLoaderTag : Int)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            lblError.text = alertInternet
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil
            
                WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_AssociatedPatner, OnResultBlock: { (responce, status) in
                    print(responce)
                    loading.endLoading()
                    self.refresher.endRefreshing()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                          self.aryForListData = NSMutableArray()
                            self.aryList = NSMutableArray()
                            self.aryList = (dict.value(forKey: "AssociatePartnersDc")as! NSArray).mutableCopy()as! NSMutableArray
                            self.aryForListData = (dict.value(forKey: "AssociatePartnersDc")as! NSArray).mutableCopy()as! NSMutableArray
                            self.tvlist.reloadData()
                            if(self.aryForListData.count == 0){
                                self.lblError.text = alertDataNotFound
                                self.tvlist.reloadData()
                            }
                        }else{
                             self.lblError.text = alertDataNotFound
                            self.tvlist.reloadData()
                        }
                    }else{
                          self.lblError.text = alertSomeError
                       
                        self.tvlist.reloadData()
                    }
                })
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AssociatedPartnerVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tvlist.dequeueReusableCell(withIdentifier: "AssociatedCell", for: indexPath as IndexPath) as! AssociatedCell
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            cell.imgVender.layer.cornerRadius =  12
            cell.imgVender.layer.borderWidth  = 1.0
            cell.imgVender.layer.masksToBounds = false
            cell.imgVender.clipsToBounds = true
             cell.imgVender.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            let strUrl = "\(dict.value(forKey: "Logo")!)"
            let strUrlwithString = strUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        cell.imgVender.setImageWith(URL(string: "\(BaseURLVendorImageDownLoad)\(strUrlwithString!)"), placeholderImage: UIImage(named: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            cell.lblName.text = "\(dict.value(forKey: "Associate_Partner")!)"
            cell.lblCategory.text = "Category: \(dict.value(forKey: "Category")!)"
            cell.lblDiscount.text = "Discount: \(dict.value(forKey: "DiscountPercent")!)%".replacingOccurrences(of: "%%", with: "%")
            cell.btnContactNumber.setTitle("\(dict.value(forKey: "ContactNo")!)", for: .normal)
            
            let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "ValidityFrom")!)", formet: "")
            let endDate = dateTimeConvertor(str: "\(dict.value(forKey: "ValidityTo")!)", formet: "")
            cell.lblValidity.text = "Validity: \(strDate) To \(endDate)"
            cell.btnContactNumber.tag = indexPath.row
            cell.btnContactNumber.addTarget(self, action: #selector(actionForCall), for: .touchUpInside)
        
        
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
 
    @objc func actionForCall(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        if callingFunction(number: "\(dict.value(forKey: "ContactNo")!)" as NSString){
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
        
    }
  
}



// MARK: - ----------------UISearchBarDelegate
// MARK: -

extension  AssociatedPartnerVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "Associate_Partner contains[c] %@ OR Category contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        
    }
}
// MARK: - ----------------Associated Cell
// MARK: -
class AssociatedCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var btnContactNumber: UIButton!
    @IBOutlet weak var imgVender: UIImageView!
    @IBOutlet weak var lblValidity: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
