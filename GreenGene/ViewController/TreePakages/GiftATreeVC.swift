//
//  GiftATreeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/26/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class GiftATreeVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    
    //GiftINFO
    @IBOutlet weak var tvForGitedInfo: UITableView!
    @IBOutlet weak var imgGiftedTo: UIImageView!

    @IBOutlet weak var giftedTo_txtNameTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var giftedTo_txtFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var giftedTo_txtDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var imgGiftedFrom: UIImageView!
    @IBOutlet weak var giftedFrom_txtNameTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var giftedFrom_txtFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var giftedFrom_txtDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var btnBuyNow: UIButton!
    
    var arySeletedData = NSMutableArray()
    var aryQRCodeData = NSMutableArray()
    var aryTbl = NSMutableArray()
    var amountOfPerItem = Double()
    var imagePicker = UIImagePickerController()
    var strGiftedToImageName = ""
    var strGiftedFromImageName = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        FilterData(ary: self.aryQRCodeData)
        lblTitle.text = "Tree Pakages"
        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        btnBuyNow.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        btnContinue.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        
        tvList.tableFooterView = UIView()
        tvForGitedInfo.tableFooterView = UIView()
        lblTotalAmount.textColor = hexStringToUIColor(hex:colorGreenPrimary)
        
        //Tree Pakage
        lbl1.text = "\u{2022} \(giftATree1)"
        lbl2.text = "\u{2022} \(giftATree2)"
        lbl3.text = "\u{2022} \(giftATree3)"
        lbl4.text = "\u{2022} \(giftATree4)"
        lblTotalAmount.text = "\u{20B9} \(amountOfPerItem)"
        tvList.reloadData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        segment.selectedSegmentIndex = 1;
        imgGiftedTo.layer.cornerRadius = 45.0
        imgGiftedFrom.layer.cornerRadius = 45.0

    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        if(lblTitle.text == "Gifted Information"){
                lblTitle.text = "Tree Pakages"
            tvForGitedInfo.removeFromSuperview()
        }else{
        
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func actiononSteeper(_ sender: UIStepper) {
        lblCount.text = "\(Int(sender.value))"
        lblTotalAmount.text = "\u{20B9} \(calculateAmount(countQrCode: Int(sender.value)))"
    }
    
    @IBAction func actionOnSegment(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 1:
            break
        case 0:
            self.navigationController?.popViewController(animated: false)
            
        default:
            break
        }
        
    }
    @IBAction func actionOnContinue(_ sender: UIButton) {
        
        tvForGitedInfo.frame = CGRect(x: 0, y: Int(viewHeader.frame.maxY), width: Int(self.view.frame.width), height: Int(self.view.frame.height) - Int(viewHeader.frame.maxY))
        self.view.addSubview(tvForGitedInfo)
        lblTitle.text = "Gifted Information"
        
    }
    @IBAction func actionOnBuyNow(_ sender: UIButton) {
        
        if(giftedTo_txtFullName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Gifted to " + alertSignUPFullName, viewcontrol: self)

        }else if(giftedFrom_txtFullName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Gifted from " + alertSignUPFullName, viewcontrol: self)

        }else{
            if(self.amountOfPerItem != 0.0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "BuyQRCodeDetailVC")as! BuyQRCodeDetailVC
                let dictGiftData = NSMutableDictionary()
                dictGiftData.setValue(self.giftedTo_txtNameTitle.text! + self.giftedTo_txtFullName.text!, forKey: "giftTo_Name")
                dictGiftData.setValue(self.strGiftedToImageName, forKey: "giftTo_ImageName")
                dictGiftData.setValue(self.imgGiftedTo.image, forKey: "giftTo_Image")
                dictGiftData.setValue(self.giftedTo_txtDesignation.text, forKey: "giftTo_Designation")
                
                dictGiftData.setValue(self.giftedFrom_txtNameTitle.text! + self.giftedFrom_txtFullName.text!, forKey: "giftFrom_Name")
                dictGiftData.setValue(self.strGiftedFromImageName, forKey: "giftFrom_ImageName")
                dictGiftData.setValue(self.imgGiftedFrom.image, forKey: "giftFrom_Image")
                dictGiftData.setValue(self.giftedFrom_txtDesignation.text, forKey: "giftFrom_Designation")
                testController.dictGiftData = dictGiftData

                let dict = self.aryQRCodeData.object(at: 0)as! NSDictionary
                let dictData = NSMutableDictionary()
                dictData.setValue(self.arySeletedData, forKey: "ExtraPay")
                dictData.setValue("\(self.lblTotalAmount.text!)", forKey: "TotalAmount")
                dictData.setValue("\(Int(self.stepper.value))", forKey: "NumberOfQRCode")
                dictData.setValue(Double(amountOfPerItem), forKey: "AmountPerQRCode")
                dictData.setValue("\(dict.value(forKey: "QRCodePlanId")!)", forKey: "QRCodePlanId")
                testController.dictFullData = dictData
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }
    // MARK: - ---------------IBAction (GiftedInfo)
    // MARK: -
    
    @IBAction func actionEditImageGiftedTo(_ sender: Any) {
       OpenImagePicker(tag: 1)
    }
    
    
    @IBAction func actionNameTitleGiftedTo(_ sender: Any) {
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        vc.strTitle = "Select"
        vc.strViewComeFrom = "Picker"
        
        vc.strTag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        vc.aryTBL = (pickerData as NSArray).mutableCopy()as! NSMutableArray
        self.present(vc, animated: false, completion: {})
    }
    
    @IBAction func actionEditImageGiftedFrom(_ sender: Any) {
        OpenImagePicker(tag: 2)

    }
    
    
    @IBAction func actionNameTitleGiftedFrom(_ sender: Any) {
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        vc.strTitle = "Select"
        vc.strTag = 2
        vc.strViewComeFrom = "Picker"
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        vc.aryTBL = (pickerData as NSArray).mutableCopy()as! NSMutableArray
        self.present(vc, animated: false, completion: {})
    }
    
    
   
    
    func FilterData(ary: NSMutableArray) {
        self.aryTbl = NSMutableArray()
        self.arySeletedData = NSMutableArray()
     
        if(ary.count >= 2){
            let dict = ary.object(at: 1)as! NSDictionary
            amountOfPerItem = Double(Int("\(dict.value(forKey: "Price")!)")!)
            self.aryTbl = (dict.value(forKey: "TagPriceMasterDc")as! NSArray).mutableCopy()as! NSMutableArray
        }
        self.tvList.reloadData()
        lblTotalAmount.text = "\u{20B9} \(calculateAmount(countQrCode: Int(lblCount.text!)!))"
        
    }
    // MARK: - ---------------Other Function's
    // MARK: -
    
    func OpenImagePicker(tag : Int)  {
        imagePicker.view.tag = tag
        imagePicker.delegate = self
        let alert = UIAlertController(title: alertGallery, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
//            alert.popoverPresentationController?.sourceView = sender
//            alert.popoverPresentationController?.sourceRect = sender .bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            showToastForSomeTime(title: alertMessage, message: "You don't have camera", time: 3, viewcontrol: self)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func calculateAmount(countQrCode : Int) -> Double {
        let amountonlyQRcode = Int(lblCount.text!)!*Int(amountOfPerItem)
        var amountExtra = 0.0
        for item in arySeletedData {
            let dict = (item as AnyObject)as! NSDictionary
            amountExtra = amountExtra +  Double(Int("\(dict.value(forKey: "Price")!)")!)
        }
        let amountExtraFinal = amountExtra * Double(Int(lblCount.text!)!)
        return amountExtraFinal + Double(amountonlyQRcode)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension GiftATreeVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "GiftQRCodeCell", for: indexPath as IndexPath) as! GiftQRCodeCell
        let dict = aryTbl.object(at: indexPath.row)as! NSDictionary
        cell.lblAmount.text = "\u{20B9} \(dict.value(forKey: "Price")!)"
        cell.lblTitle.text = "\(dict.value(forKey: "TagTitle")!)"
        
        if(arySeletedData.contains(dict)){
            cell.imgCheck.image = UIImage(named: "check")
        }else{
            cell.imgCheck.image = UIImage(named: "uncheck")
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryTbl.object(at: indexPath.row)as! NSDictionary
        if(arySeletedData.contains(dict)){
            arySeletedData.remove(dict)
        }else{
            arySeletedData.add(dict)
        }
        lblTotalAmount.text = "\u{20B9} \(calculateAmount(countQrCode: Int(lblCount.text!)!))"
        
        self.tvList.reloadData()
    }
    
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class GiftQRCodeCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension GiftATreeVC: CommonTableDelegate {
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
        
    }
    
    
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        if(tag == 1){
            giftedTo_txtNameTitle.text = strTitle
        }else if (tag == 2){
            giftedFrom_txtNameTitle.text = strTitle
            
        }
    }
    
}
//MARK:-
//MARK:- ---------UIImagePickerControllerDelegate-----------

extension GiftATreeVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            if(imagePicker.view.tag == 1){
                self.imgGiftedTo.image = pickedImage
                self.strGiftedToImageName = getUniqueString()
            }else if (imagePicker.view.tag == 2){
                self.imgGiftedFrom.image = pickedImage
                self.strGiftedFromImageName = getUniqueString()
            }
            
            dismiss(animated: true, completion: nil)
        }
    }
}
