//
//  GeneratedQRCodeDetailVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/24/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class BuyQRCodeDetailVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var tvlist: UITableView!
    var promocodeStatus = false

    var dictFullData = NSMutableDictionary()

    
    var arySelectedExtraPay = NSMutableArray()
    var dictGiftData = NSMutableDictionary()

    var numberOfQrCode = String()
    var totalAmount = String()
    var amountOfPerItem = Double()
    var QRCodePlanId = String()
    
    var promoCOde = ""

    // MARK: - ---------------IBAction
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        btnPayNow.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        SetUPDataOnScreen()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnPayNow.isUserInteractionEnabled = true
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnPayNow(_ sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
       
        var tagType = ""
        if(dictGiftData.count == 0){
            tagType = "GIFT A TREE"
        }else{
           tagType = "GeneratedQRCode"
        }
        
        if("\(self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: ""))" == "0.0"){
            
            let temDict = NSMutableDictionary()
            temDict.setValue("0.0", forKey: "TXNAMOUNT")
            temDict.setValue("", forKey: "STATUS")
            temDict.setValue("", forKey: "TXNID")
            temDict.setValue("", forKey: "ORDERID")
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
            let date = Date()
            let dateString = dateFormatter.string(from: date)
            temDict.setValue(dateString, forKey: "TXNDATE")
            temDict.setValue("", forKey: "BANKNAME")
            temDict.setValue("", forKey: "GATEWAYNAME")
            temDict.setValue("Coupon", forKey: "PAYMENTMODE")
            temDict.setValue("", forKey: "CURRENCY")
            temDict.setValue("", forKey: "CHECKSUMHASH")
            call_SavePaymentDetailOnServer(dictPaymentData: temDict)
            
        }else{
            createCheckSumHash(strAmount: "\(self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: ""))", tagForPayType: tagType)
        }
    }
    
   
    // MARK: - ---------------Extra Function
    // MARK: -
    
    func SetUPDataOnScreen(){
        self.arySelectedExtraPay = NSMutableArray()
        self.arySelectedExtraPay = (dictFullData.value(forKey: "ExtraPay")as! NSArray).mutableCopy()as! NSMutableArray
        self.totalAmount = "\(dictFullData.value(forKey: "TotalAmount")!)"
        self.numberOfQrCode = "\(dictFullData.value(forKey: "NumberOfQRCode")!)"
        self.amountOfPerItem = dictFullData.value(forKey: "AmountPerQRCode")as! Double
        self.QRCodePlanId = "\(dictFullData.value(forKey: "QRCodePlanId")!)"
        
        let dict = NSMutableDictionary()
        dict.setValue("Number of QR Code", forKey: "TagTitle")
        dict.setValue("\(Int(self.amountOfPerItem))", forKey: "Price")
        if (arySelectedExtraPay.contains(dict)){
            arySelectedExtraPay.removeObject(at: 0)
        }
        arySelectedExtraPay.insert(dict, at: 0)
        self.tvlist.reloadData()
        self.promocodeStatus = false
    }
    
    func promoCodeCalculation(amountPromocode : Int) {
        
        let discountPrice = amountPromocode * Int(self.numberOfQrCode)!
        
        let dict = NSMutableDictionary()
        dict.setValue("Promocode Discount", forKey: "TagTitle")
        dict.setValue("\(amountPromocode)", forKey: "Price")
        self.arySelectedExtraPay.add(dict)
        self.totalAmount =  "\(Double(self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: ""))! - Double(discountPrice))"
        if (arySelectedExtraPay.contains(dict)){
            arySelectedExtraPay.remove(dict)
        }
        arySelectedExtraPay.add(dict)
        promocodeStatus = true
        self.tvlist.reloadData()
    }
    
    
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_SavePaymentDetailOnServer(dictPaymentData : NSMutableDictionary)  {
        self.view.endEditing(true)
   
            var strEmail = ""
            var strAddress = ""
            var strUserID = ""

            if (loginDict.count != 0){
                let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                strEmail = "\(dict.value(forKey: "Email_Id")!)"
                let mobile = "\(dict.value(forKey: "Mobile_Number")!)"
                let address = "\(dict.value(forKey: "Address")!)"
                let state = "\(dict.value(forKey: "StateName")!)"
                let city = "\(dict.value(forKey: "CityName")!)"
                let zipCode = "\(dict.value(forKey: "Zip_Code")!)"
                strAddress = "\(address),\(city),\(state)\nPincode: \(zipCode)\nEmail: \(strEmail)\nMobile: \(mobile)"
                strUserID = "\(dict.value(forKey: "User_Id")!)"
            }
            
            
            var dictSendData = NSMutableDictionary()
            dictSendData.setValue(strUserID, forKey: "User_Id")
            dictSendData.setValue(numberOfQrCode, forKey: "No_Of_QRCode")
            dictSendData.setValue(QRCodePlanId, forKey: "QRCodePlanId")

            
            dictSendData.setValue("\(dictPaymentData.value(forKey: "TXNAMOUNT")!)", forKey: "Total_Amount")
            dictSendData.setValue(strAddress, forKey: "ShippingAddress")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "STATUS")!)", forKey: "Status")
            dictSendData.setValue(strEmail, forKey: "EmailId")
       
            dictSendData.setValue("\(dictPaymentData.value(forKey: "TXNID")!)", forKey: "PayTM_TranjectionId")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "ORDERID")!)", forKey: "OrderId")
           
            let date = dateStringToFormatedDateString(dateToConvert: "\(dictPaymentData.value(forKey: "TXNDATE")!)", dateFormat: "MM/dd/yyyy")
            
            dictSendData.setValue(date, forKey: "TranjectionDate")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "BANKNAME")!)", forKey: "BankName")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "GATEWAYNAME")!)", forKey: "GatewayName")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "PAYMENTMODE")!)", forKey: "PaymentMode")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "CURRENCY")!)", forKey: "Currency")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "CHECKSUMHASH")!)", forKey: "CheckSum")
            
            let aryQRCodeOrder = NSMutableArray()
            for item in self.arySelectedExtraPay{
                let dictItem = NSMutableDictionary()
                if((item as AnyObject).value(forKey: "Tag_Price_Id") != nil){
                    dictItem.setValue((item as AnyObject).value(forKey: "Tag_Price_Id"), forKey: "Tag_Price_Id")
                    aryQRCodeOrder.add(dictItem)
                }
                
             
            }
            dictSendData.setValue(aryQRCodeOrder, forKey: "QRCodeOrderOptionMobileDc")
        
        //----GiftTree
        dictSendData.setValue("", forKey: "GiftedBy")
        dictSendData.setValue("", forKey: "GiftedByDesignation")
        dictSendData.setValue("", forKey: "GiftedByImage")
        dictSendData.setValue("", forKey: "GiftedTo")
        dictSendData.setValue("", forKey: "GiftedToDesignation")
        dictSendData.setValue("", forKey: "GiftedToImage")
      
        let aryTempImages = NSMutableArray()
        var dict = NSMutableDictionary()
        
        if self.dictGiftData.count !=  0{
            // Images
            
            let to_imageName = "\(dictGiftData.value(forKey: "giftTo_ImageName")!)"
            let to_image = dictGiftData.value(forKey: "giftTo_Image")as! UIImage
            let to_Name = "\(dictGiftData.value(forKey: "giftTo_Name")!)"
            let to_Designation = "\(dictGiftData.value(forKey: "giftTo_Designation")!)"
          
            let from_imageName = "\(dictGiftData.value(forKey: "giftFrom_ImageName")!)"
            let from_image = dictGiftData.value(forKey: "giftFrom_Image")as! UIImage
            let from_Name = "\(dictGiftData.value(forKey: "giftFrom_Name")!)"
            let from_Designation = "\(dictGiftData.value(forKey: "giftFrom_Designation")!)"
            
            if(to_imageName.count != 0){
                dict = NSMutableDictionary()
                dict.setValue(to_image, forKey: "image")
                dict.setValue(to_imageName, forKey: "imageName")
                aryTempImages.add(dict)
            }
            if(from_imageName.count != 0){
                dict = NSMutableDictionary()
                dict.setValue(from_image, forKey: "image")
                dict.setValue(from_imageName, forKey: "imageName")
                aryTempImages.add(dict)
            }
                        
            dictSendData.setValue(from_Name, forKey: "GiftedBy")
            dictSendData.setValue(from_Designation, forKey: "GiftedByDesignation")
            dictSendData.setValue(from_imageName, forKey: "GiftedByImage")
         
            dictSendData.setValue(to_Name, forKey: "GiftedTo")
            dictSendData.setValue(to_Designation, forKey: "GiftedToDesignation")
            dictSendData.setValue(to_imageName, forKey: "GiftedToImage")
        }
        
        var json1 = Data()
        var jsonAdd = NSString()
        
        if JSONSerialization.isValidJSONObject(dictSendData) {
            // Serialize the dictionary
            json1 = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
            jsonAdd = String(data: json1, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonAdd)")
        }
            if(dictSendData.count == 0){
                jsonAdd = ""
            }
           dictSendData = NSMutableDictionary()
           dictSendData.setValue(jsonAdd, forKey: "QRCodeOrderTableMobileDc")

        // For Promocode and Value is 00
        if("\(self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: ""))" == "0.0"){
            CallOrderQRCodeAPI(sender: btnPayNow, dictData: dictSendData, aryImages: aryTempImages, strAPIName: URL_AddQRCodeOrder)
        }
        // For Payment
        else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentProcessVC")as! PaymentProcessVC
            testController.dictData = dictSendData
            testController.strAPIName = URL_AddQRCodeOrder
            testController.aryImages = aryTempImages
            self.navigationController?.pushViewController(testController, animated: false)
        }
        
        
        
        }

    func call_ApplyPromoCode_API(CoupenCode : String)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
            
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let api = URL_GetOrganizationCoupenCode + "User_Id=\(dict.value(forKey: "User_Id")!)&CoupenCode=\(CoupenCode)"
            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: api, OnResultBlock: { (responce, status) in
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                      
                        if(Int(self.numberOfQrCode)! > Int("\(dict.value(forKey: "CoupenLimit")!)")!){
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PromoCodeLimitMessage + "\(dict.value(forKey: "CoupenLimit")!) QR Code.", viewcontrol: self)
                        }else{
                            self.promoCodeCalculation(amountPromocode: Int("\(dict.value(forKey: "Success")!)")!)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: dict.value(forKey: "Success")as! String, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            })
        }
    }
    
    
    func CallOrderQRCodeAPI(sender : UIButton , dictData : NSMutableDictionary , aryImages : NSMutableArray ,strAPIName : String ) {
        
        customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)

        WebService.callAPIWithMultiImage(parameter: dictData, url: strAPIName, image: aryImages, fileName: "image", OnResultBlock: { (responce, status) in
            print(responce)
            customeDotLoaderRemove()
            if (status == "success"){
                if (responce.value(forKey: "Result")as! String == "True"){
                    
                    let alert = UIAlertController(title: alertMessage, message: "\(responce.value(forKey: "Success")!)", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: DashBoardVC.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated:false)
                                break
                            }
                        }
                    }))
                    self.present(alert, animated: true)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(responce.value(forKey: "Success")!)", viewcontrol: self)

                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

            }
        })
    }
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension BuyQRCodeDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return section == 3 ? arySelectedExtraPay.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Address", for: indexPath as IndexPath) as! AddressCell
            loginDict = getLoginData()
            if (loginDict.count != 0){
                let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                let name = "\(dict.value(forKey: "Name")!)"
                let email = "\(dict.value(forKey: "Email_Id")!)"
                let mobile = "\(dict.value(forKey: "Mobile_Number")!)"
                let address = "\(dict.value(forKey: "Address")!)"
                let state = "\(dict.value(forKey: "StateName")!)"
                let city = "\(dict.value(forKey: "CityName")!)"
                let zipCode = "\(dict.value(forKey: "Zip_Code")!)"
                cell.lblName.text = name
                cell.lblAddress.text = "\(address),\(city),\(state)\nPincode: \(zipCode)\nEmail: \(email)\nMobile: \(mobile)"
            }
            return cell
        }else if(indexPath.section == 1){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Paymentdetail", for: indexPath as IndexPath) as! AddressCell
            cell.lblnumberOfProduct.text = "\(numberOfQrCode)"
            if(dictGiftData.count != 0){
                let strGiftedBy = "\(dictGiftData.value(forKey: "giftFrom_Name")!)"
                let strGiftedByDesignation = "\(dictGiftData.value(forKey: "giftFrom_Designation")!)"
                let strGiftedTo = "\(dictGiftData.value(forKey: "giftTo_Name")!)"
                let strGiftedToDesignation = "\(dictGiftData.value(forKey: "giftTo_Designation")!)"
                cell.lblGiftDetail.text = "Gifted To - \(strGiftedTo),\(strGiftedToDesignation)\nGifted From - \(strGiftedBy),\(strGiftedByDesignation)"
            }else{
                  cell.lblGiftDetail.text = ""
            }
            return cell
        }
 //       else if(indexPath.section == 2){
//            let cell = tvlist.dequeueReusableCell(withIdentifier: "PromoCodeCell", for: indexPath as IndexPath) as! AddressCell
//            cell.txtPromocode.delegate = self
//            cell.btnApply.layer.cornerRadius = 5.0
//            cell.btnApply.tag = indexPath.row
//            cell.btnApply.backgroundColor = hexStringToUIColor(hex: colorOrangPrimary)
//            cell.btnApply.addTarget(self, action: #selector(actionOnApplyButton), for: .touchUpInside)
//            if(promocodeStatus){
//               cell.btnApply.setTitle("Remove", for: .normal)
//                cell.txtPromocode.isUserInteractionEnabled = false
//                cell.txtPromocode.alpha = 0.8
//                cell.txtPromocode.placeholder = "Applied Successfully"
//                cell.txtPromocode.titleColor = hexStringToUIColor(hex: colorGreenPrimary)
//            }else{
//                cell.btnApply.setTitle("Apply", for: .normal)
//                cell.txtPromocode.isUserInteractionEnabled = true
//                cell.txtPromocode.text = ""
//                cell.txtPromocode.alpha = 1.0
//                cell.txtPromocode.placeholder = "Promo Code"
//
//            }
//            return cell
 //       }
        else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "extraCell", for: indexPath as IndexPath) as! extraCell
            let dict = arySelectedExtraPay.object(at: indexPath.row)as! NSDictionary
            let amount = Double("\(dict.value(forKey: "Price")!)")! * Double(numberOfQrCode)!
            if("\(dict.value(forKey: "TagTitle")!)" == "Promocode Discount"){
                cell.lblExtraTitle.text = "\(dict.value(forKey: "TagTitle")!)"
                cell.lblAmount.text = "-\u{20B9} \(amount)"
            }else{
                cell.lblExtraTitle.text = "\(dict.value(forKey: "TagTitle")!)(\(dict.value(forKey: "Price")!)*\(numberOfQrCode))"
                cell.lblAmount.text = "\u{20B9} \(amount)"
            }
            
            if self.promoCOde == "" {
                cell.lblPromoCode.text = "\u{20B9} 0.0"
            }else{
                cell.lblPromoCode.text = "\u{20B9} \((self.promoCOde.replacingOccurrences(of: "\u{20B9} ", with: ""))).0"
            }
           // cell.lblPromoCode.text = "\u{20B9} \((self.promoCOde.replacingOccurrences(of: "\u{20B9} ", with: ""))).0"
            
            let promocode = Int(self.promoCOde)
            let totalAmt = Int(self.totalAmount)
            
            let remainingAmt: Int = (Int(totalAmount) ?? 0) - (Int(promoCOde) ?? 0)
            
            //cell.lblTotalAmount.text = "\u{20B9} \((self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: "")))"
            
            self.totalAmount =  "\(Double(self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: ""))! - (Double(promoCOde) ?? 0.0))"
            
            cell.lblTotalAmount.text = "\u{20B9} \((self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: "")))"
            return cell
        }
       
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//        if(section == 0){
//            return "Billing Address"
//        }else if(section == 1){
//            return "QR Code Detail"
//        }else if(section == 2){
//            return "Promo Code"
//        }else if(section == 3){
//            return "Payment Detail"
//        }
//        return ""
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section != 3 ? 0.0 : 50.0
    }
    
//    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
//        return section == 0 ? "" : "Total Amount : \u{20B9} \((self.totalAmount.replacingOccurrences(of: "\u{20B9} ", with: "")))"
//    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.textAlignment = .right
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tvlist {
            
            // HEADER VIEW
            let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  50))
            viewheader.backgroundColor = UIColor.white
            
            // INSIDE LABEL
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height:50))
            lbl.backgroundColor = #colorLiteral(red: 0.8202493925, green: 0.8202493925, blue: 0.8202493925, alpha: 1)
            lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            lbl.font = UIFont.boldSystemFont(ofSize: 18)
            
            // HEADER BOTTOM LINE
            let bottomBorder = CALayer()
            bottomBorder.frame = CGRect(x: 0.0, y: 43.0, width: viewheader.frame.size.width, height:  1.5)
           
            
            if tableView == tvlist{
                if(section == 0){
                    lbl.text = "  Billing Address"
                }
                if(section == 1){
                    lbl.text = "  QR Code Detail"
                }
//                if(section == 2){
//                    lbl.text = "  Promo Code"
//                }
                if(section == 2){
                    lbl.text = "  Payment Detail"
                }
                viewheader.addSubview(lbl)
                return viewheader
            }
            else
            {
                return viewheader
            }
        }else {
            return UIView()
        }
    }
    
    @objc func actionOnApplyButton(sender : UIButton) {
        
        if(sender.titleLabel?.text == "Remove"){
            self.arySelectedExtraPay.removeLastObject()
            self.SetUPDataOnScreen()
        }else{
            let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tvlist)
            let indexPath = self.tvlist.indexPathForRow(at: buttonPosition)
            let cell = self.tvlist.cellForRow(at: indexPath!)as! AddressCell
            if(cell.txtPromocode.text?.count != 0){
                call_ApplyPromoCode_API(CoupenCode: cell.txtPromocode.text!)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PromoCode, viewcontrol: self)
            }
            self.view.endEditing(true)
        }
        
      
    }
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class AddressCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblnumberOfProduct: UILabel!
    @IBOutlet weak var lblGiftDetail: UILabel!
    
    @IBOutlet weak var txtPromocode: SkyFloatingLabelTextField!
    @IBOutlet weak var btnApply: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
      
    }
}
class extraCell: UITableViewCell {
   
    @IBOutlet weak var lblExtraTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}


// MARK: - ---------------IBAction
// MARK: -
extension BuyQRCodeDetailVC : PGTransactionDelegate{
    
    
    
    func createCheckSumHash(strAmount : String , tagForPayType : String) {
        let dictLogin = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmSSS"
        let result = formatter.string(from: date)
        let orderIdUnique="Order_iOS_\(dictLogin.value(forKey: "User_Id")!)_\(result)";
        
        let dict = NSMutableDictionary()
        
        
        if("\(dictLogin.value(forKey: "User_Id")!)" == "93"){
            
            dict.setValue("Citize73156154292382", forKey: "MID")
            dict.setValue(orderIdUnique, forKey: "ORDER_ID")
            dict.setValue("\(dictLogin.value(forKey: "User_Id")!)", forKey: "CUST_ID")
            dict.setValue("Retail", forKey: "INDUSTRY_TYPE_ID")
            dict.setValue("WAP", forKey: "CHANNEL_ID")
            dict.setValue(strAmount, forKey: "TXN_AMOUNT")
            dict.setValue("Website", forKey: "WEBSITE")
        dict.setValue("https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIdUnique)", forKey: "CALLBACK_URL")
            
        }else{
            
            
            dict.setValue(MerchantID, forKey: "MID")
            dict.setValue(orderIdUnique, forKey: "ORDER_ID")
            dict.setValue("\(dictLogin.value(forKey: "User_Id")!)", forKey: "CUST_ID")
            dict.setValue("\(IndustryID)", forKey: "INDUSTRY_TYPE_ID")
            dict.setValue("\(ChannelID)", forKey: "CHANNEL_ID")
            if("\(dictLogin.value(forKey: "User_Id")!)" == "1930"){
                dict.setValue("1.00", forKey: "TXN_AMOUNT")
            }else{
                dict.setValue(strAmount, forKey: "TXN_AMOUNT")
            }
            
            dict.setValue("\(Website)", forKey: "WEBSITE")
        dict.setValue("https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIdUnique)", forKey: "CALLBACK_URL")
        }
        
       
        print(dict)
        
        
        WebService.callAPIBYPOST(parameter: dict, url: "\(GENERATE_CHECKSUM)") { (responce, status) in
            
            if(status == "success"){
                let dictData = responce.value(forKey: "data")as! NSDictionary
                let strCheckSum = "\(dictData.value(forKey: "CHECKSUMHASH")!)"
                print(strCheckSum)
                self.createPayment(strAmount: strAmount, checkSumHashh: strCheckSum, orderidNew: orderIdUnique)
            }else{
                let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                   
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func createPayment(strAmount : String , checkSumHashh : String , orderidNew : String){
        var orderDict = [String : String]()
        let dictLoginData = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
        if("\(dictLoginData.value(forKey: "User_Id")!)" == "93"){
        
            
            orderDict["MID"] = "Citize73156154292382";
            orderDict["CHANNEL_ID"] = "WAP";
            orderDict["INDUSTRY_TYPE_ID"] = "Retail";
            orderDict["WEBSITE"] = "Website";
            orderDict["TXN_AMOUNT"] = "\(strAmount)"
            orderDict["ORDER_ID"] = "\(orderidNew)";
            orderDict["CUST_ID"] = "\((String(describing: dictLoginData.value(forKey: "User_Id")!)))";
            orderDict["CHECKSUMHASH"] = "\(checkSumHashh)";
            orderDict["CALLBACK_URL"] = "\(callBackURL)\(orderidNew)"
            
            print(orderDict)
        }else{
            orderDict["MID"] = "\(MerchantID)";
            orderDict["CHANNEL_ID"] = "\(ChannelID)";
            orderDict["INDUSTRY_TYPE_ID"] = "\(IndustryID)";
            orderDict["WEBSITE"] = "\(Website)";
            
            if("\(dictLoginData.value(forKey: "User_Id")!)" == "1930"){
                orderDict["TXN_AMOUNT"] = "1.00"
            }else{
                orderDict["TXN_AMOUNT"] = "\(strAmount)"
            }
            
            orderDict["ORDER_ID"] = "\(orderidNew)";
            orderDict["CUST_ID"] = "\((String(describing: dictLoginData.value(forKey: "User_Id")!)))";
            orderDict["CALLBACK_URL"] = "\(callBackURL)\(orderidNew)"
            orderDict["CHECKSUMHASH"] = "\(checkSumHashh)";
            print(orderDict)
        }
        let pgOrder = PGOrder(params: orderDict )
        let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
        if(type_Production_Staging == "Staging"){
            transaction!.serverType = eServerTypeStaging
        }else{
            if("\(dictLoginData.value(forKey: "User_Id")!)" == "93"){
                transaction!.serverType = eServerTypeStaging
            }else{
                transaction!.serverType = eServerTypeProduction
            }
        }
        transaction!.merchant = PGMerchantConfiguration.default()
        transaction!.loggingEnabled = true
        transaction!.delegate = self
        
        if responds(to: #selector(setter: self.edgesForExtendedLayout)) {
            let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 64.0))
            headerView.backgroundColor = UIColor.clear
            let topBar = UIView(frame: CGRect(x: 0.0, y: 20.0, width: self.view.frame.width, height: 50.0))
            topBar.backgroundColor = UIColor.clear
            headerView.addSubview(topBar)
            if(type_Production_Staging == "Staging"){
                headerView.addSubview(topBar)

            }
            let cancelButton = UIButton(type: .system)
            cancelButton.setTitle("Back", for: .normal)
            cancelButton.frame = CGRect(x: 8.0, y: 25.0, width: 40.0, height: 40.0)
            cancelButton.tintColor = UIColor.lightGray
            transaction?.cancelButton = cancelButton
        }else{
            let topBar = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 50.0))
            topBar.backgroundColor = UIColor.clear
            if(type_Production_Staging == "Staging"){
                transaction?.topBar = topBar
            }
            let cancelButton = UIButton(type: .system)
            cancelButton.frame = CGRect(x: 8.0, y: 5.0, width: 40.0, height: 40.0)
            cancelButton.setTitle("Back", for: .normal)
            cancelButton.tintColor = UIColor.lightGray
            
            transaction?.cancelButton = cancelButton
        }
      self.navigationController?.pushViewController(transaction!, animated: true)
    }

  
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        print(responds)
        
        self.navigationController?.popViewController(animated: true)
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    let status = jsonresponse["STATUS"]as! String
                    if(status == "TXN_SUCCESS"){
                        call_SavePaymentDetailOnServer(dictPaymentData: (jsonresponse as NSDictionary).mutableCopy()as! NSMutableDictionary)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            } catch {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        
        self.navigationController?.popViewController(animated: true)

        
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        controller.view.isUserInteractionEnabled = false
        

        print("error : ",error)
        self.navigationController?.popViewController(animated: true)
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)


    }
    
    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        controller.view.isUserInteractionEnabled = false
        
        print(response)
        self.navigationController?.popViewController(animated: true)

    }
    
    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        controller.view.isUserInteractionEnabled = false
        
        print("error : ",error)
        print("didFailTransaction : ",response)
        self.navigationController?.popViewController(animated: true)
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "FailTransaction", viewcontrol: self)

        }
  
    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        print("error : ",error)
        print("didCancelTransaction : ",response)

    }
    //
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print("didFinishCASTransaction : ",response)
        //  showAlert(controller: controller, title: "cas", message: "")
    }
    
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension BuyQRCodeDetailVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 25)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
