//
//  TreePakagesVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/23/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class BuyQRcodeVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgQRcode: UIImageView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnBuyNow: UIButton!
    @IBOutlet weak var lblQty: UILabel!
    
    @IBOutlet weak var txtCoupon: SkyFloatingLabelTextField!
    
    @IBOutlet weak var heightSegment: NSLayoutConstraint!
    @IBOutlet weak var btnApply: UIButton!
    
    var amtGenerateQRcode = 0
    var amtGiftATree = 0
    var promoCode = ""
    var arySeletedData = NSMutableArray()
    var aryTbl = NSMutableArray()
    var aryGiftData = NSMutableArray()
    var aryQRPlanData = NSMutableArray()

    var refresher = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.heightSegment.constant = 0
        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        btnBuyNow.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        segment.tintColor = hexStringToUIColor(hex:colorGreenPrimary)
       // lblTotalAmount.textColor = hexStringToUIColor(hex:colorGreenPrimary)
        //Tree Pakage
        lbl1.text = "\u{2022} \(TreePakage1)"
        lbl2.text = "\u{2022} \(TreePakage2)"
        lbl3.text = "\u{2022} \(TreePakage3)"
        lbl4.text = "\u{2022} \(TreePakage4)"
        lblNote.text = TreePakageNote
        imgQRcode.layer.cornerRadius = 8.0
        imgQRcode.layer.borderWidth = 1.0
        imgQRcode.layer.borderColor = hexStringToUIColor(hex: colorOrangPrimary).cgColor
        lblTotalAmount.text = "\u{20B9} 0.0"
        lblQty.text = "5"
        lblCount.text = "5"
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvList!.addSubview(refresher)
        lblTotalAmount.text = "\(calculateAmount(countQrCode: 1))"
       // lblTotalAmount.text = "55"
        call_TreePackagesList_API(strLoaderTag: 1)
        segment.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        segment.selectedSegmentIndex = 0;

    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        call_TreePackagesList_API(strLoaderTag: 0)
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
    
    @IBAction func actionOnSegment(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            break
        case 1:
            
            if(self.aryGiftData.count != 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "GiftATreeVC")as! GiftATreeVC
                testController.aryQRCodeData = self.aryQRPlanData
                self.navigationController?.pushViewController(testController, animated: false)
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertGift_a_tree_detail, viewcontrol: self)
                  segment.selectedSegmentIndex = 0;
            }
           
        default:
           break
        }
        
    }
    
    @IBAction func actiononSteeper(_ sender: UIStepper) {
       lblCount.text = "\(Int(sender.value))"
       lblTotalAmount.text = "\u{20B9} \(calculateAmount(countQrCode: Int(sender.value)))"
    }
    
    @IBAction func actionOnMinus(_ sender: UIButton) {
        
        if lblQty.text == "5" {
            
        }else{
            var qty = Int(lblCount.text ?? "")
            qty = qty! - 1
            
            self.lblCount.text = "\(qty ?? 0)"
            self.lblQty.text = "\(qty ?? 0)"
            
            self.lblTotalAmount.text = "\(11 * qty!).0"
        }
    }
    
    @IBAction func actionOnPlus(_ sender: UIButton) {
        var qty = Int(lblCount.text ?? "")
        qty = qty! + 1
        
        self.lblCount.text = "\(qty ?? 0)"
        self.lblQty.text = "\(qty ?? 0)"
        
        self.lblTotalAmount.text = "\(11 * qty!).0"
    }
    
    
    @IBAction func actionOnCoupon(_ sender: UIButton) {
        if(sender.titleLabel?.text == "Remove"){
            self.btnApply.setTitle("APPLY", for: .normal)
            self.txtCoupon.placeholder = "Coupon Code"
            self.txtCoupon.text = ""
            self.promoCode = ""
        }else{
            if(txtCoupon.text?.count != 0){
                call_ApplyPromoCode_API(CoupenCode:txtCoupon.text!)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PromoCode, viewcontrol: self)
            }
            self.view.endEditing(true)
        }
    }
    
    
    func call_ApplyPromoCode_API(CoupenCode : String)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
            
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let api = URL_GetOrganizationCoupenCode + "User_Id=\(dict.value(forKey: "User_Id")!)&CoupenCode=\(CoupenCode)"
            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: api, OnResultBlock: { (responce, status) in
                print(responce)
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        
                        self.btnApply.setTitle("Remove", for: .normal)
                        self.txtCoupon.placeholder = "APPLY SUCCESSFULLY"
                        self.promoCode = "\(dict.value(forKey: "CoupenLimit") ?? "")"
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_PromoCodeLimitMessage + "\(dict.value(forKey: "CoupenLimit")!) QR Code.", viewcontrol: self)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: dict.value(forKey: "Success")as! String, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            })
        }
    }
    
    @IBAction func actionOBuyNow(_ sender: UIStepper) {
        if(self.aryQRPlanData.count != 0){
            
            let dict = self.aryQRPlanData.object(at: 0)as! NSDictionary
            let dictData = NSMutableDictionary()
            dictData.setValue(self.arySeletedData, forKey: "ExtraPay")
            dictData.setValue("\(self.lblTotalAmount.text!)", forKey: "TotalAmount")
            dictData.setValue("\(Int(lblQty.text ?? "") ?? 0)", forKey: "NumberOfQRCode")
            dictData.setValue(Double(amtGenerateQRcode), forKey: "AmountPerQRCode")
            dictData.setValue("\(dict.value(forKey: "QRCodePlanId")!)", forKey: "QRCodePlanId")

            
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "BuyQRCodeDetailVC")as! BuyQRCodeDetailVC
            testController.dictFullData = dictData
            testController.promoCOde = self.promoCode
            self.navigationController?.pushViewController(testController, animated: true)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertGenerateQRCodeDetail, viewcontrol: self)
        }
    }
  
    
    func calculateAmount(countQrCode : Int) -> Double {
        let amountonlyQRcode = Int(lblCount.text!)!*amtGenerateQRcode
        var amountExtra = 0.0
        for item in arySeletedData {
            let dict = (item as AnyObject)as! NSDictionary
            amountExtra = amountExtra +  Double(Int("\(dict.value(forKey: "Price")!)")!)
        }
        let amountExtraFinal = amountExtra * Double(Int(lblCount.text!)!)
        return amountExtraFinal + Double(amountonlyQRcode)
        
    }
    
    func FilterData(ary: NSMutableArray) {
        self.aryTbl = NSMutableArray()
        self.arySeletedData = NSMutableArray()
        self.aryGiftData = NSMutableArray()

        if(ary.count != 0){
            let dict = ary.object(at: 0)as! NSDictionary
            amtGenerateQRcode = Int("\(dict.value(forKey: "Price")!)")!
            self.aryTbl = (dict.value(forKey: "TagPriceMasterDc")as! NSArray).mutableCopy()as! NSMutableArray
        }
        if(ary.count >= 2){
            let dict = ary.object(at: 1)as! NSDictionary
            amtGiftATree = Int("\(dict.value(forKey: "Price")!)")!
            self.aryGiftData = (dict.value(forKey: "TagPriceMasterDc")as! NSArray).mutableCopy()as! NSMutableArray
        }
      
        self.tvList.reloadData()
        lblTotalAmount.text = "\(calculateAmount(countQrCode: Int(lblCount.text!)!))"

    }
    
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_TreePackagesList_API(strLoaderTag : Int)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
          
        }else{
            let loading = DPBasicLoading(table: tvList, fontName: "HelveticaNeue")
            strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil
            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetTagPriceMaster, OnResultBlock: { (responce, status) in
                print(responce)
                loading.endLoading()
                self.refresher.endRefreshing()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryQRPlanData = NSMutableArray()
                        self.aryQRPlanData = (dict.value(forKey: "QRCodePlanMasterDc")as! NSArray).mutableCopy()as! NSMutableArray
                        self.FilterData(ary: (dict.value(forKey: "QRCodePlanMasterDc")as! NSArray).mutableCopy()as! NSMutableArray)
                      
                    }else{
                       
                    }
                }else{
                  
                }
            })
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension BuyQRcodeVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "GenerateQRCodeCell", for: indexPath as IndexPath) as! GenerateQRCodeCell
        let dict = aryTbl.object(at: indexPath.row)as! NSDictionary
        cell.lblAmount.text = "\u{20B9} \(dict.value(forKey: "Price")!)"
        cell.lblTitle.text = "\(dict.value(forKey: "TagTitle")!)"

        if(arySeletedData.contains(dict)){
           cell.imgCheck.image = UIImage(named: "check")
        }else{
            cell.imgCheck.image = UIImage(named: "uncheck")
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryTbl.object(at: indexPath.row)as! NSDictionary
        if(arySeletedData.contains(dict)){
            arySeletedData.remove(dict)
        }else{
            arySeletedData.add(dict)
        }
        lblTotalAmount.text = "\u{20B9} \(calculateAmount(countQrCode: Int(lblCount.text!)!))"

        self.tvList.reloadData()
    }
    
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class GenerateQRCodeCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
