//
//  WelComeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/25/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import QuartzCore

class WelComeVC: UIViewController {
   
    //MARK:
    //MARK: IBOutlet
    @IBOutlet weak var pagerController: UIPageControl!
    @IBOutlet weak var collectionView_Welcome: UICollectionView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!

    //MARK:
    //MARK: CustomeVariable
    
    let  ary_CollectionData = [["title":"Why use greenGENE?","subtitle" :"This application can be used to get a tree under your name, get your tree QR tagged, or gift a tree to someone.","image":"Intro1"],["title":"What’s so great about greenGENE?","subtitle" :"Get a tree under your name and be proud of contributing towards a greener nation. A QR Tag can be easily scanned to know the details of your tree’s birth.","image":"Intro2"],["title":"How do I do it?","subtitle" :"Simply go into the application and explore various options to contribute. Join the mission towards a greener habitat!","image":"Intro3"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let collectionViewLayout = (self.collectionView_Welcome.collectionViewLayout as! UICollectionViewFlowLayout)
        collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.btnLogin.layer.cornerRadius = 10.0
        self.btnSignUp.layer.cornerRadius = 10.0
        self.btnLogin.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        self.btnSignUp.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        
        let backgroundImage = UIImage(named: "Rectangle 31849")
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = .scaleAspectFill
        collectionView_Welcome.backgroundView = imageView
        
        startRain()
        startThunderstorm()
        
    }
    
    func startRain() {
          let raindropWidth: CGFloat = 2.0
          let raindropHeight: CGFloat = 10.0
          let screenWidth = view.bounds.width
          
          // Generate raindrops continuously
          Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
              let startX = CGFloat(arc4random_uniform(UInt32(screenWidth)))
              let raindrop = RaindropView(frame: CGRect(x: startX, y: -raindropHeight, width: raindropWidth, height: raindropHeight))
              self.view.addSubview(raindrop)
              
              UIView.animate(withDuration: 1.0, animations: {
                  raindrop.frame.origin.y = self.view.bounds.height
              }, completion: { _ in
                  raindrop.removeFromSuperview() // Remove raindrop when animation completes
              })
          }
      }
      
      func startThunderstorm() {
          // Flash view for simulating lightning effect
          let flashView = UIView(frame: view.bounds)
          flashView.backgroundColor = UIColor.white
          flashView.alpha = 0.0
          
          // Add flash view to the main view
          view.addSubview(flashView)
          
          // Simulate lightning effect
          Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { _ in
              UIView.animate(withDuration: 0.1, animations: {
                  flashView.alpha = 1.0
              }, completion: { _ in
                  UIView.animate(withDuration: 0.5, animations: {
                      flashView.alpha = 0.0
                  })
              })
          }
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
   
    @IBAction func actionOnLogin_SignUp(_ sender: UIButton) {
//          let testController = mainStoryboard.instantiateViewController(withIdentifier: "LogInRegisterVC")as! LogInRegisterVC
//            testController.strViewComeFrome = (sender.titleLabel?.text!)!
//            self.navigationController?.pushViewController(testController, animated: true)
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let home = storyBoard.instantiateViewController(withIdentifier: "LOGINVC") as! LOGINVC
        navigationController?.pushViewController(home, animated: true);
    }
    
    @IBAction func actionOnSignUp(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let forgotPassword = storyBoard.instantiateViewController(withIdentifier: "SIGNUPVC") as! SIGNUPVC
        navigationController?.pushViewController(forgotPassword, animated: false);
    }
    
    
    @IBAction func actionOnSkip(_ sender: UIButton) {
        deleteAllRecords(strEntity:"LoginData")
        deleteAllRecords(strEntity:"AboutTree")
        deleteAllRecords(strEntity:"CaretakerList")
        deleteAllRecords(strEntity:"MYtree")
        deleteAllRecords(strEntity:"Notification")
        deleteAllRecords(strEntity:"OrganizationTree")

        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
}
extension WelComeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCell", for: indexPath as IndexPath) as! CommonCollectionCell
        let dict = ary_CollectionData[indexPath.row]as NSDictionary
        cell.welcome_lblTitle.text = dict["title"]as? String
        cell.welcome_lblDetail.text = dict["subtitle"]as? String
        cell.welcome_image.image = UIImage(named: "\(dict["image"]!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: (self.collectionView_Welcome.frame.size.width) , height:(self.collectionView_Welcome.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.8
        cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collectionView_Welcome.contentOffset.x
        let w = self.collectionView_Welcome.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pagerController.currentPage = (currentPage)
    }
}

