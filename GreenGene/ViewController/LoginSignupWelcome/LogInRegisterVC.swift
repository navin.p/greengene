//
//  LogInRegisterVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK: ---------------Protocol-----------------
protocol refreshSignUpView : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}

class LogInRegisterVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnRember: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblLogin: UILabel!
    
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var lblSignUp: UILabel!
    
    @IBOutlet weak var tvForLogin: UITableView!
    @IBOutlet weak var tvForSignUp: UITableView!
    //Login
    @IBOutlet weak var login_txtMobile: SkyFloatingLabelTextField!
  @IBOutlet weak var login_txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btn_LogIN: UIButton!
    @IBOutlet weak var viewBtnLogin: UIView!

    //SIGNUP
    @IBOutlet weak var txtFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var btnIAgree: UIButton!
    @IBOutlet weak var btn_SignUP: UIButton!
    @IBOutlet weak var viewBtnSignUP: UIView!

    var strViewComeFrome = String()
    var aryForStateList = NSMutableArray()
    var aryForCityList = NSMutableArray()

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.tvForLogin.isHidden = true
        self.tvForSignUp.isHidden = true
       // login_txtMobile.text = "9074563250"
       // login_txtPassword.text = "8120266886"
        lblTitle.text = strViewComeFrome
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
          lblLogin.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btn_LogIN.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btn_LogIN.layer.cornerRadius = 8.0
        btn_SignUP.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btn_SignUP.layer.cornerRadius = 8.0
     
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if strViewComeFrome == "LOGIN" {
         //   login_txtMobile.text = ""
           // login_txtPassword.text = ""
            btnRember.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            self.tvForLogin.isHidden = false
            self.tvForSignUp.isHidden = true
            lblLogin.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
            lblSignUp.backgroundColor = UIColor.lightGray
            self.tvForLogin.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            UIView.animate(withDuration: 0.4) {
                self.tvForLogin.transform = CGAffineTransform.identity
            }
        }else{
            txtFullName.text = ""
            txtEmailAddress.text = ""
            txtMobileNumber.text = ""
            txtPassword.text = ""
            txtConfirmPassword.text = ""
            txtAddress.text = ""
            txtState.text = ""
            txtCity.text = ""
        btnIAgree.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            self.tvForSignUp.isHidden = false
            self.tvForLogin.isHidden = true
            lblSignUp.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
            lblLogin.backgroundColor = UIColor.lightGray
            self.tvForSignUp.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            UIView.animate(withDuration: 0.4) {
                self.tvForSignUp.transform = CGAffineTransform.identity
            }
        }
    }
    
    // MARK: - --------------ValidationLogInView
    // MARK: -
    
    func validationLogin() -> Bool {
        if login_txtMobile.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginMobile, viewcontrol: self)
            return false
        }else if ((login_txtMobile.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginValidMobile, viewcontrol: self)
            return false
        }else if ((login_txtPassword.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginPassword, viewcontrol: self)
            return false
        }else{
            return true
        }
    }
    // MARK: - --------------ValidationSignUP
    // MARK: -
    
    func validationSignUp() -> Bool {
        if txtFullName.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPFullName, viewcontrol: self)
            return false
        }else if ((txtEmailAddress.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPEmail, viewcontrol: self)
            return false
        }
        else if !(validateEmail(email: txtEmailAddress.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPValidEmail, viewcontrol: self)
            return false
        }
        else if ((txtMobileNumber.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginMobile, viewcontrol: self)
            return false
        }
        else if ((txtMobileNumber.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginValidMobile, viewcontrol: self)
            return false
        }else if ((txtPassword.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginPassword, viewcontrol: self)
            return false
        }else if ((txtPassword.text?.count)! < 6){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPValidPassword, viewcontrol: self)
            return false
        }else if ((txtConfirmPassword.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPCPassword, viewcontrol: self)
            return false
        }else if ((txtConfirmPassword.text!) != (txtPassword.text!) ){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPValidConfirmPass, viewcontrol: self)
            return false
        }
//        else if ((txtAddress.text?.count)! == 0){
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPAddress, viewcontrol: self)
//            return false
//        }
        else if ((txtState.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPState, viewcontrol: self)
            return false
        }else if ((txtCity.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPCity, viewcontrol: self)
            return false
        }else if ((btnIAgree.currentImage == #imageLiteral(resourceName: "uncheck"))){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignTerms, viewcontrol: self)
            return false
        }else{
            return true
        }
    }
    // MARK: - --------------API Calling
    // MARK: -
    
    func call_LOGIN_API(sender: UIButton)  {
        if !(isInternetAvailable()){
           FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let loginURL = "\(URL_Login)UserName=\(String(describing: login_txtMobile.text!))&Password=\(String(describing: login_txtPassword.text!))"
            print(loginURL)
            customDotLoaderShowOnButton(btn: sender, view: self.viewBtnLogin, controller: self)

            WebService.callAPIBYGET(parameter: NSDictionary(), url: loginURL, OnResultBlock: { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        if self.btnRember.currentImage == #imageLiteral(resourceName: "uncheck"){
                            nsud.set("False", forKey: "loginRemberstatus")
                        }else{
                            nsud.set("True", forKey: "loginRemberstatus")
                        }
                        nsud.synchronize()
                    deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalDictionary(strEntity: "LoginData", strKey: "login", data: (dict).mutableCopy() as! NSMutableDictionary)
                        
                        FTIndicator.showToastMessage(AlertLogin)
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    func call_SIGNUP_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: self.viewBtnSignUP, controller: self)

  
            let urlSignUp = "\(URL_UserSignUp)Name=\(txtFullName.text!)&Email_Id=\(txtEmailAddress.text!)&Mobile_Number=\(txtMobileNumber.text!)&Password=\(txtPassword.text!)&State_Id=\(txtState.tag)&City_Id=\(txtCity.tag)&Address=\(txtAddress.text!)"
            print(urlSignUp)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: urlSignUp, OnResultBlock: { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title:alertInfo, message: alertSignUP, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                  self.strViewComeFrome = "LOGIN"
                            self.viewWillAppear(true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func call_StateList_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetStateListByCountryId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForStateList = NSMutableArray()
                        self.aryForStateList = (dict.value(forKey: "StateList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForStateList)
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func call_CityList_API(sender : UIButton) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetCityListByStateId = "\(URL_GetCityListByStateId)\(String(describing: txtState.tag))"
            print(GetCityListByStateId)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetCityListByStateId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForCityList = NSMutableArray()
                        self.aryForCityList = (dict.value(forKey: "CityList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForCityList)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
                    let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        
                if sender.tag == 0 { //State
                    vc.strTitle = "Select State"
                    vc.strTag = 1
                }else if sender.tag == 1 { // City
                    vc.strTitle = "Select City"
                    vc.strTag = 2
                }
        
                if aryData.count != 0{
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    vc.handleSignUPView = self
                    vc.aryTBL = aryData
                    self.present(vc, animated: false, completion: {})
                }
    }
 
    // MARK: - ---------------IBACTION's
    // MARK: -
    @IBAction func actionONDropDownButtons(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.tag == 0 {
            if aryForStateList.count != 0 {
                self.gotoPopView(sender: sender, aryData: aryForStateList)
            }else{
            self.call_StateList_API(sender: sender)
            }
        }else{
            
            if txtState.text?.count != 0 {
                self.call_CityList_API(sender: sender)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPStateFirst, viewcontrol: self)
            }
        }
     
    }
    
    @IBAction func actionONButtons(_ sender: UIButton) {
        if sender.tag == 0 { //Remember
            if (sender.currentImage == #imageLiteral(resourceName: "uncheck")){
                sender.setImage(#imageLiteral(resourceName: "redio"), for: .normal)
            }else{
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }else if sender.tag == 1 { // Forgot
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC")as! ForgotPasswordVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
       else if sender.tag == 2 { // Login
            if (validationLogin()){
                call_LOGIN_API(sender: sender)
            }
        }
        else if sender.tag == 3 { //Back
            self.navigationController?.popViewController(animated: true)
        }
        else if sender.tag == 5 { //I Agree
            if (sender.currentImage == #imageLiteral(resourceName: "uncheck")){
                sender.setImage(#imageLiteral(resourceName: "redio"), for: .normal)
            }else{
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }
        else if sender.tag == 6 { //SIGN UP
            if (validationSignUp()){
                call_SIGNUP_API(sender: sender)
            }
        }
    }
    @IBAction func actionONTab(_ sender: UIButton) {
        if sender.tag == 0 { // Login
            lblTitle.text = "LOGIN"

            login_txtMobile.text = ""
            login_txtPassword.text = ""
            btnRember.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            self.tvForLogin.isHidden = false
            self.tvForSignUp.isHidden = true
            lblLogin.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
            lblSignUp.backgroundColor = UIColor.lightGray
            self.tvForLogin.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            UIView.animate(withDuration: 0.4) {
                self.tvForLogin.transform = CGAffineTransform.identity
            }
        }else{ // Signup
            lblTitle.text = "SIGNUP"
            txtFullName.text = ""
            txtEmailAddress.text = ""
            txtMobileNumber.text = ""
            txtPassword.text = ""
            txtConfirmPassword.text = ""
            txtAddress.text = ""
            txtState.text = ""
            txtCity.text = ""
            btnIAgree.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
              self.tvForSignUp.isHidden = false
              self.tvForLogin.isHidden = true
            lblSignUp.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
            lblLogin.backgroundColor = UIColor.lightGray
            self.tvForSignUp.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            UIView.animate(withDuration: 0.4) {
                self.tvForSignUp.transform = CGAffineTransform.identity
            }
        }
        
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension LogInRegisterVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtFullName  {
            return  txtFiledValidation(textField: txtFullName, string: string, returnOnly: "ALL", limitValue: 30)
        }
        
        
        if textField == login_txtMobile  {
            return  txtFiledValidation(textField: login_txtMobile, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == txtMobileNumber {
            return  txtFiledValidation(textField: txtMobileNumber, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == login_txtPassword  {
            return  txtFiledValidation(textField: login_txtPassword, string: string, returnOnly: "ALL", limitValue: 11)
        }
        if textField ==  txtPassword  {
            return  txtFiledValidation(textField: txtPassword, string: string, returnOnly: "ALL", limitValue: 11)
        }
        if  textField ==  txtConfirmPassword {
            return  txtFiledValidation(textField: txtConfirmPassword, string: string, returnOnly: "ALL", limitValue: 11)
        }else{
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 60)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension LogInRegisterVC: refreshSignUpView {
    func refreshview(dictData: NSDictionary, tag: Int) {
        if tag == 1 { // State
            txtState.text = dictData.value(forKey: "State_Name")as? String
            txtState.tag = dictData.value(forKey: "State_Id")as! Int
        }else if tag == 2 { // City
            txtCity.text = dictData.value(forKey: "City_Name")as? String
            txtCity.tag = dictData.value(forKey: "City_Id")as! Int
        }
    }
}
