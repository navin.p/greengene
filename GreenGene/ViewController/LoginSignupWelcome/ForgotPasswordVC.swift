//
//  ForgotPasswordVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!

    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 8.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func actionOnButton(_ sender: UIButton) {
        if sender.tag == 0 {
            self.navigationController?.popViewController(animated: true)

        }else{
            if (validationLogin()){
                self.call_FORGOT_API(sender: sender)
            }
        }
        
    }
    // MARK: - --------------Validation
    // MARK: -
    
    func validationLogin() -> Bool {
        if txtMobile.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPEmail, viewcontrol: self)
            return false
        } else if !(validateEmail(email: txtMobile.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPValidEmail, viewcontrol: self)
            return false
        }else{
            return true
        }
    }
    // MARK: - --------------API Calling
    // MARK: -
    
    func call_FORGOT_API(sender: UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let forgotURL = "\(URL_ForgetPassword)Email_Id=\(String(describing: txtMobile.text!))"
            
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)

            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: forgotURL, OnResultBlock: { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title:alertInfo, message: "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)

                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension ForgotPasswordVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobile {
            return  txtFiledValidation(textField: txtMobile, string: string, returnOnly: "All", limitValue: 55)
            
        }else{
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
