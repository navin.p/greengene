//
//  ViewBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class DrawerVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    
    var aryMenuItems = NSMutableArray()
    weak var handleDrawerView: DrawerScreenDelegate?
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        //lblVersion.text = "Version - " + app_Version
        //tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.clear
        aryMenuItems = NSMutableArray()
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let strUserRole = "\(dict.value(forKey: "RoleId")!)"
            aryMenuItems = NSMutableArray()
            
            if(strUserRole == "1"){
//                aryMenuItems = [["title":"Home","image":"home"],["title":"Associated Partners","image":"associated_partner"],["title":"Report Tree Cutting","image":"tree_cutting"],["title":"My QR Code","image":"my_qr_code"],["title":"Fetch QR Code","image":"fetching_qr_code"],["title":"Find a Tree","image":"find_tree"],["title":"Nearby Trees","image":"near_by_tree"],["title":"About Trees","image":"about_tree-1"],["title":"Did You Know?","image":"did_you_know"],["title":"News Notification","image":"news"],["title":"Tree Tagging","image":"tree_tag"],["title":"Allocate QR Code","image":"allocate_qr_code"],["title":"Wealth Calculator","image":"wealth"],["title":"OutBox","image":"out_box"]]
                
                aryMenuItems = [["title":"Home","image":"HomeLM"],["title":"How App Works?","image":"allocate QR"],["title":"QR Tag Parks","image":"QRTaggedLM"],["title":"QR Code Order History","image":"Fetch QR"],["title":"Find Tree","image":"FindaTreeLM"],["title":"Nearby Trees","image":"NearbyTree"],["title":"News & Updates","image":"News&UpdatesLM"],["title":"Green Wealth Calculator","image":"Wealth Calc"],["title":"Did You Know?","image":"DidYorKnowLM"],["title":"About Trees","image":"AboutTreeLM"],["title":"Change Password","image":"AssociatedPartnerLM"],["title":"Fetch QR Code","image":"Fetch QR"],["title":"Tree Tagging","image":"Tree Tagging"],["title":"Tree Tagging QR Code","image":"MyQRLM"],["title":"Allocate QR Code","image":"allocate QR"],["title":"OutBox","image":"Outbox"],["title":"Logout","image":"LogInLM"]]
             //   ["title":"Task Management","image":"Task"]
                
            }else if (strUserRole == "4"){
                aryMenuItems = [["title":"Home","image":"home"],["title":"Associated Partners","image":"associated_partner"],["title":"Report Tree Cutting","image":"tree_cutting"],["title":"My QR Code","image":"my_qr_code"],["title":"Fetch QR Code","image":"fetching_qr_code"],["title":"Find a Tree","image":"find_tree"],["title":"Nearby Trees","image":"near_by_tree"],["title":"About Trees","image":"about_tree-1"],["title":"Did You Know?","image":"did_you_know"],["title":"News Notification","image":"news"],["title":"Green Wealth Calculator","image":"wealth"]]
            }  else if (strUserRole == "5"){
//                aryMenuItems = [["title":"Home","image":"home"],["title":"Associated Partners","image":"associated_partner"],["title":"Report Tree Cutting","image":"tree_cutting"],["title":"My QR Code","image":"my_qr_code"],["title":"Organization Trees","image":"my_tree"],["title":"Fetch QR Code","image":"fetching_qr_code"],["title":"Find a Tree","image":"find_tree"],["title":"Nearby Trees","image":"near_by_tree"],["title":"About Trees","image":"about_tree-1"],["title":"Did You Know?","image":"did_you_know"],["title":"News Notification","image":"news"],["title":"Wealth Calculator","image":"wealth"]]
                aryMenuItems = [["title":"Home","image":"HomeLM"],["title":"How App Works?","image":"allocate QR"],["title":"QR Tag Parks","image":"QRTaggedLM"],["title":"QR Code Order History","image":"Fetch QR"],["title":"Task Management","image":"Task"],["title":"Find Tree","image":"FindaTreeLM"],["title":"Organization Trees","image":"State"],["title":"Nearby Trees","image":"NearbyTree"],["title":"News & Updates","image":"News&UpdatesLM"],["title":"Green Wealth Calculator","image":"Wealth Calc"],["title":"Did You Know?","image":"DidYorKnowLM"],["title":"About Trees","image":"AboutTreeLM"],["title":"Change Password","image":"AssociatedPartnerLM"],["title":"Logout","image":"LogInLM"]]
            }else if (strUserRole == "2"){
                aryMenuItems = [["title":"Home","image":"HomeLM"],["title":"How App Works?","image":"allocate QR"],["title":"QR Tag Parks","image":"QRTaggedLM"],["title":"QR Code Order History","image":"Fetch QR"],["title":"Task Management","image":"Task"],["title":"Find Tree","image":"FindaTreeLM"],["title":"Nearby Trees","image":"NearbyTree"],["title":"News & Updates","image":"News&UpdatesLM"],["title":"Green Wealth Calculator","image":"Wealth Calc"],["title":"Did You Know?","image":"DidYorKnowLM"],["title":"About Trees","image":"AboutTreeLM"],["title":"Change Password","image":"AssociatedPartnerLM"],["title":"Logout","image":"LogInLM"]]
            }else if (strUserRole == "6"){
                
                aryMenuItems = [["title":"Home","image":"HomeLM"],["title":"How App Works?","image":"allocate QR"],["title":"QR Tag Parks","image":"QRTaggedLM"],["title":"QR Code Order History","image":"Fetch QR"],["title":"Find Tree","image":"FindaTreeLM"],["title":"Organization Trees","image":"State"],["title":"Nearby Trees","image":"NearbyTree"],["title":"News & Updates","image":"News&UpdatesLM"],["title":"Green Wealth Calculator","image":"Wealth Calc"],["title":"Did You Know?","image":"DidYorKnowLM"],["title":"About Trees","image":"AboutTreeLM"],["title":"Change Password","image":"AssociatedPartnerLM"],["title":"Logout","image":"LogInLM"]]
                
            }else{
                aryMenuItems = [["title":"Home","image":"home"],["title":"Associated Partners","image":"associated_partner"],["title":"Report Tree Cutting","image":"tree_cutting"],["title":"My QR Code","image":"my_qr_code"],["title":"Find a Tree","image":"find_tree"],["title":"Nearby Trees","image":"near_by_tree"],["title":"About Trees","image":"about_tree-1"],["title":"Did You Know?","image":"did_you_know"],["title":"News Notification","image":"news"],["title":"Green Wealth Calculator","image":"wealth"]]
            }
            
        }else{
            
            aryMenuItems = [["title":"Home","image":"HomeLM"],["title":"How App Works?","image":"allocate QR"],["title":"QR Tag Parks","image":"QRTaggedLM"],["title":"QR Code Order History","image":"Fetch QR"],["title":"Find Tree","image":"FindaTreeLM"],["title":"Nearby Trees","image":"NearbyTree"],["title":"News & Updates","image":"News&UpdatesLM"],["title":"Did You Know?","image":"DidYorKnowLM"],["title":"About Trees","image":"AboutTreeLM"],["title":"Change Password","image":"AssociatedPartnerLM"],["title":"Login","image":"LogInLM"]]
            
        }
        
        
        let imageView = UIImageView(image: UIImage(named: "Mask Group 33"))
        imageView.frame = CGRect(x: 0, y: 200, width: tvlist.frame.width, height: 150) // Adjust height as needed
        imageView.contentMode = .scaleToFill
        tvlist.tableFooterView = imageView
        
        tvlist.delegate = self
        tvlist.dataSource = self
        tvlist.reloadData()
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.dismiss(animated: false) {}
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DrawerVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return aryMenuItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != 0 {
            let cell = tvlist.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! DrawerCell
            let strTitle = (aryMenuItems[indexPath.row]as AnyObject).value(forKey: "title")as! String
            let strImage = (aryMenuItems[indexPath.row]as AnyObject).value(forKey: "image")as! String
            cell.btnTitle.setTitle(strTitle, for: .normal)
            cell.btnimg.setImage(UIImage(named:strImage), for: .normal)
           // cell.btnimg.tintColor = hexStringToUIColor(hex: colorGreenPrimary)
         //   cell.btnTitle.setTitleColor(hexStringToUIColor(hex: colorGreenPrimary), for: .normal)
            cell.backgroundColor = UIColor.clear
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer1", for: indexPath as IndexPath) as! DrawerCell
            cell.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)

            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                cell.lblName.text = "\(dict.value(forKey: "Name")!)"
              //  cell.lblName.text =  cell.lblName.text?.uppercased()
                let strProfileUrl = "\(dict.value(forKey: "Profile_Image")!)"
                cell.imgProile.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
                if(strProfileUrl.count != 0){
                    cell.imgProile.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                       
                    }, usingActivityIndicatorStyle: .gray)
                }
                cell.lblSubtitle.text = "\(dict.value(forKey: "User_Name")!)\nView your profile"
                cell.btnLOGIN.isHidden = true
            }else{
//                cell.btnLOGIN.layer.cornerRadius = 8.0
//                cell.btnLOGIN.backgroundColor = hexStringToUIColor(hex: colorOrangPrimary)
//                cell.lblName.text = "XYZ"
//                cell.lblSubtitle.text = "XXXXXX0000"
//                cell.btnLOGIN.isHidden = false
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.imgProile.layer.cornerRadius =  cell.imgProile.frame.width / 2
            cell.imgProile.layer.borderColor = UIColor.white.cgColor
            cell.imgProile.layer.borderWidth  = 1.0
            cell.imgProile.layer.masksToBounds = false
            cell.imgProile.clipsToBounds = true
            cell.imgProile.contentMode = .scaleAspectFill
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if DeviceType.IS_IPAD {
            if indexPath.section == 0 {
                return 132
            }else{
                return 80
            }
        }else{
            if indexPath.section == 0 {
                return 130
            }else{
                return 45
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
//        customView.backgroundColor = UIColor.groupTableViewBackground
//        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
//        button.setTitle("LOGOUT", for: .normal)
//        button.setTitleColor(hexStringToUIColor(hex: colorGreenPrimary), for: .normal)
//        button.addTarget(self, action: #selector(buttonActionLOGOUT), for: .touchUpInside)
//        customView.addSubview(button)
//        return customView
//    }
    
    
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if (section == 1){
//            return loginDict.count == 0 ? 0 : 40
//            
//        }
//        return 0
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.section == 0){
            if (loginDict.count == 0){
                self.dismiss(animated: false) {
                    self.handleDrawerView?.refreshDrawerScreen(strType: "LOGIN", tag: indexPath.row)
                }
            }else{
                self.dismiss(animated: false) {
                    self.handleDrawerView?.refreshDrawerScreen(strType: "PROFILE", tag: indexPath.row)
                }
            }
        }else{
            self.dismiss(animated: false) {
                self.handleDrawerView?.refreshDrawerScreen(strType: (self.aryMenuItems[indexPath.row]as AnyObject).value(forKey: "title") as! String, tag: indexPath.row)
            }
        }
    }
    
    @objc func buttonActionLOGOUT(_ sender: UIButton!) {
        self.dismiss(animated: false) {
            self.handleDrawerView?.refreshDrawerScreen(strType: "LogOut", tag: 0)
        }
        
    }
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class DrawerCell: UITableViewCell {
    //DashBoard
    
    @IBOutlet weak var imgProile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var btnimg: UIButton!
    @IBOutlet weak var btnLOGIN: UIButton!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        roundCorners()
    }
    
    func roundCorners() {
        let cornerRadius: CGFloat = 39
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: [.bottomLeft],
                                    cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
}
//MARK:
//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}
