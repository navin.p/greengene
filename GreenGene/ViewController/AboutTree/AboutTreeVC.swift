//
//  AboutTreeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/30/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class AboutTreeVC: UIViewController {
 
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblError: UILabel!
    
    // MARK: - --------------Variable
    // MARK: -
    var ary_of_AboutData = NSMutableArray()
    var aryForSearchData = NSMutableArray()
    var refresher = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        self.call_AboutTreeList_API()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        call_AboutTreeList_API()
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: Any) {

       self.navigationController?.popViewController(animated: true)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_AboutTreeList_API()  {
        if !(isInternetAvailable()){
            getAbouttreeDataFromLocal()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")
            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetPlantList, OnResultBlock: { (responce, status) in
                print(responce)
                loading.endLoading()
                self.refresher.endRefreshing()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"AboutTree")
                        saveDataInLocalArray(strEntity: "AboutTree", strKey: "abouttree", data: (dict.value(forKey: "PlantList")as! NSArray).mutableCopy() as! NSMutableArray)
                        self.getAbouttreeDataFromLocal()
                    }else{
                        self.lblError.text = "\(dict.value(forKey: "Success")as! String)"
                    }
                }else{
                    self.lblError.text = alertSomeError
                }
            })
        }
    }
    // MARK: - ---------------GetDataFromLocal
    // MARK: -
    func getAbouttreeDataFromLocal()   {
        let aryTemp = getDataFromLocal(strEntity: "AboutTree", strkey: "abouttree")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "abouttree") ?? 0)
            }
        }
        
        print(aryList)
        if aryList.count != 0 {
            self.ary_of_AboutData = NSMutableArray()
            self.ary_of_AboutData = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
            self.aryForSearchData = NSMutableArray()
            self.aryForSearchData = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
        }else{
            if !(isInternetAvailable()){
                self.lblError.text = alertInternet
            }else{
                self.lblError.text = alertDataNotFound
            }
        }
        self.tvlist.reloadData()
        
        
    }
}

    

    

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AboutTreeVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryForSearchData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "AboutTreeCell", for: indexPath as IndexPath) as! AboutTreeCell
        let dict = aryForSearchData.object(at: indexPath.row)as! NSDictionary
        cell.imgTree.layer.cornerRadius =  12
        cell.imgTree.layer.borderWidth  = 1.0
        cell.imgTree.layer.masksToBounds = false
        cell.imgTree.clipsToBounds = true
        cell.imgTree.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        var strProfileUrl = "\(dict["Plant_Image"]as! String)"
        if(strProfileUrl.count != 0){
            if (strProfileUrl.contains("~/")){
                strProfileUrl = strProfileUrl.replacingOccurrences(of: "~/", with: "")
            }
            let urlImage = "\(BaseURLImageDownLoad1)\(strProfileUrl)"
            cell.imgTree.setImageWith(URL(string: "\(urlImage)"), placeholderImage: UIImage(named: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
        }, usingActivityIndicatorStyle: .gray)
            cell.lblName.text = "\(dict.value(forKey: "Plant_Name")!)"
            cell.lbldetail.text = "\(dict.value(forKey: "Description")!)"
        
        }
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 109.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AbouttreeDetailVC")as! AbouttreeDetailVC
        let dict = removeNullFromDict(dict: (self.aryForSearchData.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        testController.dict = dict
        testController.strComeFrom = "About"
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
  
    

}



// MARK: - ----------------UISearchBarDelegate
// MARK: -

extension  AboutTreeVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "Plant_Name contains[c] %@ OR Plant_Name contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.ary_of_AboutData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryForSearchData = NSMutableArray()
            self.aryForSearchData = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryForSearchData = NSMutableArray()
            self.aryForSearchData = self.ary_of_AboutData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        
    }
}
// MARK: - ----------------Associated Cell
// MARK: -
class AboutTreeCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lbldetail: UILabel!
    @IBOutlet weak var imgTree: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
