//
//  AbouttreeDetailVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/1/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class AbouttreeDetailVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imagePlant: UIImageView!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var viewHeader: CardView!
   
    var dict = NSMutableDictionary()
    var strComeFrom = String()

    // MARK: - ----------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        imagePlant.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        imagePlant.addGestureRecognizer(tap)
        if strComeFrom == "About" {
            lblTitle.text = (dict.value(forKey: "Plant_Name")as! String)
            txtDescription.text = (dict.value(forKey: "Description")as! String)
            var strProfileUrl = "\(dict["Plant_Image"]as! String)"
            if(strProfileUrl.count != 0){
                if (strProfileUrl.contains("~/")){
                    strProfileUrl = strProfileUrl.replacingOccurrences(of: "~/", with: "")
                }
                let urlImage = "\(BaseURLImageDownLoad1)\(strProfileUrl)"
                imagePlant.setImageWith(URL(string: urlImage), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    print(url ?? 0)
                }, usingActivityIndicatorStyle: .white)
            }
        }else{
            print(dict)
        
            var strTitle = "Not Available"
            var strDetail = "Not Available"
            var strimage = "default"
            var strSourceOfInfo = "Not Available"

            if dict.value(forKey: "Tree_Name") != nil  && "\(dict.value(forKey: "Tree_Name")!)" != ""{
                strTitle = "\(dict.value(forKey: "Tree_Name")!)"
            }
            if dict.value(forKey: "Description") != nil  && "\(dict.value(forKey: "Description")!)" != ""{
                strDetail = "\(dict.value(forKey: "Description")!)"
            }
            if dict.value(forKey: "Images") != nil  && "\(dict.value(forKey: "Images")!)" != ""{
                strimage = "\(dict.value(forKey: "Images")!)"
            }
            if dict.value(forKey: "SourceOfInformation") != nil  && "\(dict.value(forKey: "SourceOfInformation")!)" != ""{
                strSourceOfInfo = "\(dict.value(forKey: "SourceOfInformation")!)"
            }
            if(strimage.count != 0){
                imagePlant.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strimage)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }else{
                imagePlant.image = #imageLiteral(resourceName: "default_img")
            }
            txtDescription.text = "Source Of Information : \(strSourceOfInfo)\n \n\(strDetail)"
            txtDescription.setContentOffset(.zero, animated: true)

            lblTitle.text = strTitle
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        txtDescription.scrollRangeToVisible(NSMakeRange(0, 0))
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)


    }
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.imagePlant.image!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
