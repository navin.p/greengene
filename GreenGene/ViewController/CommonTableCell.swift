//
//  CommonTableCell.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/27/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class CommonTableCell: UITableViewCell {
// List
    @IBOutlet weak var List_lblTitle: UILabel!

    //-------DashBoard
    
    @IBOutlet weak var dashBoard_Image: UIImageView!
        @IBOutlet weak var dashBoard_lblTitle: UILabel!
    
    //--AS A Craetaker
    @IBOutlet weak var caretaker_lblPlantName: UILabel!
    @IBOutlet weak var caretaker_lblPlantLocation: UILabel!
    @IBOutlet weak var caretaker_lblOwnerName: UILabel!
    @IBOutlet weak var caretaker_lblOwnerNumber: UILabel!
    @IBOutlet weak var caretaker_lblDate: UILabel!
    @IBOutlet weak var caretaker_lblQRCode: UILabel!
    @IBOutlet weak var caretaker_BtnMap: UIButton!

    //MyTrees
    @IBOutlet weak var tree_BtnQRcode: UIButton!
    
    @IBOutlet weak var tree_lblDate: UILabel!
    @IBOutlet weak var tree_lblQr: UILabel!
 
    @IBOutlet weak var tree_lblAddress: UILabel!
    @IBOutlet weak var tree_lblPlantName: UILabel!
    
    @IBOutlet weak var tree_btnImg: UIButton!
    
    @IBOutlet weak var tree_img: UIImageView!
    @IBOutlet weak var tree_Btnmap: UIButton!
    @IBOutlet weak var tree_BtnRetag: UIButton!
    @IBOutlet weak var tree_BtnStatus: UIButton!

    //My Tree Detail
    
    @IBOutlet weak var treeDetail_Imge: UIImageView!
    
    @IBOutlet weak var viewOrganization: CardView!
    @IBOutlet weak var treeDetail_lblCaretaker: UILabel!
    
    @IBOutlet weak var treeDetail_lblDescription: UILabel!
     @IBOutlet weak var treeDetail_lblIsPest: UILabel!
    @IBOutlet weak var treeDetail_Caretakername: UILabel!

     @IBOutlet weak var treeDetail_lblIsTirm: UILabel!
     @IBOutlet weak var treeDetail_lblIsFertilizer: UILabel!
    @IBOutlet weak var treeDetail_lblDate: UILabel!
    @IBOutlet weak var treeDetail_btnImg: UIButton!

    //--Notification
    
    @IBOutlet weak var notification_lblAddress: UILabel!
        @IBOutlet weak var notification_lblDetail: UILabel!
        @IBOutlet weak var notification_lblDate: UILabel!
    
    // Did you Know
    
    @IBOutlet weak var didYou_Title: UILabel!
    @IBOutlet weak var didYou_Detail: UILabel!
    @IBOutlet weak var didYou_Address: UILabel!
    @IBOutlet weak var didYou_Age: UILabel!
    @IBOutlet weak var didYou_image: UIImageView!

    // OUTBOX
    
    @IBOutlet weak var outBox_lblQRCode: UILabel!
    @IBOutlet weak var outBox_lblDate: UILabel!
    @IBOutlet weak var outBox_lblTitle: UILabel!
    
    
    @IBOutlet weak var bgVIew: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 self.selectionStyle = .none
        // Configure the view for the selected state
    }

}
