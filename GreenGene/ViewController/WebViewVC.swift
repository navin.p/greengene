//
//  WebViewVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var btnError: UIButton!
    var strViewComeFrom = String()
    var strURL = String()

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        lbltitle.text = strViewComeFrom
        activityView.isHidden = true
        if !(isInternetAvailable()){
            self.viewForError.isHidden = false
            self.btnError.setTitle(alertInternet, for: .normal)
            self.imgError.image = #imageLiteral(resourceName: "no-wifi")
        }else{
            self.viewForError.isHidden = true
            webView.delegate = self
            activityView.isHidden = false
            if(strViewComeFrom == "Terms & Conditions  "){
                webView.loadHTMLString(strURL, baseURL: nil)
            }else{
                webView.loadRequest(URLRequest(url: URL(string:strURL)!))
            }
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: Any) {
        
        
      if(strViewComeFrom == "Terms & Conditions  " || strViewComeFrom == appName){
            self.navigationController?.popViewController(animated: true)
        }else{
            var status = false
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: OtherVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated:true)
                    status = true
                    break
                }
            }
            if !(status){
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DashBoardVC.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated:true)
                        break
                    }
                }
            }
        }
        
       
       
    }
    // MARK: - ----------------UIWebViewDelegate
    // MARK: -
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityView.isHidden = false
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      activityView.isHidden = true
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityView.isHidden = true
    }

}
