//
//  CongratulationVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/22/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class CongratulationVC: UIViewController {
   
    @IBOutlet weak var logoVendor: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblPresent: UILabel!

    var dictData = NSMutableDictionary()
    // MARK: - ----------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        if(dictData.value(forKey: "vendordt") is NSArray){
            if((dictData.value(forKey: "vendordt")as! NSArray).count != 0){
                var dictTemp = NSMutableDictionary()
                dictTemp = ((dictData.value(forKey: "vendordt")as! NSArray).object(at: 0) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let strUrl = "\(dictTemp.value(forKey: "Logo")!)"
                logoVendor.setImageWith(URL(string: "\(BaseURLVendorImageDownLoad)\(strUrl)"), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                },usingActivityIndicatorStyle: .gray)
                
                lblMessage.text = "You have recived a discount of \(dictTemp.value(forKey: "DiscountPercent")!)% for being a green customer."
                 lblUserName.text = "\(dictTemp.value(forKey: "DiscountTo")!)"
                 lblPresent.text = "\(dictTemp.value(forKey: "DiscountPercent")!)%"
            }
            
        }
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.lblPresent.alpha = 0.4
        self.lblPresent.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.lblPresent.alpha = 1
            self.lblPresent.transform = .identity
        }) { _ in
        }
        
        
        
        
       
        UIView.animate(withDuration: 1.0) {
         
        }
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
}
