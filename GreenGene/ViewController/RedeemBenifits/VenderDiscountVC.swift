//
//  VenderDiscountVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class VenderDiscountVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgVender: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTotalDiscount: UILabel!
    @IBOutlet weak var lblDiscountOffres: UILabel!
    @IBOutlet weak var lblTotalCreditUsed: UILabel!
    @IBOutlet weak var lblTotalCreditLeft: UILabel!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var lblMessageShow: UILabel!

    // MARK: - --------------Variable
    // MARK: -
    var aryForListData = NSMutableArray()
    var refresher = UIRefreshControl()
    var dictVenderData = NSMutableDictionary()
     var strQRcode = String()
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        btnRedeem.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        imgVender.layer.cornerRadius =  8
        imgVender.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        imgVender.layer.borderWidth  = 1.0
        imgVender.layer.masksToBounds = false
        imgVender.clipsToBounds = true
        imgVender.contentMode = .scaleAspectFill
        self.setDataOnview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        call_GetVenderDetailByQRCODE(strQRCode: strQRcode)
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        call_GetVenderDetailByQRCODE(strQRCode: strQRcode)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func actionOnRedeem(_ sender: UIButton) {
        call_RedeemAPI()
    }
    
    // MARK: - ---------------API's Calling
    func call_GetVenderDetailByQRCODE(strQRCode: String)  {
        self.view.endEditing(true)
        
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let GetVenderDetailByQRCode = "\(URL_AssociatedPatnerDetail)QRCode=\(strQRCode)&User_Id=\(dict.value(forKey: "User_Id")!)"
            print(GetVenderDetailByQRCode)
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
             loading.startLoading(text: "Loading...")
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetVenderDetailByQRCode, OnResultBlock: { (responce, status) in
                print(responce)
                loading.endLoading()
                self.refresher.endRefreshing()

                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.dictVenderData = NSMutableDictionary()
                        self.dictVenderData = dict.mutableCopy()as! NSMutableDictionary
                        self.setDataOnview()
                    }else{
                    }
                }else{
                }
            })
        }
    }
    // MARK: - --------------API Calling
    // MARK: -
    func call_RedeemAPI(){
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            if(dictVenderData.value(forKey: "vendordt") is NSArray){
                if((dictVenderData.value(forKey: "vendordt")as! NSArray).count != 0){
                    var dictTemp = NSMutableDictionary()
                    dictTemp = ((dictVenderData.value(forKey: "vendordt")as! NSArray).object(at: 0) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let GetScanGreenWealthCard = "\(URL_ScanGreenWealthCard)User_Id=\(dict.value(forKey: "User_Id")!)&Associate_Partner_Id=\(dictTemp.value(forKey: "Associate_Partner_Id")!)"
                    print(GetScanGreenWealthCard)
                    customDotLoaderShowOnButton(btn: btnRedeem, view: self.view, controller: self)
                    WebService.callAPIBYGET(parameter: NSDictionary(), url: GetScanGreenWealthCard, OnResultBlock: { (responce, status) in
                        print(responce)
                        customeDotLoaderRemove()
                        if (status == "success"){
                            let dict  = (responce.value(forKey: "data")as! NSDictionary)
                            if (dict.value(forKey: "Result")as! String == "True"){
                                let strMsg = "\(dict.value(forKey: "Success")as! String)"
                                if(strMsg == "Your green wealth card discount have done successfully."){
                                    let vc: CongratulationVC = self.storyboard!.instantiateViewController(withIdentifier: "CongratulationVC") as! CongratulationVC
                                    vc.dictData = self.dictVenderData
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                }
                                else {
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMsg, viewcontrol: self)
                                }
                                
                            }else{
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                            }
                        }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    })

                }
                
            }
        }
    }
    
    // MARK: - ---------------EXTRA
    // MARK: -
    func showAlertWithAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
        let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
           
            
        }))
        viewcontrol.present(alert, animated: true, completion: nil)
    }
    
    func setDataOnview() {
        if(dictVenderData.value(forKey: "vendordt") is NSArray){
            if((dictVenderData.value(forKey: "vendordt")as! NSArray).count != 0){
                var dictTemp = NSMutableDictionary()
                dictTemp = ((dictVenderData.value(forKey: "vendordt")as! NSArray).object(at: 0) as! NSDictionary).mutableCopy()as! NSMutableDictionary
               // lblTitle.text = "\(dictTemp.value(forKey: "Associate_Partner")!)"
                lblAddress.text = "\(dictTemp.value(forKey: "Associate_Partner")!)"
                lblUserName.text = "\(dictTemp.value(forKey: "Address")!)"
                lblTotalCreditLeft.text = "\(dictTemp.value(forKey: "TotalCreditScoreLeft")!)"
                lblTotalCreditUsed.text = "\(dictTemp.value(forKey: "TotalCreditScoreUsed")!)"
                lblTotalDiscount.text = "\(dictTemp.value(forKey: "CreditScoreLimit")!)"
                lblDiscountOffres.text = "\(dictTemp.value(forKey: "DiscountPercent")!)"
                let strUrl = "\(dictTemp.value(forKey: "Logo")!)"
                imgVender.setImageWith(URL(string: "\(BaseURLVendorImageDownLoad)\(strUrl)"), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
            
            //For ReddemData
            
            if((dictVenderData.value(forKey: "discountdt")as! NSArray).count != 0){
                aryForListData = NSMutableArray()
                aryForListData = (dictVenderData.value(forKey: "discountdt")as! NSArray).mutableCopy()as! NSMutableArray
                aryForListData.count != 0 ? (self.lblMessageShow.text = "Transactions") : (self.lblMessageShow.text = "Click on Redeem to avail Benefits")
                self.tvlist.reloadData()
            }
            
            
            
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension VenderDiscountVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryForListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tvlist.dequeueReusableCell(withIdentifier: "VendorDiscountCell", for: indexPath as IndexPath) as! VendorDiscountCell
        let dict = aryForListData.object(at: indexPath.row)as! NSDictionary
        let discountDate = dateTimeConvertor(str: "\(dict.value(forKey: "DiscountDate")!)", formet: "")
         cell.lblDate.text = discountDate
        
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
   
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class VendorDiscountCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewRedio: CardView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
