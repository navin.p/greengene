//
//  TreeCuttingReportVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 6/10/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import  CoreData
import CoreLocation


class TreeCuttingReportVC: UIViewController {
  
    @IBOutlet weak var viewHeader: CardView!
    
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtContactNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtTreeName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtNoOfTree: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtViewLocation: KMPlaceholderTextView!
    
    @IBOutlet weak var txtViewDescription: KMPlaceholderTextView!
    @IBOutlet weak var txtOtherName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnBrowse: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imageForComplain: UIImageView!
    
    @IBOutlet weak var scrollContain: UIScrollView!

    @IBOutlet weak var height_OtherTxt: NSLayoutConstraint!
    
    var imagePicker = UIImagePickerController()
    var imageName = String()
    var ary_of_TreeName = NSMutableArray()
    var strLatitude = "0"
    var strLongitude = "0"
    var locationManager = CLLocationManager()
    // MARK: - ----------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnBrowse.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnBrowse.layer.cornerRadius = 4.0
        btnSubmit.layer.cornerRadius = 4.0
        height_OtherTxt.constant = 0.0
        getCurrentLocation()
        loginDict = getLoginData()
        if (loginDict.count != 0){
            let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            txtName.text = "\(dict.value(forKey: "Name")!)"
            txtContactNumber.text = "\(dict.value(forKey: "Mobile_Number")!)"
        }
        getAbouttreeDataFromLocal(opentag: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(txtTreeName.text == "Other"){
            height_OtherTxt.constant = 50.0
        }else{
            height_OtherTxt.constant = 0.0
        }
    }
    
    override func viewWillLayoutSubviews() {
        scrollContain.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height:700)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        locationManager.stopUpdatingLocation()
        print("Location Stop")
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnTreeName(_ sender: Any) {
        self.view.endEditing(true)
        if(self.ary_of_TreeName.count == 0){
             call_AboutTreeList_API()
        }else{
            self.getAbouttreeDataFromLocal(opentag: 1)
        }
       
    }
    
   
    @IBAction func actionOnSubmit(_ sender: Any) {
        if validation() {
            if(self.imageName != ""){
                self.uploadImage()
            }else {
                self.CallTreeCuttingComplainAPI()
            }
        }
    }
    @IBAction func actionOnBrowse(_ sender: Any) {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let CameraAction = UIAlertAction(title: "Camera", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let GalleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(GalleryAction)
        optionMenu.addAction(CameraAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)

    }
    
    // MARK: - Validation
    // MARK: -

    func validation() -> Bool {
        if(txtName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTreeCuttingName, viewcontrol: self)
            return false
        }
        else if (txtContactNumber.text?.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginMobile, viewcontrol: self)
            return false
        } else if (txtContactNumber.text!.count <= 9) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginValidMobile, viewcontrol: self)
            return false
        } else if (txtTreeName.text?.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Name, viewcontrol: self)
            return false
        }else if (txtNoOfTree.text?.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertnNoOfTree, viewcontrol: self)
            return false
        }else if (txtViewLocation.text?.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Location, viewcontrol: self)
            return false
        }else if (txtViewDescription.text?.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStatusDescription, viewcontrol: self)
            return false
        }
        else if (self.imageName.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTreeImage, viewcontrol: self)
            return false
        }
        else{
            return true
        }
    }
    // MARK: - ---------------Other Function's
    // MARK: -
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "You don't have camera", viewcontrol: self)
        }
    }
    
    func openGallary()
    {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK:- --------------- API CAlling
    //MARK:
    
    func uploadImage() {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURLImageUPLOADTreeCutting, image: self.imageForComplain.image!, imageName: imageName, OnResultBlock: { (responce, status) in
                if (status == "success"){
                    if (responce.value(forKey: "message")as! String == "SUCCESS"){
                        self.CallTreeCuttingComplainAPI()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func CallTreeCuttingComplainAPI() {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                let AddTreeCuttingComplaint = "\(URL_AddTreeCuttingComplaint)Cutting_Complaint_Id=0&ComplainantName=\(self.txtName.text!)&ComplainantMobileNo=\(txtContactNumber.text!)&PlantName=\(txtTreeName.text!)&NoOfTree=\(txtNoOfTree.text!)&Description=\(txtViewDescription.text!)&Lat=\(self.strLatitude)&Long=\(self.strLongitude)&Location=\(txtViewLocation.text!)&Attachment=\(imageName)"
            
          
            let dictData = NSMutableDictionary()
            dictData.setValue("0", forKey: "Cutting_Complaint_Id")
            dictData.setValue("\(self.txtName.text!)", forKey: "ComplainantName")
            dictData.setValue("\(self.txtContactNumber.text!)", forKey: "ComplainantMobileNo")
            dictData.setValue("\(self.txtTreeName.text!)", forKey: "PlantName")
            dictData.setValue("\(self.txtNoOfTree.text!)", forKey: "NoOfTree")
            dictData.setValue("\(self.txtViewDescription.text!)", forKey: "Description")
            dictData.setValue("\(self.strLatitude)", forKey: "Lat")
            dictData.setValue("\(self.strLongitude)", forKey: "Long")
            dictData.setValue("\(self.txtViewLocation.text!)", forKey: "Location")
            dictData.setValue("\(self.imageName)", forKey: "Attachment")
            
                print(AddTreeCuttingComplaint)
                WebService.callAPIBYPOST(parameter: dictData, url: URL_AddTreeCuttingComplaint, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationPlantStatus"), object: nil)
                            let alert = UIAlertController(title:alertInfo, message: "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
        }
    }
    
    func call_AboutTreeList_API()  {
        if !(isInternetAvailable()){
            getAbouttreeDataFromLocal(opentag: 1)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetPlantList, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"AboutTree")
                        saveDataInLocalArray(strEntity: "AboutTree", strKey: "abouttree", data: (dict.value(forKey: "PlantList")as! NSArray).mutableCopy() as! NSMutableArray)
                        self.getAbouttreeDataFromLocal(opentag: 1)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            //     }
        }
    }
    //MARK:- --------------getAbouttreeDataFromLocal
    //MARK:
    
    func getAbouttreeDataFromLocal(opentag : Int)   {
        let aryTemp = getDataFromLocal(strEntity: "AboutTree", strkey: "abouttree")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "abouttree") ?? 0)
            }
        }
        print(aryList)
        
      
            if aryList.count != 0 {
                self.ary_of_TreeName = NSMutableArray()
                self.ary_of_TreeName = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
                if(opentag == 1){
                let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
                vc.strTitle = "Select  Tree Name"
                vc.strTag = 12
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.delegate = self
                vc.aryTBL =  self.ary_of_TreeName
                self.present(vc, animated: false, completion: {})
            }
        }
    }
    
    // MARK: - ----------------getCurrentLocation
    // MARK: -

    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension TreeCuttingReportVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageForComplain.image = pickedImage
            self.imageName = getUniqueString()
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  false, completion: nil)
    }
}
// MARK: - ----------------Selection Delegate
// MARK: -
extension TreeCuttingReportVC: CommonTableDelegate {
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        
    }
    
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
        txtTreeName.text = dictData.value(forKey: "Plant_Name")as? String
        txtTreeName.tag = Int ("\(dictData.value(forKey: "Plant_Id")!)")!
       
    }
}

// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension TreeCuttingReportVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtContactNumber {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if textField == txtNoOfTree  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 3)
        }
        if (textField == txtName) || (textField == txtOtherName) {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 30)
        }
       return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate  Methods----------

extension TreeCuttingReportVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        self.strLatitude = String(userLocation.coordinate.latitude)
        self.strLongitude = String(userLocation.coordinate.longitude)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if(placemarks != nil){
                let placemark = placemarks! as [CLPlacemark]
                if placemark.count>0{
                    let placemark = placemarks![0]
                    var strAddress = ""
                    if(placemark.locality != nil){
                        strAddress = "\(placemark.subLocality!), "
                    }
                    if(placemark.administrativeArea != nil){
                        strAddress =  strAddress + "\(placemark.administrativeArea!), "
                    }
                    if(placemark.country != nil){
                        strAddress =  strAddress + "\(placemark.country!)"
                    }
                    if(placemark.postalCode != nil){
                        strAddress =  strAddress + "\n\(placemark.postalCode!)"
                    }
                    self.txtViewLocation.text = strAddress
                    self.locationManager.stopUpdatingLocation()
                }
            }
            
        }
    }
}
