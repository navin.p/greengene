//
//  FatchQRCodeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 2/13/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class FatchQRCodeVC: UIViewController {
    
    @IBOutlet weak var img_Plant: UIImageView!
    @IBOutlet weak var lblQrCode: UILabel!
    @IBOutlet weak var lblQRCodeStatus: UILabel!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!
    @IBOutlet weak var txtEnterQRCode: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_FindTreeQRCodeAPI()  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            if (loginDict.count != 0){
                    let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                
                let urlTreeDetail = URL_GetQRCode + "\((dict.value(forKey: "User_Id")!))&Donated_Plant_Id=\(txtEnterQRCode.text!)"
                print(urlTreeDetail)
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                WebService.callAPIBYGET(parameter: NSDictionary(), url: urlTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                          self.lblQrCode.text = "QR-Code \((dict.value(forKey: "QRCodeDetail")as! NSDictionary).value(forKey: "Donated_Plant_Id")!)"
                           let status = "\((dict.value(forKey: "QRCodeDetail")as! NSDictionary).value(forKey: "IsTagged")!)"
                            let strImageUrl = "\(BaseURLQrcodeImageDownload)\((dict.value(forKey: "QRCodeDetail")as! NSDictionary).value(forKey: "QRCode")!).png"
                            if(strImageUrl.count != 0){
                                self.img_Plant.setImageWith(URL(string: "\(strImageUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
                                    self.img_Plant.isUserInteractionEnabled = true
                                    self.img_Plant.addGestureRecognizer(tapGestureRecognizer)
                                }, usingActivityIndicatorStyle: .gray)
                            }else{
                                self.img_Plant.image = #imageLiteral(resourceName: "default_img")
                            }
                            
                            if status == "1"{
                                self.lblQRCodeStatus.text = "Tree Status : Tagged"
                                 self.lblQRCodeStatus.textColor = hexStringToUIColor(hex: colorGreenPrimary)
                            }else{
                                self.lblQRCodeStatus.text = "Tree Status : UnTagged"
                                self.lblQRCodeStatus.textColor = UIColor.darkGray
                            }
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                            
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
    @objc func connected(_ sender:AnyObject){
        print("yo tap image number : \(sender.view.tag)")
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.img_Plant.image!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:
//MARK: UITextFieldDelegate
extension FatchQRCodeVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(txtEnterQRCode.text?.count != 0){
            self.call_FindTreeQRCodeAPI()
        }else{
           
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(txtEnterQRCode.text?.count != 0){
            self.call_FindTreeQRCodeAPI()
        }else{
         
        }
    }
}
