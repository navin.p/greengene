//
//  AppDelegate.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/25/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManager
import UserNotifications
import Firebase
import UserNotifications
import FirebaseMessaging
import Alamofire

let appd = UIApplication.shared.delegate as! AppDelegate

let isFlaseDashboard = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let notificationCenter = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow:0.5) as Date)
        IQKeyboardManager.shared().isEnabled = true;
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        } else {
            let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        }
        if DeviceType.IS_IPAD {
            mainStoryboard = UIStoryboard.init(name: "Storyboard_iPad", bundle: nil)
        }else{
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }
        
        FTIndicator.setIndicatorStyle(UIBlurEffectStyle.dark)
        
        
        //1 FireBase & Register Push Notification
        FirebaseApp.configure()
        self.registerForRemoteNotifications()
        
        gotoViewController()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
      
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "GreenGene")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    func gotoViewController()  {
        if (nsud.value(forKey: "loginRemberstatus") != nil) {
           // if (nsud.value(forKey: "loginRemberstatus")as! String == "True") {
                let obj_ISVC : DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }else{
                let obj_ISVC : WelComeVC = mainStoryboard.instantiateViewController(withIdentifier: "WelComeVC") as! WelComeVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }
      //  }else{
      //      let obj_ISVC : WelComeVC = mainStoryboard.instantiateViewController(withIdentifier: "WelComeVC") as! WelComeVC
      //      let navigationController = UINavigationController(rootViewController: obj_ISVC)
      //      rootViewController(nav: navigationController)
       // }
        
    }
//    func rootViewController(nav : UINavigationController)  {
//        self.window?.rootViewController = nav
//        self.window?.makeKeyAndVisible()
//        nav.setNavigationBarHidden(true, animated: true)
//    }
    
    
    func rootViewController(nav : UINavigationController)  {
        UIApplication.shared.windows.first?.rootViewController = nav
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        nav.navigationController?.isNavigationBarHidden = true
        nav.setNavigationBarHidden(true, animated: false)
    }
    
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}

// MARK: --------UNNotification/Messaging Delegate Method----------
extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate{
    //MARK: RegisterPushNotification
    func registerForRemoteNotifications()  {
        if #available(iOS 10, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if granted == true
                {
                    print("Notification Allow")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }                                                                                        
                    Messaging.messaging().delegate = self
                }
                else
                {
                    print("Notification Don't Allow")
                }
            }
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 1
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let state: UIApplication.State = UIApplication.shared.applicationState
        if state == .active {
                        let dict = (userInfo as AnyObject)as! NSDictionary
                        let aps = (dict.value(forKey: "aps")as! NSDictionary)
                        let alert = (aps.value(forKey: "alert")as! NSDictionary)
                        let strTitle = (alert.value(forKey: "title"))
                        let strbody = (alert.value(forKey: "body"))
            FTIndicator.showNotification(with: UIImage(named: "logo"), title: strTitle as? String, message: strbody as? String, autoDismiss: true, tapHandler: {
                 self.hendelNotidata(userInfo: userInfo as NSDictionary, activeStatus: false)
            }) {
                
            }
        }
        else  {
            hendelNotidata(userInfo: userInfo as NSDictionary, activeStatus: false)
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let dictApsData : NSDictionary = response.notification.request.content.userInfo as NSDictionary
        hendelNotidata(userInfo: dictApsData, activeStatus: false)
    }
    
    func hendelNotidata(userInfo :NSDictionary , activeStatus : Bool){
        
        let obj_ISVC : DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        
        obj_ISVC.dictNotificationData = userInfo.mutableCopy()as! NSMutableDictionary
        obj_ISVC.activeStatus = activeStatus
        
        let navigationController = UINavigationController(rootViewController: obj_ISVC)
        rootViewController(nav: navigationController)
    }
    
    
    // MARK: ---------GET TOKEN Func----------
    // MARK: -
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "greenGENE_FCM_Token")
        nsud.setValue("greenGENE5445", forKey: "greenGENE_DEVICE_ID")
        nsud.synchronize()
    }
}

