//
//  CareTakerDetailVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/2/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
class CareTakerDetailVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblPlantName: UILabel!
    @IBOutlet weak var btnUnderMaintainance: UIButton!

    var aryListDetail = NSMutableArray()
    var dictData = NSMutableDictionary()
    var indexP = Int()

    var strPlantName = String()
    var strDonated_Plant_Id = String()
    let refreshControl = UIRefreshControl()
   
    

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        btnUnderMaintainance.tag = 10
       //btnUnderMaintainance.isUserInteractionEnabled = false
        if #available(iOS 10.0, *) {
            tvlist.refreshControl = refreshControl
        } else {
            tvlist.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "NotificationPlantStatus"), object: nil)

        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        lblPlantName.textColor = hexStringToUIColor(hex: colorGreenPrimary)

        btnUnderMaintainance.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
         // btnUnderMaintainance.layer.cornerRadius = 8.0
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
        if dictData.value(forKey: "PlantMasterDc") is String {
            FTIndicator.showToastMessage("Plant Status Not Available!")
        }else{
            self.aryListDetail = (dictData.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
        }
        self.lblPlantName.text = "Plant Name : \(strPlantName)"
        if (aryListDetail.count == 0){
            self.tvlist.isHidden = true
            self.lblError.text = alertDataNotFound
            self.imgError.image = #imageLiteral(resourceName: "notfound")
        }else{
            let dict = removeNullFromDict(dict: (aryListDetail.object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            if "\(dict.value(forKey: "Created_Date")!)" != "" {
                var fullNameArr = "\(dict.value(forKey: "Created_Date")!)".components(separatedBy: ".")
                let strFirst = fullNameArr[0] // First
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let serverDate = dateFormatter.date(from: strFirst)!
                let date2 = Date()
                let dateFormatter2 = DateFormatter()
                dateFormatter2.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let myString = dateFormatter2.string(from: date2)
                let cureentDate: Date? = dateFormatter2.date(from: myString)
                let next15days = serverDate.addingTimeInterval(TimeInterval(treeUpdateTime*60*60*24))
                print(serverDate)
                print(next15days)
                if(next15days == cureentDate! || next15days < cureentDate!){
                    btnUnderMaintainance.tag = 0
                }else{
                    btnUnderMaintainance.tag = 10

                }
                //   let currentDate = dateFormatter2.date(from: date2)
            }
            
            self.tvlist.reloadData()
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnUnderMaintainance.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.8) {
            self.btnUnderMaintainance.transform = CGAffineTransform.identity
        }
    }
    
    // MARK: - -------------Other Function
    // MARK: -
    @objc private func refreshData(_ sender: Any) {
        self.call_CareTakerList_API()
    }
    @objc func methodOfReceivedNotification(notification: Notification){
        call_CareTakerList_API() 
    }
    func setDataOnView()  {
        btnUnderMaintainance.tag = 10
        dictData = NSMutableDictionary()
        dictData = removeNullFromDict(dict: (self.aryListDetail.object(at: indexP)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        print(dictData)
        
        if dictData.value(forKey: "PlantMasterDc") is String {
            FTIndicator.showToastMessage("Plant Status Not Available!")
        }else{
       
            self.aryListDetail = (dictData.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
        }
        if (aryListDetail.count == 0){
            self.tvlist.isHidden = true
            self.lblError.text = alertDataNotFound
            self.imgError.image = #imageLiteral(resourceName: "notfound")
        }else{
                      let dict = removeNullFromDict(dict: (aryListDetail.object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            if "\(dict.value(forKey: "Created_Date")!)" != "" {
                var fullNameArr = "\(dict.value(forKey: "Created_Date")!)".components(separatedBy: ".")
                let strFirst = fullNameArr[0] // First
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let serverDate = dateFormatter.date(from: strFirst)!
                let date2 = Date()
                let dateFormatter2 = DateFormatter()
                dateFormatter2.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let myString = dateFormatter2.string(from: date2)
                let cureentDate: Date? = dateFormatter2.date(from: myString)
                let next15days = serverDate.addingTimeInterval(TimeInterval(treeUpdateTime*60*60*24))
               print(serverDate)
                print(next15days)
                if(next15days == cureentDate! || next15days < cureentDate!){
                    btnUnderMaintainance.tag = 0
                }else{
                    btnUnderMaintainance.tag = 10
                }
             //   let currentDate = dateFormatter2.date(from: date2)
            }
            self.tvlist.reloadData()
        }
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_CareTakerList_API()  {
        if !(isInternetAvailable()){
            self.refreshControl.endRefreshing()
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let GetCareTaker = "\(URL_GetCareTaker)\(dict.value(forKey: "User_Id")!)"
                print(GetCareTaker)
                if aryListDetail.count == 0{
                    FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                }
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetCareTaker, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                     self.refreshControl.endRefreshing()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            deleteAllRecords(strEntity:"CaretakerList")
                            saveDataInLocalArray(strEntity: "CaretakerList", strKey: "caretakerList", data: (dict.value(forKey: "PlantDonationList")as! NSArray).mutableCopy() as! NSMutableArray)
                            self.getCareTakerDataFromLocal()
                            self.setDataOnView()
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
    // MARK: - ---------------GetDataFromLocal
    // MARK: -
    func getCareTakerDataFromLocal()   {
        let aryTemp = getDataFromLocal(strEntity: "CaretakerList", strkey: "caretakerList")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "caretakerList") ?? 0)
            }
        }
        
        print(aryList)
        if aryList.count != 0 {
            self.aryListDetail = NSMutableArray()
            self.aryListDetail = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
        }else{
            if !(isInternetAvailable()){
                self.tvlist.isHidden = true
                self.lblError.text = alertInternet
                self.imgError.image = #imageLiteral(resourceName: "no-wifi")
            }else{
                self.tvlist.isHidden = true
                self.lblError.text = alertDataNotFound
                self.imgError.image = #imageLiteral(resourceName: "notfound")
            }
        }
        
        
    }
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func actionONUnderMaintainance(_ sender: UIButton) {
        
        if sender.tag == 10 {
            FTIndicator.showToastMessage(alertnUnderMaintainance)
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "UnderMainTainanceVC")as! UnderMainTainanceVC
            testController.strDonated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id")!)"
            testController.strPlantID = "\(dictData.value(forKey: "Plant_Id")!)"
            testController.strPlant_Name = "\(dictData.value(forKey: "Plant_Name")!)"

            self.navigationController?.pushViewController(testController, animated: true)
        }
    
        
    }
    @objc func dismissFullscreenImage(_ sender: UIButton) {
        print(sender.tag)
        let imgView = UIImageView()
        let dict = removeNullFromDict(dict: (aryListDetail.object(at: (sender.tag))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
        imgView.image = #imageLiteral(resourceName: "default_img")
        if(strProfileUrl.count != 0){
            imgView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
        }
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = imgView.image!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CareTakerDetailVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return 10
        return aryListDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "TreeListDetail", for: indexPath as IndexPath) as! CommonTableCell
        
        let dict = removeNullFromDict(dict: (aryListDetail.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        print(dict)
        var strCare_Taker = "\(dict.value(forKey: "Care_Taker")!)"
        if strCare_Taker == "" {
            strCare_Taker = "Not Available"
        }
        var strCare_Taker_MobileNo = "\(dict.value(forKey: "Care_Taker_MobileNo")!)"
        if strCare_Taker_MobileNo == "" {
            strCare_Taker_MobileNo = "Not Available"
        }
        
        var strPlant_Status_Description = "\(dict.value(forKey: "Plant_Status_Description")!)"
        if strPlant_Status_Description == "" {
            strPlant_Status_Description = "Not Available"
        }
        var strIsTrim = "\(dict.value(forKey: "IsTrim")!)"
        if strIsTrim == "" {
            strIsTrim = "NO"
        }else{
            
            if strIsTrim == "0" {
                strIsTrim = "NO"
            }else{
                strIsTrim = "YES"
            }
            
        }
        var strIsFertilizer = "\(dict.value(forKey: "IsFertilizer")!)"
        if strIsFertilizer == "" {
            strIsFertilizer = "NO"
        }else{
            if strIsFertilizer == "0" {
                strIsFertilizer = "NO"
            }else{
                strIsFertilizer = "YES"
            }
        }
        
        var strIsPestControl = "\(dict.value(forKey: "IsPestControl")!)"
        if strIsPestControl == "" {
            strIsPestControl = "NO"
        }else{
            if strIsPestControl == "0" {
                strIsPestControl = "NO"
            }else{
                strIsPestControl = "YES"
            }
        }
        let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
        if(strProfileUrl.count != 0){
            cell.treeDetail_Imge.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
        }
        
        cell.treeDetail_btnImg.tag = indexPath.row
        cell.treeDetail_btnImg.addTarget(self, action: #selector(dismissFullscreenImage(_:)), for: .touchUpInside)
        if(strCare_Taker == "" || strCare_Taker == "Not Available") && (strCare_Taker_MobileNo == "" || strCare_Taker_MobileNo == "Not Available"){
            cell.treeDetail_lblCaretaker.text = "Not Available"
        }else{
                cell.treeDetail_lblCaretaker.text = strCare_Taker
        }
        
        cell.treeDetail_lblDescription.text = "\(strPlant_Status_Description)"
        cell.treeDetail_lblIsPest.text = "\(strIsPestControl)"
        cell.treeDetail_lblIsTirm.text = "\(strIsTrim)"
        cell.treeDetail_lblIsFertilizer.text = "\(strIsFertilizer)"
        let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
        cell.treeDetail_lblDate.text = strDate
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}
