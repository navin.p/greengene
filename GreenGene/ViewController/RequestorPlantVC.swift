//
//  RequestorPlantVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/3/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class RequestorPlantVC: UIViewController {

    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNoofTree: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNoofPurchesTree: SkyFloatingLabelTextField!

    @IBOutlet weak var viewHeader: CardView!

    @IBOutlet weak var btnSubmit: UIButton!

    
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNoofTree.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 8.0
        loginDict = getLoginData()
        print(loginDict)
        if (loginDict.count != 0){
               let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let name = "\(dict.value(forKey: "Name")!)"
            let email = "\(dict.value(forKey: "Email_Id")!)"
            let mobile = "\(dict.value(forKey: "Mobile_Number")!)"
            let address = "\(dict.value(forKey: "Address")!)"
            txtName.text = name
            txtEmail.text = email
            txtAddress.text = address
            txtMobile.text = mobile
            if (txtAddress.text! == ""){
                txtAddress.isUserInteractionEnabled = true
            }else{
                txtAddress.isUserInteractionEnabled = false
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSubmit(_ sender: Any) {
        if  validation() {
            call_SubmitForRequestPlant()
        }
    }
    // MARK: - --------------API Calling
    // MARK: -

    func call_SubmitForRequestPlant()  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
         let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            
           let AddPlantDonationRequest = "\(URL_AddPlantDonationRequest)User_Id=\(dict.value(forKey: "User_Id")!)&No_Of_Plant=\(String(describing: txtNoofTree.text!))"

            
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: AddPlantDonationRequest, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                     
                        let alert = UIAlertController(title: alertInfo, message: dict.value(forKey: "Success")as? String, preferredStyle: UIAlertControllerStyle.alert)
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    // MARK: - ----------------Validation
    // MARK: -
    
    func validation() -> Bool {
        if txtName.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPFullName, viewcontrol: self)
            return false
        }else  if txtEmail.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPEmail, viewcontrol: self)
            return false
        }else  if txtMobile.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLoginMobile, viewcontrol: self)
            return false
        }
//        else  if txtAddress.text?.count == 0 {
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPAddress, viewcontrol: self)
//            return false
//        }
        else  if txtNoofTree.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertnNoOfTree, viewcontrol: self)
            return false
        }
        else  if txtNoofPurchesTree.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertnNoOfTreeGiftCard, viewcontrol: self)
            return false
        }else{
            return true
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension RequestorPlantVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtNoofTree {
            return  txtFiledValidation(textField: txtNoofTree, string: string, returnOnly: "NUMBER", limitValue: 3)
            
        }
        if textField == txtNoofPurchesTree {
            return  txtFiledValidation(textField: txtNoofPurchesTree, string: string, returnOnly: "NUMBER", limitValue: 3)
            
        }else{
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
