//
//  DonateVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/2/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class DonateVC: UIViewController {
    
    // MARK: - ----------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewheader: CardView!
    @IBOutlet weak var btnPreceed: UIButton!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!

    @IBOutlet weak var txtContributedTo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!

    // MARK: - ----------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtContributedTo.text = "\(appName) - CitizenCOP Foundation"
        let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
         txtName.text =  "\(dict.value(forKey: "Name")!)"
        txtContributedTo.isUserInteractionEnabled = false
        txtName.isUserInteractionEnabled = false
        viewheader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
       // btnPreceed.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnPreceed.layer.cornerRadius = 15
        btn1.layer.cornerRadius = 4.0
        btn1.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        btn1.layer.borderWidth = 1.0
        btn1.setTitle("\u{20B9} 11", for: .normal)
        btn2.layer.cornerRadius = 4.0
        btn2.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        btn2.layer.borderWidth = 1.0
        btn2.setTitle("\u{20B9} 51", for: .normal)

        btn3.layer.cornerRadius = 4.0
        btn3.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        btn3.layer.borderWidth = 1.0
        btn3.setTitle("\u{20B9} 151", for: .normal)

        btn4.layer.cornerRadius = 4.0
        btn4.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        btn4.layer.borderWidth = 1.0
        btn4.setTitle("\u{20B9} 551", for: .normal)
        
        txtAmount.placeholder = "\u{20B9} Amount "
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnTerms(_ sender: Any) {
       
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
        testController.strViewComeFrom = "Terms & Conditions  "
        testController.strURL = Alert_Donate
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actionONDetail(_ sender: Any) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
        testController.strViewComeFrom = appName
        testController.strURL = URL_Paytmtermconditions
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actionONProceed(_ sender: Any) {
        if(txtAmount.text! == "0" || txtAmount.text! == ""){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_DonateMinimum, viewcontrol: self)
        }else{
            createCheckSumHash(strAmount: "\((txtAmount.text! as NSString).doubleValue)", tagForPayType: "")
        }
    }
    
    @IBAction func actionONFast(_ sender: UIButton) {
        txtAmount.text = "\(sender.titleLabel?.text! ?? "")".replacingOccurrences(of: "\u{20B9} ", with: "")
    }
   


    //MARK:-----
    //MARK:
    func donateAPICall(dictPaymentData : NSMutableDictionary){
        
        var strUserID = "0"
        if (loginDict.count != 0){
            let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            strUserID = "\(dict.value(forKey: "User_Id")!)"
        }
        var dictSendData = NSMutableDictionary()
        dictSendData.setValue(strUserID, forKey: "User_Id")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "TXNAMOUNT")!)", forKey: "Total_Amount")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "TXNID")!)", forKey: "PayTM_TranjectionId")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "STATUS")!)", forKey: "Status")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "ORDERID")!)", forKey: "OrderId")
        let date = dateStringToFormatedDateString(dateToConvert: "\(dictPaymentData.value(forKey: "TXNDATE")!)", dateFormat: "MM/dd/yyyy")
        dictSendData.setValue(date, forKey: "TranjectionDate")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "BANKNAME")!)", forKey: "BankName")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "GATEWAYNAME")!)", forKey: "GatewayName")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "PAYMENTMODE")!)", forKey: "PaymentMode")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "CURRENCY")!)", forKey: "Currency")
        dictSendData.setValue("\(dictPaymentData.value(forKey: "CHECKSUMHASH")!)", forKey: "CheckSum")
        var json1 = Data()
        var jsonAdd = NSString()
        
        if JSONSerialization.isValidJSONObject(dictSendData) {
            // Serialize the dictionary
            json1 = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
            jsonAdd = String(data: json1, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonAdd)")
        }
        if(dictSendData.count == 0){
            jsonAdd = ""
        }
        dictSendData = NSMutableDictionary()
        dictSendData.setValue(jsonAdd, forKey: "DonationPaymentHistoryDc")
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentProcessVC")as! PaymentProcessVC
        testController.dictData = dictSendData
        testController.strAPIName = URL_Donation
        testController.aryImages = NSMutableArray()
        self.navigationController?.pushViewController(testController, animated: false)
    }
}

// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension DonateVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtAmount {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 15)
        }
       
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

// MARK: - ---------------IBAction
// MARK: -
extension DonateVC : PGTransactionDelegate{
    
    func createCheckSumHash(strAmount : String , tagForPayType : String) {
        let dictLogin = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmSSS"
        let result = formatter.string(from: date)
        let orderIdUnique="Order_iOS_\(dictLogin.value(forKey: "User_Id")!)_\(result)";
        let dict = NSMutableDictionary()
        
        
        if("\(dictLogin.value(forKey: "User_Id")!)" == "93"){
            
            dict.setValue("Citize73156154292382", forKey: "MID")
            dict.setValue(orderIdUnique, forKey: "ORDER_ID")
            dict.setValue("\(dictLogin.value(forKey: "User_Id")!)", forKey: "CUST_ID")
            dict.setValue("Retail", forKey: "INDUSTRY_TYPE_ID")
            dict.setValue("WAP", forKey: "CHANNEL_ID")
            dict.setValue(strAmount, forKey: "TXN_AMOUNT")
            dict.setValue("Website", forKey: "WEBSITE")
            dict.setValue("https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIdUnique)", forKey: "CALLBACK_URL")
            
        }else{
            
            
            dict.setValue(MerchantID, forKey: "MID")
            dict.setValue(orderIdUnique, forKey: "ORDER_ID")
            dict.setValue("\(dictLogin.value(forKey: "User_Id")!)", forKey: "CUST_ID")
            dict.setValue("\(IndustryID)", forKey: "INDUSTRY_TYPE_ID")
            dict.setValue("\(ChannelID)", forKey: "CHANNEL_ID")
            if("\(dictLogin.value(forKey: "User_Id")!)" == "90"){
                dict.setValue("1.00", forKey: "TXN_AMOUNT")
            }else{
                dict.setValue(strAmount, forKey: "TXN_AMOUNT")
            }
            
            dict.setValue("\(Website)", forKey: "WEBSITE")
            dict.setValue("https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIdUnique)", forKey: "CALLBACK_URL")
        }
        
        
        print(dict)
        
        
        WebService.callAPIBYPOST(parameter: dict, url: "\(GENERATE_CHECKSUM)") { (responce, status) in
            
            if(status == "success"){
                let dictData = responce.value(forKey: "data")as! NSDictionary
                let strCheckSum = "\(dictData.value(forKey: "CHECKSUMHASH")!)"
                print(strCheckSum)
                self.createPayment(strAmount: strAmount, checkSumHashh: strCheckSum, orderidNew: orderIdUnique)
            }else{
                let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func createPayment(strAmount : String , checkSumHashh : String , orderidNew : String){
        var orderDict = [String : String]()
        let dictLoginData = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
        if("\(dictLoginData.value(forKey: "User_Id")!)" == "93"){
            
            
            orderDict["MID"] = "Citize73156154292382";
            orderDict["CHANNEL_ID"] = "WAP";
            orderDict["INDUSTRY_TYPE_ID"] = "Retail";
            orderDict["WEBSITE"] = "Website";
            orderDict["TXN_AMOUNT"] = "\(strAmount)"
            orderDict["ORDER_ID"] = "\(orderidNew)";
            orderDict["CUST_ID"] = "\((String(describing: dictLoginData.value(forKey: "User_Id")!)))";
            orderDict["CHECKSUMHASH"] = "\(checkSumHashh)";
            orderDict["CALLBACK_URL"] = "\(callBackURL)\(orderidNew)"
            
            print(orderDict)
        }else{
            orderDict["MID"] = "\(MerchantID)";
            orderDict["CHANNEL_ID"] = "\(ChannelID)";
            orderDict["INDUSTRY_TYPE_ID"] = "\(IndustryID)";
            orderDict["WEBSITE"] = "\(Website)";
            if("\(dictLoginData.value(forKey: "User_Id")!)" == "90"){
                orderDict["TXN_AMOUNT"] = "1.00"
            }else{
                orderDict["TXN_AMOUNT"] = "\(strAmount)"
            }
            
            
            orderDict["ORDER_ID"] = "\(orderidNew)";
            orderDict["CUST_ID"] = "\((String(describing: dictLoginData.value(forKey: "User_Id")!)))";
            orderDict["CALLBACK_URL"] = "\(callBackURL)\(orderidNew)"
            orderDict["CHECKSUMHASH"] = "\(checkSumHashh)";
            print(orderDict)
        }
        let pgOrder = PGOrder(params: orderDict )
        let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
        if(type_Production_Staging == "Staging"){
            transaction!.serverType = eServerTypeStaging
        }else{
            if("\(dictLoginData.value(forKey: "User_Id")!)" == "93"){
                transaction!.serverType = eServerTypeStaging
            }else{
                transaction!.serverType = eServerTypeProduction
            }
        }
        transaction!.merchant = PGMerchantConfiguration.default()
        transaction!.loggingEnabled = true
        transaction!.delegate = self
        
        if responds(to: #selector(setter: self.edgesForExtendedLayout)) {
            let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 64.0))
            headerView.backgroundColor = UIColor.clear
            let topBar = UIView(frame: CGRect(x: 0.0, y: 20.0, width: self.view.frame.width, height: 50.0))
            topBar.backgroundColor = UIColor.clear
            headerView.addSubview(topBar)
            if(type_Production_Staging == "Staging"){
                headerView.addSubview(topBar)
                
            }
            let cancelButton = UIButton(type: .system)
            cancelButton.setTitle("Back", for: .normal)
            cancelButton.frame = CGRect(x: 8.0, y: 25.0, width: 40.0, height: 40.0)
            cancelButton.tintColor = UIColor.lightGray
            transaction?.cancelButton = cancelButton
        }else{
            let topBar = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 50.0))
            topBar.backgroundColor = UIColor.clear
            if(type_Production_Staging == "Staging"){
                transaction?.topBar = topBar
            }
            let cancelButton = UIButton(type: .system)
            cancelButton.frame = CGRect(x: 8.0, y: 5.0, width: 40.0, height: 40.0)
            cancelButton.setTitle("Back", for: .normal)
            cancelButton.tintColor = UIColor.lightGray
            
            transaction?.cancelButton = cancelButton
        }
        self.navigationController?.pushViewController(transaction!, animated: true)
    }
    
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        print(responds)
        
        self.navigationController?.popViewController(animated: false)
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    let status = jsonresponse["STATUS"]as! String
                    if(status == "TXN_SUCCESS"){
                        self.donateAPICall(dictPaymentData: (jsonresponse as NSDictionary).mutableCopy()as! NSMutableDictionary)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            } catch {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        
        self.navigationController?.popViewController(animated: false)
        
        
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        controller.view.isUserInteractionEnabled = false
        
        
        print("error : ",error)
        self.navigationController?.popViewController(animated: false)
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
        
        
    }
    
    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        controller.view.isUserInteractionEnabled = false
        
        print(response)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        controller.view.isUserInteractionEnabled = false
        
        print("error : ",error)
        print("didFailTransaction : ",response)
        self.navigationController?.popViewController(animated: false)
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "FailTransaction", viewcontrol: self)
        
    }
    
    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        print("error : ",error)
        print("didCancelTransaction : ",response)
        
    }
    //
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print("didFinishCASTransaction : ",response)
        //  showAlert(controller: controller, title: "cas", message: "")
    }
    
    
}
