//
//  ASaCaretakerVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/30/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class ASaCaretakerVC: UIViewController {

    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewSearch: CardView!
    var aryCaretakerList = NSMutableArray()
    var arytvlist = NSMutableArray()
    let refreshControl = UIRefreshControl()
    
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
           NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "NotificationPlantStatus"), object: nil)
      viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewSearch.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)

        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
        self.call_CareTakerList_API(txtafterUpdate: "")
        if #available(iOS 10.0, *) {
            tvlist.refreshControl = refreshControl
        } else {
            tvlist.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)


    }
    @objc private func refreshData(_ sender: Any) {
        txtSearch.text = ""
     
        tvlist.tag = 1
        self.call_CareTakerList_API(txtafterUpdate: "")
    }
    @objc func methodOfReceivedNotification(notification: Notification){
        call_CareTakerList_API(txtafterUpdate: "")
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_CareTakerList_API(txtafterUpdate : String)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
            refreshControl.endRefreshing()
            getNotiFicationDataFromLocal()
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                
                let dictData = NSMutableDictionary()
                dictData.setValue("0", forKey: "User_Id")
                dictData.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "CareTakerId")
                if(self.tvlist.tag == 1){
                    dictData.setValue("0", forKey: "Skip")
                }else{
                    dictData.setValue("\(self.arytvlist.count)", forKey: "Skip")
                }
                dictData.setValue("20", forKey: "Take")
                if(txtafterUpdate == ""){
                    dictData.setValue(txtafterUpdate, forKey: "CommonSearch")
                }else{
                    dictData.setValue(txtSearch.text!, forKey: "CommonSearch")
                }
                let aryOutboxSync = NSMutableArray()
                aryOutboxSync.add(dictData)
                let strJson = jsontoString(fromobject: aryOutboxSync)
                let GetMyTreeDetail = URL_GetMyTreeByUserIdLezyLoading + "\(strJson!)"
                print(GetMyTreeDetail)
                
                if aryCaretakerList.count == 0{
                    if(tvlist.tag != 1){
                           FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                    }
                 
                }
           
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    self.refreshControl.endRefreshing()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                                print(self.arytvlist.count)
                            
                            if(self.tvlist.tag == 1){
                                self.arytvlist = NSMutableArray()
                                self.aryCaretakerList = NSMutableArray()
                                deleteAllRecords(strEntity:"CaretakerList")
                                saveDataInLocalArray(strEntity: "CaretakerList", strKey: "caretakerList", data: (dict.value(forKey: "MyTreeList")as! NSArray).mutableCopy() as! NSMutableArray)
                            }else{
                                for item in  (dict.value(forKey: "MyTreeList")as! NSArray){
                                    self.arytvlist.add(item)
                                }
                                deleteAllRecords(strEntity:"CaretakerList")
                                saveDataInLocalArray(strEntity: "CaretakerList", strKey: "caretakerList", data: (self.arytvlist))
                            }
                            self.tvlist.tag = 0
                            self.getNotiFicationDataFromLocal()
                        }else{
                            self.tvlist.tableFooterView?.isHidden = true

                                if (self.arytvlist.count == 0){
                            self.tvlist.isHidden = true
                            self.lblError.text = "\(dict.value(forKey: "Success")as! String)"
                                    self.imgError.image = #imageLiteral(resourceName: "notfound")
                                    
                            }
                        }
                    }else{
                        self.tvlist.isHidden = true
                        self.lblError.text = alertSomeError
                        self.imgError.image = #imageLiteral(resourceName: "notfound")
                    }
                })
            }
        }
    }
    // MARK: - ---------------GetDataFromLocal
    // MARK: -
    func getNotiFicationDataFromLocal()   {
        let aryTemp = getDataFromLocal(strEntity: "CaretakerList", strkey: "caretakerList")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "caretakerList") ?? 0)
            }
        }
        print(aryList)
        let aryTempFinal = NSMutableArray()
        if aryList.count != 0 {
            for item in aryList.object(at: 0)as! NSArray{
                let dict  = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let donated_Plant_Id = "\(dict.value(forKey: "Donated_Plant_Id")!)"
                dict.setValue(donated_Plant_Id, forKey: "Donated_Plant_Id")
               aryTempFinal.add(dict)
            }
            
            
            self.aryCaretakerList = NSMutableArray()
            self.aryCaretakerList = aryTempFinal
            self.arytvlist = NSMutableArray()
            self.arytvlist = aryCaretakerList
            self.tvlist.isHidden = false

            self.tvlist.reloadData()
        }else{
            if !(isInternetAvailable()){
                self.tvlist.isHidden = true
                self.lblError.text = alertInternet
                self.imgError.image = #imageLiteral(resourceName: "no-wifi")
            }else{
                self.tvlist.isHidden = true
                self.lblError.text = alertDataNotFound
                self.imgError.image = #imageLiteral(resourceName: "notfound")
            }
        }
    }
    // MARK: - --------------Extra Function / Action on Table
    // MARK: -
    @objc func gotoMapDirection(_ sender: UIButton) {
        openMap(strTitle: (sender.titleLabel?.text!)!, strlat: "22.7533", strLong: "75.8937")
    }
    // MARK: - --------------Search By QRCode
    // MARK: -
    func searchAutocomplete_QRCode(Searching: NSString) -> Void {
        let resultPredicate = NSPredicate(format: "Donated_Plant_Id contains[c]%@ OR Plant_Name contains[c]%@ OR PlantedLocation contains[c]%@", Searching,Searching,Searching)
        if !(Searching.length == 0) {
            let arrayfilter = (aryCaretakerList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arytvlist = NSMutableArray()
            self.arytvlist = nsMutableArray.mutableCopy() as! NSMutableArray
            if arytvlist.count != 0 {
                self.tvlist.isHidden = false
                self.tvlist.reloadData()
            }else{
                if !(isInternetAvailable()){
                    self.tvlist.isHidden = true
                    self.lblError.text = alertInternet
                    self.imgError.image = #imageLiteral(resourceName: "no-wifi")
                }else{
                    self.tvlist.isHidden = true
                    self.lblError.text = alertDataNotFound
                    self.imgError.image = #imageLiteral(resourceName: "notfound")
                }
            }
        }
        else{
            self.arytvlist = NSMutableArray()
            self.arytvlist = aryCaretakerList.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
            if arytvlist.count != 0 {
                self.tvlist.reloadData()
            }else{
                if !(isInternetAvailable()){
                    self.tvlist.isHidden = true
                    self.lblError.text = alertInternet
                    self.imgError.image = #imageLiteral(resourceName: "no-wifi")
                }else{
                    self.tvlist.isHidden = true
                    self.lblError.text = alertDataNotFound
                    self.imgError.image = #imageLiteral(resourceName: "notfound")
                }
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ASaCaretakerVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        return arytvlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "CaretakerList", for: indexPath as IndexPath) as! CommonTableCell
        let dict = removeNullFromDict(dict: (self.arytvlist.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        print(dict)
        var strPlantName = "\(dict.value(forKey: "Plant_Name")!)"
        if strPlantName == "" {
            strPlantName = "Not Available"
        }
        cell.caretaker_lblPlantName.text = strPlantName
        
        
        var strOwnerName = "\(dict.value(forKey: "OwnerName")!)"
        var strOwnerNumber = "\(dict.value(forKey: "OwnerMobile_Number")!)"
        if strOwnerName == "" {
            strOwnerName = "Not Available"
        }
        if strOwnerNumber == "" {
            strOwnerNumber = "Not Available"
        }
        cell.caretaker_lblOwnerNumber.text = strOwnerNumber
        cell.caretaker_lblOwnerName.text = strOwnerName
        
        
        if dict.value(forKey: "DonatedPlantStatusDcs") is String {
            cell.caretaker_lblDate.text = " "

        }else{
            let ary =  (dict.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
            if ary.count != 0{
                
                let dict = removeNullFromDict(dict: (ary.object(at: (0))as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
                cell.caretaker_lblDate.text = strDate
            }
            
        }
        var strAddress = "\(dict.value(forKey: "PlantedLocation")!)"
        if strAddress == "" {
            strAddress = "Not Available"
        }
        var strQRCode = "\(dict.value(forKey: "Donated_Plant_Id")!)"
        if strQRCode == "" {
            strQRCode = "Not Available"
        }
        cell.caretaker_lblPlantLocation.text =  strAddress
        cell.caretaker_lblQRCode.text = strQRCode
        cell.caretaker_BtnMap.tag = indexPath.row
        cell.caretaker_BtnMap.setTitle(cell.caretaker_lblQRCode.text! + "\n" + cell.caretaker_lblPlantName.text! + "\n" + cell.caretaker_lblPlantLocation.text!, for: .normal)
        cell.caretaker_BtnMap.setTitleColor(UIColor.clear, for: .normal)
        cell.caretaker_BtnMap.addTarget(self, action: #selector(gotoMapDirection(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CareTakerDetailVC")as! CareTakerDetailVC
            let dict = removeNullFromDict(dict: (self.arytvlist.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            print(dict)
            
            if dict.value(forKey: "DonatedPlantStatusDcs") is String {
                FTIndicator.showToastMessage("Plant Status Not Available!")
            }else{
                testController.strPlantName = "\(dict.value(forKey: "Plant_Name")!)"
                testController.dictData = dict
                testController.indexP = indexPath.row
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
        if txtSearch.text == ""{
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if arytvlist.count > 10 {
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    spinner.startAnimating()
                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                    self.tvlist.tableFooterView = spinner
                    self.tvlist.tableFooterView?.isHidden = false
                }
            }
        }else{
            self.tvlist.tableFooterView?.isHidden = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height + 44
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom  < height {
            if txtSearch.text == ""{
                self.call_CareTakerList_API(txtafterUpdate: "")
                print(" you reached end of the table")
            }
        }
    }
}
//MARK: - UITextFieldDelegate
//MARK: -

extension ASaCaretakerVC: UITextFieldDelegate  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        if txtSearch == textField {
            if (txtAfterUpdate == "") {
                txtSearch.text = ""
                self.arytvlist = NSMutableArray()
                self.aryCaretakerList = NSMutableArray()
                self.call_CareTakerList_API(txtafterUpdate: "")
            }else{
                self.searchAutocomplete_QRCode(Searching: txtAfterUpdate)
            }

        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        if !(isInternetAvailable()){
            self.tvlist.isHidden = true
            self.lblError.text = alertInternet
            self.imgError.image = #imageLiteral(resourceName: "no-wifi")
        }else{
            self.arytvlist = NSMutableArray()
            self.aryCaretakerList = NSMutableArray()
            self.call_CareTakerList_API(txtafterUpdate: "no")
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.arytvlist = NSMutableArray()
        self.aryCaretakerList = NSMutableArray()
        self.call_CareTakerList_API(txtafterUpdate: "")
        
        return true
    }
}
