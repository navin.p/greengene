


//
//  DidYouKnowVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/16/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreLocation

//MARK: ---------------Protocol-----------------
protocol refreshDidYouKnowView : class{
    func refreshview(dictData : NSDictionary)
}
class DidYouKnowVC: UIViewController {
  
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnaddtree: UIButton!

    var aryDidYouList = NSMutableArray()
    let refreshControl = UIRefreshControl()
      var locationManager = CLLocationManager()
    
    // MARK: - -------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
           locationManager.requestWhenInUseAuthorization()
        btnaddtree.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        if #available(iOS 10.0, *) {
            tvlist.refreshControl = refreshControl
        } else {
            tvlist.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    
        let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
        self.call_GetTreeList_API(strFilterURL: "\(URL_GetHistoricalTree)Age=0&City_Id=\((dict.value(forKey: "City_Id")!))&Lat=0&Long=0&Radius=1000")
        tvlist.tableFooterView = UIView()
        lblError.text = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - --------------@IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnFilter(_ sender: Any) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "FilterDidYouKnowVC")as! FilterDidYouKnowVC
        testController.handleDidYouKnowView = self
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actionOnAddTree(_ sender: Any) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddtreeVC")as! AddtreeVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @objc private func refreshData(_ sender: Any) {
        let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
        self.call_GetTreeList_API(strFilterURL: "\(URL_GetHistoricalTree)Age=0&City_Id=\((dict.value(forKey: "City_Id")!))&Lat=0&Long=0&Radius=1000")
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_GetTreeList_API(strFilterURL : String)  {
            lblError.text = ""
        if !(isInternetAvailable()){
            self.tvlist.reloadData()
            refreshControl.endRefreshing()
            self.lblError.text = alertInternet
            
        }else{
            if (loginDict.count != 0){
               
                print(strFilterURL)
                
                let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
                loading.startLoading(text: "Loading...")

                WebService.callAPIBYGET(parameter: NSDictionary(), url: strFilterURL, OnResultBlock: { (responce, status) in
                    print(responce)
                    loading.endLoading()
                    self.refreshControl.endRefreshing()
                    if (status == "success"){
                        self.aryDidYouList = NSMutableArray()

                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            
                            if(dict.value(forKey: "HistoricalTreeList") is NSArray){
                                self.aryDidYouList = (dict.value(forKey: "HistoricalTreeList") as! NSArray).mutableCopy() as! NSMutableArray
                                self.tvlist.reloadData()
                            }else{
                                self.lblError.text = alertDataNotFound
                            }
                        }else{
                            self.lblError.text = "\(dict.value(forKey: "Success")as! String)"
                        }
                        self.tvlist.reloadData()
                    }else{
                        self.aryDidYouList = NSMutableArray()
                        self.lblError.text = alertSomeError
                        self.tvlist.reloadData()
                    }
                })
            }
        }
    }


}
//MARK:- ----------Refresh Delegate Methods----------

extension DidYouKnowVC: refreshDidYouKnowView {
    func refreshview(dictData: NSDictionary) {
  print(dictData)
        var strAGE = "\(dictData.value(forKey: "age")!)"
        let strcityid = "\(dictData.value(forKey: "cityid")!)"
        var strLongitude = "\(dictData.value(forKey: "Longitude")!)"
        var strLatitude = "\(dictData.value(forKey: "Latitude")!)"
        var strradius = "\(dictData.value(forKey: "radius")!)"
        if strradius == "" {
            strradius = "1000"
        }else{
             strradius = "\(strradius)000"
        }
        if strLongitude == "" {
            strLongitude = "0"
        }
        if strLatitude == "" {
            strLatitude = "0"
        }
        if strAGE == "" {
            strAGE = "0"
        }

        let GetHistoricalTree = "\(URL_GetHistoricalTree)Age=\(strAGE)&City_Id=\(strcityid)&Lat=\(strLatitude)&Long=\(strLongitude)&Radius=\(strradius)"
        print(GetHistoricalTree)
        self.call_GetTreeList_API(strFilterURL: GetHistoricalTree)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DidYouKnowVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryDidYouList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DidiYouKnow", for: indexPath as IndexPath) as! CommonTableCell
        let dict = removeNullFromDict(dict: (aryDidYouList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        
        var strAge = "Not Available"
        var strTitle = "Not Available"
        var strDetail = "Not Available"
        var strlocation = "Not Available"
        var strimage = "default"

        if dict.value(forKey: "Age") != nil  && "\(dict.value(forKey: "Age")!)" != ""{
            strAge = "\(dict.value(forKey: "Age")!)"
        }
        if dict.value(forKey: "Tree_Name") != nil  && "\(dict.value(forKey: "Tree_Name")!)" != ""{
            strTitle = "\(dict.value(forKey: "Tree_Name")!)"
        }
        if dict.value(forKey: "Description") != nil  && "\(dict.value(forKey: "Description")!)" != ""{
            strDetail = "\(dict.value(forKey: "Description")!)"
        }
        if dict.value(forKey: "Location") != nil  && "\(dict.value(forKey: "Location")!)" != ""{
            strlocation = "\(dict.value(forKey: "Location")!)"
        }
        if dict.value(forKey: "Images") != nil  && "\(dict.value(forKey: "Images")!)" != ""{
            strimage = "\(dict.value(forKey: "Images")!)"
        }
        
        cell.didYou_Title.text = strTitle
        cell.didYou_Detail.text = strDetail
        cell.didYou_Address.text = strlocation
        cell.didYou_Age.text = "Age : " + strAge
        if(strimage.count != 0){
            cell.didYou_image.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strimage)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
        }else{
            cell.didYou_image.image = #imageLiteral(resourceName: "default_img")
        }
  
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AbouttreeDetailVC")as! AbouttreeDetailVC
        testController.dict =  removeNullFromDict(dict: (aryDidYouList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        testController.strComeFrom = "DIDYOUKNOW"
        self.navigationController?.pushViewController(testController, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
   
}

