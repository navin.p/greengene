//
//  FilterDidYouKnowVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreLocation



//MARK: ---------------Protocol-----------------
protocol refreshFiltertreeView : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
    func refreshTreeAPI(dictData : NSDictionary ,tag : Int)

}

class FilterDidYouKnowVC: UIViewController {

    weak var handleDidYouKnowView: refreshDidYouKnowView?

    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAge: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtRadius: SkyFloatingLabelTextField!

  @IBOutlet weak var txtLocation: KMPlaceholderTextView!
    var strLatitude = "0"
    var strLongitude = "0"
    var locationManager = CLLocationManager()
    var aryForStateList = NSMutableArray()
    var aryForCityList = NSMutableArray()
   
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        self.activity.isHidden = true
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSearch.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        if (loginDict.count != 0){
            
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            
            txtState.text = "\(dict.value(forKey: "StateName")!)"
            txtState.tag = Int("\(dict.value(forKey: "State_Id")!)")!
            txtCity.text = "\(dict.value(forKey: "CityName")!)"
            txtCity.tag =  Int("\(dict.value(forKey: "City_Id")!)")!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - -------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func actionOnSelectLocation(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func actionOnSelectState(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 0
        if aryForStateList.count != 0 {
            self.gotoPopView(sender: sender, aryData: aryForStateList)
        }else{
            self.call_StateList_API(sender: sender)
        }
    }
    
    @IBAction func actionOnSelectCity(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 1
        if txtState.text?.count != 0 {
            self.call_CityList_API(sender: sender)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPStateFirst, viewcontrol: self)
        }
    }
    @IBAction func actionOnSelectRadius(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 2
        let aryForRadius = NSMutableArray()
        for index in 0...15 {
            let dict = NSMutableDictionary()
            dict.setValue("\(index)", forKey: "radius")
            aryForRadius.add(dict)
        }
           self.gotoPopView(sender: sender, aryData:  aryForRadius)
    }
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let dictData = NSMutableDictionary()
        dictData.setValue("\(txtCity.tag)", forKey: "cityid")
        dictData.setValue("\(strLongitude)", forKey: "Longitude")
        dictData.setValue("\(strLatitude)", forKey: "Latitude")
        dictData.setValue("\(txtAge.text!)", forKey: "age")
        dictData.setValue("\(txtRadius.text!)", forKey: "radius")

        DispatchQueue.main.async {
            self.handleDidYouKnowView?.refreshview(dictData: dictData)
        }
        self.navigationController?.popViewController(animated: true)

    }

   
    
    // MARK: - -------------API Calling
    // MARK: -
    func call_StateList_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetStateListByCountryId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForStateList = NSMutableArray()
                        self.aryForStateList = (dict.value(forKey: "StateList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForStateList)
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func call_CityList_API(sender : UIButton) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetCityListByStateId = "\(URL_GetCityListByStateId)\(String(describing: txtState.tag))"
            print(GetCityListByStateId)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetCityListByStateId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForCityList = NSMutableArray()
                        self.aryForCityList = (dict.value(forKey: "CityList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForCityList)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        
        if sender.tag == 0 { //State
            vc.strTitle = "Select State"
            vc.strTag = 7
        }else if sender.tag == 1 { // City
            vc.strTitle = "Select City"
            vc.strTag = 8
        }
        else if sender.tag == 2 { // City
            vc.strTitle = "Select Radius"
            vc.strTag = 9
        }
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handleFiltertreeView = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})

        }
    }

    // MARK: - ----------------getCurrentLocation
    // MARK: -
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }

}

//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension FilterDidYouKnowVC: refreshFiltertreeView {
    func refreshTreeAPI(dictData: NSDictionary, tag: Int) {
        
    }
    
    func refreshview(dictData: NSDictionary, tag: Int) {
        if tag == 7 { // State
            txtState.text = dictData.value(forKey: "State_Name")as? String
            txtState.tag = dictData.value(forKey: "State_Id")as! Int
             txtCity.text = ""
            txtCity.tag = 0
        }else if tag == 8 { // City
            txtCity.text = dictData.value(forKey: "City_Name")as? String
            txtCity.tag = dictData.value(forKey: "City_Id")as! Int
        }else if tag == 9 { // Radius
            txtRadius.text = "\(dictData.value(forKey: "radius")!)"
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension FilterDidYouKnowVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtAge {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 2)
        }
       return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate  Methods----------

extension FilterDidYouKnowVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        self.strLatitude = String(userLocation.coordinate.latitude)
        self.strLongitude = String(userLocation.coordinate.longitude)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if(placemarks != nil){
                let placemark = placemarks! as [CLPlacemark]
                if placemark.count>0{
                    let placemark = placemarks![0]
                    var strAddress = ""
                    if(placemark.locality != nil){
                        strAddress = "\(placemark.subLocality!), "
                    }
                    if(placemark.administrativeArea != nil){
                        strAddress =  strAddress + "\(placemark.administrativeArea!), "
                    }
                    if(placemark.country != nil){
                        strAddress =  strAddress + "\(placemark.country!)"
                    }
                    if(placemark.postalCode != nil){
                        strAddress =  strAddress + "\n\(placemark.postalCode!)"
                    }
                    self.txtLocation.text = strAddress
                    self.locationManager.stopUpdatingLocation()
                }
            }
            
        }
    }
}
//MARK:-
//MARK:- ----------------

extension FilterDidYouKnowVC: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
        self.txtLocation.text =  (dictData.value(forKey: "address")as! String)
        strLatitude =  (dictData.value(forKey: "lat")as! String)
        strLatitude =  (dictData.value(forKey: "long")as! String)
    }
    
    
}
