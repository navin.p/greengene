//
//  AddtreeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/20/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.


import UIKit
import CoreLocation

//MARK: ---------------Protocol-----------------
protocol refreshAddtreeView : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}
class AddtreeVC: UIViewController {
    
    
    var strLatitude = "0"
    var strLongitude = "0"
    var locationManager = CLLocationManager()

    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var txtLocation: KMPlaceholderTextView!
    
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    
    @IBOutlet weak var txtTreeName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAge: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    
    @IBOutlet weak var imgTree: UIImageView!
    var aryForStateList = NSMutableArray()
    var aryForCityList = NSMutableArray()
    var strimageName = ""
    
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activity.isHidden = true
        imagePicker.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            txtState.text = "\(dict.value(forKey: "StateName")!)"
            txtState.tag = Int("\(dict.value(forKey: "State_Id")!)")!
            txtCity.text = "\(dict.value(forKey: "CityName")!)"
            txtCity.tag =  Int("\(dict.value(forKey: "City_Id")!)")!
        }
       //self.getCurrentLocation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    // MARK: - ----------------getCurrentLocation
    // MARK: -
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnSubmit(_ sender: Any) {
        if(validation()){
            call_UPLOADIMGE_API()
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func actionOnSelectLocation(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func actionOnSelectState(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 0
        if aryForStateList.count != 0 {
            self.gotoPopView(sender: sender, aryData: aryForStateList)
        }else{
            self.call_StateList_API(sender: sender)
        }
    }
    
    @IBAction func actionOnSelectCity(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 1
        if txtState.text?.count != 0 {
            self.call_CityList_API(sender: sender)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPStateFirst, viewcontrol: self)
        }
        
    }
    
    @IBAction func actionOnPickImage(_ sender: UIButton) {
        let alert = UIAlertController(title: alertGallery, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender .bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - -------------API Calling
    // MARK: -
    
    func call_Addtree_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let AddHistoricalTree = "\(URL_AddHistoricalTree)SOI_User_Id=\(dict.value(forKey: "User_Id")!)&City_Id=\(txtCity.tag)&Tree_Name=\(txtTreeName.text!)&Description=\(txtDescription.text!)&Lat=\(self.strLatitude)&Long=\(self.strLongitude)&Location=\(txtLocation.text!)&Age=\(txtAge.text!)&Images=\(strimageName)"
            print(AddHistoricalTree)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: AddHistoricalTree, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title:alertInfo, message:  "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)

                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    func call_UPLOADIMGE_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURLImageUPLOAD, image: self.imgTree.image!, imageName: strimageName, OnResultBlock: { (responce, status) in
                if (status == "success"){
                    if (responce.value(forKey: "message")as! String == "SUCCESS"){
                        self.call_Addtree_API()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            
        }
    }
    
    
    
    
    func call_StateList_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetStateListByCountryId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForStateList = NSMutableArray()
                        self.aryForStateList = (dict.value(forKey: "StateList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForStateList)
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func call_CityList_API(sender : UIButton) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetCityListByStateId = "\(URL_GetCityListByStateId)\(String(describing: txtState.tag))"
            print(GetCityListByStateId)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetCityListByStateId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForCityList = NSMutableArray()
                        self.aryForCityList = (dict.value(forKey: "CityList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForCityList)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        
        if sender.tag == 0 { //State
            vc.strTitle = "Select State"
            vc.strTag = 5
        }else if sender.tag == 1 { // City
            vc.strTitle = "Select City"
            vc.strTag = 6
        }
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handleAddtreeView = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})

        }
    }
    
    // MARK: - ---------------Other Function's
    // MARK: -
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            showToastForSomeTime(title: alertMessage, message: "You don't have camera", time: 3, viewcontrol: self)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    // MARK: - --------------ValidationLogInView
    // MARK: -
    
    func validation() -> Bool {
        
        
        if txtTreeName.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Name, viewcontrol: self)
            return false
        }else  if txtAge.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Age, viewcontrol: self)
            return false
        } else if txtState.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_State, viewcontrol: self)
            return false
        }else if txtCity.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_City, viewcontrol: self)
            return false
        }else if txtLocation.text?.count == 0 || txtLocation.text == "Select Location" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Location, viewcontrol: self)
            return false
        }else if txtDescription.text?.count == 0 || txtLocation.text == "Description" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Description, viewcontrol: self)
            return false
        }
        else if imgTree.image == #imageLiteral(resourceName: "add_image") {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_image, viewcontrol: self)
            return false
        }
        else{
            return true
        }
    }
    
}

//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension AddtreeVC: refreshAddtreeView {
    func refreshview(dictData: NSDictionary, tag: Int) {
        if tag == 5 { // State
            txtState.text = dictData.value(forKey: "State_Name")as? String
            txtState.tag = dictData.value(forKey: "State_Id")as! Int
            txtCity.text = ""
            txtCity.tag = 0
        }else if tag == 6 { // City
            txtCity.text = dictData.value(forKey: "City_Name")as? String
            txtCity.tag = dictData.value(forKey: "City_Id")as! Int
        }
    }
}

//MARK:-
//MARK:- ---------UIImagePickerControllerDelegate-----------

extension AddtreeVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgTree.image = pickedImage
            self.strimageName = getUniqueString()
            dismiss(animated: true, completion: nil)
        }
    }
}

//MARK:-
//MARK:- ---------CLLocationManagerDelegate  Methods----------

extension AddtreeVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        self.strLatitude = String(userLocation.coordinate.latitude)
        self.strLongitude = String(userLocation.coordinate.longitude)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if(placemarks != nil){
                let placemark = placemarks! as [CLPlacemark]
                if placemark.count>0{
                    let placemark = placemarks![0]
                    var strAddress = ""
                    if(placemark.locality != nil){
                        strAddress = "\(placemark.subLocality!), "
                    }
                    if(placemark.administrativeArea != nil){
                        strAddress =  strAddress + "\(placemark.administrativeArea!), "
                    }
                    if(placemark.country != nil){
                        strAddress =  strAddress + "\(placemark.country!)"
                    }
                    if(placemark.postalCode != nil){
                        strAddress =  strAddress + "\n\(placemark.postalCode!)"
                    }
                    self.txtLocation.text = strAddress
                    self.locationManager.stopUpdatingLocation()
                }
            }
            
        }
    }
}
//MARK:-
//MARK:- ----------------

extension AddtreeVC: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
        self.txtLocation.text =  (dictData.value(forKey: "address")as! String)
        strLatitude =  (dictData.value(forKey: "lat")as! String)
        strLatitude =  (dictData.value(forKey: "long")as! String)
    }
    
   
}
