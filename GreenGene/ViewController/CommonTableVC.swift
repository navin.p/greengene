//
//  CommonTableVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/30/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//




//tag 1,2     for SignUP
// 1- state   2- City
//tag 3,4     for SignUP
// 3- state   4- City

// Addtree
//5 state 6 City
// Filtertree
//7 state 8 City 9 radius
//10 TreeTagView
//11 QRDetailView

import UIKit


//MARK: ---------------Protocol
// MARK: -
protocol CommonTableDelegate : class{
    func getDataFromCommonTableDelegate(dictData : NSDictionary ,tag : Int)
       func getDataFromPickerDelegate(strTitle : String ,tag : Int)
}

class CommonTableVC: UIViewController {
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var height_ForTBLVIEW: NSLayoutConstraint!
    @IBOutlet weak var viewFortv: CardView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var viewForPicker: CardView!
    @IBOutlet weak var pickerView: UIPickerView!


    
    var strTitle = String()
    var aryTBL = NSMutableArray()
    var aryPicker = NSMutableArray()

    var aryFilterData = NSMutableArray()
    var strTag = Int()
    var strViewComeFrom = String()
    var strTitlePicker = String()

    weak var delegate: CommonTableDelegate?
    weak var handleSignUPView: refreshSignUpView?
    weak var handleUpdateView: refreshUpdateView?
    weak var handleAddtreeView: refreshAddtreeView?
    weak var handleFiltertreeView: refreshFiltertreeView?
    weak var handleTreeTagView: refreshTreeTagView?
    weak var handleTreeQRDetailView: refreshQRDetailScreen?

    override func viewDidLoad() {
        super.viewDidLoad()
        if strViewComeFrom == "Picker" {
            pickerView.delegate = self
            pickerView.dataSource = self
            
        }else{
            tvForList.delegate = self
            tvForList.dataSource = self
            tvForList.estimatedRowHeight = 50.0
            tvForList.tableFooterView = UIView()
            aryFilterData = NSMutableArray()
            aryFilterData = aryTBL
            searchBar.layer.cornerRadius = 12.0
        }
        
      
    }
    
    override func viewWillLayoutSubviews() {
        if strViewComeFrom == "Picker" {
            viewForPicker.isHidden = false
            viewFortv.isHidden = true

        }else{
            viewForPicker.isHidden = true
            viewFortv.isHidden = false
            height_ForTBLVIEW.constant = CGFloat((aryTBL.count * 40 ) + 60 )
            if  height_ForTBLVIEW.constant > self.view.frame.size.height - 150  {
                height_ForTBLVIEW.constant = self.view.frame.size.height - 150
            }
            viewFortv.center = self.view.center
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }

    }
   
    // MARK: - ---------------Picker Action
    // MARK: -
    @IBAction func actionOnDone(_ sender: UIButton) {
        self.delegate?.getDataFromPickerDelegate(strTitle: strTitlePicker, tag: strTag)
        self.dismiss(animated: false) {
        }
        
    }
    @IBAction func actionOnClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }
        
    }
}
// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension CommonTableVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryFilterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "listcell", for: indexPath as IndexPath) as! CommonTableCell
        let dict = aryFilterData.object(at: indexPath.row)as? NSDictionary
        if strTag == 1 || strTag == 3 || strTag == 5 || strTag == 7 {
            cell.List_lblTitle.text = "\(dict?.value(forKey: "State_Name")as! String)"
        }else if (strTag == 2 || strTag == 4 || strTag == 6 || strTag == 8){
            cell.List_lblTitle.text = "\(dict?.value(forKey: "City_Name")as! String)"
        }
        else if (strTag == 9){
            cell.List_lblTitle.text = "\(dict?.value(forKey: "radius")as! String)"
        }else if (strTag == 10 || strTag == 11 || strTag == 12){
            cell.List_lblTitle.text = "\(dict?.value(forKey: "Plant_Name")as! String)"
        }
        else if (strTag == 13){
            cell.List_lblTitle.text = "\(dict?.value(forKey: "OrganizationName")as! String)"
        }else if (strTag == 1000){
            cell.List_lblTitle.text = aryFilterData.object(at: indexPath.row) as? String
        }else if (strTag == 555){
            cell.List_lblTitle.text = "\(dict?.value(forKey: "Country_Code")as! String)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryFilterData.object(at: indexPath.row)as? NSDictionary
        if self.strTag == 1 || self.strTag == 2 {
            DispatchQueue.main.async {
                self.handleSignUPView?.refreshview(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {
                }
            }
            
        }else if self.strTag == 3 || self.strTag == 4 {
            DispatchQueue.main.async {
                self.handleUpdateView?.refreshview(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {
                    
                }
            }
        }
        else if self.strTag == 5 || self.strTag == 6 {
            DispatchQueue.main.async {
                self.handleAddtreeView?.refreshview(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {
                }
            }
        }
        else if self.strTag == 7 || self.strTag == 8 || self.strTag == 9 {
            DispatchQueue.main.async {
                self.handleFiltertreeView?.refreshview(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {
                }
            }
        }
        else if self.strTag == 10 {
            DispatchQueue.main.async {
                self.handleTreeTagView?.refreshview(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {
                }
            }
        }
        else if self.strTag == 11 {
                self.handleTreeQRDetailView?.refreshview(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {
            }
        }
        else if self.strTag == 12 || self.strTag == 13  {
             self.delegate?.getDataFromCommonTableDelegate(dictData: dict!, tag: self.strTag)
            self.dismiss(animated: false) {
            }
        }else if self.strTag == 1000 {
            self.delegate?.getDataFromPickerDelegate(strTitle: "\(aryFilterData.object(at: indexPath.row))", tag: self.strTag)
            self.dismiss(animated: false)
        }else if (strTag == 555){
            self.delegate?.getDataFromCommonTableDelegate(dictData: dict!, tag: self.strTag)
            self.dismiss(animated: false)
        }
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4) {
//            cell.transform = CGAffineTransform.identity
//        }
//    }
    
}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  CommonTableVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "State_Name contains[c] %@ OR City_Name contains[c] %@ OR Plant_Name contains[c] %@ OR OrganizationName contains[c] %@ OR radius contains[c] %@", argumentArray: [Searching, Searching, Searching, Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryTBL ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryFilterData = NSMutableArray()
            self.aryFilterData = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvForList.reloadData()
        }
        else{
            self.aryFilterData = NSMutableArray()
            self.aryFilterData = self.aryTBL.mutableCopy() as! NSMutableArray
            self.tvForList.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
       
    }
}
// MARK: - -------------UIPickerViewDataSource
// MARK: -
extension CommonTableVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return aryTBL.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (aryTBL[row] as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strTitlePicker = (aryTBL[row] as! String)
        
    }
}
