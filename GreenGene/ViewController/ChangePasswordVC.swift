//
//  ChangePasswordVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/30/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
   
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtOldPass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConPass: SkyFloatingLabelTextField!
    @IBOutlet weak var btn_Submit: UIButton!
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btn_Submit.layer.cornerRadius = 8.0
        btn_Submit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ---------------IBACTION
    // MARK: -
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if validationChangePassword() {
            self.call_ChangePassword_API()
        }
    }
    @IBAction func actionOnBack(_ sender: UIButton) {

        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - --------------Validation
    // MARK: -
    func validationChangePassword() -> Bool {
        if txtOldPass.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertChangeOldPassword, viewcontrol: self)
            return false
        }else if txtNewPass.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertChangeNewPassword, viewcontrol: self)
            return false
        }
        else if txtConPass.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertChangeConPassword, viewcontrol: self)
            return false
        }
        else if txtConPass.text !=  txtNewPass.text {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertChangeValidConfirmPass, viewcontrol: self)
            return false
        }else{
            return true
        }
    }
    // MARK: - --------------API Calling
    // MARK: -
   func call_ChangePassword_API()  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
          
                let ChangePassword = "\(URL_ChangePassword)UserName=\(dict.value(forKey: "Mobile_Number")!)&OldPassword=\(String(describing: txtOldPass.text!))&NewPassword=\(String(describing: txtNewPass.text!))"
                print(ChangePassword)
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                WebService.callAPIBYGET(parameter: NSDictionary(), url: ChangePassword, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            
                            let alert = UIAlertController(title:alertInfo, message: "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in

                               self.navigationController?.popViewController(animated: true)
                            }))
                              self.present(alert, animated: true, completion: nil)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension ChangePasswordVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtOldPass  {
            return  txtFiledValidation(textField: txtOldPass, string: string, returnOnly: "ALL", limitValue: 11)
        }
        if textField == txtNewPass {
                return  txtFiledValidation(textField: txtNewPass, string: string, returnOnly: "ALL", limitValue: 11)
        }
        if textField == txtConPass  {
                return  txtFiledValidation(textField: txtConPass, string: string, returnOnly: "ALL", limitValue: 11)
        }
       return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
