//
//  GreenWealtCardVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 4/15/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class GreenWealtCardVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgQrcode: UIImageView!

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var backcard: CardView!
    @IBOutlet weak var frontcard: CardView!
    private var showingBack = true

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        loginDict = getLoginData()
        print(loginDict)
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
           lblName.text = "\(dict.value(forKey: "Name")!)"
            
            var numberOnCard = "\(dict.value(forKey: "Country_Code")!)" + "\(dict.value(forKey: "City_Code")!)"
            var plntID = "\(dict.value(forKey: "Donated_Plant_Id")!)"
            var Donated_Plant_Id = ""
            if(plntID.count != 8){
                let extra = 8 - plntID.count
                for _ in 1...extra{
                    Donated_Plant_Id =   "0" + Donated_Plant_Id
                }
            }
            plntID = Donated_Plant_Id + plntID
            numberOnCard = numberOnCard + plntID
  
           lblNumber.text = numberOnCard.pairs.joined(separator: "  ")
            let strProfileUrl = "\(dict.value(forKey: "Profile_Image")!)"
            if(strProfileUrl.count != 0){
               imgProfile.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "profile_img_1"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
            let strQrCodeUrl = "\(dict.value(forKey: "QRCode")!).png"
            if(strQrCodeUrl.count != 0){
                self.imgQrcode.setImageWith(URL(string: "\(BaseURLQrcodeImageDownload)\(strQrCodeUrl)"), placeholderImage: UIImage(named: "qr_code_scan_to_know_benifits"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
        }
       
        imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
        imgProfile.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
        imgProfile.layer.borderWidth  = 2.0
        imgProfile.layer.masksToBounds = false
        imgProfile.clipsToBounds = true
       imgProfile.contentMode = .scaleAspectFill
        
        imgQrcode.layer.borderColor = hexStringToUIColor(hex: colorGrayPrimary).cgColor
        imgQrcode.layer.borderWidth  = 1.0
        imgQrcode.layer.masksToBounds = false
      
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        frontcard.frame = CGRect(x: containerView.frame.width / 2 - 157, y: containerView.frame.height / 2 - 100, width: 315, height: 200)
        
        let viewAlfa = UIView(frame:CGRect(x: frontcard.frame.origin.x - 20, y: frontcard.frame.origin.y - 20, width: frontcard.frame.width + 40, height: frontcard.frame.height + 40))
        viewAlfa.layer.cornerRadius = 10.0
        viewAlfa.backgroundColor = UIColor.white
        viewAlfa.alpha = 0.2
      
        containerView.addSubview(viewAlfa)

        containerView.addSubview(frontcard)
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(flip))
        singleTap.numberOfTapsRequired = 1
        containerView.addGestureRecognizer(singleTap)
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @objc func flip() {
        if(showingBack){
            self.frontcard.removeFromSuperview()
            backcard.frame = CGRect(x: containerView.frame.width / 2 - 157, y: containerView.frame.height / 2 - 100, width: 315, height: 200)
                self.showingBack = false
                self.containerView.addSubview(self.backcard)
        }else{
            self.backcard.removeFromSuperview()
            frontcard.frame = CGRect(x: containerView.frame.width / 2 - 157, y: containerView.frame.height / 2 - 100, width: 315, height: 200)
                self.showingBack = true
                self.containerView.addSubview(self.frontcard)
        }
    }
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionONTapToQRcode(_ sender: Any) {

                let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.imageSHow = self.imgQrcode.image!
                self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
}

extension Collection {
    var pairs: [SubSequence] {
        var startIndex = self.startIndex
        let count = self.count
        let n = count/4 + count % 4
        return (0..<n).map { _ in
            let endIndex = index(startIndex, offsetBy: 4, limitedBy: self.endIndex) ?? self.endIndex
            defer { startIndex = endIndex }
            return self[startIndex..<endIndex]
        }
    }
}
