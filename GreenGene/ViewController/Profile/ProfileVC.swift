//
//  ProfileVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/1/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtType: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtZipCode: SkyFloatingLabelTextField!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnEdit: UIButton!

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
       btnEdit.setTitleColor(hexStringToUIColor(hex: colorGreenPrimary), for: .normal)
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.prominent)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = img.bounds
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        img.addSubview(blurEffectView)
//
        
           loginDict = getLoginData()
        if (loginDict.count != 0){
              let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let name = "\(dict.value(forKey: "Name")!)"
            let type = "\(dict.value(forKey: "RoleName")!)"
            let email = "\(dict.value(forKey: "Email_Id")!)"
            let mobile = "\(dict.value(forKey: "Mobile_Number")!)"
            let address = "\(dict.value(forKey: "Address")!)"
            let state = "\(dict.value(forKey: "StateName")!)"
            let city = "\(dict.value(forKey: "CityName")!)"
            let zipCode = "\(dict.value(forKey: "Zip_Code")!)"
            let strProfileUrl = "\(dict.value(forKey: "Profile_Image")!)"
            lblName.text = name
            txtType.text = type
            txtEmail.text = email
            txtMobile.text = mobile
            txtAddress.text = address
            txtState.text = state
            txtCity.text = city
            txtZipCode.text = zipCode
            if(strProfileUrl.count != 0){
                imgProfile.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "profile_img_1"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
            imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
            imgProfile.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
            imgProfile.layer.borderWidth  = 2.0
            imgProfile.layer.masksToBounds = false
            imgProfile.clipsToBounds = true
            imgProfile.contentMode = .scaleAspectFill
        }
         self.imgProfile.alpha = 0.4
         self.imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0) {
            self.imgProfile.alpha = 1
             self.imgProfile.transform = .identity
        }
        imgProfile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        imgProfile.addGestureRecognizer(tap)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func actionOnEditProfile(_ sender: Any) {
           DispatchQueue.main.async {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "UpdateProfile")as! UpdateProfile
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.imgProfile.image!
        self.navigationController?.pushViewController(vc, animated: true)

    }
}
