//
//  UpdateProfile.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/1/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK: ---------------Protocol-----------------
protocol refreshUpdateView : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}
class UpdateProfile: UIViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtZipCode: SkyFloatingLabelTextField!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!

    var aryForStateList = NSMutableArray()
    var aryForCityList = NSMutableArray()
    var imagePicker = UIImagePickerController()
    
    var type = ""
    var strProfileUrl = ""
    
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 8.0
        if (loginDict.count != 0){
            
            let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)

            let name = "\(dict.value(forKey: "Name")!)"
            type = "\(dict.value(forKey: "User_Type")!)"
            let email = "\(dict.value(forKey: "Email_Id")!)"
           // let mobile = "\(dict.value(forKey: "Mobile_Number")!)"
            let address = "\(dict.value(forKey: "Address")!)"
            let state = "\(dict.value(forKey: "StateName")!)"
            let stateID = "\(dict.value(forKey: "State_Id")!)"
            
            let city = "\(dict.value(forKey: "CityName")!)"
            let cityID = "\(dict.value(forKey: "City_Id")!)"
            
            let zipCode = "\(dict.value(forKey: "Zip_Code")!)"
            strProfileUrl = "\(dict.value(forKey: "Profile_Image")!)"
            txtName.text = name
            txtEmail.text = email
            txtAddress.text = address
            txtState.text = state
            txtState.tag = Int(stateID)!
            txtCity.text = city
            txtCity.tag = Int(cityID)!
            txtZipCode.text = zipCode
            if(strProfileUrl.count != 0){
                imgProfile.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "profile_img_1"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
            imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
            imgProfile.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
            imgProfile.layer.borderWidth  = 2.0
            imgProfile.layer.masksToBounds = false
            imgProfile.clipsToBounds = true
            imgProfile.contentMode = .scaleAspectFill
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.imgProfile.alpha = 0.4
        self.imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0) {
            self.imgProfile.alpha = 1
            self.imgProfile.transform = .identity
        }
        imgProfile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        imgProfile.addGestureRecognizer(tap)
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

        
    }
    @IBAction func actionOnUpdate(_ sender: Any) {
        if (validationUpdate()){
            if(self.imgProfile.tag == 99){
                call_UPLOADIMGE_API()
            }else{
                call_UPDATE_API()
            }
        }
    }
    @IBAction func actionOnDrop(_ sender: UIButton) {
        if sender.tag == 0 {
            if aryForStateList.count != 0 {
                self.gotoPopView(sender: sender, aryData: aryForStateList)
            }else{
                self.call_StateList_API(sender: sender)
            }
        }
        else if sender.tag == 1 {
            self.call_CityList_API(sender: sender)
        }
        else if sender.tag == 2 {
            let alert = UIAlertController(title: alertGallery, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender
                alert.popoverPresentationController?.sourceRect = sender .bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.imgProfile.image!
        self.navigationController?.pushViewController(vc, animated: true)
   }
    // MARK: - ---------------Other Function's
    // MARK: -
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            showToastForSomeTime(title: alertMessage, message: "You don't have camera", time: 3, viewcontrol: self)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    // MARK: - --------------ValidationSignUP
    // MARK: -
    
    func validationUpdate() -> Bool {
        if txtName.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPFullName, viewcontrol: self)
            return false
        }else if ((txtEmail.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPEmail, viewcontrol: self)
            return false
        }
        else if !(validateEmail(email: txtEmail.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPValidEmail, viewcontrol: self)
            return false
        }
//        else if ((txtAddress.text?.count)! == 0){
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPAddress, viewcontrol: self)
//            return false
//        }
        else if ((txtState.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPState, viewcontrol: self)
            return false
        }else if ((txtCity.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPCity, viewcontrol: self)
            return false
        }else if ((txtZipCode.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSignUPZipCode, viewcontrol: self)
            return false
        }else{
            return true
        }
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func call_StateList_API(sender : UIButton)  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetStateListByCountryId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForStateList = NSMutableArray()
                        self.aryForStateList = (dict.value(forKey: "StateList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForStateList)
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func call_CityList_API(sender : UIButton) {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetCityListByStateId = "\(URL_GetCityListByStateId)\(String(describing: txtState.tag))"
            print(GetCityListByStateId)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetCityListByStateId, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.aryForCityList = NSMutableArray()
                        self.aryForCityList = (dict.value(forKey: "CityList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.gotoPopView(sender: sender, aryData:  self.aryForCityList)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    func gotoPopView(sender: UIButton , aryData : NSMutableArray)  {
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        if sender.tag == 0 { //State
            vc.strTitle = "Select State"
            vc.strTag = 3
        }else if sender.tag == 1 { // City
            vc.strTitle = "Select City"
            vc.strTag = 4
        }
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handleUpdateView = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})

        }
    }
    func call_UPDATE_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let UpdateUserProfile = "\(URL_UpdateUserProfile)User_Id=\(dict.value(forKey: "User_Id")!)&Name=\(txtName.text!)&Email_Id=\(txtEmail.text!)&Gender=&Status=&Profile_Image=\(strProfileUrl)&User_Type=\(type)&State_Id=\(txtState.tag)&City_Id=\(txtCity.tag)&Address=\(txtAddress.text!)&Zip_Code=\(txtZipCode.text!)"
            print(UpdateUserProfile)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: UpdateUserProfile, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"LoginData")
                        saveDataInLocalDictionary(strEntity: "LoginData", strKey: "login", data: (dict).mutableCopy() as! NSMutableDictionary)
                        FTIndicator.showToastMessage(alertUpdate)
                        self.navigationController?.popViewController(animated: true)
                      
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    func call_UPLOADIMGE_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURLImageUPLOAD, image: self.imgProfile.image!, imageName: strProfileUrl, OnResultBlock: { (responce, status) in
                FTIndicator.dismissProgress()
                if (status == "True"){
                    if (responce.value(forKey: "message")as! String == "SUCCESS"){
                            self.call_UPDATE_API()
                    }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            
        }
    }
}
//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension UpdateProfile: refreshUpdateView {
    func refreshview(dictData: NSDictionary, tag: Int) {
        if tag == 3 { // State
            txtState.text = dictData.value(forKey: "State_Name")as? String
            txtState.tag = dictData.value(forKey: "State_Id")as! Int
              txtCity.text = ""
              txtCity.tag = 0
        }else if tag == 4 { // City
            txtCity.text = dictData.value(forKey: "City_Name")as? String
            txtCity.tag = dictData.value(forKey: "City_Id")as! Int
        }
    }
}
//MARK:-
//MARK:- ---------UIImagePickerControllerDelegate-----------

extension UpdateProfile: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgProfile.image = pickedImage
            self.strProfileUrl = getUniqueString()
            self.imgProfile.tag = 99
            dismiss(animated: true, completion: nil)
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension UpdateProfile : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtZipCode {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 5)
        }
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 50)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
