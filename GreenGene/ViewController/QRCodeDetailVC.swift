//
//  QRCodeDetailVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/6/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class QRCodeDetailVC: UIViewController {
   
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTreeNameQRCode: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnSUGGESTION: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnShare: UIButton!

    @IBOutlet weak var viewHeader: CardView!

    @IBOutlet weak var lblViews: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDonatedCount: UILabel!
    @IBOutlet weak var heightTreeCount: NSLayoutConstraint!
    @IBOutlet weak var tblDetail: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var widthUpdateButton: NSLayoutConstraint!
    @IBOutlet weak var mapTypeSegment: UISegmentedControl!

    
    var availabilityInRange = Bool()

    var dictData = NSMutableDictionary()
    var ary_of_Collection = NSMutableArray()
    var strUserRole = ""
    var strViewComeFrom = String()

    // MARK: - ----------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblDetail.tableFooterView = UIView()
        FTIndicator.dismissProgress()
        lblDonatedCount.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)

        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSUGGESTION.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnUpdate.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        imgProfile.layer.cornerRadius =  imgProfile.frame.width / 2
        imgProfile.layer.borderColor = UIColor.white.cgColor
        imgProfile.layer.borderWidth  = 2.0
        imgProfile.layer.masksToBounds = false
        imgProfile.clipsToBounds = true
        imgProfile.contentMode = .scaleAspectFill
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
        imgProfile.isUserInteractionEnabled = true
        imgProfile.tag = 1
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
        print(dictData)
        
        
        btnShare.isHidden = true
        ShowDataOnScreenByAllConditions(dictQRData: dictData)
 
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration:0.5,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.imgProfile.transform = .identity
                },
                           completion: nil)
    }
    
   
    // MARK: - --------------IBAction
    // MARK: -
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: UIButton) {
        self.view.endEditing(true)
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    @IBAction func actionOnSegementMapType(_ sender: UISegmentedControl) {
        LoadMapViewPin()
        if(sender.selectedSegmentIndex == 0){
            self.mapView.mapType = .standard
        }else if(sender.selectedSegmentIndex == 1){
            self.mapView.mapType = .satellite

        }
        
    }
    
    @IBAction func actionONShare(_ sender: UIButton) {
        DispatchQueue.main.async {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WealthCalculatorVC")as! WealthCalculatorVC
            testController.strcome = "Share"
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    @IBAction func actionOnAddress(_ sender: Any) {
     openMap(strTitle:"\(dictData.value(forKey: "PlantedLocation")!)", strlat:"\(String(describing: dictData.value(forKey: "Lat")!))" , strLong: "\(String(describing: dictData.value(forKey: "Long")!))")
    }
    
    @IBAction func actionOnSUGGESTION(_ sender: UIButton) {
        let vc: CreatFeedBackVC = self.storyboard!.instantiateViewController(withIdentifier: "CreatFeedBackVC") as! CreatFeedBackVC
        vc.dictQRdetail = dictData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func actionOnUpdate(_ sender: UIButton) {
        var strPlant_Name = ""
        
        if dictData.value(forKey: "PlantMasterDc") is NSDictionary {
            let dictPlantMasterDc = removeNullFromDict(dict: (dictData.value(forKey: "PlantMasterDc")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            strPlant_Name = "\(dictPlantMasterDc.value(forKey: "Plant_Name")!)"
            
        }
        if(ary_of_Collection.count != 0){
            let dict = removeNullFromDict(dict: (ary_of_Collection.object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            if "\(dict.value(forKey: "Created_Date")!)" != "" {
                var fullNameArr = "\(dict.value(forKey: "Created_Date")!)".components(separatedBy: ".")
                let strFirst = fullNameArr[0] // First
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let serverDate = dateFormatter.date(from: strFirst)!
                let date2 = Date()
                let dateFormatter2 = DateFormatter()
                dateFormatter2.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let myString = dateFormatter2.string(from: date2)
                let cureentDate: Date? = dateFormatter2.date(from: myString)
                let next15days = serverDate.addingTimeInterval(TimeInterval(treeUpdateTime*60*60*24))
                print(serverDate)
                print(next15days)
                if(next15days == cureentDate! || next15days < cureentDate!){
                    let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
                    vc.strDonated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id")!)"
                    vc.strPlantID = "\(dictData.value(forKey: "Plant_Id")!)"
                    vc.strPlant_Name = strPlant_Name
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    FTIndicator.showToastMessage(alertnUnderMaintainance)
                }
                //   let currentDate = dateFormatter2.date(from: date2)
            }
        }else{
            let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
            vc.strDonated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id")!)"
            vc.strPlantID = "\(dictData.value(forKey: "Plant_Id")!)"
            vc.strPlant_Name = strPlant_Name
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc func tapMyTreeFunction(sender:UITapGestureRecognizer) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "MyTreeVC")as! MyTreeVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @objc func treeDescriptionFunction(sender:UITapGestureRecognizer) {
        let strID = "\(dictData.value(forKey: "Plant_Id")!)"
        if(strID != "" && strID != "<null>"){
            call_TreeDetail_API(plantID: "\(dictData.value(forKey: "Plant_Id")!)")
        }
    }
    


    // MARK: - ---------------API's Calling
    // MARK: -

    
    func call_TreeDetail_API(plantID : String)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
         
        }else{
            let GetMyTreeDetail = URL_GetPlantDetail + "\(plantID)"
                print(GetMyTreeDetail)
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            print(dict)
                            let plantData = dict.value(forKey: "PlantList")as! NSDictionary
                            let treename = "\(plantData.value(forKey: "Plant_Name")as! String)"
                            let Description = "\(plantData.value(forKey: "Description")as! String)"

                            showAlertWithoutAnyAction(strtitle: treename, strMessage: Description, viewcontrol: self)

                        }else{
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            
        }
    }
    // MARK: - ----------------Extra
    // MARK: -
    @objc func connected(_ sender:AnyObject){
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.imgProfile.image!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func ShowDataOnScreenByAllConditions(dictQRData : NSMutableDictionary) {
        
        
        widthUpdateButton.constant = 0.0
        btnShare.isHidden = true
        var Donated_By_Count = ""
        var IsTreeType = ""
        var strUserRole = ""
        var strUserID = ""
        var strQR_UserID = ""
        var Donated_Plant_Id = 0
        var availabilityInRange = false
        var strPlantID = ""
        var isOwnerShip = ""
        var IsEnableTreeTagging = ""

        
        if (loginDict.count != 0){
            let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            if dict.value(forKey: "UserQRCodeRangeDc") is NSArray {
                if (dict.value(forKey: "UserQRCodeRangeDc") as! NSArray).count != 0{
                    let strDonated_Plant_Id = "\(dictQRData.value(forKey: "Donated_Plant_Id")!)"
                    if(strDonated_Plant_Id != "" && strDonated_Plant_Id != "<null>"){
                        Donated_Plant_Id = Int(strDonated_Plant_Id)!
                    }else{
                        Donated_Plant_Id = 0
                    }
                    for item in dict.value(forKey: "UserQRCodeRangeDc") as! NSArray{
                        var range_From  = Int()
                        let strrange_From = "\((item as AnyObject).value(forKey: "Range_From")!)"
                        var range_To  = Int()
                        let strRange_To = "\((item as AnyObject).value(forKey: "Range_To")!)"
                        if(strRange_To != "" && strRange_To != "<null>") && (strrange_From != "" && strrange_From != "<null>"){
                            range_From = Int(strrange_From)!
                            range_To = Int(strRange_To)!
                            if(range_From <= Donated_Plant_Id && range_To >= Donated_Plant_Id){
                                availabilityInRange = true
                            }
                        }
                    }
                }
            }
            
            strUserRole = "\(dict.value(forKey: "RoleId")!)"
            strUserID = "\(dict.value(forKey: "User_Id")!)"
            strQR_UserID = "\(dictQRData.value(forKey: "User_Id")!)"
            strPlantID = "\(dictQRData.value(forKey: "Plant_Id")!)"
            isOwnerShip = "\(dictQRData.value(forKey: "IsEnableOwnerShip")!)"
            Donated_By_Count = dictQRData.value(forKey: "Donated_By_Count")as! String
            IsTreeType = "\(dictQRData.value(forKey: "IsTreeType")!)"
            IsEnableTreeTagging = "\(dictQRData.value(forKey: "IsEnableTreeTagging")!)"

           
            
            
            print("strUserRole = \(strUserRole)\n----strUserID = \(strUserID)\n----strQR_UserID = \(strQR_UserID)\n----strPlantID = \(strPlantID)\n----isOwnerShip = \(isOwnerShip)\n----Donated_Plant_Id = \(Donated_Plant_Id)\n----availabilityInRange = \(availabilityInRange)\n----Donated_By_Count = \(Donated_By_Count)\n----IsTreeType = \(IsTreeType)\n----IsEnableTreeTagging = \(IsEnableTreeTagging)")
            
            
         // ---------------1 Donated Count OR Tree NAme
            
            if(Donated_By_Count == "0" || Donated_By_Count == "<null>"){
                heightTreeCount.constant = 0
            }else{
                heightTreeCount.constant = 40
            }
            if(IsTreeType == "0" || IsTreeType == "<null>"){
                lblTreeNameQRCode.isHidden = true
            }else{
                lblTreeNameQRCode.isHidden = false
            }
           
            
            // Button SHow /HIDE
            
            if (strUserRole == "2" || strUserRole == "5" ) { // Owner , organization
                if("\(isOwnerShip)" == "1"){
                    if ("\(strUserID)" == "\(strQR_UserID)"){
                        widthUpdateButton.constant = self.view.frame.width/2
                        btnShare.isHidden = false

                    }
                    else {
                        widthUpdateButton.constant = 0
                    }
                 
                }else{
                    widthUpdateButton.constant = 0

                }
            }else if(strUserRole == "1"){ //Admin
                widthUpdateButton.constant = 0

                
            }else if(strUserRole == "4"){ //caretaker
                if("\(IsEnableTreeTagging)" == "1"){
                    if(availabilityInRange){
                        widthUpdateButton.constant = self.view.frame.width/2

                    }else{
                        widthUpdateButton.constant = 0

                    }
                }else{
                    widthUpdateButton.constant = 0


                }
            }
    }
        
        // show Data
        // User RegistrationDoc
        if dictData.value(forKey: "UserRegistrationDc") is NSDictionary {
            let dictUserRagistrationDoc = removeNullFromDict(dict: (dictData.value(forKey: "UserRegistrationDc")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let strProfileUrl = "\(dictUserRagistrationDoc.value(forKey: "Profile_Image")!)"
            var strName = "\(dictUserRagistrationDoc.value(forKey: "Name")!)"
            if strName == "" {
                strName = "Not Available"
            }
            if("\(dictData.value(forKey: "OwnerAlias")!)" != "" && "\(dictData.value(forKey: "OwnerAlias")!)" != "<null>"){
                strName = "\(dictData.value(forKey: "OwnerAlias")!)"
            }
            
            lblName.text = strName
            if(strProfileUrl.count != 0){
                imgProfile.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "profile_img_1"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
        }
        else{
            if("\(dictData.value(forKey: "OwnerAlias")!)" != "" && "\(dictData.value(forKey: "OwnerAlias")!)" != "<null>"){
                lblName.text = "\(dictData.value(forKey: "OwnerAlias")!)"
            }
        }
        let strDate = "\(dictData.value(forKey: "Plantation_Date")!)"
        var strLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
        if strLocation == "" ||  strLocation == "<null>"{
            strLocation = "Not Available"
        }
        var greenWealth = "\(dictData.value(forKey: "Donated_By_Count")!)"
        if greenWealth == "" ||  greenWealth == "<null>"{
            greenWealth = "0"
        }
        if strDate == "" ||  strDate == "<null>"{
            lblDate.text = "Not Available"
        }else{
            lblDate.text = strDate
        }
        var TotalView = "\(dictData.value(forKey: "TotalView")!)"
        if TotalView == "" ||  TotalView == "<null>"{
            TotalView = "0"
        }
        lblViews.text  = "QR Views - \(TotalView)"
        lblDonatedCount.text = "green Wealth - \(greenWealth)"
        var Source = "\(dictData.value(forKey: "Source")!)"
        if Source == "" ||  Source == "<null>"{
            Source = ""
        }
        lblSource.text = Source
        // PlantMasterDc
        let strDonated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id")!)"
        let strIsShowQRCodeNo = "\(dictData.value(forKey: "IsShowQRCodeNo")!)"
        var strIsQRCodeNo = "\(dictData.value(forKey: "QRCodeNo")!)"
        
        if (strIsShowQRCodeNo == "0"||strIsShowQRCodeNo == "<null>") {
            strIsQRCodeNo = strDonated_Plant_Id
        }
        if (strIsQRCodeNo == ""||strIsQRCodeNo == "<null>") {
            strIsQRCodeNo = strDonated_Plant_Id
        }
        
        if dictData.value(forKey: "PlantMasterDc") is NSDictionary {
            let dictPlantMasterDc = removeNullFromDict(dict: (dictData.value(forKey: "PlantMasterDc")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let strPlant_Name = "\(dictPlantMasterDc.value(forKey: "Plant_Name")!) (QR-\(strIsQRCodeNo))"
            lblTreeNameQRCode.text = strPlant_Name
        }else{
            if strIsQRCodeNo == "" ||  strIsQRCodeNo == "<null>"{
                strIsQRCodeNo = "Not Available"
            }
            lblTreeNameQRCode.text = "(QR-\(strIsQRCodeNo))"
        }
        
        // DonatedPlantStatusDcs
        var aryDonatedPlantStatusDcs = NSMutableArray()
        if (dictData.value(forKey: "DonatedPlantStatusDcs") is String) {
            // lblCareTacker.text = "Caretaker: Not Available"
        }else{
            aryDonatedPlantStatusDcs = (dictData.value(forKey: "DonatedPlantStatusDcs")as! NSArray).mutableCopy()as! NSMutableArray
            ary_of_Collection = NSMutableArray()
            ary_of_Collection = aryDonatedPlantStatusDcs
        }
        
        if(ary_of_Collection.count == 1 || ary_of_Collection.count == 2){
            let dict =  NSMutableDictionary()
            dict.setValue("default_img", forKey: "Plant_Status_Image")
            dict.setValue("", forKey: "Created_Date")
            if(ary_of_Collection.count == 1){
               ary_of_Collection.add(dict)
               ary_of_Collection.add(dict)
            }else if(ary_of_Collection.count == 2){
                ary_of_Collection.add(dict)
            }
        }
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.treeDescriptionFunction))
        lblTreeNameQRCode.isUserInteractionEnabled = true
        lblTreeNameQRCode.addGestureRecognizer(tap)
        if ("\(strUserID)" == "\(strQR_UserID)"){
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapMyTreeFunction))
            lblDonatedCount.isUserInteractionEnabled = true
            lblDonatedCount.addGestureRecognizer(tap1)
        }
        self.LoadMapViewPin()
    }
    
    // MARK: - ----------------MapViewShow
    // MARK: -
 
    func LoadMapViewPin()  {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        mapView.showsUserLocation = true
        mapView.mapType = .standard
            let strlet = "\(dictData.value(forKey: "Lat")!)"
            let strLong = "\(dictData.value(forKey: "Long")!)"
            if (strlet != "") && (strLong != "")  &&  (strlet != "<null>")  && (strLong != "<null>") {
                let MomentaryLatitudeDesti = Double(strlet)
                let MomentaryLongitudeDesti = Double(strLong)
                let destinationLocation = CLLocationCoordinate2D(latitude: (MomentaryLatitudeDesti)!, longitude: (MomentaryLongitudeDesti)!)
                let tree = MKPointAnnotation()
                
                tree.title = ""
                tree.subtitle = ""
                tree.coordinate = CLLocationCoordinate2D(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude)
                self.mapView.addAnnotation(tree)
                let noLocation = tree.coordinate
                let viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 200, 200)
                self.mapView.setRegion(viewRegion, animated: false)
               // self.mapView.showAnnotations(self.mapView.annotations, animated: true)
            }
    }
}

// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension QRCodeDetailVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.ary_of_Collection.count == 0 {
            return 3
        }else{
            return self.ary_of_Collection.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QRCodeCell", for: indexPath as IndexPath) as! CommonCollectionCell
        if ary_of_Collection.count != 0 {
            let dict = removeNullFromDict(dict: (ary_of_Collection.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
            if(strProfileUrl.count != 0){
                cell.QR_Image.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
            }
            var strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
            if strDate == "Not Available" {
                strDate = "Available on next Maintenance"
            }
            cell.QR_lblTitle.text = strDate
        }else{
            cell.QR_lblTitle.text = "Not Available"
            cell.QR_Image.image = #imageLiteral(resourceName: "default_img")
        }
      
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120 , height :  150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if ary_of_Collection.count != 0 {
            let dict = removeNullFromDict(dict: (ary_of_Collection.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
            if(strProfileUrl.count != 0){
                let imgview = UIImageView()
                imgview.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    vc.imageSHow = image!
                    self.navigationController?.pushViewController(vc, animated: true)
                }, usingActivityIndicatorStyle: .gray)
            }
         
        }
      
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
}
}
// MARK: - ---------------MKMapViewDelegate
// MARK: -

extension QRCodeDetailVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            return nil
        }
        let identifier = "MyCustomAnnotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        annotationView!.image = UIImage(named: "about_tree")
        configureDetailView(annotationView: annotationView!)
        return annotationView
    }
    func configureDetailView(annotationView: MKAnnotationView) {
        let width = 85
        let height = 85
    
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(85)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotOptions()
        options.size = CGSize(width: width, height: height)
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)

        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if snapshot != nil {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                let lbltitle = UILabel(frame: CGRect(x: 90, y: 5, width: 215, height: 20))
                let lblSubtitle = UILabel(frame: CGRect(x: 90, y: lbltitle.frame.maxY, width: 215, height: 60))
                if self.ary_of_Collection.count != 0 {
                    let dict = removeNullFromDict(dict: (self.ary_of_Collection.object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                    let strProfileUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
                    if(strProfileUrl.count != 0){
                       imageView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strProfileUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                        }, usingActivityIndicatorStyle: .gray)
                    }
                    lbltitle.numberOfLines = 1
                    lblSubtitle.numberOfLines = 3
                    lbltitle.font = UIFont.systemFont(ofSize: 18)
                    lblSubtitle.font = UIFont.systemFont(ofSize: 14)
                    lbltitle.text = "Not available"
                    lblSubtitle.text = "Not available"
                    lblSubtitle.textColor = UIColor.darkGray
                    if self.dictData.value(forKey: "PlantMasterDc") is NSDictionary {
                        let dictPlantMasterDc = removeNullFromDict(dict: (self.dictData.value(forKey: "PlantMasterDc")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                        lbltitle.text = "\(dictPlantMasterDc.value(forKey: "Plant_Name")!)"
                    }
                    lblSubtitle.text = "\(self.dictData.value(forKey: "PlantedLocation")!)"
                    snapshotView.addSubview(imageView)
                    snapshotView.addSubview(lbltitle)
                    snapshotView.addSubview(lblSubtitle)
            }
        }
        
        annotationView.detailCalloutAccessoryView = snapshotView
    }
    }
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }

    
}
