//
//  PlantsCareTakerVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/6/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.


import UIKit

class PlantsCareTakerVC: UIViewController {
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAliasName: SkyFloatingLabelTextField!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var strComeFrom = String()
    var strQRCode = String()
    var dictData = NSMutableDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 8.0
        loginDict = getLoginData()
        print(loginDict)
        if (loginDict.count != 0){
            let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            let name = "\(dict.value(forKey: "Name")!)"
            let email = "\(dict.value(forKey: "Email_Id")!)"
            let mobile = "\(dict.value(forKey: "Mobile_Number")!)"
            let address = "\(dict.value(forKey: "Address")!)"
            txtName.text = name
            txtEmail.text = email
            txtAddress.text = address
            txtMobile.text = mobile
            if (txtAddress.text! == ""){
                txtAddress.isUserInteractionEnabled = true
            }else{
                txtAddress.isUserInteractionEnabled = false
            }
        }
        //For Register Fresh QR COde
        if strComeFrom == "Register for QR Code" {
            self.lblTitle.text = "Register for QR Code"
            txtAliasName.isHidden = false
        }
            // //For Request Plant CareTaker
        else{
            self.lblTitle.text = "Be A Caretaker"
            txtAliasName.isHidden = true
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            FTIndicator.dismissProgress()
        }
    }
    // MARK: - --------------API Calling
    // MARK: -
    
    func call_RequestForCaretaker()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let AddBecomeCareTakerRequest = "\(URL_AddBecomeCareTakerRequest)User_Id=\(dict.value(forKey: "User_Id")!)"
            print(URL_AddBecomeCareTakerRequest)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: AddBecomeCareTakerRequest, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title: alertInfo, message: dict.value(forKey: "Success")as? String, preferredStyle: UIAlertControllerStyle.alert)
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    func call_RequestForRegisterQRCode()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let AddBecomeCareTakerRequest = "\(URL_UpdateDonatedPlantInformation)QRCode=\(strQRCode)&User_Id=\(dict.value(forKey: "User_Id")!)&OwnerAlias=\(txtAliasName.text!)"
            print(URL_UpdateDonatedPlantInformation)
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: AddBecomeCareTakerRequest, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title: alertInfo, message: "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            //For Register Fresh QR COde
                            if self.strComeFrom == "Register for QR Code" {
                                self.checkAllValidation()
                            }
                                //For Request Plant CareTaker
                            else{
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSubmit(_ sender: Any) {
        //For Register Fresh QR COde
        if strComeFrom == "Register for QR Code" {
            call_RequestForRegisterQRCode()
        }
            //For Request Plant CareTaker
        else{
            call_RequestForCaretaker()
        }
    }
    
    
    func checkAllValidation()  {
        var strUserRole = ""
        if (loginDict.count != 0){
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            strUserRole = "\(dict.value(forKey: "RoleId")!)"
        }
        let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        
        if (strUserRole == "2" || strUserRole == "5")  { // Owner
            if("\(String(describing: dictData.value(forKey: "IsEnableOwnerShip")!))" == "1"){
                let strLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
                if strLocation == "" ||  strLocation == "<null>"{
                    let vc: TreeTaggingVC = self.storyboard!.instantiateViewController(withIdentifier: "TreeTaggingVC") as! TreeTaggingVC
                    vc.dictData =  dictData
                     vc.strcome = "FROMQRCODE"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension PlantsCareTakerVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtAddress {
            return  txtFiledValidation(textField: txtAddress, string: string, returnOnly: "ALL", limitValue: 50)
        }
        if textField == txtAliasName {
            return  txtFiledValidation(textField: txtAliasName, string: string, returnOnly: "ALL", limitValue: 25)
        }else{
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
