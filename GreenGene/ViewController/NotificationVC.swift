//
//  NotificationVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/25/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class NotificationVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    
    var aryMyNotiList = NSMutableArray()
    let refreshControl = UIRefreshControl()
    
    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 100.0
        if (loginDict.count != 0){
            self.call_MyNotificationList_API()
        }
        if #available(iOS 10.0, *) {
            tvlist.refreshControl = refreshControl
        } else {
            tvlist.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionONBack(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)

    }
    @objc private func refreshData(_ sender: Any) {
        self.call_MyNotificationList_API()
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_MyNotificationList_API()  {
        if !(isInternetAvailable()){
            refreshControl.endRefreshing()
          getNotiFicationDataFromLocal()
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let GetKnowledgeMania = "\(URL_GetKnowledgeMania)\(dict.value(forKey: "City_Id")!)"
                print(GetKnowledgeMania)
                if aryMyNotiList.count == 0{
                    FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                }
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetKnowledgeMania, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    self.refreshControl.endRefreshing()
                    if (status == "success"){
                        self.tvlist.isHidden = false

                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            deleteAllRecords(strEntity:"Notification")
                            saveDataInLocalArray(strEntity: "Notification", strKey: "notificationList", data: (dict.value(forKey: "KnowledgeManiaList")as! NSArray).mutableCopy() as! NSMutableArray)
                            self.getNotiFicationDataFromLocal()
                        }else{
                            self.tvlist.isHidden = true
                            self.lblError.text = "Notification not available."
                            self.imgError.image = #imageLiteral(resourceName: "notfound")
                        
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
        }
    }
    
    // MARK: - ---------------GetDataFromLocal
    // MARK: -
    func getNotiFicationDataFromLocal()   {
        let aryTemp = getDataFromLocal(strEntity: "Notification", strkey: "notificationList")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "notificationList") ?? 0)
            }
        }
        
        print(aryList)
        if aryList.count != 0 {
             self.aryMyNotiList = NSMutableArray()
            self.aryMyNotiList = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
            self.tvlist.reloadData()
        }else{
            if !(isInternetAvailable()){
                self.tvlist.isHidden = true
                self.lblError.text = alertInternet
                self.imgError.image = #imageLiteral(resourceName: "no-wifi")
            }else{
                self.tvlist.isHidden = true
                self.lblError.text = "Notification not available."
                self.imgError.image = #imageLiteral(resourceName: "notfound")
            }
        }
      
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NotificationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return 10
          return aryMyNotiList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "NotiList", for: indexPath as IndexPath) as! CommonTableCell
          let dict = removeNullFromDict(dict: (aryMyNotiList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        cell.notification_lblAddress.textColor = hexStringToUIColor(hex: colorGreenPrimary)
        cell.notification_lblAddress.text = "\(dict.value(forKey: "Title")!)"
        cell.notification_lblDetail.text = "\(dict.value(forKey: "Description")!)"
        let strDate = dateTimeConvertor(str: "\(dict.value(forKey: "Created_Date")!)", formet: "")
        cell.notification_lblDate.text = strDate
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}
