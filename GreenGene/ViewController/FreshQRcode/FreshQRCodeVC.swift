//
//  FreshQRCodeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/30/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class FreshQRCodeVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTreeCount: UILabel!
    
    var ary_of_Collection = NSMutableArray()
    var refresher = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "No QR Code Available"
        lblTitle.textColor = hexStringToUIColor(hex: colorOrangPrimary)
        self.collection.tag  = 1
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.collection!.addSubview(refresher)
        call_FreshQRCodeList_API(strLoaderTag: 1)
        DispatchQueue.main.async {
            self.call_MyTreeCountt_API()
        }
        viewHeader.backgroundColor = hexStringToUIColor(hex:colorGreenPrimary)
        let collectionViewLayout = (self.collection.collectionViewLayout as! UICollectionViewFlowLayout)
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0,left: 0, bottom: 0, right: 0)
        collectionViewLayout.scrollDirection = .vertical
        self.collection.collectionViewLayout = collectionViewLayout
        self.collection.dataSource = self
        self.collection.delegate = self
        self.collection.reloadData()
    }
    

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        call_FreshQRCodeList_API(strLoaderTag: 0)
    }
    // MARK: - ---------------API's Calling
    // MARK: -
    func call_FreshQRCodeList_API(strLoaderTag : Int)  {
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                
                let dictData = NSMutableDictionary()
                    dictData.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "User_Id")
               
                dictData.setValue("0", forKey: "CareTakerId")
                if(self.collection.tag == 1){
                    dictData.setValue("0", forKey: "Skip")
                }else{
                    dictData.setValue("\(self.ary_of_Collection.count)", forKey: "Skip")
                }
                dictData.setValue("20", forKey: "Take")
                dictData.setValue("", forKey: "CommonSearch")
                dictData.setValue("0", forKey: "Plant_Id")

                let aryOutboxSync = NSMutableArray()
                aryOutboxSync.add(dictData)
                let strJson = jsontoString(fromobject: aryOutboxSync)
                
                let GetMyTreeDetail = URL_GetMyTreeByUserIdLezyLoading + "\(strJson!)"
                print(GetMyTreeDetail)
                let loading = DPBasicLoading(collection: collection, fontName: "HelveticaNeue")
                loading.startLoading(text: "Loading...")

                
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    loading.endLoading()
                    self.refresher.endRefreshing()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            print(self.ary_of_Collection.count)
                            if(self.collection.tag == 1){
                                self.ary_of_Collection = NSMutableArray()
                                self.ary_of_Collection = (dict.value(forKey: "MyTreeList")as! NSArray).mutableCopy()as! NSMutableArray
                            }else{
                                for item in  (dict.value(forKey: "MyTreeList")as! NSArray){
                                    self.ary_of_Collection.add(item)
                                }
                            }
                            self.collection.tag = 0
                            self.collection.reloadData()
                        }else{
                            
                        }
                    }else{
                      
                    }
                })
            }
        }
    }
    
    //MARK: Count Tree & Qr Code
    func call_MyTreeCountt_API()  {
         
        self.view.endEditing(true)
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                let dictData = NSMutableDictionary()
                
                dictData.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "User_Id")
                dictData.setValue("0", forKey: "Plant_Id")
                dictData.setValue("0", forKey: "CareTakerId")
                dictData.setValue("0", forKey: "Skip")
                dictData.setValue("5", forKey: "Take")
                dictData.setValue("", forKey: "CommonSearch")
                dictData.setValue("false", forKey: "IsTreeTagged")
                dictData.setValue("0", forKey: "Organization_Coupen_Id")
                dictData.setValue("0", forKey: "State_Id")
                dictData.setValue("0", forKey: "City_Id")
                
                
            
                let aryOutboxSync = NSMutableArray()
                aryOutboxSync.add(dictData)
                
                let strJson = jsontoString(fromobject: dictData)
                
                let GetMyTreeDetail = URL_GetMyTreeByCount + "\(strJson!)"
                print(GetMyTreeDetail)
                
                let loading = DPBasicLoading(table: UITableView(), fontName: "HelveticaNeue")
                loading.startLoading(text: "Loading...")
                
                WebService.callAPIBYGET(parameter: NSDictionary(), url: GetMyTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    loading.endLoading()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            let aryList = dict.value(forKey: "MyTreeCount") as? NSDictionary ?? [:]
                            self.lblTreeCount.text = "Total My Trees : \(aryList.value(forKey: "TotalUntaggedTree") ?? "") / \(aryList.value(forKey: "TotalTree") ?? "")"
                        }else{
                            
                        }
                    }else{
                    
                    }
                })
            }
        }
    }
    
}
// MARK: - ----------------UICollectionViewDelegate
//CALayer -

extension FreshQRCodeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_of_Collection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       lblTitle.text = "Move to Tree Location, Tap on Qr Code & Tag Tree."
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FreshQRCodeCell", for: indexPath as IndexPath) as! FreshQRCodeCell
        let dict =  ary_of_Collection[indexPath.row]as! NSDictionary
       cell.freshQRCode_lbl_Title.text = "\(dict.value(forKey: "QRCode")!)"

        let strImageUrl = "\(BaseURLQrcodeImageDownload)\((dict.value(forKey: "QRCode")as! String)).png"
        cell.img_FreshQRCode.setImageWith(URL(string: "\(strImageUrl)"), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
        }, usingActivityIndicatorStyle: .gray)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection.frame.size.width)  / 2, height:(self.collection.frame.size.width)  / 2 )
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict =  ary_of_Collection[indexPath.row]as! NSDictionary
        let vc: TreeTaggingVC = self.storyboard!.instantiateViewController(withIdentifier: "TreeTaggingVC") as! TreeTaggingVC
        vc.dictData =  dict.mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class FreshQRCodeCell: UICollectionViewCell {
    
    @IBOutlet weak var img_FreshQRCode: UIImageView!
    
    
    @IBOutlet weak var freshQRCode_lbl_Title: UILabel!
}
