//
//  PaymentProcessVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/26/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class PaymentProcessVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnLoader: UIButton!

    var dictData = NSMutableDictionary()
    var strAPIName = String()
    var countAPI = 0
    var aryImages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.textColor = hexStringToUIColor(hex: colorOrangPrimary)

  
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lblTitle.text = "Payment Processing..."
        lblMessage.text = "Please Wait – While your payment request processing"
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            customDotLoaderShowOnButton(btn: self.btnLoader, view: self.view, controller: self)
            
            if(self.strAPIName == URL_Donation){
                WebService.callAPIBYPOSTWithRaw(json: self.dictData.value(forKey: "DonationPaymentHistoryDc") as! String, url: self.strAPIName) { (responce, status) in
                    self.countAPI = self.countAPI + 1
                    print(responce)
                    customeDotLoaderRemove()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            self.lblTitle.text = "Your payment has been processed successfully"
                            self.lblMessage.text = "\(dict.value(forKey: "Success")!)"
                            self.btnLoader.setTitle("Success", for: .normal)
                            self.btnLoader.setTitleColor(hexStringToUIColor(hex: colorGreenPrimary), for: .normal)
                            self.lblTitle.textColor = hexStringToUIColor(hex: colorGreenPrimary)
                            
                            
                        }else{
                            if(self.countAPI < 3){
                                self.viewWillAppear(true)
                            }else{
                                self.lblTitle.text = "Payment Pending..."
                                self.lblMessage.text = "We are facing some issue while processing your request, if your amount is deducted don't panic your amount is safe with us, it will be processed shortly. In case you need some help feel free to reach us."
                                self.lblTitle.textColor = hexStringToUIColor(hex: colorOrangPrimary)
                                self.btnLoader.setTitle("Contact us", for: .normal)
                                self.btnLoader.setTitleColor(hexStringToUIColor(hex: colorOrangPrimary), for: .normal)
                            }
                            
                        }
                    }else{
                        if(self.countAPI < 3){
                            self.viewWillAppear(true)
                        }else{
                            self.lblTitle.text = "Payment Pending..."
                            self.lblMessage.text = "We are facing some issue while processing your request, if your amount is deducted don't panic your amount is safe with us, it will be processed shortly. In case you need some help feel free to reach us."
                            self.lblTitle.textColor = hexStringToUIColor(hex: colorOrangPrimary)
                            self.btnLoader.setTitle("Contact us", for: .normal)
                            self.btnLoader.setTitleColor(hexStringToUIColor(hex: colorOrangPrimary), for: .normal)
                        }
                        
                    }
                }
                
                
            }else{
                
                WebService.callAPIWithMultiImage(parameter: self.dictData, url: self.strAPIName, image: self.aryImages, fileName: "image", OnResultBlock: { (responce, status) in
                    self.countAPI = self.countAPI + 1
                    print(responce)
                    customeDotLoaderRemove()
                    if (status == "success"){
                        if (responce.value(forKey: "Result")as! String == "True"){
                            self.lblTitle.text = "Your payment has been processed successfully"
                            self.lblMessage.text = "\(responce.value(forKey: "Success")!)"
                            self.btnLoader.setTitle("Success", for: .normal)
                            self.btnLoader.setTitleColor(hexStringToUIColor(hex: colorGreenPrimary), for: .normal)
                            self.lblTitle.textColor = hexStringToUIColor(hex: colorGreenPrimary)
                            
                            
                        }else{
                            if(self.countAPI < 3){
                                self.viewWillAppear(true)
                            }else{
                                self.lblTitle.text = "Payment Pending..."
                                self.lblMessage.text = "We are facing some issue while processing your request, if your amount is deducted don't panic your amount is safe with us, it will be processed shortly. In case you need some help feel free to reach us."
                                self.lblTitle.textColor = hexStringToUIColor(hex: colorOrangPrimary)
                                self.btnLoader.setTitle("Contact us", for: .normal)
                                self.btnLoader.setTitleColor(hexStringToUIColor(hex: colorOrangPrimary), for: .normal)
                            }
                            
                        }
                    }else{
                        if(self.countAPI < 3){
                            self.viewWillAppear(true)
                        }else{
                            self.lblTitle.text = "Payment Pending..."
                            self.lblMessage.text = "We are facing some issue while processing your request, if your amount is deducted don't panic your amount is safe with us, it will be processed shortly. In case you need some help feel free to reach us."
                            self.lblTitle.textColor = hexStringToUIColor(hex: colorOrangPrimary)
                            self.btnLoader.setTitle("Contact us", for: .normal)
                            self.btnLoader.setTitleColor(hexStringToUIColor(hex: colorOrangPrimary), for: .normal)
                        }
                        
                    }
                })
                
            }
    
        })
       
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated:false)
                break
            }
        }
        
    }
    
    @IBAction func actionOnContactus(_ sender: UIButton) {
        if(sender.titleLabel?.text == "Contact us"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
            testController.strViewComeFrom = "Contact us"
            testController.strURL = URL_About
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
}
