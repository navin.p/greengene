//
//  AddressGetFromMapVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/14/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
//MARK:
//MARK: ---------------Protocol-----------------

protocol AddressFromMapScreenDelegate : class{
    func GetAddressFromMapScreen(dictData : NSDictionary ,tag : Int)
}
class AddressGetFromMapVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnAddress: UIButton!
    
    @IBOutlet weak var navingation_Item: UINavigationItem!
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?
    var delegate: AddressFromMapScreenDelegate?
    var strLat = "0.0"
    var strLong = "0.0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
       mapView.showsUserLocation = true
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnAddress.layer.cornerRadius = 10.0
        btnAddress.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        getCurrentLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }
    //MARK:
    //MARK: --------------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        let dict = NSMutableDictionary()
        dict.setValue(lblAddress.text!, forKey: "address")
        dict.setValue(self.strLat, forKey: "lat")
        dict.setValue(self.strLong, forKey: "long")
        delegate?.GetAddressFromMapScreen(dictData: dict, tag: 0)
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - ----------------getCurrentLocation
    // MARK: -
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
    func mapReload()  {
        let viewRegion = MKCoordinateRegionMakeWithDistance(mapView.centerCoordinate, 200, 200)
        mapView.setRegion(viewRegion, animated: false)
        mapView.delegate = self
        let buttonItem = MKUserTrackingBarButtonItem(mapView: mapView)
        self.navingation_Item.rightBarButtonItem = buttonItem
        self.mapView.showAnnotations(mapView.annotations, animated: true)
       
    }
    

    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
           
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    self.lblAddress.text = addressString
                    self.strLat = "\(pdblLatitude)"
                    self.strLong = "\(pdblLongitude)"
                    print(addressString)
                }
        })
    }
}
// MARK: - ---------------MKMapViewDelegate
// MARK: -


extension AddressGetFromMapVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        print( mapView.centerCoordinate.latitude)
        getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")

    }
  
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
         getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")
    }

    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation != nil {
            // Zoom to user location
               self.mapReload()
                locationManager.stopUpdatingLocation()
            }
        }
    }

