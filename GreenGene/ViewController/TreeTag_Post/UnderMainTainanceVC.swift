//
//  UnderMainTainanceVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/2/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData

class UnderMainTainanceVC: UIViewController {
    
    @IBOutlet weak var btnGood: UIButton!
    @IBOutlet weak var btnAverage: UIButton!
    @IBOutlet weak var btnPoor: UIButton!
    @IBOutlet weak var btnTrimming: UIButton!
    @IBOutlet weak var btnPestControl: UIButton!
    @IBOutlet weak var btnFertilizer: UIButton!
    
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    @IBOutlet weak var imgBeforeplant: UIImageView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnBrowse: UIButton!

    
    @IBOutlet weak var txtTreeHeight: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtTreeGirth: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtTreeStem: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtTreeName: SkyFloatingLabelTextField!
    
    
    
    
    var imagePicker = UIImagePickerController()
    var strProfileUrl = ""
    var strPlantCondtionStatus = ""
    var strPlantID = String()
    var strPlant_Name = String()

    var strDonated_Plant_Id = String()

    var strComeFrom = String()
    var networkStatus = Bool()
    var ary_of_TreeName = NSMutableArray()
    var qrCode = String()
    
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        print(strPlantID)
        imagePicker.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
     
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.layer.cornerRadius = 8.0
        
        txtDescription.backgroundColor = UIColor.white
        btnBrowse.layer.cornerRadius = 5.0
        btnBrowse.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        getAbouttreeDataFromLocal(opentag: 0)
        txtTreeName.text = strPlant_Name
        txtTreeName.tag = Int ("\(strPlantID)")!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
  
    

    //MARK:- --------------getAbouttreeDataFromLocal
    //MARK:
    
  func getAbouttreeDataFromLocal(opentag : Int)   {
        let aryTemp = getDataFromLocal(strEntity: "AboutTree", strkey: "abouttree")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "abouttree") ?? 0)
            }
        }
        print(aryList)
        if aryList.count != 0 {
            ary_of_TreeName = NSMutableArray()
            ary_of_TreeName = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
            if(opentag == 1){
                let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
                vc.strTitle = "Select  Tree Name"
                vc.strTag = 12
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.delegate = self
                vc.aryTBL =  ary_of_TreeName
                self.present(vc, animated: false, completion: {})
            }
            
        }
        
    }
    // MARK: - ---------------Other Function's
    // MARK: -
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            showToastForSomeTime(title: alertMessage, message: "You don't have camera", time: 3, viewcontrol: self)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    // MARK: - --------------ValidationSignUP
    // MARK: -
    
    func validationUpdate() -> Bool {
       
        if(txtTreeHeight.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTreeHeight, viewcontrol: self)
          return false
        }
        
        else if(txtTreeGirth.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTreeGirth, viewcontrol: self)
            return false
        }
        else if(txtTreeStem.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTreeStem, viewcontrol: self)

            return false
        }
        else if(txtTreeName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Name, viewcontrol: self)

            return false
        }
//        else if(txtDescription.text?.count == 0){
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStatusDescription, viewcontrol: self)
//             return false
//        }
        else if ((strProfileUrl.count) == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStatusBeforImage, viewcontrol: self)
            return false
        }
        return true
    }
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        let alert = UIAlertController(title: alertMessage, message: Alert_PostUpdateBack, preferredStyle: UIAlertControllerStyle.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction (title: "Cancel", style: .default, handler: { (nil) in
        }))
        alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
             self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionONSubmit(_ sender: Any) {
        if !(isInternetAvailable()){
               if (validationUpdate()){
                saveDataInLocalFor_Outbox(strQRCode: qrCode)}
        }else{
            if(networkStatus){
                   if (validationUpdate()){
                    saveDataInLocalFor_Outbox(strQRCode: qrCode)}
            }else{
                if (validationUpdate()){
                    if(self.strProfileUrl.count != 0){
                        call_UPLOADIMGE_API()
                    }else{
                        // self.call_UPDATEPlantStatus_API()
                    }
                }
            }
        }

    }
    
  
    
    @IBAction func actionOnUploadImage(_ sender: UIButton) {
         openCamera()
       // openGallary()
    }
    @IBAction func actionONRadio(_ sender: UIButton) {
        if (sender.tag == 0){
            btnGood.setImage(#imageLiteral(resourceName: "redio"), for: .normal)
            btnAverage.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnPoor.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            strPlantCondtionStatus = "Good"
        }else if (sender.tag == 1){
            btnGood.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnAverage.setImage(#imageLiteral(resourceName: "redio"), for: .normal)
            btnPoor.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            strPlantCondtionStatus = "Average"
            
        }else if (sender.tag == 2){
            btnGood.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnAverage.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            btnPoor.setImage(#imageLiteral(resourceName: "redio"), for: .normal)
            strPlantCondtionStatus = "Poor"
        }
    }
    @IBAction func actionONCheckBox(_ sender: UIButton) {
        if (sender.tag == 0){
            if (btnTrimming.currentImage == #imageLiteral(resourceName: "uncheck")){
                btnTrimming.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            }else{
                btnTrimming.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
            
        }else if (sender.tag == 1){
            if (btnPestControl.currentImage == #imageLiteral(resourceName: "uncheck")){
                btnPestControl.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            }else{
                btnPestControl.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }else if (sender.tag == 2){
            if (btnFertilizer.currentImage == #imageLiteral(resourceName: "uncheck")){
                btnFertilizer.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            }else{
                btnFertilizer.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }
    }
    
    @IBAction func actionOnTreeName(_ sender: Any) {
        
        self.view.endEditing(true)
        if(self.ary_of_TreeName.count == 0){
            call_AboutTreeList_API()
        }else{
          self.getAbouttreeDataFromLocal(opentag: 1)
        }
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    func saveDataInLocalFor_Outbox(strQRCode : String){
        
        //------For Check Data Update or Add
        let predicate = NSPredicate(format: "qrcode = %@",strQRCode)
        let obj = getDataFromLocalUsingPredicate(strEntity: "Outbox", pedicate: predicate)
        
        //-UPDATE
        if obj.count > 0 {
            if (loginDict.count != 0){
                
                var strIsTrim = "True"
                if (btnTrimming.currentImage == #imageLiteral(resourceName: "uncheck")){
                    strIsTrim = "False"
                }
                var strPestControl = "True"
                if (btnPestControl.currentImage == #imageLiteral(resourceName: "uncheck")){
                    strPestControl = "False"
                }
                var strFertilizer = "True"
                if (btnFertilizer.currentImage == #imageLiteral(resourceName: "uncheck")){
                    strFertilizer = "False"
                }
               
                
                obj.setValue(txtDescription.text, forKey: "plantdescription")
                obj.setValue(strPlantCondtionStatus, forKey: "plantcondition")
                obj.setValue(strPestControl, forKey: "pestcontrol")
                obj.setValue(strFertilizer, forKey: "fertilizer")
                obj.setValue(strIsTrim, forKey: "istrim")
                obj.setValue(self.strProfileUrl, forKey: "plantImagename")
                obj.setValue(txtTreeStem.text!, forKey: "stemsize")
                obj.setValue(txtTreeHeight.text!, forKey: "stemHeight")
                obj.setValue(txtTreeGirth.text!, forKey: "stemGirth")

                let context = AppDelegate.getContext()
                do {
                    try context.save()
                    saveImageOnDocument(image: self.imgBeforeplant.image!, strname: self.strProfileUrl)
                    
                    FTIndicator.showNotification(withTitle: alertInfo, message: "Data Save Successfully.")
                  self.navigationController?.popViewController(animated: true)
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                } catch {
                    
                }
            }
        }
            //-ADD NEW
        else{}
    }
    
    
    // MARK: - ---------------API Calling
    // MARK: -
    func call_UPDATEPlantStatus_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
  
            var strIsTrim = "True"
            if (btnTrimming.currentImage == #imageLiteral(resourceName: "uncheck")){
                strIsTrim = "False"
            }
            var strPestControl = "True"
            if (btnPestControl.currentImage == #imageLiteral(resourceName: "uncheck")){
                strPestControl = "False"
            }
            var strFertilizer = "True"
            if (btnFertilizer.currentImage == #imageLiteral(resourceName: "uncheck")){
                strFertilizer = "False"
            }
          
            
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary

                let AddDonatedPlantStatus = "\(URL_AddDonatedPlantStatus)Donated_Plant_Id=\(self.strDonated_Plant_Id)&Plant_Status=Accept&Plant_Status_Description=\(txtDescription.text!)&Plant_Suggession=&Plant_Status_Image=\(strProfileUrl)&Plant_Condition=\(strPlantCondtionStatus)&IsTrim=\(strIsTrim)&IsPestControl=\(strPestControl)&IsFertilizer=\(strFertilizer)&Care_Taker_Id=\(dict.value(forKey: "User_Id")!)&StemSize=\(txtTreeStem.text!)&Height=\(txtTreeHeight.text!)&Girth=\(txtTreeGirth.text!)&Plant_Id=\(txtTreeName.tag)"
                print(AddDonatedPlantStatus)
          
                WebService.callAPIBYGET(parameter: NSDictionary(), url: AddDonatedPlantStatus, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationPlantStatus"), object: nil)
                            
                            let alert = UIAlertController(title:alertInfo, message: "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                  var returnStatus  = Bool()
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: MyTreeVC.self) {
                                        returnStatus = true
                                        _ =  self.navigationController!.popToViewController(controller, animated:true)
                                        break
                                    }
                                }
                                
                                if !(returnStatus){
                                    for controller in self.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: DashBoardVC.self) {
                                            returnStatus = true
                                            _ =  self.navigationController!.popToViewController(controller, animated:true)
                                            break
                                        }
                                    }
                                }

                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                })
            }
    
        }
    }
    func call_UPLOADIMGE_API()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURLImageUPLOAD, image: self.imgBeforeplant.image!, imageName: strProfileUrl, OnResultBlock: { (responce, status) in
                if (status == "True"){
                    self.call_UPDATEPlantStatus_API()
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            
        }
    }
    func call_AboutTreeList_API()  {
        if !(isInternetAvailable()){
            getAbouttreeDataFromLocal(opentag: 1)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetPlantList, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"AboutTree")
                        saveDataInLocalArray(strEntity: "AboutTree", strKey: "abouttree", data: (dict.value(forKey: "PlantList")as! NSArray).mutableCopy() as! NSMutableArray)
                        self.getAbouttreeDataFromLocal(opentag: 1)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            //     }
        }
    }
    
}
//MARK:-
//MARK:- ---------UIImagePickerControllerDelegate-----------

extension UnderMainTainanceVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgBeforeplant.image = pickedImage
            self.strProfileUrl = getUniqueString()
            self.imgBeforeplant.tag = 99
            dismiss(animated: true, completion: nil)
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension UnderMainTainanceVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtTreeHeight || textField == txtTreeGirth || textField == txtTreeStem{
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 8)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
// MARK: - ----------------Selection Delegate
// MARK: -
extension UnderMainTainanceVC: CommonTableDelegate {
    func getDataFromPickerDelegate(strTitle: String, tag: Int) {
        
    }
    
    func getDataFromCommonTableDelegate(dictData: NSDictionary, tag: Int) {
        txtTreeName.text = dictData.value(forKey: "Plant_Name")as? String
        txtTreeName.tag = Int ("\(dictData.value(forKey: "Plant_Id")!)")!
    }
}
