//
//  TreeTaggingVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 8/22/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation

//MARK: ---------------Protocol-----------------
protocol refreshTreeTagView : class{
    func refreshview(dictData : NSDictionary ,tag : Int)
}


class TreeTaggingVC: UIViewController {
    @IBOutlet weak var txtTreeName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtdate: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtViewLocation: KMPlaceholderTextView!
    
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    
    @IBOutlet weak var heightlocationView: NSLayoutConstraint!
    
    @IBOutlet weak var lbllocationTitle: UILabel!
    @IBOutlet weak var btnlocation: UIButton!

    var locationManager = CLLocationManager()
    var dictData = NSMutableDictionary()
    var ary_of_TreeName = NSMutableArray()
    
    var strLatitude = "0"
    var strLongitude = "0"
    var strcome = String()
    var networkStatus = Bool()
    var qrCode = String()

    // MARK: - ----------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictData)
        btnSubmit.layer.cornerRadius = 10.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        if(networkStatus){
            heightlocationView.constant = 0.0
            lbllocationTitle.isHidden = true
            btnlocation.isHidden = true
        }else{
            heightlocationView.constant = 100.0
            lbllocationTitle.isHidden = false
            btnlocation.isHidden = false
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtdate.text = formatter.string(from: picker.date)
        self.getAbouttreeDataFromLocal(opentag: 0)
        self.getCurrentLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    // MARK: - ----------------    @IBAction
    // MARK: -
    
    @IBAction func actionOnPickerClose(_ sender: Any) {
        viewPicker.removeFromSuperview()
    }
    @IBAction func actionOnPickerOk(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtdate.text = formatter.string(from: picker.date)
        viewPicker.removeFromSuperview()
        
    }
    @IBAction func actionOnSelectTreeName(_ sender: UIButton) {
        if(self.ary_of_TreeName.count == 0){
            call_AboutTreeList_API()
        }else{
            self.getAbouttreeDataFromLocal(opentag: 1)
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {

        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func actionOnlocation(_ sender: UIButton) {
        self.view.endEditing(true)
//        DispatchQueue.main.async {
//            self.getPlacePickerView()
//        }
    }
    @IBAction func actionOnSelectDate(_ sender: UIButton) {
        
        picker.maximumDate = Date()
        self.viewPicker.frame = self.view.frame
        self.view.addSubview(self.viewPicker)
        viewPicker.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            self.viewPicker.transform = CGAffineTransform.identity
        }
    }
    @IBAction func actionOnOk(_ sender: UIButton) {
        
        //-- networkStatus for check internet if true when  networkStatus true
        
        if !(networkStatus){
            if (txtViewLocation.text.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertTreeTag_location, viewcontrol: self)
            }
            
        }
       if (txtTreeName.text?.count == 0) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddtree_Name, viewcontrol: self)
        }else{
            if !(isInternetAvailable()){
                    saveDataInLocalFor_Outbox(strQRCode: qrCode)
            }else{
                if(networkStatus){
                    saveDataInLocalFor_Outbox(strQRCode: qrCode)
                }else{
                    call_Addtree_TaggingAPI()
                }
            }
        }
    }


    
    // MARK: - -------------API Calling
    // MARK: -
    func call_Addtree_TaggingAPI()  {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let strUserRole = "\(dict.value(forKey: "RoleId")!)"
            var Donated_Plant_Id = ""
            var User_Id = ""
            var Care_Taker_Id = ""
            var Plant_Id = ""
            var Gifted_By_Id = ""
            let CareDayFrequency = "\(treeUpdateTime)"
            var PlantedLocation = ""
            var Plantation_Date = ""
            var Lat = ""
            var Long = ""
            
            let dictTemp = removeNullFromDict(dict:dictData)
            if("\(dictTemp.value(forKey: "Donated_Plant_Id")!)" != ""){
                Donated_Plant_Id = "\(dictTemp.value(forKey: "Donated_Plant_Id")!)"
            }
            
            if(strUserRole == "2" || strUserRole == "5") // owner , Organization
            {
                User_Id  = "\(dict.value(forKey: "User_Id")!)"
            }else{
                User_Id = "0"
            }
            Care_Taker_Id = "\(dict.value(forKey: "User_Id")!)"
            Plant_Id = "\(txtTreeName.tag)"
            Gifted_By_Id = "\(dict.value(forKey: "User_Id")!)"
            PlantedLocation = txtViewLocation.text!
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
          
            let date = formatter.date(from: txtdate.text!)
            formatter.dateFormat = "MM/dd/yyyy"
            Plantation_Date = formatter.string(from: date!)
            print(Plantation_Date)
            Lat = self.strLatitude
            Long = self.strLongitude
            let AddTreeTagging = "\(URL_AddTreeTagging)Donated_Plant_Id=\(Donated_Plant_Id)&User_Id=\(User_Id)&Care_Taker_Id=\(Care_Taker_Id)&Plant_Id=\(Plant_Id)&Gifted_By_Id=\(Gifted_By_Id)&CareDayFrequency=\(CareDayFrequency)&PlantedLocation=\(PlantedLocation)&Plantation_Date=\(Plantation_Date)&Lat=\(Lat)&Long=\(Long)&PlantedArea="
            print(AddTreeTagging)
            
            
            
            WebService.callAPIBYGET(parameter: NSDictionary(), url: AddTreeTagging, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        let alert = UIAlertController(title:alertInfo, message:  "\(dict.value(forKey: "Success")as! String)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
                            vc.strDonated_Plant_Id =  "\(Donated_Plant_Id)"
                            vc.strPlantID =  "\(Plant_Id)"
                            vc.strComeFrom = "TreeTagging"
                            vc.strPlant_Name = "\(self.txtTreeName.text!)"

                            self.navigationController?.pushViewController(vc, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }

    func call_AboutTreeList_API()  {
        if !(isInternetAvailable()){
            getAbouttreeDataFromLocal(opentag: 1)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: URL_GetPlantList, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        deleteAllRecords(strEntity:"AboutTree")
                        saveDataInLocalArray(strEntity: "AboutTree", strKey: "abouttree", data: (dict.value(forKey: "PlantList")as! NSArray).mutableCopy() as! NSMutableArray)
                        self.getAbouttreeDataFromLocal(opentag: 1)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
            //     }
        }
    }
    // MARK: - ---------------GetDataFromLocal
    // MARK: -
    
    
    
    func saveDataInLocalFor_Outbox(strQRCode : String){
        
        //------For Check Data Update or Add
        let predicate = NSPredicate(format: "qrcode = %@",strQRCode)
        let obj = getDataFromLocalUsingPredicate(strEntity: "Outbox", pedicate: predicate)
        
        //-UPDATE
        if obj.count > 0 {
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
               
                obj.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "userid")
                obj.setValue(self.strLatitude, forKey: "lat")
                obj.setValue(self.strLongitude, forKey: "long")
                obj.setValue(txtTreeName.text!, forKey: "treename")
                obj.setValue("\(txtTreeName.tag)", forKey: "treeId")
                obj.setValue("\(txtdate.text!)", forKey: "date")
                obj.setValue(txtViewLocation.text!, forKey: "location")
                obj.setValue(strQRCode, forKey: "qrcode")
                
                obj.setValue("\(dict.value(forKey: "RoleId")!)", forKey: "role")
               
                obj.setValue("", forKey: "plantdescription")
                obj.setValue("", forKey: "plantcondition")
                obj.setValue("", forKey: "pestcontrol")
                obj.setValue("", forKey: "fertilizer")
                obj.setValue("", forKey: "istrim")
                obj.setValue("", forKey: "plantImagename")
                obj.setValue("", forKey: "stemsize")


                
                
                let context = AppDelegate.getContext()
                do {
                    try context.save()
                    
                    let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
                    vc.networkStatus =  true
                    vc.qrCode = self.qrCode
                    vc.strComeFrom = "TreeTagging"

                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                } catch {
                    
                }
            }
        }
            //-ADD NEW
        else{
            let context = AppDelegate.getContext()
            let entity =  NSEntityDescription.entity(forEntityName: "Outbox", in: context)
            let obj = NSManagedObject(entity: entity!, insertInto: context)
            if (loginDict.count != 0){
                let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
                
                obj.setValue("\(dict.value(forKey: "User_Id")!)", forKey: "userid")
                obj.setValue(self.strLatitude, forKey: "lat")
                obj.setValue(self.strLongitude, forKey: "long")
                obj.setValue(txtTreeName.text!, forKey: "treename")
                obj.setValue("\(txtTreeName.tag)", forKey: "treeId")
                obj.setValue("\(txtdate.text!)", forKey: "date")
                obj.setValue(txtViewLocation.text!, forKey: "location")
                obj.setValue(strQRCode, forKey: "qrcode")
                obj.setValue("\(dict.value(forKey: "RoleId")!)", forKey: "role")
                obj.setValue("", forKey: "plantdescription")
                obj.setValue("", forKey: "plantcondition")
                obj.setValue("", forKey: "pestcontrol")
                obj.setValue("", forKey: "fertilizer")
                obj.setValue("", forKey: "istrim")
                obj.setValue("", forKey: "plantImagename")
                obj.setValue("", forKey: "stemsize")
                //save the object
                do {
                    try context.save()
                    let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
                    vc.networkStatus =  true
                    vc.qrCode = self.qrCode
                    vc.strComeFrom = "TreeTagging"

                    self.navigationController?.pushViewController(vc, animated: true)
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                } catch {
                    
                }
            }
        }
    }
    
    
    
    func getAbouttreeDataFromLocal(opentag : Int)   {
        let aryTemp = getDataFromLocal(strEntity: "AboutTree", strkey: "abouttree")
        let aryList = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryList.add(obj.value(forKey: "abouttree") ?? 0)
            }
        }
        print(aryList)
      
            if aryList.count != 0 {
                self.ary_of_TreeName = NSMutableArray()
                self.ary_of_TreeName = (aryList.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
                if(opentag == 1){
                let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
                vc.strTitle = "Select  Tree Name"
                vc.strTag = 10
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.handleTreeTagView = self
                vc.aryTBL =  self.ary_of_TreeName
                self.present(vc, animated: false, completion: {})
            }
        }
        
    }
    //MARK:-
    //MARK:- ---------getCurrentAddress--------
  
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
               showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
}

//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension TreeTaggingVC: refreshTreeTagView {
    func refreshview(dictData: NSDictionary, tag: Int) {
        txtTreeName.text = dictData.value(forKey: "Plant_Name")as? String
        txtTreeName.tag = Int ("\(dictData.value(forKey: "Plant_Id")!)")!
        
    }
}

//MARK:-
//MARK:- ---------CLLocationManagerDelegate  Methods----------

extension TreeTaggingVC: CLLocationManagerDelegate {
func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let userLocation :CLLocation = locations[0] as CLLocation
    print("user latitude = \(userLocation.coordinate.latitude)")
    print("user longitude = \(userLocation.coordinate.longitude)")
    self.strLatitude = String(userLocation.coordinate.latitude)
    self.strLongitude = String(userLocation.coordinate.longitude)
    let geocoder = CLGeocoder()
    geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
        if (error != nil){
            print("error in reverseGeocode")
        }
        if(placemarks != nil){
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                var strAddress = ""
                if(placemark.locality != nil){
                    strAddress = "\(placemark.subLocality!), "
                }
                if(placemark.administrativeArea != nil){
                    strAddress =  strAddress + "\(placemark.administrativeArea!), "
                }
                if(placemark.country != nil){
                    strAddress =  strAddress + "\(placemark.country!)"
                }
                if(placemark.postalCode != nil){
                    strAddress =  strAddress + "\n\(placemark.postalCode!)"
                }
                self.txtViewLocation.text = strAddress
                self.locationManager.stopUpdatingLocation()
            }
        }
     
    }
  }
}
