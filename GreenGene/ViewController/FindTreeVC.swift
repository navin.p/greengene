//
//  FindTreeVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 2/12/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FindTreeVC: UIViewController  {
    @IBOutlet var viewTreeDetail: UIView!
    @IBOutlet weak var lblownerName: UILabel!
    @IBOutlet weak var img_Plant: UIImageView!
    @IBOutlet weak var lblPlantLocation: UILabel!
    @IBOutlet weak var lblQRCode: UILabel!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var stackCardView: UIStackView!
    
    @IBOutlet weak var lblPlantName: UILabel!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: CardView!

    @IBOutlet weak var viewForError: UIView!
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var txtEnterQRCode: UITextField!
    var locationManager = CLLocationManager()
    @IBOutlet weak var heightForViewHeader1: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    var tempArray = NSDictionary()
    var aryPlantList = NSMutableArray()
    
    
    var dictData =  NSDictionary()
    var isSlideMenu = false
    var isSearchFilter = false
    
    var arytreeList = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        btnMap.tag = 1
        mapView.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
      
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        
        //Zoom to user location
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation, 200, 200)
              self.mapView.setRegion(viewRegion, animated: true)
        }
        self.locationManager = locationManager
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
        
        if self.isSlideMenu == true{
            self.lblTitle.text = "\(dictData.value(forKey: "OrganizationName") ?? "")"
            self.heightForViewHeader1.constant = 0
            self.viewHeader1.isHidden = true
            self.GetUpvanTreeBySearchFilter()
            self.stackView.isHidden = false
            self.stackCardView.isHidden = false
        }
        //MARK: Search Filet
        if self.isSearchFilter == true{
            self.heightForViewHeader1.constant = 0
            self.stackCardView.isHidden = false
            loadDataOnMap()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - ----------------IBAction
    // MARK: -
    
    @IBAction func actionONBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnMap(_ sender: UIButton) {
        
        let dictData = arytreeList.object(at: sender.tag) as! NSDictionary
        let Plant_Name  = ""
        if (dictData.value(forKey: "PlantMasterDc") is NSDictionary) {
            
            let plantMaster = (dictData.value(forKey: "PlantMasterDc") as! NSDictionary)
            var Plant_Name = "\(plantMaster.value(forKey: "Plant_Name")!)"
            if(Plant_Name == "" || Plant_Name == "<null>"){
                Plant_Name = "Not Available"
            }
        }
        var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
        if PlantedLocation == "" ||  PlantedLocation == "<null>" {
            PlantedLocation = "Not Available"
        }
        
        var Lat = "\(dictData.value(forKey: "Lat")!)"
        if Lat == "" ||  Lat == "<null>" {
            Lat = "0.0"
        }
        var Long = "\(dictData.value(forKey: "Long")!)"
        if Long == "" ||  Long == "<null>" {
            Long = "0.0"
        }
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(Lat),\(Long)&directionsmode=driving") {
                    UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(strlat: Lat, strlong: Long, strTitle: Plant_Name + "\n" + PlantedLocation)
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser(strlat: Lat, strlong: Long, strTitle: Plant_Name + "\n" + PlantedLocation)
        }
        
    }
    @IBAction func actionOnClose(_ sender: UIButton) {
            viewTreeDetail.removeFromSuperview()
    }
    
    
    @IBAction func actionOnDesctiption(_ sender: UIButton) {
        let dictData = arytreeList.object(at: sender.tag) as! NSDictionary
        print(tempArray)
        self.call_GetPlantListById(strPlantId: "\(tempArray.value(forKey: "Plant_Id") ?? "")")
    }
    
    
    @IBAction func actionOnStatus(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let PlantTreeVC = storyBoard.instantiateViewController(withIdentifier: "PlantTreeVC") as! PlantTreeVC
        //let dictData = arytreeList.object(at: sender.tag) as! NSDictionary
        PlantTreeVC.dict = tempArray
        self.navigationController?.pushViewController(PlantTreeVC, animated: false);
    }
    
    @IBAction func actionOnLocation(_ sender: UIButton) {
        let dictData = arytreeList.object(at: sender.tag) as! NSDictionary
        let Plant_Name  = ""
        if (dictData.value(forKey: "PlantMasterDc") is NSDictionary) {
            
            let plantMaster = (dictData.value(forKey: "PlantMasterDc") as! NSDictionary)
            var Plant_Name = "\(plantMaster.value(forKey: "Plant_Name")!)"
            if(Plant_Name == "" || Plant_Name == "<null>"){
                Plant_Name = "Not Available"
            }
        }
        var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
        if PlantedLocation == "" ||  PlantedLocation == "<null>" {
            PlantedLocation = "Not Available"
        }
        
        var Lat = "\(dictData.value(forKey: "Lat")!)"
        if Lat == "" ||  Lat == "<null>" {
            Lat = "0.0"
        }
        var Long = "\(dictData.value(forKey: "Long")!)"
        if Long == "" ||  Long == "<null>" {
            Long = "0.0"
        }
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(Lat),\(Long)&directionsmode=driving") {
                    UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(strlat: Lat, strlong: Long, strTitle: Plant_Name + "\n" + PlantedLocation)
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser(strlat: Lat, strlong: Long, strTitle: Plant_Name + "\n" + PlantedLocation)
        }
    }
    
    @IBAction func actionOnToggle(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)

            let saveAction = UIAlertAction(title: "Search Filter", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                let home = storyBoard.instantiateViewController(withIdentifier: "QRTagParksSerachVC") as! QRTagParksSerachVC
                home.dict = self.dictData
                home.aryList = self.arytreeList
                self.navigationController?.pushViewController(home, animated: true);
            })
            
            let deleteAction = UIAlertAction(title: "Count Summary", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
                let home = storyBoard.instantiateViewController(withIdentifier: "CountTreeVC") as! CountTreeVC
                home.dictData = self.dictData
                self.navigationController?.pushViewController(home, animated: true);
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func actionOnSort(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        let home = storyBoard.instantiateViewController(withIdentifier: "UpvanViewController") as! UpvanViewController
        home.dict = self.dictData
        home.aryList = self.arytreeList
        navigationController?.pushViewController(home, animated: true);
    }
    
    
    func openTrackerInBrowser(strlat : String , strlong : String , strTitle : String){
        openMap(strTitle: strTitle, strlat: strlat, strLong: strlong)
        
    }
    
    // MARK: - ---------------API's Calling
    // MARK: -
    
    func call_FindTreeAPI()  {
        self.view.endEditing(true)
        mapView.tag = 1
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{

                let urlTreeDetail = URL_GetPlantedDetailByQRCode + "\(txtEnterQRCode.text!)"
                print(urlTreeDetail)
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                WebService.callAPIBYGET(parameter: NSDictionary(), url: urlTreeDetail, OnResultBlock: { (responce, status) in
                    print(responce)
                    FTIndicator.dismissProgress()
                    if (status == "success"){
                        let dict  = (responce.value(forKey: "data")as! NSDictionary)
                        if (dict.value(forKey: "Result")as! String == "True"){
                            print(self.arytreeList.count)
                            self.mapView.isHidden = false
                            self.arytreeList = NSMutableArray()
                            self.arytreeList = (dict.value(forKey: "PlantDonationList")as! NSArray).mutableCopy()as! NSMutableArray
                            self.loadDataOnMap()
                        }else{
                            if (self.arytreeList.count == 0){
                                self.mapView.isHidden = true
                                self.lblError.text = "\(dict.value(forKey: "Success")as! String)"
                                self.imgError.image = #imageLiteral(resourceName: "notfound")
                            }
                        }
                    }else{
                        self.mapView.isHidden = true
                        self.lblError.text = alertSomeError
                        self.imgError.image = #imageLiteral(resourceName: "notfound")
                    }
                })
        }
    }
    
    //MARK: near by tree
    func GetUpvanTreeBySearchFilter() {
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
               
            let dict = NSMutableDictionary()
            
            dict.setValue(dictData.value(forKey: "OrganizationLat"), forKey: "Lat")
            dict.setValue(dictData.value(forKey: "OrganizationLong"), forKey: "Long")
            dict.setValue("50", forKey: "Radius")
            dict.setValue("", forKey: "Plant_Type")
            dict.setValue("0", forKey: "UserId")
            dict.setValue("0", forKey: "Plant_Id")
            dict.setValue("0", forKey: "Donated_Plant_Id")
            dict.setValue(dictData.value(forKey: "Organization_Coupen_Id"), forKey: "Organization_Coupen_Id")
            dict.setValue("0", forKey: "Take")
            dict.setValue("0", forKey: "Skip")
            dict.setValue("", forKey: "CommonSearch")
            
            WebService.callAPIBYPOST(parameter: dict, url: URL_GetUpvanTreeBySearchFilter, OnResultBlock: { (responce, status) in
                print(responce)
                FTIndicator.dismissProgress()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        self.mapView.isHidden = false
                        self.arytreeList = NSMutableArray()
                        self.arytreeList = (dict.value(forKey: "dt")as! NSArray).mutableCopy()as! NSMutableArray
                        self.loadDataOnMap()
                    }else{
                        if (self.arytreeList.count == 0){
                            self.mapView.isHidden = true
                            self.lblError.text = "\(dict.value(forKey: "Success")as! String)"
                            self.imgError.image = #imageLiteral(resourceName: "notfound")
                            self.stackView.isHidden = true
                        }
                    }
                }else{
                    self.mapView.isHidden = true
                    self.lblError.text = alertSomeError
                    self.imgError.image = #imageLiteral(resourceName: "notfound")
                }
            })
        }
    }
    
    func call_GetPlantListById(strPlantId: String)  {
        self.view.endEditing(true)
        viewTreeDetail.removeFromSuperview()
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetPlantDetailByQRCode = "\(URL_GetPlantDetail)\(strPlantId)"
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetPlantDetailByQRCode, OnResultBlock: { (responce, status) in
                if (status == "success"){
                    //  let dict = data[0] as? NSDictionary
                    
                    let dict = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        
                        let aryPlantList = (dict.value(forKey: "PlantList")as! NSDictionary).mutableCopy()as? NSMutableDictionary ?? NSMutableDictionary()
                        showAlertWithoutAnyAction(strtitle: "\(aryPlantList.value(forKey: "Plant_Name") ?? "")", strMessage: "\(aryPlantList.value(forKey: "Description") ?? "")", viewcontrol: self)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                    
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    // MARK: - --------------Extra function
    // MARK: -
    func loadDataOnMap()  {
        print(arytreeList)
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        for (index, location) in arytreeList.enumerated() {
            print("Item \(index): \(location)")
            print(location)
            var strlet = ""
            var strLong = ""
            if ((location as AnyObject).value(forKey: "Lat") is String){
                strlet = ((location as AnyObject).value(forKey: "Lat")as! String)
            }
            if ((location as AnyObject).value(forKey: "Long") is String){
                strLong = ((location as AnyObject).value(forKey: "Long")as! String)
            }
            if (strlet != "") && (strLong != "")  &&  (strlet != "<null>")  && (strLong != "<null>") {
                let latitude: CLLocationDegrees = Double (strlet)!
                let longitude: CLLocationDegrees = Double (strLong)!
                let location1: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location1
                annotation.title =  ""
                annotation.subtitle = "\(index)"
                mapView.addAnnotation(annotation)
                mapView.showAnnotations(mapView.annotations, animated: true)
                mapView.showsUserLocation = true
            }
        }
    }
    

    @objc func AnotationhandleTap(_ sender: UITapGestureRecognizer) {
        
        let tag = sender.view?.tag
        viewTreeDetail.frame = self.view.frame
        
        viewTreeDetail.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            self.viewTreeDetail.transform = CGAffineTransform.identity
            self.view.addSubview(self.viewTreeDetail)
            
        }
        
        self.tempArray = [:]
        let dictData = arytreeList.object(at: tag!) as! NSDictionary
        self.tempArray = dictData
        
        if isSlideMenu == true {
            
            let Plant_Name = "\(dictData.value(forKey: "Plant_Name") ?? "")"
            let PlantedLocation = "\(dictData.value(forKey: "PlantedLocation") ?? "")"
            let Donated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id") ?? "")"
            let strImageUrl = "\(dictData.value(forKey: "Plant_Status_Image")!)"
            
            btnMap.tag = tag!
            lblPlantName.text = Plant_Name
            lblPlantLocation.text = PlantedLocation
            lblQRCode.text = Donated_Plant_Id
            img_Plant.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
            img_Plant.layer.borderWidth  = 1.0
            img_Plant.layer.masksToBounds = false
            img_Plant.clipsToBounds = true
            img_Plant.contentMode = .scaleAspectFill
            
            let showImage = BaseURLImageDownLoad + strImageUrl
            
            img_Plant.setImageWith(URL(string: showImage), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
                self.img_Plant.isUserInteractionEnabled = true
                self.img_Plant.addGestureRecognizer(tapGestureRecognizer)
            }, usingActivityIndicatorStyle: .gray)
            
        }else{
            var Plant_Name  = ""
            if (dictData.value(forKey: "PlantMasterDc") is NSDictionary) {
                
                let plantMaster = (dictData.value(forKey: "PlantMasterDc") as! NSDictionary)
                Plant_Name = "\(plantMaster.value(forKey: "Plant_Name")!)"
                if(Plant_Name == "" || Plant_Name == "<null>"){
                    Plant_Name = "Not Available"
                }
            }
            var PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
            if PlantedLocation == "" ||  PlantedLocation == "<null>" {
                PlantedLocation = "Not Available"
            }
            var Donated_Plant_Id = "\(dictData.value(forKey: "Donated_Plant_Id")!)"
            if Donated_Plant_Id == "" ||  Donated_Plant_Id == "<null>" {
                Donated_Plant_Id = "Not Available"
            }
            
            
            btnMap.tag = tag!
            //lblownerName.text = OwnerName
            lblPlantName.text = Plant_Name
            lblPlantLocation.text = PlantedLocation
            lblQRCode.text = Donated_Plant_Id
            //   img_Plant.layer.cornerRadius =  img_Plant.frame.width / 2
            img_Plant.layer.borderColor = hexStringToUIColor(hex: colorGreenPrimary).cgColor
            img_Plant.layer.borderWidth  = 1.0
            img_Plant.layer.masksToBounds = false
            img_Plant.clipsToBounds = true
            img_Plant.contentMode = .scaleAspectFill
            
            if (dictData.value(forKey: "DonatedPlantStatusDcs") is NSArray) {
                
                let DonatedPlantStatusDcsArray = (dictData.value(forKey: "DonatedPlantStatusDcs") as! NSArray)
                if(DonatedPlantStatusDcsArray.count != 0){
                    if DonatedPlantStatusDcsArray.lastObject is NSDictionary {
                        let dict = DonatedPlantStatusDcsArray.lastObject as! NSDictionary
                        let strImageUrl = "\(dict.value(forKey: "Plant_Status_Image")!)"
                        if(strImageUrl.count != 0){
                            img_Plant.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(strImageUrl)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
                                self.img_Plant.isUserInteractionEnabled = true
                                self.img_Plant.addGestureRecognizer(tapGestureRecognizer)
                            }, usingActivityIndicatorStyle: .gray)
                        }else{
                            img_Plant.image = #imageLiteral(resourceName: "default_img")
                        }
                    }
                }
            }
        }
        
    }
    
    
    @objc func connected(_ sender:AnyObject){
        print("yo tap image number : \(sender.view.tag)")
        let vc: PhotoViewVC = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewVC") as! PhotoViewVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.imageSHow = self.img_Plant.image!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}



//MARK:
//MARK: MKMapViewDelegate
extension FindTreeVC :  MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        print("\(String(describing: annotation.subtitle!))")
        annotationView!.image = UIImage(named: "about_tree")
        configureDetailView(annotationView: annotationView!, tag:(annotation.subtitle as? NSString)!)
        return annotationView
        
        
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
    
    func configureDetailView(annotationView: MKAnnotationView , tag : NSString) {
        
        let index = Int("\(tag)")!
        
        let dictData = arytreeList.object(at: index) as! NSDictionary
        var Plant_Name = ""
        var PlantedLocation = ""
        var PlantedImage = ""
        
        if isSlideMenu == true {
            
            Plant_Name = "\(dictData.value(forKey: "Plant_Name") ?? "")"
            PlantedLocation = "\(dictData.value(forKey: "PlantedLocation") ?? "")"
            PlantedImage = "\(dictData.value(forKey: "Plant_Status_Image")!)"
            
            if(Plant_Name == "" || Plant_Name == "<null>"){
                Plant_Name = "Not Available"
            }
            
            if PlantedLocation == "" ||  PlantedLocation == "<null>" {
                PlantedLocation = "Not Available"
            }
            
            let showImage = BaseURLImageDownLoad + PlantedImage
            
            img_Plant.setImageWith(URL(string: showImage), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
                self.img_Plant.isUserInteractionEnabled = true
                self.img_Plant.addGestureRecognizer(tapGestureRecognizer)
            }, usingActivityIndicatorStyle: .gray)
            
        }else if isSearchFilter == true {
            
            Plant_Name = "\(dictData.value(forKey: "Plant_Name") ?? "")"
            PlantedLocation = "\(dictData.value(forKey: "PlantedLocation") ?? "")"
            PlantedImage = "\(dictData.value(forKey: "Plant_Status_Image")!)"
            
            if(Plant_Name == "" || Plant_Name == "<null>"){
                Plant_Name = "Not Available"
            }
            
            if PlantedLocation == "" ||  PlantedLocation == "<null>" {
                PlantedLocation = "Not Available"
            }
            
            let showImage = BaseURLImageDownLoad + PlantedImage
            
            img_Plant.setImageWith(URL(string: showImage), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
                self.img_Plant.isUserInteractionEnabled = true
                self.img_Plant.addGestureRecognizer(tapGestureRecognizer)
            }, usingActivityIndicatorStyle: .gray)
            
            
        } else{
            if (dictData.value(forKey: "PlantMasterDc") is NSDictionary) {
                let plantMaster = (dictData.value(forKey: "PlantMasterDc") as! NSDictionary)
                Plant_Name = "\(plantMaster.value(forKey: "Plant_Name")!)"
                if(Plant_Name == "" || Plant_Name == "<null>"){
                    Plant_Name = "Not Available"
                }
            }
         
            PlantedLocation = "\(dictData.value(forKey: "PlantedLocation")!)"
            if PlantedLocation == "" ||  PlantedLocation == "<null>" {
                PlantedLocation = "Not Available"
            }
          
            if (dictData.value(forKey: "DonatedPlantStatusDcs") is NSArray) {
                let DonatedPlantStatusDcsArray = (dictData.value(forKey: "DonatedPlantStatusDcs") as! NSArray)
                if(DonatedPlantStatusDcsArray.count != 0){
                    if DonatedPlantStatusDcsArray.lastObject is NSDictionary {
                        let dict = DonatedPlantStatusDcsArray.lastObject as! NSDictionary
                        PlantedImage = "\(dict.value(forKey: "Plant_Status_Image")!)"
                    }
                }
            }
        }

     
        
        let width = 85
        let height = 85
        
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(85)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotOptions()
        options.size = CGSize(width: width, height: height)
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if snapshot != nil {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                let lbltitle = UILabel(frame: CGRect(x: 90, y: 5, width: 215, height: 20))
                let lblSubtitle = UILabel(frame: CGRect(x: 90, y: lbltitle.frame.maxY, width: 215, height: 60))
                if(PlantedImage.count != 0){
                    imageView.setImageWith(URL(string: "\(BaseURLImageDownLoad)\(PlantedImage)"), placeholderImage: #imageLiteral(resourceName: "default_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    }, usingActivityIndicatorStyle: .gray)
                }
                lbltitle.numberOfLines = 1
                lblSubtitle.numberOfLines = 3
                lbltitle.font = UIFont.systemFont(ofSize: 18)
                lblSubtitle.font = UIFont.systemFont(ofSize: 14)
                lbltitle.text = "Not available"
                lblSubtitle.text = "Not available"
                lblSubtitle.textColor = UIColor.darkGray
                lbltitle.text = "\(Plant_Name)"
                lblSubtitle.text = "\(PlantedLocation)"
                
                snapshotView.addSubview(imageView)
                snapshotView.addSubview(lbltitle)
                snapshotView.addSubview(lblSubtitle)
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.AnotationhandleTap(_:)))
            snapshotView.addGestureRecognizer(tap)
            snapshotView.isUserInteractionEnabled = true
            snapshotView.tag = index
            annotationView.detailCalloutAccessoryView = snapshotView
        }
    }
}
//MARK:
//MARK: UITextFieldDelegate
extension FindTreeVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(txtEnterQRCode.text?.count != 0){
           // self.call_FindTreeAPI()
        }else{
            self.mapView.isHidden = true
            self.lblError.text = alertQRCodeRequired
            self.imgError.image = #imageLiteral(resourceName: "notfound")
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(txtEnterQRCode.text?.count != 0){
            self.call_FindTreeAPI()
        }else{
            self.mapView.isHidden = true
            self.lblError.text = alertQRCodeRequired
            self.imgError.image = #imageLiteral(resourceName: "notfound")
        }
    }
}

//MARK:
//MARK: CLLocationManagerDelegate
extension FindTreeVC : CLLocationManagerDelegate{
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        region.center = mapView.userLocation.coordinate
        self.mapView.setRegion(region, animated: true)
    }
}
