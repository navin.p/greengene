//
//  QRScannerController.swift
//  QRCodeReader
//
//  Created by Simon Ng on 13/10/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

// GET Discount 


import UIKit
import AVFoundation

class QRScannerController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtQRCode: UITextField!
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnLoader: UIButton!

    @IBOutlet weak var heightTopView: NSLayoutConstraint!
    
    
    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var topbar: UIView!
    var scanSession = 0
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var viewComeFrom = String()
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
  
    
    
    // MARK: -  LifeCYcle

    override func viewDidLoad() {
        super.viewDidLoad()
        topbar.backgroundColor = hexStringToUIColor(hex: colorGreenPrimary)
let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
        // Get the back-facing camera for capturing videos
    //    let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .unspecified)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
//            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Move the message label and top bar to the front
        view.bringSubview(toFront: messageLabel)
        view.bringSubview(toFront: topbar)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubview(toFront: qrCodeFrameView)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        
        layer.videoOrientation = orientation
        
        videoPreviewLayer?.frame = self.view.bounds
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         messageLabel.text = "No QR code is detected"
        btnSearch.layer.cornerRadius = 5.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: colorOrangPrimary)
        if(viewComeFrom == "QRCode"){
            heightTopView.constant = 130
            lblTitle.text = "Either Scan or Enter QR Code"
        }else{
            
            heightTopView.constant = 130
            lblTitle.text = "Scan QR Code"

        }
          scanSession = 0
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let connection =  self.videoPreviewLayer?.connection  {
            
            let currentDevice: UIDevice = UIDevice.current
            
            let orientation: UIDeviceOrientation = currentDevice.orientation
            
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                
                switch (orientation) {
                case .portrait: updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                
                    break
                    
                case .landscapeRight: updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                
                    break
                    
                case .landscapeLeft: updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                
                    break
                    
                case .portraitUpsideDown: updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                
                    break
                    
                default: updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                
                    break
                }
            }
        }
    }
 // MARK: -  IBAction
    @IBAction func actionONBack(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func actionOnsearch(_ sender: Any) {
        if(txtQRCode.text?.count != 0){
            call_GetDetailByQRCODE(strQRCode: txtQRCode.text!)
        }else{
            FTIndicator.showNotification(withTitle: alertMessage, message: alertQRCodeRequired)
        }
        
    }
    
    // MARK: - ------------- API Calling
    // MARK: - -----
    
    func call_GetDetailByQRCODE(strQRCode: String)  {
        self.view.endEditing(true)
        
        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let GetPlantDetailByQRCode = "\(URL_GetPlantDetailByQRCode)\(strQRCode)"
            print(GetPlantDetailByQRCode)
            customDotLoaderShowOnButton(btn: btnLoader, view: self.view, controller: self)
            
            // FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetPlantDetailByQRCode, OnResultBlock: { (responce, status) in
                print(responce)
                
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        
                        print((dict as NSDictionary).mutableCopy() as! NSMutableDictionary)
                        let dictData = (dict as NSDictionary).mutableCopy() as! NSMutableDictionary
                        if(dictData.value(forKey: "PlantDonationList") is String){
                            
                        }else{
                            let aryTemp = (dictData.value(forKey: "PlantDonationList") as! NSArray)
                            if(aryTemp.count != 0){
                                
                                let dictDataTemp = removeNullFromDict(dict: (aryTemp.object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                                self.checkAllConditionAndRedirectNextView(dictDataTemp: dictDataTemp, strQRCode: strQRCode)
                            }
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    
    //---------- BY QRCODE
    
    func call_GetVenderDetailByQRCODE(strQRCode: String)  {
        self.view.endEditing(true)

        if !(isInternetAvailable()){
            FTIndicator.showNotification(withTitle: "", message: alertInternet)
        }else{
            let dict = (loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary
            let GetVenderDetailByQRCode = "\(URL_AssociatedPatnerDetail)QRCode=\(strQRCode)&User_Id=\(dict.value(forKey: "User_Id")!)"
            print(GetVenderDetailByQRCode)
            customDotLoaderShowOnButton(btn: btnLoader, view: self.view, controller: self)
            WebService.callAPIBYGET(parameter: NSDictionary(), url: GetVenderDetailByQRCode, OnResultBlock: { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                if (status == "success"){
                    let dict  = (responce.value(forKey: "data")as! NSDictionary)
                    if (dict.value(forKey: "Result")as! String == "True"){
                        
                        if(dict.value(forKey: "vendordt") is NSArray){
                            if((dict.value(forKey: "vendordt")as! NSArray).count != 0){
                                let vc: VenderDiscountVC = self.storyboard!.instantiateViewController(withIdentifier: "VenderDiscountVC") as! VenderDiscountVC
                                vc.strQRcode = strQRCode
                                vc.dictVenderData = dict.mutableCopy()as! NSMutableDictionary
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                self.showAlertWithAction(strtitle: alertMessage, strMessage: "\(alertDataNotFound)", viewcontrol: self)
                            }
                        }else{
                            self.showAlertWithAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }else{
                        self.showAlertWithAction(strtitle: alertMessage, strMessage: "\(dict.value(forKey: "Success")as! String)", viewcontrol: self)
                    }
                }else{
                    self.showAlertWithAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            })
        }
    }
    func showAlertWithAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
        let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
            self.scanSession = 0
            self.messageLabel.text = "No QR code is detected"

        }))
        viewcontrol.present(alert, animated: true, completion: nil)
    }
    // MARK: - --------------Check All Condition and Redirect next Controller
    // MARK: -
    func checkAllConditionAndRedirectNextView(dictDataTemp: NSDictionary ,strQRCode : String)  {
      
        var strUserRole = ""
        var strUserID = ""
        var strQR_UserID = ""
        var Donated_Plant_Id = 0
        var availabilityInRange = false
        var strPlantID = ""
        var isOwnerShip = ""
        var strPlant_Name = ""
        var IsEnableTreeTagging = ""

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            FTIndicator.dismissProgress()
            if (loginDict.count != 0){
                let dict = removeNullFromDict(dict: ((loginDict.value(forKey: "UserList")as! NSArray).object(at: 0)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                
                if dict.value(forKey: "UserQRCodeRangeDc") is NSArray {
                    if (dict.value(forKey: "UserQRCodeRangeDc") as! NSArray).count != 0{
                        let strDonated_Plant_Id = "\(dictDataTemp.value(forKey: "Donated_Plant_Id")!)"
                        var Donated_Plant_Id  = Int()
                        if(strDonated_Plant_Id != "" && strDonated_Plant_Id != "<null>"){
                            Donated_Plant_Id = Int(strDonated_Plant_Id)!
                        }else{
                            Donated_Plant_Id = 0
                        }
                        for item in dict.value(forKey: "UserQRCodeRangeDc") as! NSArray{
                            var range_From  = Int()
                            let strrange_From = "\((item as AnyObject).value(forKey: "Range_From")!)"
                            var range_To  = Int()
                            let strRange_To = "\((item as AnyObject).value(forKey: "Range_To")!)"
                            if(strRange_To != "" && strRange_To != "<null>") && (strrange_From != "" && strrange_From != "<null>"){
                                range_From = Int(strrange_From)!
                                range_To = Int(strRange_To)!
                                if(range_From <= Donated_Plant_Id && range_To >= Donated_Plant_Id){
                                    availabilityInRange = true
                                }
                            }
                        }
                    }
                }
                
                strUserRole = "\(dict.value(forKey: "RoleId")!)"
                strUserID = "\(dict.value(forKey: "User_Id")!)"
                strQR_UserID = "\(dictDataTemp.value(forKey: "User_Id")!)"
                strPlantID = "\(dictDataTemp.value(forKey: "Plant_Id")!)"
                isOwnerShip = "\(dictDataTemp.value(forKey: "IsEnableOwnerShip")!)"
                IsEnableTreeTagging = "\(dictDataTemp.value(forKey: "IsEnableTreeTagging")!)"

                if dictDataTemp.value(forKey: "PlantMasterDc") is NSDictionary {
                    let dictPlantMasterDc = removeNullFromDict(dict: (dictDataTemp.value(forKey: "PlantMasterDc")as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                     strPlant_Name = "\(dictPlantMasterDc.value(forKey: "Plant_Name")!)"
                    
                }
                
                print("strUserRole = \(strUserRole)----strUserID = \(strUserID)----strQR_UserID = \(strQR_UserID)----strPlantID = \(strPlantID)----isOwnerShip = \(isOwnerShip)----Donated_Plant_Id = \(Donated_Plant_Id)----availabilityInRange = \(availabilityInRange)")
                
                if(strUserRole == "2" || strUserRole == "5"){ //owner , Organization
                    if(strQR_UserID  == "" || strQR_UserID == "<null>") {
                       
                        if(isOwnerShip == "1"){
                            let vc: PlantsCareTakerVC = self.storyboard!.instantiateViewController(withIdentifier: "PlantsCareTakerVC") as! PlantsCareTakerVC
                            vc.strComeFrom = "Register for QR Code"
                            vc.strQRCode = strQRCode
                            vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                            vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        
                        if (strQR_UserID == strUserID){
                           if strPlantID == "" ||  strPlantID == "<null>"{
                            let vc: TreeTaggingVC = self.storyboard!.instantiateViewController(withIdentifier: "TreeTaggingVC") as! TreeTaggingVC
                            vc.dictData =  dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                           }else{
                            let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                            vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }else{
                            let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                            vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                       
                    }
                }
                    
                else if(strUserRole == "4"){ //caretaker
                    
                    if("\(IsEnableTreeTagging)" == "1"){
                        if(availabilityInRange){
                            if strPlantID == "" ||  strPlantID == "<null>"{
                                let vc: TreeTaggingVC = self.storyboard!.instantiateViewController(withIdentifier: "TreeTaggingVC") as! TreeTaggingVC
                                vc.dictData =  dictDataTemp.mutableCopy()as! NSMutableDictionary
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                                vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }else{
                            let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                            vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        
                        let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                        vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else if(strUserRole == "1"){ //Admin
                    
                    if self.viewComeFrom == "TreeTagging"{
                         if strPlantID == "" ||  strPlantID == "<null>"{
                            let vc: TreeTaggingVC = self.storyboard!.instantiateViewController(withIdentifier: "TreeTaggingVC") as! TreeTaggingVC
                            vc.dictData =  dictDataTemp.mutableCopy()as! NSMutableDictionary
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let vc: UnderMainTainanceVC = self.storyboard!.instantiateViewController(withIdentifier: "UnderMainTainanceVC") as! UnderMainTainanceVC
                            vc.strPlantID =  "\(strPlantID)"
                            vc.strPlant_Name = strPlant_Name
                            vc.strDonated_Plant_Id =  "\(Donated_Plant_Id)"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }else{
                        let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                        vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
            } else{
                if(dictDataTemp.count != 0){
                    let vc: QRCodeDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "QRCodeDetailVC") as! QRCodeDetailVC
                    vc.dictData = dictDataTemp.mutableCopy()as! NSMutableDictionary
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }


}
// MARK: -  AVCaptureMetadataOutputObjectsDelegate

extension QRScannerController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
          //  messageLabel.text = "No QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                launchApp(decodedURL: metadataObj.stringValue!)
                messageLabel.text = "QR code is detected"
            }
        }
    }
    func launchApp(decodedURL: String) {
        if  scanSession == 1 {
            return
        }
        messageLabel.text = "QR code is detected"

        scanSession = 1
        
        if("\(decodedURL)" == "http://greengene.citizencop.org/benefits.html"){
            let vc: AssociatedPartnerVC = self.storyboard!.instantiateViewController(withIdentifier: "AssociatedPartnerVC") as! AssociatedPartnerVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if(viewComeFrom == "TreeTagging"){
            self.call_GetDetailByQRCODE(strQRCode: "\(decodedURL)")

        }
        
        
        else if(viewComeFrom == "GETDiscount"){
             var aryStr = decodedURL.split(separator: "^")
            if(aryStr.count != 0){
                self.call_GetVenderDetailByQRCODE(strQRCode: "\(aryStr[0])")
            }else{
                self.showAlertWithAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
        else if(viewComeFrom == "QRCode"){
            self.call_GetDetailByQRCODE(strQRCode: "\(decodedURL)")
        }
       
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension QRScannerController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtQRCode  {
            return  txtFiledValidation(textField: txtQRCode, string: string, returnOnly: "All", limitValue: 45)
         }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
