//
//  AnimationFile.swift
//  GreenGene
//
//  Created by Sourabh Khare  on 5/29/24.
//  Copyright © 2024 Saavan_patidar. All rights reserved.
//

import Foundation

class LeafView: UIImageView {
    
    static let leafImages = ["leaves", "palm-leaf", "sakura" , "summer" , "fall"]
    
    init(frame: CGRect, imageName: String) {
        super.init(frame: frame)
        self.image = UIImage(named: imageName)
        self.contentMode = .scaleAspectFit
        
        // Set initial position and size
        let size = CGFloat.random(in: 30...40) // Random leaf size
        self.frame = CGRect(x: CGFloat.random(in: 0...(UIScreen.main.bounds.width - size)),
                            y: -size,
                            width: size,
                            height: size)
        
        // Add animation
        let duration = TimeInterval.random(in: 10...20) // Random falling duration
        UIView.animate(withDuration: duration, delay: 0, options: [.curveLinear], animations: {
            self.frame.origin.y = UIScreen.main.bounds.height
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// FOR BUTTON

class ShiningButton: UIButton {
    
    // MARK: - Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupShiningAnimation()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupShiningAnimation()
    }
    
    // MARK: - Animation
    
    private func setupShiningAnimation() {
        let shineAnimation = CABasicAnimation(keyPath: "opacity")
        shineAnimation.fromValue = 0.0
        shineAnimation.toValue = 1.0
        shineAnimation.duration = 1.0
        shineAnimation.autoreverses = true
        shineAnimation.repeatCount = .infinity
        layer.add(shineAnimation, forKey: "shining")
    }
}


class RaindropView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.blue
        layer.cornerRadius = 2.0 // Make the raindrop circular
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ToastView: UIView {
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    init(message: String, frame: CGRect) {
        super.init(frame: frame)
      //  backgroundColor = UIColor.black.withAlphaComponent(0.7)
        backgroundColor = hexStringToUIColor(hex: "#9DB72D").withAlphaComponent(0.7)
        layer.cornerRadius = 10
        
        messageLabel.text = message
        addSubview(messageLabel)
        
        // Adjust messageLabel constraints
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension UIViewController {
    
    func showToast(message: String) {

        let toastView = ToastView(message: message, frame: CGRect(x: 40, y: 80, width: view.frame.size.width - 80, height: 100))
        //view.addSubview(toastView)
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            window.addSubview(toastView)
            toastView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                toastView.leadingAnchor.constraint(equalTo: window.leadingAnchor ,constant: 20),
                toastView.trailingAnchor.constraint(equalTo: window.trailingAnchor , constant: -20),
                toastView.topAnchor.constraint(equalTo: window.safeAreaLayoutGuide.topAnchor, constant: 20)
            ])
            
            UIView.animate(withDuration: 0.5, delay: 2.0, options: .curveEaseOut, animations: {
                toastView.alpha = 0.0
            }, completion: { _ in
                toastView.removeFromSuperview()
            })
        }
        
    }
    
}
