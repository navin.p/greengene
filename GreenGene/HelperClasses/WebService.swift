//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Alamofire


class WebService: NSObject,NSURLConnectionDelegate {
    
    class func callAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        AF.request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if let data = response.value
                {
                    OnResultBlock(data as! NSDictionary,"success")
                }
                break
                
            case .failure(_):
                //print(response.result.error)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
                
            }
            
        }
        
    }
    
    //MARK:
    //MARK: API UPLOAD IMAGE
    
    class func callAPIWithImageOnlyOne(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        if isInternetAvailable() {
            AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                multiPartFormData.append(UIImageJPEGRepresentation(image, 0.1)!, withName: "photo_path", fileName: "\(imageName)", mimeType: "image/jpeg")
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url).responseJSON { response in
                print(response.request ?? 0)  // original URL request
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata), "True")
                }else{
                    let dic = NSMutableDictionary.init()
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    OnResultBlock(dic, "false")
                }
            }
            
        }else{
            
            let dic = NSMutableDictionary.init()
            dic.setValue(alertInternet, forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        }
        
    }
    
    class func callAPIWithImageMultiple(parameter:NSDictionary,url:String,image:[UIImage],imageName:[String],arrayList : NSMutableArray, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        if isInternetAvailable() {
            AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                
                
                if arrayList.count > 0 {
                    for i in arrayList {
                        let dict = i as? NSDictionary
                        multiPartFormData.append(UIImageJPEGRepresentation(dict?.value(forKey: "image") as! UIImage, 0.1)!, withName: "photo_path", fileName: "\(dict?.value(forKey: "imageName") ?? "")", mimeType: "image/jpeg")
                    }
                }
               
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url).responseJSON { response in
                print(response.request ?? 0)  // original URL request
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata), "True")
                }else{
                    let dic = NSMutableDictionary.init()
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    OnResultBlock(dic, "false")
                }
            }
            
        }else{
            
            let dic = NSMutableDictionary.init()
            dic.setValue(alertInternet, forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        }
        
    }

    
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "Result" == (k as AnyObject)as! String
                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"success")
                    }
                   else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                break
            case .failure(_):
                print(response.result)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    class func callAPIBYPOSTWithRaw(json:String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let url = URL(string: url)!
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        var requestFor = URLRequest(url: url)
        requestFor.httpMethod = HTTPMethod.post.rawValue
        requestFor.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        requestFor.httpBody = jsonData
        
        AF.request(requestFor).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"success")
                }
                break
            case .failure(_):
                print(response.result)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
        
     
    }
    
    
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        AF.request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"success")
                }
                break
            case .failure(_):
                print(response.result)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    // service for multiple
    class func callAPIWithMultiImage(parameter:NSDictionary,url:String,image:NSMutableArray,fileName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            if(image.count>0)
            {
                for (index, item ) in image.enumerated()
                {
                    let img = ((item as AnyObject) as! NSDictionary).value(forKey: "image") as! UIImage
                    // let img = (item as AnyObject) as! UIImage
                    
                    let imgName = ((item as AnyObject) as! NSDictionary).value(forKey: "imageName") as! String
                    if  let imageData = UIImageJPEGRepresentation(img, 0.5) {
                        
                        
                        multiPartFormData.append(imageData, withName: "\(fileName)\(index)", fileName: imgName, mimeType: "image/png")
                    }
                }
                
            }
            
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) .responseJSON { response in
            print(response.request ?? 0)  // original URL request
            if let data = response.value
            {
                let dictdata = NSMutableDictionary()
                dictdata.setValue(data, forKey: "data")
                OnResultBlock((dictdata), "True")
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic, "false")
            }
        }
    }
    class func callAPIWithImageandName(parameter:NSDictionary,url:String,image:UIImage,imageName:String,filetype : String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            if  let imageData = UIImageJPEGRepresentation(image, 0.5) {
                multiPartFormData.append(imageData, withName: filetype, fileName: imageName, mimeType: "image/png")
            }
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) .responseJSON { response in
            print(response.request ?? 0)  // original URL request
            if let data = response.value
            {
                let dictdata = NSMutableDictionary()
                dictdata.setValue(data, forKey: "data")
                OnResultBlock((dictdata), "True")
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic, "false")
            }
        }
    }
    
    class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        
        AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            if  let imageData = UIImageJPEGRepresentation(image, 0.5) {
                multiPartFormData.append(imageData, withName: withName, fileName: fileName, mimeType: "image/png")
            }
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
        }, to: url) .responseJSON { response in
            print(response.request ?? 0)  // original URL request
            if let data = response.value
            {
                let dictdata = NSMutableDictionary()
                dictdata.setValue(data, forKey: "data")
                OnResultBlock((dictdata), "True")
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic, "false")
            }
        }
    }
    
    func requestWith(endUrl: String, imageData: Data?, parameters: NSMutableDictionary, onCompletion: ((_ dict: NSDictionary) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            //"Content-type": "multipart/form-data"
            :]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            // multipartFormData.append("\(90)".data(using: String.Encoding.utf8)!, withName: "user_id")
            
            if let data = imageData{
                multipartFormData.append(data, withName: "File", fileName: "image.jpg", mimeType: "image/png")
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers) .responseJSON { response in
            print(response.request ?? 0)  // original URL request
            if let data = response.value
            {
                let dictdata = NSMutableDictionary()
                dictdata.setValue(data, forKey: "data")
              //  OnResultBlock((dictdata))
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
               // OnResultBlock(dic)
            }
        }
    }
    //MARK:
    //MARK: API With Secureity Parameter
    
    
    func callSecurityAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let finalParameter = self.addSecurity_Parameter(parameter: parameter as! NSMutableDictionary)
        AF.request(url, method: .post, parameters: finalParameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value{
                    OnResultBlock(data as! NSDictionary,"success")
                }
                break
                
            case .failure(_):
                print(response.result)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
        
    }
    
    func callAPIWithSecurity_Image(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        
        let finalParameter = self.addSecurity_Parameter(parameter: parameter as! NSMutableDictionary)
        
        AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            if  let imageData = UIImageJPEGRepresentation(image, 0.5) {
                let fileName = String(format: "%f.jpeg", NSDate.init().timeIntervalSince1970)
                
                multiPartFormData.append(imageData, withName: imageName, fileName: fileName, mimeType: "image/jpeg")
            }
            
            for (key, value) in finalParameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            
        }, to: url).responseJSON { response in
            print(response.request ?? 0)  // original URL request
            if let data = response.value
            {
                let dictdata = NSMutableDictionary()
                dictdata.setValue(data, forKey: "data")
                OnResultBlock((dictdata), "True")
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic, "false")
            }
        }
    }
    
    //MARK:
    //MARK: Get Device ID
    func get_DeviceID() -> String {
        let device = UIDevice.current
        let str_deviceID = device.identifierForVendor?.uuidString
        return str_deviceID!
    }
    
    //MARK:
    //MARK: Get Current Time
    func get_currentTime() -> String
    {
        let timeFormat = DateFormatter.init()
        timeFormat.dateFormat = "mmddHHmmss"
        return timeFormat.string(from: Date.init())
        
    }
    
    //MARK:
    //MARK: Get Current Time
    func addSecurity_Parameter(parameter:NSMutableDictionary) -> NSDictionary
    {
        let strTimeStamp = self.get_currentTime()
        let strDeviceType = "2"
        let str_Token = String.init(format: "%@%@%@%@", "masterkey","123456789",strDeviceType,strTimeStamp)
        let str_md5 = str_Token.md5
        
        parameter.setValue("123456789", forKey: "user_device_token")
        parameter.setValue(strDeviceType, forKey: "user_device_type")
        parameter.setValue(strTimeStamp, forKey: "timestamp")
        parameter.setValue(str_md5, forKey: "md5_key")
        
        return parameter
    }
    // MARK: - Convert String To Dictionary Function
    
    class func convertToDictionary(text: String) -> NSDictionary {
        do {
            
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Dictionary<String, Any>
            // print(data)
            let firstElement: NSDictionary = data! as NSDictionary
            return firstElement
        } catch {
            print(error.localizedDescription)
        }
        return NSDictionary()
    }
    
    
    class func convertToArray(text: String) ->NSArray {
        do {
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Array<Dictionary<String, Any>>
            //print(data)
            let firstElement: NSArray = data! as NSArray
            return firstElement
        }
        catch{
            print ("Handle error")
        }
        return NSArray()
    }
    
}

//MARK:
//MARK: MD5 Generate
extension String  {
    var md5: String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.deallocate(capacity: digestLen)
        
        return String(format: hash as String)
    }
}
