//
//  Constant.swift
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import Alamofire

//93 ID For Apple USERID
//328 ID for only 1 rs
//MARK:
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var storyboard_name = String()
var colorGreenPrimary = "146633"
var colorGrayPrimary = "FBFAF7"
var colorOrangPrimary = "F5821F"
var colorDarkPrimary = "5F3813"


var pickerData = ["Mr." , "Ms." , "Mrs."]
var treeUpdateTime = 30


//MARK: Version Information
var app_Version : String = "1.1.5"
var app_VersionDate : String = "01/July/2024"
var app_VersionSupport : String = "Requires iOS 12.0 or later.Compatible with iPhone."
var loginDict = NSDictionary()
var deviceID = UIDevice.current.identifierForVendor?.uuidString

//MARK:
//MARK: PAYtm

//Staging  // Merchant Key----s3s6g_CRhXcWu%4Y
//let GENERATE_CHECKSUM = "http://gcapi.citizencop.org/Checksumcsharp/GenerateChecksum.aspx"
//let VERIFY_CHECKSUM = " http://gcapi.citizencop.org/Checksumcsharp/VerifyChecksum.aspx"
//let MerchantID = "Citize73156154292382"
//let Website = "APPSTAGING"
//let IndustryID = "Retail"
//let ChannelID = "WAP"
//let Transaction_Status  = "https://securegw-stage.paytm.in/order/status"
//let Production_server = "https://securegw-stage.paytm.in/theia/processTransaction"//
//let type_Production_Staging = "Staging"
//let callBackURL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="

//Production
let Transaction_Status  = "https://securegw-stage.paytm.in/order/status"
let GENERATE_CHECKSUM = "http://gcapi.citizencop.org/Checksumcsharp/GenerateChecksumProduction.aspx"
let VERIFY_CHECKSUM = "http://gcapi.citizencop.org/Checksumcsharp/VerifyChecksumProduction.aspx"
let MerchantID = "Citize31131848116084"
let Website = "APPPROD"
let IndustryID = "Retail105"
let ChannelID = "WAP"
let type_Production_Staging = "Production"
let callBackURL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="


//MARK: Alert Messages

var appID : String = "https://itunes.apple.com/in/app/greengene/id1429856765?mt=8"
var appName = "greenGENE"
var alertThankyou = "Thank You!"
var alertMessage = "Alert!"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Sorry , Data is not Available!"
var alertDataDescriptionNotFound = "Sorry , DesAlert_Content_Othert Available!"
var alertLogin = "Please, Login to Application."
var alertSomeError = "Somthing went wrong please try again!"
var alertLogOut = "Are you sure want to logout ?"
var alertGallery = "Choose Image from"
var alertCalling = "Your device doesn't support this feature."
var AlertLogin = "Logged in successfully!"
//TreeTagging
var alertTreeTag_location = "Choose Current location!"

//Addtree
var alertAddtree_Name = "Tree name is required."
var alertAddtree_Age = "Tree Age Required !"
var alertAddtree_State = "State Required !"
var alertAddtree_City = "City Required !"
var alertAddtree_Location = "Location Required !"
var alertAddtree_Description = "Tree Description Required !"
var alertAddtree_image = "Tree Image Required !"

//Login
var alertLoginMobile = "Mobile number is required."
var alertLoginValidMobile = "Mobile number is Invalid."
var alertLoginPassword = "Password is required."
//SignUp
var alertSignUPFullName = "Full Name is required."
var alertSignUPEmail = "Email Address is required."
var alertSignUPValidEmail = "Email Address is Invalid"
var alertSignUPValidPassword = "Password Should be minimum six characters !"
var alertSignUPCPassword = "Confirm password is required."
var alertSignUPValidConfirmPass = "Please enter the same password as above."
var alertSignUPAddress = "Address is required."
var alertSignUPState = "State is required."
var alertSignUPCity = "City is required."
var alertSignTerms = "Please agree to the Terms and Conditions Checkbox."
var alertSignUPZipCode = "ZipCode is required."
var alertSignUPStateFirst = "Please select State first."

var alertSignUP = "Thank you for Registration."
var alertUpdate = "Your profile is updated successfully."
var alertPlantUpdate = "Plant Post updated Succussfully."

var alertStatusDescription = "Description Required !"
var alertStatusBeforImage = "Before Image Required !"
var alertnNoOfTree = "Number Of Trees Required !"
var alertnNoOfTreeGiftCard = "Number Of Trees Gift Cards Required !"

var alertTreeStem = "Tree stem is required."
var alertTreeGirth = "Tree girth is required."
var alertTreeHeight = "Tree height is required."
var alertTreeImage = "Tree image is required."



var alertnUnderMaintainance = "Maintenance record can be added every 30 days."
var alertnComingSoon = "Coming Soon..."
var alertBeACareTaker = "Your willingness to become a plant caretaker is registered. Someone from Team greenGENE will contact you for assigning set of trees. Email should come from rakesh@citizencop.org"
var alertPlantRequest = "Your willingness for Plantation / Tree Gift Cards is registred. Someone from Team greenGENE will contact you for assigning set of trees."


//Change Password
var alertChangeOldPassword = "Old Password Required!"
var alertChangeNewPassword = "New Password Required!"
var alertChangeConPassword = "Confirm Password Required!"
var alertChangeValidConfirmPass = "Confirm Password don't match. Try again !"
// Find A Tree

var alertQRCodeRequired = "QRCode Required!"
var alert_Feedback = "Suggestion Required!"

// Tree Cutting

var alertTreeCuttingName = "Name Required !"


//Tree Pakage
var TreePakage1 = "Generate a unique QR code that can be used online or through PDF."
var TreePakage2 = "Reminder to track the tree in every 30 days."
var TreePakage3 = "Facility and training for tree tagging."
var TreePakage4 = "Server Storage, 1 time tagging and 24 subsequent taggings."
var TreePakageNote = "Tree procurement, plantation and maintenance will be done by the buyer."

var giftATree1 = "A green tree momento."
var giftATree2 = "Physical green wealth card."
var giftATree3 = "Plantation of a 5-6 feet tree."
var giftATree4 = "2 Years of maintenance with monthly updates."

var Alert_Donate = "<p>Citizen COP Foundation is a Non-Profit, Non-Government Organisation, registered at Indore city of Madhya Pradesh, India.</p><p>Our prime objective is to make innovative use of technology for bringing a positive impact on people's life.</p><p>The Foundation works in several areas and has positively impacted millions of people in some or the other way.</p><p>Details of the Foundation can be seen at www.citizencop.org, Some major activities done are Women Safety and Empowerment, Environment Conservation, Cybercrime Awareness, Distribution of Surplus food to needy ones and many others.</p><p>Any financial support or donation for supporting the foundation in continuing making positive social impacts is highly welcomed. We would be immensely grateful for the same. All the donations attract tax benefits under section 80G of the Income Tax Act in India.</p>"


var Alert_DonateMinimum = "Please make a donation of minimum 1 Rs."
var Alert_NumberOfQRcode = "Number of QR Code is required."
var Alert_OrganizationName = "Organisation is Required."

var Alert_PromoCodeLimitMessage = "Promo code applicable on max"
var Alert_PromoCode = "Promo code is required."

var Alert_PostUpdateBack = "Are you sure want to leave?\nThe data might not be saved."
var Alert_TreeLocation = "Tree location is not available."
var Alert_TreeLocationclouser = "Move a little closer towards your tree"
var Alert_TreeLocationclouserYes = "Now you can re-tag a tree\nClick on"


var Alert_Content_Splash = "Congratulations on joining the mission for a greener habitat!"
var Alert_Content_Menu = "It takes you to options like Find a Tree, Associated Partners, Report Tree Cutting, My QR code, News, etc."
var Alert_Content_Other = "It takes you to options like Change Password, About the app, Contact us, Terms & Conditions, Share App, etc."
var Alert_Content_Banner = "Clicking the Banner will open a detailed help doc."
var Alert_Content_GenerateQRCode = "It gives you options to create a unique QR Code and Gift a Tree.\n\nAfter generating the QR Code, go to My QR Code to access it!"
var Alert_Content_ScanQRCode = "From here you can Scan the QR Code or enter the QR Code Number."
var Alert_Content_GreenWealthCard = "View your Green Wealth Card from here."
var Alert_Content_Mytrees = "Search your trees by name, area or QR Code."
var Alert_Content_RedeemBenefits = "Scan at the Vendor’s outlet and get rewards for being a green citizen!"
var Alert_Content_Donate = "Make a Donation towards a greener nation."



var alertGift_a_tree_detail = "Gift a tree detail is not Available!"

var alertGenerateQRCodeDetail = "Generate QRCode Detail is not Available!"



var reduisMeter = 10
//MARK:
//MARK: IPHONE SCREEN WIDTH AND HEIGHT BOUND CONDITION
let kIS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let kIS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
let kIS_RETINA = UIScreen.main.scale >= 2.0
let kSYSTEM_VERSION = Float(UIDevice.current.systemVersion)

let kSCREEN_X = UIScreen.main.bounds.origin.x
let kSCREEN_Y = UIScreen.main.bounds.origin.y
let kSCREEN_WIDTH = UIScreen.main.bounds.size.width
let kSCREEN_HEIGHT = UIScreen.main.bounds.size.height

let kSCREEN_MAX_LENGTH = max(kSCREEN_WIDTH, kSCREEN_HEIGHT)
let kSCREEN_MIN_LENGTH = min(kSCREEN_WIDTH, kSCREEN_HEIGHT)

let kIS_IPHONE_4_OR_LESS = (kIS_IPHONE && kSCREEN_MAX_LENGTH < 568.0)
let kIS_IPHONE_5 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 568.0)
let kIS_IPHONE_6_OR_7 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 667.0)
let kIS_IPHONE_6P_OR_7P = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 736.0)

////MARK:
//MARK:  Message

var kClientId = "155262836863-vv38ojgodr5sb3crh280bcrq1ahlp4ks.apps.googleusercontent.com    "
var deviceType = "3"

////MARK:
//MARK:  WEB SERVICES URL

var BaseURL :String = "http://gcapi.citizencop.org/api/mobile/"
var BaseURL1 :String = "http://gcapi.citizencop.org/api/giftcard/"
var BaseURLWeb :String = "http://greencity.citizencop.org/"
var BaseURLImageDownLoad :String = "http://greencity.citizencop.org/commonimage/"
 var BaseURLImageDownLoad1 :String = "http://greencity.citizencop.org/"
var BaseURLVendorImageDownLoad :String = "http://greencity.citizencop.org/vendorimages/"
var BaseURLTaskImages : String = "http://greencity.citizencop.org/taskimages/"
var BaseURLPledgelogo :String = "http://greencity.citizencop.org/pledgelogo/"
var BaseURLQrcodeImageDownload :String = "http://greencity.citizencop.org/Qrcode/"

 var BaseURLImageUPLOAD :String = "http://gcapi.citizencop.org/api/File/UploadAsync"
 var BaseURLImageUPLOADTreeCutting :String = "http://gcapi.citizencop.org/api/cuttingattachment/UploadAsync"

var URL_UpdateTask = BaseURL1 + "UpdateTask?"
var URL_Disclaimer :String = BaseURLWeb + "disclaimer"
var URL_About :String = BaseURLWeb + "aboutapp"
var URL_Concept :String = BaseURLWeb + "concept"
var URL_privacypolicy :String = BaseURLWeb + "privacypolicy"
var URL_returnpolicy :String = BaseURLWeb + "returnpolicy"
var URL_termconditions :String = BaseURLWeb + "termconditions"
var URL_Paytmtermconditions :String = BaseURLWeb + "paymenttermsconditions"
var URL_greengene_help_doc :String = "http://greencity.citizencop.org/assets/images/greengene_help_doc.pdf"




var URL_ContactUS :String = BaseURLWeb + "contactus"
var URL_KnowMore :String = BaseURLWeb + "knowmore"

var URL_Login :String = BaseURL + "GetUserLogin?"
var URL_ForgetPassword :String = BaseURL + "ForgetPasswordEmailId?"


var URL_GetCountryList :String = BaseURL + "GetCountryList"
var URL_GetStateListByCountryId :String = BaseURL + "GetStateListByCountryId?Country_Id=1"
var URL_GetCityListByStateId :String = BaseURL + "GetCityListByStateId?State_Id="
var URL_UserSignUp :String = BaseURL + "UserSignUp?"
var URL_GetKnowledgeMania :String = BaseURL + "GetKnowledgeMania?City_Id="
var URL_GetPlantList :String = BaseURL + "GetPlantList"
var URL_UpdateUserProfile :String = BaseURL + "UpdateUserProfile?"

var URL_AddDonatedPlantStatus :String = BaseURL1 + "AddTreeStatusWithPlantId?"
var URL_GetMyTreeDetail :String = BaseURL1 + "GetPlantDetailByUserId?User_Id="
var URL_GetMyTreeByUserIdLezyLoading :String = BaseURL1 + "GetMyTreeByUserId?MyTreeSearchDc="
var URL_GetMyTreeByCount :String = BaseURL1 + "GetmyTreeCountByJson?MyTreeSearchDc="
var URL_GetCareTaker :String = BaseURL1 + "GetPlantDetailByCareTakerId?CareTakerId="

var URL_GetPlantDetailByQRCode :String = BaseURL1 + "GetPlantDetailByQRCodeOptimized?QRCode="
var URL_AddPlantDonationRequest :String = BaseURL1 + "AddPlantDonationRequest?"
var URL_AddBecomeCareTakerRequest :String = BaseURL1 + "AddBecomeCareTakerRequest?"

var URL_PledgeAccept :String = BaseURL1 + "AddPledgeAccept?"
var URL_UpdateDonatedPlantInformation :String = BaseURL1 + "UpdateDonatedPlantInformationWithAlias?"

var URL_GetPledge :String = BaseURL + "GetPledge?City_Id="
var URL_GetUserLoginByUserId :String = BaseURL + "GetUserLoginByUserId?User_Id="
var URL_ChangePassword :String = BaseURL + "ChangePassword?"
var URL_AddDeviceRegistration :String = BaseURL + "AddDeviceRegistration?"


var URL_GetHistoricalTree :String = BaseURL1 + "GetHistoricalTree?"
var URL_AddHistoricalTree :String = BaseURL1 + "AddHistoricalTree?"
var URL_AddTreeTagging :String = BaseURL1 + "AddTreeTaggingWithArea?"
var URL_OfflineDataSave :String = BaseURL1 + "UploadTreeTaggingDataUsingFormData?TreeTaggingList="
var URL_GetDonatedTreeCount :String = BaseURL1 + "GetDonatedTreeCountByUserId?User_Id="
var URL_GetPlantedTreeByLocation :String = BaseURL1 + "GetPlantedTreeByLocation?User_Id="
var URL_GetPlantedDetailByQRCode :String = BaseURL1 + "GetPlantDetailByDonatedPlantId?Donated_Plant_Id="
var URL_GetQRCode :String = BaseURL1 + "GetQRCodeDetailById?User_Id="
var URL_GetPlantDetail :String = BaseURL + "GetPlantListById?Plant_Id="
var URL_GetUpvanTreeBySearchFilter = BaseURL1 + "GetUpvanTreeBySearchFilter"
var URL_AddSuggestion :String = BaseURL1 + "AddSuggestion"
var URL_GetUpvanTreeCountSummaryBySearchFilter = BaseURL1 + "GetUpvanTreeCountSummaryBySearchFilter"
var URL_AddTreeCuttingComplaint :String = BaseURL1 + "AddTreeCuttingComplaint?"
var URL_AssociatedPatner :String = BaseURL1 + "GetAssociatePartners"
var URL_AssociatedPatnerDetail :String = BaseURL1 + "GetAssociatePartnerCreditDetail?"
var URL_ScanGreenWealthCard :String = BaseURL1 + "ScanGreenWealthCard?"
var URL_GetTagPriceMaster :String = BaseURL1 + "GetTagPriceMaster"
var URL_AddQRCodeOrder :String = BaseURL1 + "AddQRCodeOrder?"
var URL_GetQRCodeOrderByUserId :String = BaseURL1 + "GetQRCodeOrderByUserId?User_Id="
var URL_Donation :String = BaseURL1 + "AddDonationPaymentHistory?"

var URL_QRCodePdfonEmail :String = BaseURL1 + "SendQRCodePdfonEmail?"
var URL_AvailableUsedQRCodeRange :String = BaseURL1 + "AvailableUsedQRCodeRange?"
var URL_GetOrganizationDropdown :String = BaseURL1 + "GetOrganizationDropdown?"

var URL_GetOrganizationCoupenCode :String = BaseURL1 + "GetOrganizationCoupenCode?"
var URL_GetPlantMonthlyStatusByDonatedPlantId = BaseURL1 + "GetPlantMonthlyStatusByDonatedPlantId?Donated_Plant_Id="
var URL_GetUntaggedQRCodeByRange :String = BaseURL1 + "GetUntaggedQRCodeByRange?User_Id="

//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0

    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

func getLoginData() -> NSDictionary{
  let aryTemp = getDataFromLocal(strEntity: "LoginData", strkey: "login")
  let aryLogin = NSMutableArray()
    if aryTemp.count > 0 {
        for j in 0 ..< aryTemp.count {
            var obj = NSManagedObject()
            obj = aryTemp[j] as! NSManagedObject
            aryLogin.add(obj.value(forKey: "login") ?? 0)
        }
    }
    if aryLogin.count != 0 {
        return aryLogin.object(at: 0)as! NSDictionary
    }
    return NSDictionary()
    
  
    
}


//MARK:
//MARK: OTHER FUNCTION
func jsontoString(fromobject:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: fromobject, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func txtFiledValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    let characterOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdeghijklmnopqrstuvwxyz")
    
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValidnumber = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strcharacterOnly = characterOnly.isSuperset(of: stringFromTextField as CharacterSet)
    
    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)
    
    if returnOnly == "NUMBER" {
        if strValidnumber == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strcharacterOnly == false{
            return false
        }
    }
    
    
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}

func txtViewValidation(textField : UITextView , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    if returnOnly == "CHAR" {
        if strValid == true{
            return false
        }
    }
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    return true;
}

func getTodayString() -> String{
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    let today_string = String(year!) + "_" + String(month!) + "_" + String(day!) + "_" + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
    return today_string
}



func getUniqueString()-> String{
    var strName = "\(Date()).jpg".replacingOccurrences(of: "-", with: "")
    strName = strName.replacingOccurrences(of: " ", with: "")
    strName = strName.replacingOccurrences(of: "+", with: "")
    return  strName.replacingOccurrences(of: ":", with: "")
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func removeNullFromDict (dict : NSMutableDictionary) -> NSMutableDictionary
{
    let dic = dict;
    for (key, value) in dict {
        let val : NSObject = value as! NSObject;
        if(val.isEqual(NSNull()))
        {
            dic.setValue("", forKey: (key as? String)!)
        } else if(val.isEqual("<null>"))
        {
            dic.setValue("", forKey: (key as? String)!)
        }
        else
        {
            dic.setValue(value, forKey: key as! String)
        }
    }
    return dic;
}

//MARK:
//MARK: Local Directory

func getImagefromDirectory(strname : String) -> UIImage{
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        if strname == ""{
            return UIImage()
        }else{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strname)
            if(imageURL.path.count != 0){
                return  UIImage(contentsOfFile: imageURL.path)!
            }
             return UIImage()
        }
    }
    return UIImage()
}

func saveImageOnDocument(image : UIImage , strname : String)  {
    let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    // create a name for your image
    let fileURL = documentsDirectoryURL.appendingPathComponent("\(strname)")
    if !FileManager.default.fileExists(atPath: fileURL.path) {
        do {
            try  UIImageJPEGRepresentation(image,0.0)!.write(to: fileURL)
            print("Image Added Successfully = \(strname)" )
        } catch {
            print(error)
        }
    } else {
        print("Image Not Added")
    }
}

func removeImageFromDirectory(itemName:String) {
    let fileManager = FileManager.default
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    guard let dirPath = paths.first else {
        return
    }
    let filePath = "\(dirPath)/\(itemName)"
    print("Delete file Name : \(itemName)")
    do {
        try fileManager.removeItem(atPath: filePath)
    } catch let error as NSError {
        print(error.debugDescription)
    }
}

//MARK:
//MARK: Related to date formet

func dateTimeConvertor(str: String , formet : String) -> String {
    if str != "" && str != "<null>" {
        var fullNameArr = str.components(separatedBy: ".")
        let strFirst = fullNameArr[0] // First
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        if(formet != "Sort"){
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        else{
            dateFormatter.dateFormat = "MM/dd/yyyy"
            if  (dateFormatter.date(from: strFirst) == nil) {
                dateFormatter.dateFormat = "dd/MM/yyyy"
            }
        }
        let date = dateFormatter.date(from: strFirst)!
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = tempLocale
        let dateString1 = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = tempLocale
       // let dateString2 = dateFormatter.string(from: date)
       // let finlString = "Date : \(dateString1)  Time :  \(dateString2)"
        let finlString = "\(dateString1)"
        if(finlString == ""){
            return  " "
        }
        return finlString
    }else{
         return "Not Available"
    }
}

func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}
//MARK:
//MARK: Animation

func ShakeAnimation(textfiled: UITextField) {
    let shake = CAKeyframeAnimation(keyPath: "transform.translation.x")
    shake.duration = 0.1
    shake.repeatCount = 3
    shake.autoreverses = true
    shake.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    textfiled.layer.add(shake, forKey: "shake")
}



func validateEmail(email: String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
}






//MARK:
//MARK:  Internet validation

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}



func calculateSizeFromString(message : String ,view : UIView) -> CGFloat{
    let myString: NSString = message as NSString
    let size: CGSize = myString.size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.0)])
    return size.height * 2
}

func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}

func showLocationAccessAlertAction(viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
    alert.addAction(UIAlertAction (title: "Go to setting", style: .default, handler: { (nil) in
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }))
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}

func showToastForSomeTime(title : String , message : String , time : Int, viewcontrol : UIViewController){
    let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
    viewcontrol.present(alert, animated: true, completion: nil)
    // change to desired number of seconds (in this case 5 seconds)
    let when = DispatchTime.now() + 2
    DispatchQueue.main.asyncAfter(deadline: when){
        // your code with delay
        alert.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:
//MARK:  Calling & Message & OpenMap & Attribute string

func setAttributeText(lblText : UILabel) {
    let lblPocCell = NSAttributedString(string: lblText.text!,attributes: [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
    lblText.attributedText = lblPocCell
}

func checkNullValue(str : String) -> String! {
    var strValue = String()
    if str == "nil" || str == "<null>" {
        strValue = ""
    }else{
        strValue = "\(str)"
    }
    return strValue
    
}



func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}


//MARK:
//MARK:  Get Data from Local
func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}



func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}


func getDataFromLocalUsingPredicate(strEntity: String ,pedicate : NSPredicate)-> NSArray {
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = pedicate
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocalUsingAntity(entityName : String) -> NSMutableArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
    do {
        //go get the results
        return  (try AppDelegate.getContext().fetch(fetchRequest) as NSArray).mutableCopy() as! NSMutableArray
        
    } catch {
        print("Error with request: \(error)")
    }
    return NSMutableArray()
}



func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}


func openMap(strTitle : String , strlat : String , strLong : String) {
    if !(strTitle == "<null>" || strTitle == "" || strTitle == "Not Available"){
        let latitude: CLLocationDegrees = Double(strlat)!
        let longitude: CLLocationDegrees = Double(strLong)!
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = strTitle
    
        mapItem.openInMaps(launchOptions: options)
    }
 
    
}
extension UITextField {
   
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
//MARK: DotLoaderCustome
var dots = DotsLoader()
var btnTransprantDots = UIButton()
var viewForDot = UIView()


func customDotLoaderShowOnWeb(frame : CGRect , message : String , controller : UIViewController){
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()
    btnTransprantDots.frame = controller.view.frame
    controller.view.addSubview(btnTransprantDots)
    viewForDot = UIView()
    viewForDot.frame = frame
    viewForDot.backgroundColor = UIColor.white
    
    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.startAnimating()
    dots.backgroundColor = UIColor.white
    dots.frame = CGRect(x:Int(viewForDot.frame.width / 2)  - 100 , y: Int(viewForDot.frame.height / 2 - 25), width: 200, height: 50)
    dots.tintColor = hexStringToUIColor(hex: colorGreenPrimary)
    viewForDot.addSubview(dots)
    controller.view.addSubview(viewForDot)
    
}

func customDotLoaderShowOnFull(message : String , controller : UIViewController){
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()
    
    btnTransprantDots.frame = controller.view.frame
    btnTransprantDots.backgroundColor = UIColor.black
    btnTransprantDots.alpha = 0.5
    controller.view.addSubview(btnTransprantDots)
    viewForDot = UIView()
    viewForDot.frame = CGRect(x: btnTransprantDots.frame.width / 2 - 75, y: btnTransprantDots.frame.height / 2 - 25, width: 150, height: 50)
    viewForDot.backgroundColor = UIColor.white
    viewForDot.layer.cornerRadius = 10.0
    controller.view.addSubview(viewForDot)
    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.startAnimating()
    dots.backgroundColor = UIColor.white
    dots.frame = CGRect(x: 5, y: 5, width: 140, height: 40)
    dots.tintColor = hexStringToUIColor(hex: colorGreenPrimary)
    viewForDot.addSubview(dots)
    controller.view.addSubview(viewForDot)
    
}
func customDotLoaderShowOnButton(btn : UIButton ,view : UIView, controller : UIViewController){
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()
    btnTransprantDots.frame = controller.view.frame
    controller.view.addSubview(btnTransprantDots)
    dots = DotsLoader()
    if(btn.tag == 99){
       dots.dotsCount = 3
    }else{
         dots.dotsCount = 5
    }
   
    dots.dotsRadius = 5
    dots.layer.cornerRadius = 2.0
    dots.startAnimating()
    dots.backgroundColor = UIColor.clear
    dots.frame = btn.frame
    dots.center = view.center
    dots.tintColor = hexStringToUIColor(hex:colorOrangPrimary)
    view.addSubview(dots)
    
}
func customeDotLoaderRemove(){
    for item in viewForDot.subviews {
        item.removeFromSuperview()
    }
    dots.removeFromSuperview()
    btnTransprantDots.removeFromSuperview()
    viewForDot.removeFromSuperview()
    
}


class VersionCheck {

    public static let shared = VersionCheck()

    var newVersionAvailable: Bool?
    var appStoreVersion: String?

    func checkAppStore(callback: ((_ versionAvailable: Bool?, _ version: String?, _ message: String?)->Void)? = nil) {
        let ourBundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        AF.request("https://itunes.apple.com/lookup?bundleId=\(ourBundleId)").responseJSON { response in
            var isNew: Bool?
            var versionStr: String?
            var versionMsg: String?

            if let json = response.value as? NSDictionary,
               let results = json["results"] as? NSArray,
               let entry = results.firstObject as? NSDictionary,
               let appVersion = entry["version"] as? String,
               let ourVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,let appVersionNotes = entry["releaseNotes"] as? String
            {
                isNew = ourVersion != appVersion
                versionStr = appVersion
                versionMsg = appVersionNotes

            }

            self.appStoreVersion = versionStr
            self.newVersionAvailable = isNew
            callback?(isNew, versionStr, versionMsg)
        }
    }
}
